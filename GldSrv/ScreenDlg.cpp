// ScreenDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GldSrv.h"
#include "GlideServer.h"
#include "ScreenDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CScreenDlg dialog


CScreenDlg::CScreenDlg(UINT idd, CWnd* pParent /*=NULL*/)
	: CDialog(idd, pParent)
{
    m_idd = idd;
    m_fmv_mode = false;
	//{{AFX_DATA_INIT(CScreenDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CScreenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CScreenDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CScreenDlg, CDialog)
	//{{AFX_MSG_MAP(CScreenDlg)
	ON_WM_WINDOWPOSCHANGED()
	ON_WM_MOUSEACTIVATE()
	ON_WM_HELPINFO()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CScreenDlg message handlers

BOOL CScreenDlg::Create(CGlideServer *server)
{
    m_server = server;
    return CDialog::Create(m_idd, NULL);
}

void CScreenDlg::OnCancel() 
{
}

void CScreenDlg::OnOK() 
{
}




void CScreenDlg::OnWindowPosChanged(WINDOWPOS FAR* lpwndpos) 
{
	CDialog::OnWindowPosChanged(lpwndpos);
	
	HWND dos_wnd = m_server->GetDosWindow();
    if(dos_wnd != NULL)
    {
        if(m_server->GetOSType() == OS_W98)
        {
            ::SetForegroundWindow(dos_wnd);
            ::ShowWindow(dos_wnd, SW_HIDE);
        }

        if(m_server->GetOSType() == OS_W2K)
        {
            ::SetForegroundWindow(m_hWnd);
            ::ShowWindow(dos_wnd, SW_HIDE);
        }
    }
}




BOOL CScreenDlg::PreTranslateMessage(MSG* pMsg) 
{
    if(m_server->GetOSType() == OS_W2K)
    {
        UINT message = pMsg->message;
        HWND dos_wnd = m_server->GetDosWindow();
        
        if(dos_wnd != NULL && (message == WM_KEYUP || message == WM_KEYDOWN || message == WM_SYSKEYUP || message == WM_SYSKEYDOWN))
        {
            if(m_fmv_mode)
            {
                m_fmv_dismissed = true;
            }
            else
            {
                ::SendMessage(dos_wnd, message, pMsg->wParam, pMsg->lParam);
            }
        }
    }
    
	return CDialog::PreTranslateMessage(pMsg);
}




int CScreenDlg::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message) 
{
    
    if(m_server->GetOSType() == OS_W98)
    {
        HWND dos_wnd = m_server->GetDosWindow();
        if(dos_wnd != NULL)
        {
            ::SetForegroundWindow(dos_wnd);
            ::ShowWindow(dos_wnd, SW_HIDE);
        }

        return MA_NOACTIVATEANDEAT;
    }

	return CDialog::OnMouseActivate(pDesktopWnd, nHitTest, message);
}



BOOL CScreenDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
    return FALSE;
}

void CScreenDlg::SetFMVMode(bool fmv_mode)
{
    m_fmv_mode      = fmv_mode;
    m_fmv_dismissed = false;
}

bool CScreenDlg::FMVIsDismissed()
{
    return m_fmv_dismissed;
}