#include <dos.h>
#include <process.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "pipe.h"
#include "vxd.h"
#include "npipe.h"

#define BUF_SIZE (20 * 1024)

#define VERSION 9

#define CMD_Message (1)
#define CMD_GrGlideInit (2)
#define CMD_SstWinOpen (5)
#define CMD_ConfirmationRequest (35)
#define CMD_DisplayLFB (51)
#define CMD_DisplayVESA (61)
#define CMD_VESAStart (62)
#define CMD_VESAEnd (63)

#define GR_TEXFMT_RGB_565 (0xa)
#define GR_TEXFMT_ARGB_1555 (0xb)

unsigned __near __minreal = 100*1024;

static pipe_t *pipe;
static int running_nt = 0;

static char prnt_buf[BUF_SIZE];
static int buf_i;

static void printc(char c)
{
    prnt_buf[buf_i++] = c;
}

static void printdigit(int i)
{
    printc(i > 9 ? (i - 10 + 'A') : (i + '0'));
}

static void print_int(int i, int base)
{
    char buf[20];
    int d = 0;
    
    if(i < 0)
    {
        printc('-');
        i = -i; /*FIXME: Doesn't work for MIN_INT */
    }
    
    if(i == 0)
    {
        printc('0');
    }
    else
    {
        while(i)
        {
            buf[d++] = i%base;
            i /= base;
        }
        
        while(d)
        {
            printdigit(buf[--d]);
        }
    }
}


static void myprintf(char *s, int a, int b, int c, int d, int e)
{
   /*
    * Assumes that arguments are on the stack,
    * and that each are of integer size
    */
    int arg_i;
    int val;

    buf_i = 0;

    prnt_buf[buf_i++] = CMD_Message;

    if(buf_i + 64 >= BUF_SIZE)
        return;
        
    arg_i = 0;

    while(*s)
    {
        switch(arg_i)
        {
            case 0: val = a; break;
            case 1: val = b; break;
            case 2: val = c; break;
            case 3: val = d; break;
            case 4: val = e; break;
        }
        
        switch(*s)
        {
        case '%':
            ++s;
            switch(*s)
            {
            case 'x':
                ++s;
                print_int(val, 16);
                arg_i++;
                break;
            case 'd':
                ++s;
                print_int(val, 10);
                arg_i++;
                break;
            }
            break;

        default:
            printc(*s++);
            break;
        }
    }

    prnt_buf[buf_i++] = 0;
    
    pipe->send(prnt_buf, buf_i);
}


static void send_char(int c)
{
    prnt_buf[0] = c;
    pipe->send(prnt_buf, 1);
}

static void send_int(int i)
{
    prnt_buf[0] = i;
    prnt_buf[1] = (i>>8);
    prnt_buf[2] = (i>>16);
    prnt_buf[3] = (i>>24);
    pipe->send(prnt_buf, 4);
}

static void synchronise(void)
{
    int i;
    int n_reply_bytes = 16;
    
    send_char(CMD_ConfirmationRequest);

    for(i = 0; i < n_reply_bytes; i++)
        pipe->readChar();
}

#define NUM_MODES (20)
#define OEMStringLen  (6)


typedef struct
{
    unsigned int edi;
    unsigned int esi;
    unsigned int ebp;
    unsigned int reserved;
    unsigned int ebx;
    unsigned int edx;
    unsigned int ecx;
    unsigned int eax;
    unsigned short flags;
    unsigned short es;
    unsigned short ds;
    unsigned short fs;
    unsigned short gs;
    unsigned short ip;
    unsigned short cs;
    unsigned short sp;
    unsigned short ss;
}RM_Call;

typedef struct
{
    unsigned short offset;
    unsigned short segment;
} RM_Ptr;

#define LINEAR(rm)  ((unsigned char *)(((rm).segment << 4) + (rm).offset))

typedef struct
{
    char            VESASignature[4];
    unsigned short VESAVersion;
    unsigned short OEMStringPtr_offset;
    unsigned short OEMStringPtr_segment;
    char            Capabilities[4];
    unsigned short VideoModePtr_offset;
    unsigned short VideoModePtr_segment;
    unsigned short TotalMemory;
    char            Reserved[236];
} VgaInfoBlock;


typedef struct
{
    unsigned short ModeAttributes;
    unsigned  char WinAAttributes;
    unsigned  char WinBAttributes;
    unsigned short WinGranularity;
    unsigned short WinSize;
    unsigned short WinASegment;
    unsigned short WinBSegment;
    unsigned short WinFuncPtr_offset;
    unsigned short WinFuncPtr_segment;
    unsigned short BytesPerScanLine;
    unsigned short XResolution;
    unsigned short YResolution;
    unsigned  char XCharSize;
    unsigned  char YCharSize;
    unsigned  char NumberOfPlanes;
    unsigned  char BitsPerPixel;
    unsigned  char NumberOfBanks;
    unsigned  char MemoryModel;
    unsigned  char BankSize;
    unsigned  char NumberOfImagePages;
    unsigned  char ReservedOne;
    unsigned  char RedMaskSize;
    unsigned  char RedFieldPosition;
    unsigned  char GreenMaskSize;
    unsigned  char GreenFieldPosition;
    unsigned  char BlueMaskSize;
    unsigned  char BlueFieldPosition;
    unsigned  char RsvdMaskSize;
    unsigned  char RsvdFieldPosition;
    unsigned  char DirectColorModeInfo;
    unsigned  char Reserved[216];
} ModeInfoBlock;


static RM_Ptr window_ptr;
static RM_Ptr rmstuff;

extern void __interrupt __far rmhandler(void);
extern RM_Ptr ormhandler;
extern RM_Ptr pmcallback;



static void allocate_dos_mem(RM_Ptr *rm, int size)
{
    union REGS regs;

    rm->segment = 0;
    regs.x.eax = 0x100;
    regs.x.ebx = (size+15)/16;

    int386(0x31, &regs, &regs);

    rm->segment = regs.x.eax;
    rm->offset  = 0;
}


static void set_vga_info(RM_Ptr __far *block)
{
    VgaInfoBlock *info = (VgaInfoBlock *) LINEAR(*block);
    int modes_offset = 0x22;
    int oem_string_offset = modes_offset + (NUM_MODES+1) * sizeof(short);
    short *modes = (short *) (LINEAR(*block) + modes_offset);
    int i;
    
    memset(info, 0, 256);

    info->VESASignature[0] = 'V';
    info->VESASignature[1] = 'E';
    info->VESASignature[2] = 'S';
    info->VESASignature[3] = 'A';

    info->VESAVersion = 0x0102;

    info->OEMStringPtr_segment = block->segment;
    info->OEMStringPtr_offset  = block->offset + oem_string_offset;

    info->Capabilities[0] = 0;
    info->Capabilities[1] = 0;
    info->Capabilities[2] = 0;
    info->Capabilities[3] = 0;

    info->VideoModePtr_segment = block->segment;
    info->VideoModePtr_offset  = block->offset + modes_offset;

    info->TotalMemory = 0x00a;

    i = 0;
    
    modes[i++] = 0x100;
    modes[i++] = 0x101;
    modes[i++] = 0x102;
    modes[i++] = 0x103;
    modes[i++] = 0x104;
    modes[i++] = 0x105;
    modes[i++] = 0x106;
    modes[i++] = 0x107;
    modes[i++] = 0x108;
    modes[i++] = 0x109;
    modes[i++] = 0x10a;
    modes[i++] = 0x10b;
    modes[i++] = 0x10c;
    modes[i++] = 0x10d;
    modes[i++] = 0x10e;
    modes[i++] = 0x10f;
    modes[i++] = 0x110;
    modes[i++] = 0x111;
    modes[i++] = 0x112;
    modes[i++] = 0x113;
    modes[i++] = -1;

    strcpy(LINEAR(*block) + oem_string_offset, "GLIDOS");
}


static void set_mode_info(RM_Ptr __far *block, int mode)
{
    ModeInfoBlock *info = (ModeInfoBlock *) LINEAR(*block);

    memset(info, 0, 256);

    info->ModeAttributes = 0x1f;
    if(mode == 0x110)
        info->ModeAttributes &= ~1;
        
    info->WinAAttributes = 0x7;
    info->WinBAttributes = 0;
    info->WinGranularity = 64;
    info->WinSize        = 64;
    info->WinASegment    = window_ptr.segment;
    info->WinBSegment    = 0;
    info->WinFuncPtr_segment = 0;
    info->WinFuncPtr_offset = 0;
    switch(mode)
    {
        case 0x101:
        info->BytesPerScanLine = 640;
        break;
        case 0x110:
        case 0x111:
        info->BytesPerScanLine = 1280;
        break;

        default:
        info->BytesPerScanLine = 640;
        break;
    }
    info->XResolution    = 640;
    info->YResolution    = 480;
    info->XCharSize      = 6;
    info->YCharSize      = 8;
    info->NumberOfPlanes = 1;
    switch(mode)
    {
        case 0x101:
        info->BitsPerPixel   = 8;
        break;

        case 0x110:
        case 0x111:
        info->BitsPerPixel   = 16;
        break;

        default:
        info->BitsPerPixel   = 8;
        break;
    }
    info->NumberOfBanks  = 1;
    switch(mode)
    {
        case 0x101:
        info->MemoryModel = 4;
        break;

        case 0x110:
        case 0x111:
        info->MemoryModel = 6;
        break;
        
        default:
        info->MemoryModel = 4;
        break;
    }
    info->MemoryModel    = 4;
    info->BankSize       = 0;
    info->NumberOfImagePages = 1;
    info->ReservedOne    = 1;
    switch(mode)
    {
        case 0x110:
        info->RedMaskSize = 5;
        info->RedFieldPosition = 0xA;
        info->GreenMaskSize = 5;
        info->GreenFieldPosition = 0x5;
        info->BlueMaskSize = 5;
        info->BlueFieldPosition = 0;
        break;
        
        case 0x111:
        info->RedMaskSize = 5;
        info->RedFieldPosition = 0xB;
        info->GreenMaskSize = 6;
        info->GreenFieldPosition = 0x5;
        info->BlueMaskSize = 5;
        info->BlueFieldPosition = 0;
        break;
    }
}


static int mode = 0x101;
static int mode_supported = 0;
static int win_pos;
static char video_mem[0xa0000];

static void send_winopen(void)
{
    send_char(CMD_SstWinOpen);
    send_int(7 /* GR_RESOLUTION_640x480 */);
    send_int(0 /* GR_REFRESH_60Hz */);
    send_int(0 /* GR_COLORFORMAT_ARGB */);
    send_int(0 /* GR_ORIGIN_UPPER_LEFT */);
    send_int(2);
    send_int(1);
    synchronise();
}

static void send_frame(void)
{
    int even_used = 0;
    int odd_used = 0;
    int i;
    int fmt;
    unsigned short *vm = (unsigned short *) video_mem;

    for(i = 0; i < 480; i++)
    {
        if(vm[i*640+320] != 0)
        {
            if(i & 1)
                odd_used = 1;
            else
                even_used = 1;
        }
    }

    switch(mode)
    {
        case 0x110:
        fmt = GR_TEXFMT_ARGB_1555;
        break;

        case 0x111:
        fmt = GR_TEXFMT_RGB_565;
        break;
    }

    if(odd_used && even_used)
    {
        send_char(CMD_DisplayVESA);
        send_int(0);
        send_int(fmt);
        
        pipe->send((char *)vm, 640*480*2);
        
        synchronise();
    }
    else
    {
        send_char(CMD_DisplayVESA);
        send_int(1);
        send_int(fmt);
        
        for(i = 0; i < 240; i++)
            pipe->send((char *)(vm + (2 * i + odd_used) * 640), 640*2);
            
        synchronise();
    }
}

static void vesa_func(RM_Call __far *rm)
{
    switch(rm->eax & 0xff)
    {
        case 0:
        {
            RM_Ptr block;
            
            block.segment = rm->es;
            block.offset  = rm->edi;
            set_vga_info(&block);
        }
        break;

        case 1:
        {
            RM_Ptr block;

            block.segment = rm->es;
            block.offset  = rm->edi;
            
            set_mode_info(&block, rm->ecx);
        }
        break;

        case 2:
        {
            mode = rm->ebx;

            mode_supported = (mode == 0x110 || mode == 0x111);
            
            win_pos = -1;

            myprintf("VESA mode 0x%x", mode, 0, 0, 0, 0);
            
            if(mode_supported)
            {
                send_winopen();
            }
        }
        break;

        case 3:
        {
            rm->ebx = mode;
        }
        break;

        case 5:
        if(mode_supported)
        {
            if(0 <= win_pos && win_pos < 10)
                memcpy(video_mem + win_pos * 0x10000, LINEAR(window_ptr), 0x10000);

            if(rm->edx < win_pos)
                send_frame();
                
            if(rm->edx < 10)
                memcpy(LINEAR(window_ptr), video_mem + rm->edx * 0x10000, 0x10000);

            win_pos = rm->edx;
        }
        break;

        default:
        if(mode_supported)
        {
            myprintf("VESA cmd %x, %x, %x, %x", rm->eax, rm->ebx, rm->ecx, rm->edx, 0);
        }
        break;
    }

    rm->eax = 0x4f;
}



static RM_Call __far rm_call;

static void __interrupt __far callback(union INTPACK r)
{
    RM_Call __far *call;
    RM_Ptr  rm_stack;
    RM_Ptr  *ret_addr;

    call = MK_FP(r.x.es, r.x.edi);
    
    switch(call->eax & 0xff00)
    {
        case 0xf00:
        call->eax = 0x5003;
        call->ebx = 0x9f;
        break;

        case 0x4f00:
        vesa_func(call);
        break;
    }
    
    rm_stack.segment = call->ss;
    rm_stack.offset  = call->sp;

    ret_addr = (RM_Ptr *) LINEAR(rm_stack);
    
    call->cs = ret_addr->segment;
    call->ip = ret_addr->offset;

    call->sp += 4;
}


static void hook_rm10(void)
{
    union REGS r;
    struct SREGS sr;

    r.x.eax = 0x303;
    sr.ds  = FP_SEG(callback);
    r.x.esi = FP_OFF(callback);
    sr.es  = FP_SEG(&rm_call);
    r.x.edi = FP_OFF(&rm_call);
    r.x.ecx = 0;
    r.x.edx = 0;
    int386x(0x31, &r, &r, &sr);
    pmcallback.segment = r.x.ecx;
    pmcallback.offset  = r.x.edx;

    r.x.eax = 0x200;
    r.x.ebx = 0x10;
    int386(0x31, &r, &r);
    ormhandler.segment = r.x.ecx;
    ormhandler.offset  = r.x.edx;
    
    allocate_dos_mem(&rmstuff, 128);
    
    memcpy(LINEAR(rmstuff), (void *) rmhandler, 128);

    r.x.eax = 0x201;
    r.x.ebx  = 0x10;
    r.x.ecx = rmstuff.segment;
    r.x.edx = rmstuff.offset;
    int386(0x31, &r, &r);
}

void main(int argc, const char *argv[])
{
    if(argc < 2)
    {
        printf("usage: gldvesa.exe <program> <arg1>...<argn>n");
        return;
    }

    pipe = pipe_open();

    running_nt = (pipe->os == pipe_os_nt);

    window_ptr.segment = 0xA000;
    window_ptr.offset  = 0;
    
    hook_rm10();

    send_char(CMD_VESAStart);

    printf("Glidos VESA support starting\n");
    
    spawnvp( P_WAIT, argv[1], argv+1);
    
    printf("Glidos VESA support stopping\n");

    send_char(CMD_VESAEnd);
}
