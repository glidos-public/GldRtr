// Mouse.h: interface for the CMouse class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MOUSE_H__9F2D2CBD_C818_4E17_8E77_D5EF27511344__INCLUDED_)
#define AFX_MOUSE_H__9F2D2CBD_C818_4E17_8E77_D5EF27511344__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "dinput.h"

class CMouse  
{
public:
	void Initialise();
	void Poll(int *x, int *y, int *buttons);
	void Release();
	void Capture(HWND hWnd);
	CMouse();
	virtual ~CMouse();

private:
	bool m_acquired;
	int m_y;
	int m_x;
	IDirectInput7 *m_dinput;
    IDirectInputDevice7 *m_mouse;
};

#endif // !defined(AFX_MOUSE_H__9F2D2CBD_C818_4E17_8E77_D5EF27511344__INCLUDED_)
