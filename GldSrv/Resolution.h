#pragma once

#include "sdk2_glide.h"

class CResolution
{
public:
    struct Size
    {
        unsigned int width;
        unsigned int height;
    };

private:
    unsigned int _width;
    unsigned int _height;
    bool _isOkay;

public:
    CResolution();
    CResolution(unsigned int width, unsigned int height);
    CResolution(GrScreenResolution_t res);
    CResolution(CString &string);

    operator Size() const;
    operator GrScreenResolution_t() const;
    operator CString() const;

    bool IsOkay();
    bool IsStandard();
};
