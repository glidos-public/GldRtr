// GameSettingString.h: interface for the CGameSettingString class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAMESETTINGSTRING_H__515499D3_D797_4C5A_A8F8_A097237DBAA0__INCLUDED_)
#define AFX_GAMESETTINGSTRING_H__515499D3_D797_4C5A_A8F8_A097237DBAA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "GameSetting.h"

class CGameSettingString : public CGameSetting<CString>
{
public:
	CGameSettingString &operator=(CString &val);

private:
	bool    Parse(CString &val);
	CString Print();
};


#endif // !defined(AFX_GAMESETTINGSTRING_H__515499D3_D797_4C5A_A8F8_A097237DBAA0__INCLUDED_)
