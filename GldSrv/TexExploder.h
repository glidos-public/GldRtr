// TexExploder.h: interface for the CTexPartitionOutput class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXEXPLODER_H__BE027886_D1E2_47A5_8DFF_D26C9ADCA7E0__INCLUDED_)
#define AFX_TEXEXPLODER_H__BE027886_D1E2_47A5_8DFF_D26C9ADCA7E0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TexPartition.h"
#include "BmpWriter.h"
#include "MappingFile.h"
#include "TexSet.h"


class CTexPartitionOutput : public CTexPartition
{
private:
    CTexSet<CTexElement, CTexElementFactory>    m_texset;
    CBmpWriter m_bmp;

public:
	bool UsedRectangle(int n, GrVertex *vlist);
	void Initialise(CMappingFile *mapper, unsigned char hash[], GuTexPalette *pal, char *data);
	void Finalise();
	CTexPartitionOutput(GrTextureFormat_t fmt, int width, int height);
	virtual ~CTexPartitionOutput();
};

#endif // !defined(AFX_TEXEXPLODER_H__BE027886_D1E2_47A5_8DFF_D26C9ADCA7E0__INCLUDED_)
