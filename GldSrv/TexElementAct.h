// TexElementAct.h: interface for the CTexElementAct class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXELEMENTACT_H__85E1A524_D621_4DE3_B9CD_DB26316F646B__INCLUDED_)
#define AFX_TEXELEMENTACT_H__85E1A524_D621_4DE3_B9CD_DB26316F646B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TexElement.h"

class CTexElementAct : public CTexElement  
{
public:
    virtual void Reset() = 0;
    virtual void Override(int n, GrVertex *vlist) = 0;
    virtual bool Load(GrLOD_t lod) = 0;

	CTexElementAct();
	CTexElementAct(CBox &b, CString &name);
	virtual ~CTexElementAct();

};

#endif // !defined(AFX_TEXELEMENTACT_H__85E1A524_D621_4DE3_B9CD_DB26316F646B__INCLUDED_)
