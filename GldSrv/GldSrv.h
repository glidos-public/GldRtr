/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide server
 * 
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.3  2003/05/26 09:08:21  paul
 * Add French version of app
 *
 * Revision 1.2  2001/06/20 16:17:01  paul
 * Send "abort" command to VXD on shutdown to stop the server thread
 * hanging in a get call.
 *
 * Add buttons for shutdown, and saving the edit box.
 *
 * Us commands in protocol, only GrGlideInit at this stage.
 *
 ************************************************************/

// GldSrv.h : main header file for the GLDSRV application
//

#if !defined(AFX_GLDSRV_H__8CE9ACE4_6150_11D5_B9B5_00C0DFF0F563__INCLUDED_)
#define AFX_GLDSRV_H__8CE9ACE4_6150_11D5_B9B5_00C0DFF0F563__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CGldSrvApp:
// See GldSrv.cpp for the implementation of this class
//

class CGldSrvApp : public CWinApp
{
public:
    HINSTANCE m_resDLL;
	CGldSrvApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGldSrvApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CGldSrvApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GLDSRV_H__8CE9ACE4_6150_11D5_B9B5_00C0DFF0F563__INCLUDED_)
