/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide server
 * 
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.3  2004/08/11 06:28:50  paul
 * Better use of filter graphs for playing mp3s.  Now able
 * to stop the previous sample before playig a new one.
 *
 * Revision 1.2  2001/06/20 16:17:01  paul
 * Send "abort" command to VXD on shutdown to stop the server thread
 * hanging in a get call.
 *
 * Add buttons for shutdown, and saving the edit box.
 *
 * Us commands in protocol, only GrGlideInit at this stage.
 *
 ************************************************************/

// Thread.cpp: implementation of the CThread class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Thread.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CThread::CThread()
{
	m_thread = NULL;
	m_dying = false;
}

CThread::~CThread()
{
	if(m_thread)
	{
		Kill();
		Wait();
		delete m_thread;
	}
}

void CThread::Start()
{
	ASSERT(m_thread == NULL);
	m_thread = AfxBeginThread(Hook, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
	m_thread->m_bAutoDelete = false;
	m_thread->ResumeThread();
}

void CThread::Kill()
{
	m_dying = true;
	if(m_thread) ::PostThreadMessage(m_thread->m_nThreadID, WM_USER_KILL_THREAD, 0, 0);
}

void CThread::Wait()
{
	MSG msg;

	BOOL going = (m_thread != NULL);
	while(going)
	{
		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			DispatchMessage(&msg);

		if(MsgWaitForMultipleObjects(1, &(m_thread->m_hThread), false, INFINITE, QS_ALLINPUT) == 0)
			going = false;
	}
}

UINT CThread::Hook(void *arg)
{
	/* AfxSocketInit(); */
	CoInitialize(NULL);
	((CThread *) arg)->Execute();
	CoUninitialize();
	return 0;
}

bool CThread::Dying()
{
	return m_dying;
}
