#if !defined(AFX_PROP3DMONITOR_H__7ABF60A1_57F8_41D2_AA18_9834A5F1DD2F__INCLUDED_)
#define AFX_PROP3DMONITOR_H__7ABF60A1_57F8_41D2_AA18_9834A5F1DD2F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Prop3DMonitor.h : header file
//

#include "GameRecord.h"

/////////////////////////////////////////////////////////////////////////////
// CProp3DMonitor dialog

class CProp3DMonitor : public CPropertyPage
{
    DECLARE_DYNCREATE(CProp3DMonitor)

private:
    void FillControls(const StereoMap *sm);
    void ReadControls(StereoMap *sm);
    int          m_count;
    CGameRecord *m_rec;

    void EnableDisable();

// Construction
public:
    void Init(CGameRecord *rec);
    CProp3DMonitor();
    ~CProp3DMonitor();

// Dialog Data
    //{{AFX_DATA(CProp3DMonitor)
    enum { IDD = IDD_PROP_3DMON };
    CButton   m_mode6;
    CButton   m_mode5;
    CButton   m_mode4;
    CButton   m_mode3;
    CButton   m_mode2;
    CButton   m_mode1;
    CButton   m_mode0;
    CComboBox m_presets;
    CEdit    m_offset3;
    CEdit    m_offset2;
    CEdit    m_offset1;
    CEdit    m_limit3;
    CEdit    m_limit2;
    CEdit    m_limit1;
    CEdit    m_factor3;
    CEdit    m_factor2;
    CEdit    m_factor1;
    CButton    m_eq3;
    CButton    m_eq2;
    CButton    m_stereoscopic;
    //}}AFX_DATA


// Overrides
    // ClassWizard generate virtual function overrides
    //{{AFX_VIRTUAL(CProp3DMonitor)
    public:
    virtual BOOL OnSetActive();
    virtual BOOL OnKillActive();
    protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL

// Implementation
protected:
    // Generated message map functions
    //{{AFX_MSG(CProp3DMonitor)
    afx_msg void OnStereoscopic();
    afx_msg void OnEq2();
    afx_msg void OnEq3();
    afx_msg void OnSelchangePresets();
    afx_msg void OnMode0();
    afx_msg void OnMode1();
    afx_msg void OnMode2();
    afx_msg void OnMode3();
    afx_msg void OnMode4();
    afx_msg void OnMode5();
    afx_msg void OnMode6();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROP3DMONITOR_H__7ABF60A1_57F8_41D2_AA18_9834A5F1DD2F__INCLUDED_)
