// GameRecord.cpp: implementation of the CGameRecord class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "wrapper_config.h"
#include "GldSrv.h"
#include "GameRecord.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CGameRecord::Write(FILE *f)
{
    INT_PTR n, i;
	
	n = m_fields.GetSize();
	
	for(i = 0; i < n; i++)
		fputs((LPCSTR)m_fields[i]->Write(), f);
	
	fprintf(f, "\n");
};

CGameRecord *CGameRecord::Read(FILE *f)
{
	char         buf[256];
	CGameRecord *rec;
	
	rec = new CGameRecord;
	
	for(;;)
	{
		if(fgets(buf, 256, f) == NULL)
			break;
		
		CString line(buf);
		line.TrimLeft();
		line.TrimRight();
		
		if(line.IsEmpty())
			break;
		
		int pos = line.Find(':');
		
		if(pos == -1)
			continue;
		
		CString key(line.Left(pos));
		CString val(line.Mid(pos+1));
		key.TrimRight();
		val.TrimLeft();
		
        INT_PTR n, i;
		
		n = rec->m_fields.GetSize();
		
		for(i = 0; i < n && !rec->m_fields[i]->Read(key, val); i++)
			;
	}

	rec->dos_full_screen = rec->dos_graphics && !rec->vesa_support;
	
	if(rec->name.IsDefined())
	{
		return rec;
	}
	else
	{
		delete rec;
		
		return NULL;
	}
}

FxU32 CGameRecord::ConfigFlags()
{
    return (full_screen ? 0
                        : WRAPPER_FLAG_WINDOWED)
         | (mipmaps     ? WRAPPER_FLAG_MIPMAPS
                        : 0)
         | (palette_tricks_cache ? WRAPPER_FLAG_PALETTED_TEXTURE_CACHE
                                 : 0)
         | (managed_textures ? WRAPPER_FLAG_MANAGED_TEXTURES
                             : 0)
         | (colour_16bit ? WRAPPER_FLAG_16BIT_COLOR
                             : 0)
         | (no_mode_change ? WRAPPER_FLAG_NO_MODE_CHANGE
                             : 0);
}
