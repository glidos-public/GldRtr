// PlayBackground.cpp: implementation of the CPlayBackground class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "PlayBackground.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPlayBackground::CPlayBackground()
{

}

CPlayBackground::~CPlayBackground()
{

}

void CPlayBackground::Start(CString &file)
{
	if(m_current == file)
	{
		Poll();
	}
	else
	{
		m_current = file;
		CPlayAudioFile::Stop();
		CPlayAudioFile::Start(file);
	}
}

void CPlayBackground::Stop()
{
	CPlayAudioFile::Stop();
	m_current = CString();
}

void CPlayBackground::Poll()
{
	if(!Playing())
		if(m_current != CString())
		    CPlayAudioFile::Start(m_current);
}

void CPlayBackground::Pause()
{
	CPlayAudioFile::Stop();
}
