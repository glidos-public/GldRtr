// RenderStore.h: interface for the CRenderStore class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RENDERSTORE_H__F4EBCCED_D0A8_4656_ACE0_32F2D1F7D5F1__INCLUDED_)
#define AFX_RENDERSTORE_H__F4EBCCED_D0A8_4656_ACE0_32F2D1F7D5F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "sdk2_glide.h"
#include "GlideDll.h"
#include "GlideState.h"

class CRenderStore  
{
private:
    struct Record
    {
        int       nverts;
        GrVertex *vlist;
        int       tex_id;

        Record   *next;

        Record()
        {
            vlist = NULL;
        };

        ~Record()
        {
            delete [] vlist;
        };
    };

    Record      *m_list;
    CGlideDll   *m_wrapper;
    CGlideState *m_glide_state;

public:
	void Store(int nverts, GrVertex *vlist, int tex_id);
    void Render();

	CRenderStore(CGlideDll *wrapper, CGlideState *glide_state);
	virtual ~CRenderStore();
};

#endif // !defined(AFX_RENDERSTORE_H__F4EBCCED_D0A8_4656_ACE0_32F2D1F7D5F1__INCLUDED_)
