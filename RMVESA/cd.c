#include <dos.h>
#include "pipe.h"

#include "cd.h"

typedef struct
{
    unsigned long int start;
} CDCmd;

#define NCDCMD (10)

static CDCmd cd_cmd_buf[NCDCMD];
static int   cd_cmd_index = 0;

static void store_cd_cmd(unsigned long int start)
{
    if(cd_cmd_index < NCDCMD)
    {
        cd_cmd_buf[cd_cmd_index].start = start;
        cd_cmd_index += 1;
    }
}

static void (__interrupt __far *old2f)();

static void __interrupt __far new2f_cd(union INTPACK r)
{
    if(r.w.ax == 0x1510)
    {
        /* Its a device request aimed at mscdex
         * es:bx is the real mode address of the
         * request header */
        unsigned char __far *req = (unsigned char __far *) (r.w.es:>r.w.bx);

        /* Return success indication */
        req[3] = 0;
        req[4] = 1;

        switch(req[2])
        {
        case 3: /* IOCTL input */
            {
                unsigned char __far *ctrl;

                ctrl = *((unsigned char __far **)(req+14));

                switch(ctrl[0])
                {
                case 6: /* Divice status */
                    ctrl[1] = 2;
                    ctrl[2] = 2;
                    ctrl[3] = 0;
                    ctrl[4] = 0;
                    break;

                case 9: /* Media changed */
                    ctrl[1] = 255;
                    break;

                case 10: /* Audio disc info */
                    ctrl[1] = 1;
                    ctrl[2] = 65;
                    ctrl[3] = 0;
                    ctrl[4] = 0;
                    ctrl[5] = 65;
                    ctrl[6] = 0;
                    break;

                case 11: /* Audio track info */
                    ctrl[2] = 0;
                    ctrl[3] = 0;
                    ctrl[4] = ctrl[1] - 1;
                    ctrl[5] = 0;
                    ctrl[6] = (ctrl[1] == 1) ? 4 : 0;
                    break;

                case 12: /* Q channel */
                    req[4] |= (Pipe_playStatus() ? 2 : 0);
                    break;
                }
            }
            break;

        case 131: /* SEEK */
            store_cd_cmd(0);

            Pipe_setPlayStatus(1);
            break;

        case 132: /* PLAY */
            store_cd_cmd(*((unsigned long int __far *)(req+14)));

            Pipe_setPlayStatus(1);
            req[4] = 3;
            break;

        case 133: /* STOP */
            store_cd_cmd(0);
            break;
        }

        return; /* Don't chain */
    }

    /* In cases where we don't deal with request,
     * we chain to the original handler */
    _chain_intr(old2f);
}

void CD_init(void)
{
    old2f = _dos_getvect(0x2f);

    _dos_setvect(0x2f, new2f_cd);
}

void CD_sendCommands(void)
{
    int i;

    for(i = 0; i < cd_cmd_index; i++)
    {
        Pipe_sendChar(CMD_Mp3Play);
        Pipe_sendInt(cd_cmd_buf[i].start);
    }

    cd_cmd_index = 0;
}
