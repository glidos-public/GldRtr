// W2KPipe.cpp: implementation of the CW2KPipe class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "str_code.h"
#include "GldSrv.h"
#include "W2KPipe.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CW2KPipe::CW2KPipe()
{
    m_pipe = INVALID_HANDLE_VALUE;
    overlapped.hEvent = INVALID_HANDLE_VALUE;
    overlapped_write.hEvent = INVALID_HANDLE_VALUE;
	m_connected = false;
}

CW2KPipe::~CW2KPipe()
{
    if(m_pipe != INVALID_HANDLE_VALUE)
        CloseHandle(m_pipe);

    if(overlapped.hEvent != INVALID_HANDLE_VALUE)
        CloseHandle(overlapped.hEvent);

    if(overlapped_write.hEvent != INVALID_HANDLE_VALUE)
        CloseHandle(overlapped_write.hEvent);
}

bool CW2KPipe::Create()
{
    static const char pname[] = {STRCODE('\\','\\','.','\\'),STRCODE('P','I','P','E'),
        STRCODE('\\','G','L','D'),STRCODE('P','I','P','E'),STRCODE('1',0,0,0)};
    char pnbuf[20];
    str_decode(pnbuf, pname);
    m_pipe = CreateNamedPipe(pnbuf,
                             PIPE_ACCESS_DUPLEX|FILE_FLAG_OVERLAPPED,
                             0,
                             1,
                             0x100000,
                             0x100000,
                             1000,
                             NULL);

    if(m_pipe == INVALID_HANDLE_VALUE)
        return false;

    overlapped.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    overlapped_write.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    if(overlapped.hEvent == INVALID_HANDLE_VALUE || overlapped.hEvent == INVALID_HANDLE_VALUE)
        return false;

	return true;
}

bool CW2KPipe::Read(char *buf, int requested, unsigned long *returned)
{
	bool done = false;
	
	while(!done)
	{
        if(!m_connected)
        {
            overlapped.Offset = 0;
            overlapped.OffsetHigh = 0;

            if(ConnectNamedPipe(m_pipe, &overlapped) == FALSE)
            {
                DWORD error = GetLastError();
                switch(error)
                {
                    case ERROR_PIPE_CONNECTED:
                        break;

                    case ERROR_PIPE_LISTENING:
                    case ERROR_IO_PENDING:
                        if(m_shutdown)
                        {
                            *returned = 0;
                            return true;
                        }

                        PumpWhileWaiting(overlapped.hEvent);
                        break;
                    
                    default:
                        return false;
                }
            }

            m_connected = true;

            ResetEvent(overlapped.hEvent);
        }

        overlapped.Offset = 0;
        overlapped.OffsetHigh = 0;

        if(ReadFile(m_pipe, (LPVOID)buf, requested, NULL, &overlapped))
        {
            done = true;
            GetOverlappedResult(m_pipe, &overlapped, returned, FALSE);
        }
        else
        {
            if(GetLastError() == ERROR_IO_PENDING)
            {
                if(m_shutdown)
                {
                    *returned = 0;
                    return false;
                }

                PumpWhileWaiting(overlapped.hEvent);
                done = true;
                GetOverlappedResult(m_pipe, &overlapped, returned, FALSE);
            }
            else
            {
                if(DisconnectNamedPipe(m_pipe))
                    m_connected = false;
                else
                    return false;
            }

            ResetEvent(overlapped.hEvent);
        }
    }
    
    return !m_shutdown;
}

void CW2KPipe::Wake()
{
    m_shutdown = true;
    SetEvent(overlapped.hEvent);
}

CW2KPipe *CW2KPipe::NewPipe()
{
    CW2KPipe *res = new CW2KPipe();

    if(res == NULL)
        return NULL;

    if(res->Create())
    {
        return res;
    }
    else
    {
        delete res;
        return NULL;
    }
}

bool CW2KPipe::Write(char *buf, int n)
{
	unsigned long sent;

	while(n > 0)
	{
        overlapped_write.Offset = 0;
        overlapped_write.OffsetHigh = 0;
        if(WriteFile(m_pipe, buf, n, NULL, &overlapped_write))
        {
            GetOverlappedResult(m_pipe, &overlapped_write, &sent, FALSE);
        }
        else
        {
            if(GetLastError() == ERROR_IO_PENDING)
            {
                if(m_shutdown)
                    return false;

                PumpWhileWaiting(overlapped_write.hEvent);
                GetOverlappedResult(m_pipe, &overlapped_write, &sent, FALSE);
            }
            else
            {
                return false;
            }

            ResetEvent(overlapped_write.hEvent);
        }

		buf += sent;
		n -= sent;
	}

    return !m_shutdown;
}
