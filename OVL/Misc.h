extern void get_current_dir(char *buf);
#pragma aux get_current_dir =  \
    "mov eax, 4700h"           \
    "mov edx, 0h"              \
    "int 21h"                  \
    parm [esi]                 \
    modify [eax edx];



extern void text_mode(void);
#pragma aux text_mode =          \
    "mov eax, 3h"                \
    "int 10h"                    \
    modify [eax];



extern void memcpy(char *dst, char *src, int count);
#pragma aux memcpy =             \
    "rep movsb"                  \
    parm [edi] [esi] [ecx];



extern void chain(void (__interrupt __far *prev_handler)());
#pragma aux chain =                \
    "mov eax, ecx"                 \
    "mov ecx, ebx"                 \
    "mov esp, ebp"                 \
    "xchg ecx,[ebp+28h]"           \
    "xchg eax,[ebp+2ch]"           \
    "pop gs"                       \
    "pop fs"                       \
    "pop es"                       \
    "pop ds"                       \
    "pop edi"                      \
    "pop esi"                      \
    "pop ebp"                      \
    "pop ebx"                      \
    "pop ebx"                      \
    "pop edx"                      \
    "retf"                         \
    parm [cx ebx];


extern void (__interrupt __far *getvect(int vect))();
#pragma aux getvect =              \
    "and eax, 0ffh"                 \
    "or  eax, 3500h"                \
    "push es"                      \
    "int 21h"                      \
    "mov dx, es"                   \
    "pop es"                       \
    parm [eax]                     \
    value [dx ebx];



extern void setvect(int vect, void (__interrupt __far *handler)());
#pragma aux setvect =              \
    "push ds"                      \
    "mov ds,cx"                    \
    "mov edx,ebx"                  \
    "and eax, 0ffh"                \
    "or eax, 2500h"                \
    "int 21h"                      \
    "pop ds"                       \
    parm [eax] [cx ebx]            \
    modify [edx];


extern int get_timer_mode(void);
#pragma aux get_timer_mode =       \
    "in  al, 40h"                  \
    "in  al, 40h"                  \
    "mov al, 0e2h"                 \
    "out 43h, al"                  \
    "in  al, 40h"                  \
    "and eax, 0ffh"                \
    value [eax];


extern int get_timer_count(void);
#pragma aux get_timer_count =      \
    "sti"                          \
    "in  al, 40h"                  \
    "in  al, 40h"                  \
    "mov al, 0h"                   \
    "out 43h, al"                  \
    "in  al, 40h"                  \
    "and eax, 0ffh"                \
    "mov ebx, eax"                 \
    "in  al, 40h"                  \
    "and eax, 0ffh"                \
    "shl eax, 8h"                  \
    "or eax, ebx"                  \
    "cli"                          \
    modify [ebx]                   \
    value [eax];


extern void set_timer_divider(int d);
#pragma aux set_timer_divider =    \
    "sti"                          \
    "mov al, 34h"                  \
    "out 43h, al"                  \
    "mov eax, ebx"                 \
    "out 40h, al"                  \
    "shr eax, 8h"                  \
    "out 40h, al"                  \
    "cli"                          \
    parm [ebx]                     \
    modify [eax];


extern void do_int08(void);
#pragma aux do_int08 =             \
    "int 8h";


extern void int08end(void);
#pragma aux int08end =            \
    "mov al, 20h"                 \
    "out 20h, al"                 \
    modify [eax];

void misc_init(void);
