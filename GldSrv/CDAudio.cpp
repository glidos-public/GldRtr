// CDAudio.cpp: implementation of the CCDAudio class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CDAudio.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define POLL_DELAY (10)

CCDAudio::CCDAudio()
{
    m_playing = false;
    m_open    = false;
	m_enabled = true;
}

CCDAudio::~CCDAudio()
{
}

void CCDAudio::Play(unsigned int stt, unsigned int duration)
{
	if(m_enabled)
	{
		m_start    = stt;
		m_duration = duration;
		
		if(!m_open)
		{
			open();
			m_open = true;
		}
		
		m_playing = true;
		
		start();
	}
}

void CCDAudio::Poll()
{
	if(m_enabled)
	{
		if(!m_playing)
			return;
		
		if(!m_open)
		{
			open();
			m_open = true;
		}
		
		if(!playing())
			start();
	}
}

unsigned int CCDAudio::frames2msf(unsigned int f)
{
    unsigned int frames, seconds, minutes;

    frames = f;
    seconds = f / 75;
    frames -= seconds * 75;
    minutes = seconds / 60;
    seconds -= minutes * 60;

    return minutes | (seconds<<8) | (frames<<16);
}


void CCDAudio::open()
{
    MCIERROR err;
    MCI_OPEN_PARMS oparms;
    MCI_SET_PARMS sparms;

    oparms.dwCallback = NULL;
    oparms.lpstrAlias = NULL;
    oparms.lpstrDeviceType = (LPCSTR) MCI_DEVTYPE_CD_AUDIO;
    oparms.lpstrElementName = NULL;
    
    err = ::mciSendCommand(0, MCI_OPEN, MCI_OPEN_TYPE|MCI_OPEN_TYPE_ID|MCI_OPEN_SHAREABLE, (DWORD) &oparms);
    /*
    if(err != 0)
    {
        CString s;
        s.Format("MCI_OPEN1: %d", err);
        AfxMessageBox(s);
    }
    */
    
    m_id = oparms.wDeviceID;
    
    sparms.dwTimeFormat = MCI_FORMAT_MSF;
    
    err = ::mciSendCommand(m_id, MCI_SET, MCI_SET_TIME_FORMAT, (DWORD) &sparms);
    /*
    if(err != 0)
    {
        CString s;
        s.Format("MCI_OPEN2: %d", err);
        AfxMessageBox(s);
    }
    */
}

void CCDAudio::close()
{
    MCI_GENERIC_PARMS parms;
    MCIERROR err;

    err = ::mciSendCommand(m_id, MCI_CLOSE, 0, (DWORD) &parms);
    /*
    if(err != 0)
    {
        CString s;
        s.Format("MCI_CLOSE: %d", err);
        AfxMessageBox(s);
    }
    */
}

void CCDAudio::start()
{
    MCIERROR err;
    MCI_PLAY_PARMS pparms;

    pparms.dwCallback = NULL;
    pparms.dwFrom     = frames2msf(m_start);
    pparms.dwTo       = frames2msf(m_start+m_duration);
    
    err = ::mciSendCommand(m_id, MCI_PLAY, MCI_FROM|MCI_TO, (DWORD) &pparms);
    /*
    if(err != 0)
    {
        CString s;
        s.Format("MCI_PLAY: %d", err);
        AfxMessageBox(s);
    }
    */
}

bool CCDAudio::playing()
{
    MCIERROR err;
    MCI_STATUS_PARMS sparms;
    
    sparms.dwItem = MCI_STATUS_MODE;
    
    err = ::mciSendCommand(m_id, MCI_STATUS, MCI_STATUS_ITEM, (DWORD) &sparms);
    
    /* Treat an error as playing so that we don't keep
     * trying to restart */
    return err != 0 || sparms.dwReturn == MCI_MODE_PLAY;
}

void CCDAudio::Close()
{
	if(m_enabled)
	{
		m_playing = false;
		
		if(m_open)
		{
			close();
			m_open = false;
		}
	}
}

void CCDAudio::Disable()
{
	m_enabled = false;
}
