// TexElement.cpp: implementation of the CTexElement class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "sdk2_glide.h"
#include "sdk2_glideutl.h"
#include "TexElement.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexElement::CTexElement()
{
}

CTexElement::CTexElement(CBox &b, CString &name) : CBox(b), m_name(name)
{

}

CTexElement::~CTexElement()
{

}

CString &CTexElement::GetName()
{
    return m_name;
}
