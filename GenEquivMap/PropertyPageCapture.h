#if !defined(AFX_PROPERTYPAGECAPTURE_H__5804708F_F060_49E1_9788_5BB9C7A1C550__INCLUDED_)
#define AFX_PROPERTYPAGECAPTURE_H__5804708F_F060_49E1_9788_5BB9C7A1C550__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropertyPageCapture.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageCapture dialog

#include "PropertyPageBase.h"

class CPropertyPageCapture : public CPropertyPageBase
{
	DECLARE_DYNCREATE(CPropertyPageCapture)

// Construction
public:
	CPropertyPageCapture();
	~CPropertyPageCapture();

// Dialog Data
	//{{AFX_DATA(CPropertyPageCapture)
	enum { IDD = IDD_PROPPAGE_CAPTURE };
	CString	m_folder;
	CString	m_level;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropertyPageCapture)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyPageCapture)
	afx_msg void OnButtonCapture();
	afx_msg void OnButtonBrowseFolder();
	afx_msg void OnButtonBrowseLevel();
	//}}AFX_MSG
    void OnUpdateCaptureButton(CCmdUI *pCmdUI);
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTYPAGECAPTURE_H__5804708F_F060_49E1_9788_5BB9C7A1C550__INCLUDED_)
