// PropertyPageCapture.cpp : implementation file
//

#include "stdafx.h"
#include "openssl/md5.h"
#include "PHDFile.h"
#include "TexOutput.h"
#include "genequivmap.h"
#include "PropertyPageCapture.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageCapture property page

IMPLEMENT_DYNCREATE(CPropertyPageCapture, CPropertyPageBase)

CPropertyPageCapture::CPropertyPageCapture() : CPropertyPageBase(CPropertyPageCapture::IDD)
{
	//{{AFX_DATA_INIT(CPropertyPageCapture)
	m_folder = _T("");
	m_level = _T("");
	//}}AFX_DATA_INIT
}

CPropertyPageCapture::~CPropertyPageCapture()
{
}

void CPropertyPageCapture::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropertyPageCapture)
	DDX_Text(pDX, IDC_EDIT_FOLDER, m_folder);
	DDX_Text(pDX, IDC_EDIT_LEVEL, m_level);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropertyPageCapture, CPropertyPageBase)
	//{{AFX_MSG_MAP(CPropertyPageCapture)
	ON_BN_CLICKED(IDC_BUTTON_CAPTURE, OnButtonCapture)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_FOLDER, OnButtonBrowseFolder)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_LEVEL, OnButtonBrowseLevel)
	//}}AFX_MSG_MAP
    ON_UPDATE_COMMAND_UI(IDC_BUTTON_CAPTURE, OnUpdateCaptureButton)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageCapture message handlers

void CPropertyPageCapture::OnButtonCapture() 
{	
    CPHDFile level;

    if(level.Open(m_level))
    {
        CTexHandlerOutput handler(m_folder + "\\Capture");
        int               i;
        CString levelName = m_level.Mid(m_level.ReverseFind('\\')+1).SpanExcluding(".");
        FILE *f = fopen((const char *)(m_folder + "\\Capture\\" + levelName + "keys.txt"), "wb");

        /* Pass textures to a Glidos, texture-capture
         * object */
        for(i = 0; i < level.GetNumTiles(); i++)
        {
            unsigned char  md5sum[16];
            Tile          *tile = level.GetTile(i);

            MD5((unsigned char *)tile, sizeof(Tile), md5sum);

            handler.GrTexDownloadTable((GuTexPalette *)level.GetPalette());
            handler.GuTexAllocateMemory(i, GR_TEXFMT_P_8, 256, 256, GR_LOD_256);
            handler.GuTexDownloadMipMap(i, md5sum, (char *)tile);

            CString hashName = handler.GetMappingFile()->NameFromHash(md5sum);
            CString key;
            key.Format("%s %s_%d\n", (const char *)hashName, (const char *)levelName, i);
            fputs((const char *)key, f);
        }

        fclose(f);

        for(i = 0; i < level.GetNumTexUses(); i++)
        {
            GrVertex  vert[2];
            TexUse   *use = level.GetTexUse(i);

            handler.GuTexSource(use->texid);

            vert[0].tmuvtx[0].sow = (float) use->xlow;
            vert[0].tmuvtx[0].tow = (float) use->ylow;
            vert[0].oow           = 1.0f;

            vert[1].tmuvtx[0].sow = (float) use->xhigh;
            vert[1].tmuvtx[0].tow = (float) use->yhigh;
            vert[1].oow           = 1.0f;

            handler.GrDrawPolygonVertexList(2, vert);
        }

        AfxMessageBox("Capture complete", MB_OK);
    }
}

void CPropertyPageCapture::OnButtonBrowseFolder() 
{
	if(GetFolder(m_folder))
        UpdateData(FALSE);
}

void CPropertyPageCapture::OnButtonBrowseLevel() 
{
    CFileDialog dlg(TRUE, NULL, NULL, 0, "Level File (*.phd;*.tub)|*.phd;*.tub|All files (*.*)|*.*||");

    if(dlg.DoModal() == IDOK)
    {
        m_level = dlg.GetPathName();
        UpdateData(FALSE);
    }
}

void CPropertyPageCapture::OnUpdateCaptureButton(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(!(m_level.IsEmpty() || m_folder.IsEmpty()));
}
