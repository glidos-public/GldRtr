// ImagePNG.cpp: implementation of the CImagePNG class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ImagePNG.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImagePNG::CImagePNG()
{
    m_png_ptr      = NULL;
    m_info_ptr     = NULL;
    m_row_pointers = NULL;
}

CImagePNG::~CImagePNG()
{
   if(m_png_ptr != NULL)
       png_destroy_read_struct(&m_png_ptr, (m_info_ptr != NULL ? &m_info_ptr : NULL), NULL);
}

bool CImagePNG::Load(FILE *f, int size)
{
    m_png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (m_png_ptr == NULL)
        return false;
    
    m_info_ptr = png_create_info_struct(m_png_ptr);
    if (m_info_ptr == NULL)
        return false;
    
    if(setjmp(png_jmpbuf(m_png_ptr)))
        return false;
    
    png_init_io(m_png_ptr, f);
    
    png_set_sig_bytes(m_png_ptr, size);
    
    png_read_png(m_png_ptr, m_info_ptr, PNG_TRANSFORM_EXPAND|PNG_TRANSFORM_BGR, NULL);
    
    m_width    = png_get_image_width(m_png_ptr, m_info_ptr);
    m_height   = png_get_image_height(m_png_ptr, m_info_ptr);
    m_channels = png_get_channels(m_png_ptr, m_info_ptr);

    m_row_pointers = png_get_rows(m_png_ptr, m_info_ptr);
    if(m_row_pointers == NULL)
        return false;
    
    fclose(f);
    
    return true;
}

int CImagePNG::GetWidth()
{
    return m_width;
}

int CImagePNG::GetHeight()
{
    return m_height;
}

COLORREF CImagePNG::GetPixel(int x, int y)
{
    png_bytep      row;
    unsigned char *pixel;
    COLORREF       res;
    
    row = m_row_pointers[m_height - y - 1];
    
    if(row == NULL)
    {
        res = 0xff000000;
    }
    else
    {
        pixel = row + x * m_channels;
        
        switch(m_channels)
        {
        case 1:
            res = pixel[0] | (pixel[0] << 8) | (pixel[0] << 16) | 0xff000000;
            break;
            
        case 2:
            res = pixel[0] | (pixel[0] << 8) | (pixel[0] << 16) | (pixel[1] << 24);
            break;
            
        case 3:
            res =  pixel[0] | (pixel[1] << 8) | (pixel[2] << 16) | 0xff000000;
            break;
            
        case 4:
            res =  pixel[0] | (pixel[1] << 8) | (pixel[2] << 16) | (pixel[3] << 24);
            break;
            
        default:
            res = 0xff000000;
            break;
        }
    }
    
    return res;
}

bool CImagePNG::HasAlpha()
{
    return m_channels > 3;
}

CImage *CImagePNG::TryLoad(FILE *f, unsigned char *buf, int size)
{
    CImagePNG *img = new CImagePNG;

    if(img == NULL)
        return NULL;

    if(png_sig_cmp(buf, 0, size) == 0 && img->Load(f, size))
        return img;

    delete img;

    return NULL;
}

