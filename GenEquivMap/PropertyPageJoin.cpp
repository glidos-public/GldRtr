// PropertyPageJoin.cpp : implementation file
//

#include "stdafx.h"
#include "genequivmap.h"
#include "PropertyPageJoin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageJoin property page

IMPLEMENT_DYNCREATE(CPropertyPageJoin, CPropertyPageBase)

CPropertyPageJoin::CPropertyPageJoin() : CPropertyPageBase(CPropertyPageJoin::IDD)
{
	//{{AFX_DATA_INIT(CPropertyPageJoin)
	m_file1 = _T("");
	m_file2 = _T("");
	//}}AFX_DATA_INIT
}

CPropertyPageJoin::~CPropertyPageJoin()
{
}

void CPropertyPageJoin::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropertyPageJoin)
	DDX_Text(pDX, IDC_EDIT_FILE1, m_file1);
	DDX_Text(pDX, IDC_EDIT_FILE2, m_file2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropertyPageJoin, CPropertyPageBase)
	//{{AFX_MSG_MAP(CPropertyPageJoin)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_FILE1, OnButtonBrowseFile1)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_FILE2, OnButtonBrowseFile2)
	ON_BN_CLICKED(IDC_BUTTON_JOIN, OnButtonJoin)
	//}}AFX_MSG_MAP
    ON_UPDATE_COMMAND_UI(IDC_BUTTON_JOIN, OnUpdateJoinButton)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageJoin message handlers

void CPropertyPageJoin::OnButtonBrowseFile1() 
{
    CFileDialog dlg(TRUE);

    if(dlg.DoModal() == IDOK)
    {
        m_file1 = dlg.GetPathName();
        UpdateData(FALSE);
    }
}

void CPropertyPageJoin::OnButtonBrowseFile2() 
{
    CFileDialog dlg(TRUE);

    if(dlg.DoModal() == IDOK)
    {
        m_file2 = dlg.GetPathName();
        UpdateData(FALSE);
    }
}

void CPropertyPageJoin::OnButtonJoin() 
{
    ReadState state;

    state.key = 0;

    if(!ReadEquiv(state, m_file1))
        return;

    if(!ReadEquiv(state, m_file2))
        return;

    CString ofile = GetOutputFileName("newequiv.txt");

    FILE *f;

    f = ofile.IsEmpty() ? NULL
                        : fopen((LPCSTR)ofile, "w");

    if(f)
    {
        fprintf(f, "GLIDOS TEXTURE EQUIV\n");

        KeyToImageFileSet::iterator miter;
        
        for(miter = state.keyToSet.begin(); miter != state.keyToSet.end(); miter++)
        {
            ImageFileSet &set = miter->second;
            
            if(set.size() > 1)
            {
                ImageFileSet::iterator  siter;

                fprintf(f, "\nBeginEquiv\n");
                
                for(siter = set.begin(); siter != set.end(); siter++)
                    fprintf(f, "%s\n", siter->c_str());

                fprintf(f, "EndEquiv\n");
            }
        }

        fclose(f);

        AfxMessageBox("File generated", MB_OK);
    }
    else
    {
        AfxMessageBox("File generation failed", MB_OK);
    }
}

void CPropertyPageJoin::OnUpdateJoinButton(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(!(m_file1.IsEmpty() || m_file2.IsEmpty()));
}
