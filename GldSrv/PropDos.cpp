// PropDos.cpp : implementation file
//

#include "stdafx.h"
#include "gldsrv.h"
#include "PropDos.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropDos property page

IMPLEMENT_DYNCREATE(CPropDos, CPropertyPage)

CPropDos::CPropDos() : CPropertyPage(CPropDos::IDD)
{
	//{{AFX_DATA_INIT(CPropDos)
	m_dos_emu = CDosEmu::DOSEMU_WINDOWS;
	//}}AFX_DATA_INIT
}

CPropDos::~CPropDos()
{
}

void CPropDos::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropDos)
	DDX_Control(pDX, IDC_STATIC_JUDDER, m_static_judder);
	DDX_Control(pDX, IDC_STATIC_PIT, m_static_pit);
	DDX_Control(pDX, IDC_STATIC_GRAPH, m_static_graph);
	DDX_Control(pDX, IDC_VESA_SUPPORT, m_vesa_support);
	DDX_Control(pDX, IDC_DOS_GRAPHICS, m_dos_graphics);
	DDX_Control(pDX, IDC_EDIT_JUDDER, m_judder);
	DDX_Control(pDX, IDC_SPIN_JUDDER, m_judder_spin);
	DDX_Radio(pDX, IDC_DOS_WINDOWS, m_dos_emu);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropDos, CPropertyPage)
	//{{AFX_MSG_MAP(CPropDos)
	ON_BN_CLICKED(IDC_DOS_WINDOWS, OnDosEmu)
	ON_BN_CLICKED(IDC_DOS_VDOS32, OnDosEmu)
	ON_BN_CLICKED(IDC_DOS_DOSBOX, OnDosEmu)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropDos message handlers

void CPropDos::Init(CGameRecord *rec)
{
    m_rec = rec;
}

BOOL CPropDos::OnSetActive() 
{
    m_dos_emu    = (CDosEmu::DosEmu)m_rec->dos_emu;
    m_dos_graphics.SetCheck((m_dos_emu == CDosEmu::DOSEMU_WINDOWS) && m_rec->dos_graphics);
    m_vesa_support.SetCheck((m_dos_emu != CDosEmu::DOSEMU_VDOS32) && m_rec->vesa_support);

    m_judder_spin.SetBuddy(&m_judder);
    m_judder.SetWindowText((m_dos_emu != CDosEmu::DOSEMU_WINDOWS) ? "0" : (LPCSTR)m_rec->control_judder_fix.Print());

    UpdateData(FALSE);

    m_dos_graphics.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));
    m_vesa_support.EnableWindow((m_dos_emu != CDosEmu::DOSEMU_VDOS32));
    m_judder.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));
    m_judder_spin.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));
    m_static_graph.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));
    m_static_judder.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));
    m_static_pit.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));

	return CPropertyPage::OnSetActive();
}

BOOL CPropDos::OnKillActive() 
{
    char buf[256];

    UpdateData();

    m_rec->dos_emu         = (CDosEmu::DosEmu)m_dos_emu;
    
    if(m_dos_emu == CDosEmu::DOSEMU_WINDOWS)
    {
        m_judder.GetWindowText(buf, 256);
        
        if(!m_rec->control_judder_fix.Parse(CString(buf))
            || m_rec->control_judder_fix < 0 || m_rec->control_judder_fix > 100)
        {
            AfxMessageBox("Judder Fix must be between 0 and 100", MB_OK);
            
            return FALSE;
        }
        
        m_rec->dos_graphics       = (m_dos_graphics.GetCheck() != FALSE);
    }

    if(m_dos_emu != CDosEmu::DOSEMU_VDOS32)
    {
        m_rec->vesa_support       = (m_vesa_support.GetCheck() != FALSE);
    }

	return CPropertyPage::OnKillActive();
}

void CPropDos::OnDosEmu() 
{	
	UpdateData();

    m_dos_graphics.SetCheck((m_dos_emu == CDosEmu::DOSEMU_WINDOWS) && m_rec->dos_graphics);
    m_vesa_support.SetCheck((m_dos_emu != CDosEmu::DOSEMU_VDOS32) && m_rec->vesa_support);

    m_judder_spin.SetBuddy(&m_judder);
    m_judder.SetWindowText((m_dos_emu != CDosEmu::DOSEMU_WINDOWS) ? "0" : (LPCSTR)m_rec->control_judder_fix.Print());

    m_dos_graphics.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));
    m_vesa_support.EnableWindow((m_dos_emu != CDosEmu::DOSEMU_VDOS32));
    m_judder.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));
    m_judder_spin.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));
    m_static_graph.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));
    m_static_judder.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));
    m_static_pit.EnableWindow((m_dos_emu == CDosEmu::DOSEMU_WINDOWS));
}
