// RenderStore.cpp: implementation of the CRenderStore class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "RenderStore.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRenderStore::CRenderStore(CGlideDll *wrapper, CGlideState *glide_state)
{
    m_list        = NULL;
    m_wrapper     = wrapper;
    m_glide_state = glide_state;
}

CRenderStore::~CRenderStore()
{
    while(m_list != NULL)
    {
        Record *tmp = m_list;

        m_list = m_list->next;

        delete tmp;
    }
}

void CRenderStore::Store(int nverts, GrVertex *vlist, int tex_id)
{
    Record *rec = new Record();
    int     i;

    rec->nverts = nverts;
    rec->vlist  = new GrVertex[nverts];
    for(i = 0; i < nverts; i++)
        rec->vlist[i]  = vlist[i];
    rec->tex_id = tex_id;
    rec->next   = m_list;
    m_list      = rec;
}

void CRenderStore::Render()
{
    if(m_list != NULL)
    {
        m_wrapper->grAlphaTestFunction(GR_CMP_GREATER);
        m_wrapper->grAlphaBlendFunction(GR_BLEND_SRC_ALPHA,
            GR_BLEND_ONE_MINUS_SRC_ALPHA,
            GR_BLEND_ONE,
            GR_BLEND_ZERO);
        
        m_wrapper->grAlphaCombine(GR_COMBINE_FUNCTION_SCALE_OTHER,
            GR_COMBINE_FACTOR_ONE,
            GR_COMBINE_LOCAL_NONE,
            GR_COMBINE_OTHER_TEXTURE,
            FXFALSE);

        m_wrapper->grColorCombine(GR_COMBINE_FUNCTION_SCALE_OTHER,
            GR_COMBINE_FACTOR_LOCAL,
            GR_COMBINE_LOCAL_ITERATED,
            GR_COMBINE_OTHER_TEXTURE,
            FXFALSE);

        while(m_list != NULL)
        {
            Record *tmp = m_list;
            
            m_wrapper->guTexSource(tmp->tex_id);
            m_wrapper->grDrawPolygonVertexList(tmp->nverts, tmp->vlist);
            
            m_list = m_list->next;
            
            delete tmp;
        }
        
        m_glide_state->RestoreAlphaBlendFunction();
        m_glide_state->RestoreAlphaCombine();
        m_glide_state->RestoreColorCombine();
        m_glide_state->RestoreTexSource();
        m_wrapper->grAlphaTestFunction(GR_CMP_ALWAYS);
    }
}
