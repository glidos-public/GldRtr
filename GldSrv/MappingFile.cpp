// MappingFile.cpp: implementation of the CMappingFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "MappingFile.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define MASTER_SIG "GLIDOS TEXTURE MAPPING"
#define EQUIV_SIG  "GLIDOS TEXTURE EQUIV"
#define BASE_MARKER "BASE:"
#define ROOT_MARKER "ROOT:"
#define BEGIN_EQUIV "BeginEquiv"
#define END_EQUIV   "EndEquiv"
#define WHITE_SPACE " \t\r\n"
#define BUFSIZE     (257)
#define HASHNAMESIZE (32)


TexElementInfo::TexElementInfo(const CBox &b, const CString &p)
: box(b), path(p) {}

CMappingFile::CMappingFile()
{
}

CMappingFile::~CMappingFile()
{
}

void CMappingFile::Initialise(const CString &folder)
{
    CFileFind finder;

    m_folder = folder;

    CString master_path = folder + "\\*.*";
    BOOL working = finder.FindFile((LPCSTR)master_path);
    while(working)
    {
        working = finder.FindNextFile();
        
        FILE *f = finder.IsDirectory() ? NULL
                                       : fopen((LPCSTR)finder.GetFilePath(), "r");
        if(f)
        {
            char    buf[BUFSIZE];
            if(fgets(buf, BUFSIZE, f))
            {
                if(memcmp(buf, MASTER_SIG, strlen(MASTER_SIG)) == 0)
                {
                    ReadMappingFile(f, buf);
                }
                else if(memcmp(buf, EQUIV_SIG, strlen(EQUIV_SIG)) == 0)
                {
                    ReadEquivFile(f, buf);
                }
            }
            
            fclose(f);
        }
    }

    finder.Close();
}

CStringArray *CMappingFile::GetNameSet()
{
    NameMap        map;
    CStringArray  *set;
    CFileFind     finder;
    BOOL          going;
    POSITION      pos;

    going = finder.FindFile((LPCSTR)(m_folder + "\\*.*"));
    while(going)
    {
        going = finder.FindNextFile();

        if(finder.IsDirectory())
        {
            CString name = finder.GetFileName();

            if(name.GetLength() == HASHNAMESIZE
                  && name.SpanIncluding("0123456789ABCDEF").GetLength() == HASHNAMESIZE)
                map[std::string((LPCSTR)name)] = 1;
        }
    }

    pos = m_map_dir.GetStartPosition();
    while(pos != NULL)
    {
        CString key, val;

        m_map_dir.GetNextAssoc(pos, key, val);

        map[std::string((LPCSTR)key)] = 1;
    }

    ImageFileToKey::iterator iter;

    for(iter = m_elemtokey.begin(); iter != m_elemtokey.end(); iter++)
        map[iter->first.substr(0, HASHNAMESIZE)] = 1;

    set = new CStringArray;

    NameMap::iterator miter;

    for(miter = map.begin(); miter != map.end(); miter++)
        set->Add(CString(miter->first.c_str()));

    return set;
}

TexElementInfoList *CMappingFile::GetElementInfoList(unsigned char hash[])
{
	return GetElementInfoList(NameFromHash(hash));
}

TexElementInfoList *CMappingFile::GetElementInfoList(const CString &name)
{
    CString             dir;
    CString             root;
    CFileFind           finder;
    CString             search;
    BOOL                going;
    TexElementInfoList *list;

    // Make list for returning the result
    list = new TexElementInfoList;

    // Find where the mapping file says to look
    // for these textures
    GetRootAndDir(name, root, dir);

    // Iterate through the folder for the textures
    search = dir + "\\*.*";

    going = finder.FindFile((LPCSTR)search);
    while(going)
    {
        int xmin, xmax, ymin, ymax;

        going = finder.FindNextFile();
        CString fname = finder.GetFileName();

        if(sscanf((LPCSTR)fname, "(%d--%d)(%d--%d).", &xmin, &xmax, &ymin, &ymax) == 4)
            list->push_back(TexElementInfo(CBox(xmin, xmax, ymin, ymax), FollowLink(dir + "\\" + fname, root)));
    }

    finder.Close();

    // Now look for equavalences. upper_bound of XXXXXXXX will take
    // us straight to where all the XXXXXXXX\(a--b)(c--d) strings are.
    ImageFileToKey::iterator kiter;
    for(kiter = m_elemtokey.upper_bound((LPCSTR)name); kiter != m_elemtokey.end(); kiter++)
    {
        std::string ifile = kiter->first;
        // All XXXXXXX mappings will be grouped to gether, so break
        // when we find one that isn't
        if(memcmp(ifile.c_str(), (LPCSTR)name, name.GetLength()) != 0)
            break;

        ImageFileSet            &set   = m_keytoset[kiter->second];
        ImageFileSet::iterator   siter;
        
        // Look for an alternative for this texture.
        for(siter = set.begin(); siter != set.end(); siter++)
        {
            std::string ofile = *siter;
            
            // If ifile existed we would already have included it
            // with the above folder enumeration
            if(ifile != ofile)
            {
                // Separate the hash from the coords
                CString hashname, coord;

                SplitAtSlash(CString(ofile.c_str()), hashname, coord);

                // Lookup where this hash gets mapped;
                GetRootAndDir(hashname, root, dir);

                // See if the coord file exists theere
                if(finder.FindFile((LPCSTR)(dir + "\\" + coord + ".*")))
                {
                    int xmin, xmax, ymin, ymax;

                    finder.FindNextFile();

                    // Need to get the box from ifile
                    SplitAtSlash(CString(ifile.c_str()), hashname, coord);
                    if(sscanf((LPCSTR)coord, "(%d--%d)(%d--%d).", &xmin, &xmax, &ymin, &ymax) == 4)
                        list->push_back(TexElementInfo(CBox(xmin, xmax, ymin, ymax),
                                           FollowLink(CString(finder.GetFilePath()), root)));


                    finder.Close();

                    // That texture dealt with, so no need to look at any
                    // further alternatives
                    break;
                }

                finder.Close();
            }
        }
    }


    return list;
}


CString CMappingFile::GetFolder(unsigned char hash[])
{
	CString name = NameFromHash(hash);
    CString val;

	return (m_map_dir.Lookup((LPCSTR)name, val))
        ? val
        : m_folder + "\\" + name;
}

CString CMappingFile::NameFromHash(unsigned char hash[])
{
	CString res("");
	int i;

	for(i = 0; i < 16; i++)
	{
		CString s;

		s.Format("%02X", hash[i]);
		res += s;
	}

    return res;
}

CString CMappingFile::FollowLink(CString &path, CString &root)
{
    CString res;
    char    linebuf[BUFSIZE];
    FILE   *f;
    char   *line;

    res = path;

    f = fopen((LPCSTR)path, "rb");
    if(f)
    {
        line = fgets(linebuf, BUFSIZE, f);
        if(line && memcmp(line, "TLNK:", 5) == 0)
        {
            CString file(line+5);

            file.TrimLeft();
            file.TrimRight();

            res = root + file;
        }

        fclose(f);
    }

    return res;
}

void CMappingFile::ReadMappingFile(FILE *f, char *buf)
{
    CString base;
    CString root;
    
    while(fgets(buf, BUFSIZE, f))
    {
        /* Remove comments */
        char *pos = strstr(buf, "//");
        if(pos != NULL)
            *pos = 0;
        
        if(root.IsEmpty() && memcmp(buf,ROOT_MARKER, strlen(ROOT_MARKER)) == 0)
        {
            root = CString(buf + strlen(ROOT_MARKER));
            root.TrimLeft();
            root.TrimRight();
            
            if(root[1] != ':')
                root = m_folder + "\\" + root;
            
            base = root;
        }
        else if(!root.IsEmpty())
        {
            if(memcmp(buf,BASE_MARKER, strlen(BASE_MARKER)) == 0)
            {
                base = CString(buf + strlen(BASE_MARKER));
                base.TrimLeft();
                base.TrimRight();
                
                if(base[1] != ':')
                    base = root + base;
            }
            else
            {
                char *from, *to, *extra;
                
                from = strtok(buf, WHITE_SPACE);
                if(from == NULL)
                    continue;
                
                to = strtok(NULL, WHITE_SPACE);
                if(to == NULL)
                    continue;
                
                extra = strtok(NULL, WHITE_SPACE);
                if(extra != NULL)
                    continue;
                
                m_map_dir[from] = (LPCSTR)(base + to);
                m_map_root[from] = root;
            }
        }
    }
}

void CMappingFile::ReadEquivFile(FILE *f, char *buf)
{
    int  key     = 0;
    bool inequiv = false;

    while(fgets(buf, BUFSIZE, f))
    {
        if(inequiv)
        {
            if(memcmp(buf, END_EQUIV, strlen(END_EQUIV)) == 0)
            {
                key += 1;
                inequiv = false;
            }
            else
            {
                CString name(buf);

                name.TrimLeft();
                name.TrimRight();

                m_elemtokey[(LPCSTR)name] = key;
                
                ImageFileSet &set = m_keytoset[key];
                set.push_back((LPCSTR)name);
            }
        }
        else
        {
            if(memcmp(buf, BEGIN_EQUIV, strlen(BEGIN_EQUIV)) == 0)
            {
                inequiv = true;
            }
        }
    }
}

void CMappingFile::GetRootAndDir(const CString &name, CString &root, CString &dir)
{
	if(!m_map_dir.Lookup((LPCSTR)name, dir))
        dir = m_folder + "\\" + name;

	if(!m_map_root.Lookup((LPCSTR)name, root))
        root = m_folder + "\\";
}

void CMappingFile::SplitAtSlash(const CString &name, CString &hash, CString &coord)
{
    int pos = name.Find('\\');

    if(pos != -1)
    {
        hash = name.Left(pos);
        coord = name.Mid(pos+1);
    }
    else
    {
        hash = name;
        coord = "";
    }
}
