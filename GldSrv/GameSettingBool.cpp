// GameSettingBool.cpp: implementation of the CGameSettingBool class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "GameSettingBool.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CGameSettingBool &CGameSettingBool::operator=(const bool &val)
{
	if(val != m_value)
		m_defined = true;
	
	m_value   = val;
	
	return *this;
};

bool CGameSettingBool::Parse(CString &val)
{
	if(val[0] == 'Y' || val[0] == 'y')
	{
		m_value = true;
		
		return true;
	}
	
	if(val[0] == 'N' || val[0] == 'n')
	{
		m_value = false;
		
		return true;
	}
	
	return false;
};

CString CGameSettingBool::Print()
{
	return m_value ? "Yes" : "No";
};
