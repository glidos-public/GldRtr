// StoredStruct.cpp: implementation of the CStoredStruct class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "StoredStruct.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CStoredStruct &CStoredStruct::operator=(const CStoredStruct &v)
{
	return *this;
}

void CStoredStruct::AddField(CStoredStructField *field)
{
	m_fields.Add(field);
};

void CStoredStructField::Init(CStoredStruct *st)
{
	st->AddField(this);
};
