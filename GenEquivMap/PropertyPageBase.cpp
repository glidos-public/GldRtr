// PropertyPageBase.cpp : implementation file
//

#include "stdafx.h"

#include "openssl/md5.h"
#include "openssl/rsa.h"

#include "genequivmap.h"
#include "PropertyPageBase.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define BUFSIZE     (257)
#define EQUIV_SIG   "GLIDOS TEXTURE EQUIV"
#define BEGIN_EQUIV "BeginEquiv"
#define END_EQUIV   "EndEquiv"

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageBase property page

IMPLEMENT_DYNCREATE(CPropertyPageBase, CPropertyPage)

CPropertyPageBase::CPropertyPageBase(UINT id) : CPropertyPage(id)
{
	//{{AFX_DATA_INIT(CPropertyPageBase)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CPropertyPageBase::~CPropertyPageBase()
{
}

void CPropertyPageBase::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropertyPageBase)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropertyPageBase, CPropertyPage)
	//{{AFX_MSG_MAP(CPropertyPageBase)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageBase message handlers

BOOL CPropertyPageBase::PreTranslateMessage(MSG* pMsg) 
{
    UpdateData();
    UpdateDialogControls(this, TRUE);

	return CPropertyPage::PreTranslateMessage(pMsg);
}
CString CPropertyPageBase::HashImage(CImage *img)
{
    unsigned char md5sum[16];
    MD5_CTX       c;
    int           width;
    int           height;
    int           x, y;

    MD5_Init(&c);

    width  = img->GetWidth();
    height = img->GetHeight();

    for(y = 0; y < height; y++)
    {
        for(x = 0; x < width; x++)
        {
            COLORREF col = img->GetPixel(x, y);

            MD5_Update(&c, &col, sizeof(col));
        }
    }

    MD5_Final(md5sum, &c);

    return NameFromHash(md5sum);
}

CString CPropertyPageBase::NameFromHash(unsigned char hash[])
{
	CString res("");
	int i;

	for(i = 0; i < 16; i++)
	{
		CString s;

		s.Format("%02X", hash[i]);
		res += s;
	}

    return res;
}

CString CPropertyPageBase::GetOutputFileName(LPCSTR fname)
{
    CFileDialog dlg(FALSE, ".txt", fname);

    dlg.DoModal();

    return dlg.GetFileName();
}

CString CPropertyPageBase::DropExtension(CString &name)
{
    int pos  = name.ReverseFind('.');
                        
    return (pos == -1 ? name
                      : name.Left(pos));
}

bool CPropertyPageBase::ReadEquiv(ReadState &state, const CString &file)
{
    FILE *f;
    char  buf[BUFSIZE];
    bool  inequiv;
    bool  first;
    int   groupKey;

    f = fopen((LPCSTR)file, "r");
    if(f == NULL)
    {
        AfxMessageBox("Cannot open file");
        return false;
    }

    if(fgets(buf, BUFSIZE, f) == NULL
        || memcmp(buf, EQUIV_SIG, strlen(EQUIV_SIG)) != 0)
    {
        fclose(f);
        AfxMessageBox("Not an equivalence file", MB_OK);
        return false;
    }

    inequiv = false;

    while(fgets(buf, BUFSIZE, f))
    {
        if(inequiv)
        {
            if(memcmp(buf, END_EQUIV, strlen(END_EQUIV)) == 0)
            {
                inequiv = false;
            }
            else
            {
                CString name(buf);

                name.TrimLeft();
                name.TrimRight();

                LPCSTR  cname(name);

                if(first)
                {
                    // We are reading the first of a group, and
                    // this will determin the key for the group
                    if(state.elemToKey.count(cname) == 0)
                    {
                        // Not met before. Use the next free key
                        // for the group.
                        groupKey = state.key++;
                        state.elemToKey[cname] = groupKey;
                        state.keyToSet[groupKey] = ImageFileSet::vector(1, cname);
                    }
                    else
                    {
                        // Met before.  Use its key for the group
                        groupKey = state.elemToKey[cname];
                    }

                    first = false;
                }
                else
                {
                    if(state.elemToKey.count(cname) == 0)
                    {
                        // Not met before. Just make it part of this group
                        state.elemToKey[cname] = groupKey;
                        state.keyToSet[groupKey].push_back(cname);
                    }
                    else
                    {
                        int oldkey = state.elemToKey[cname];

                        // If already mapped to groupKey then there's
                        // nothing to do.
                        if(oldkey != groupKey)
                        {
                            ImageFileSet          &nset   = state.keyToSet[oldkey];
                            ImageFileSet          &gset   = state.keyToSet[groupKey];
                            ImageFileSet::iterator iter;
                            
                            // Change all users of the old key to use the group key
                            for(iter = nset.begin(); iter != nset.end(); iter++)
                                state.elemToKey[*iter] = groupKey;
                            
                            // Copy the users of the old key into the group key's set
                            gset.insert(gset.end(), nset.begin(), nset.end());
                            
                            // Remove remaining record of the old key
                            state.keyToSet.erase(oldkey);
                        }
                    }
                }

            }
        }
        else
        {
            if(memcmp(buf, BEGIN_EQUIV, strlen(BEGIN_EQUIV)) == 0)
            {
                inequiv = true;
                first   = true;
            }
        }
    }

    fclose(f);

    return true;
}

bool CPropertyPageBase::GetFolder(CString &folder)
{
    LPMALLOC pMalloc; 
    /* Gets the Shell's default allocator */ 
    if (::SHGetMalloc(&pMalloc) == NOERROR) 
    { 
        BROWSEINFO bi; 
        char pszBuffer[MAX_PATH]; 
        LPITEMIDLIST pidl; 
        // Get help on BROWSEINFO struct - it's got all the bit settings. 
        bi.hwndOwner = GetSafeHwnd(); 
        bi.pidlRoot = NULL; 
        bi.pszDisplayName = pszBuffer; 
        bi.lpszTitle = _T("Select a Directory"); 
        bi.ulFlags = BIF_RETURNFSANCESTORS | BIF_RETURNONLYFSDIRS; 
        bi.lpfn = NULL; 
        bi.lParam = 0; 
        // This next call issues the dialog box. 
        if ((pidl = ::SHBrowseForFolder(&bi)) != NULL) 
        { 
            if (::SHGetPathFromIDList(pidl, pszBuffer)) 
            {
                // At this point pszBuffer contains the selected path */. 
                folder = pszBuffer;
                return true;
            } 
            // Free the PIDL allocated by SHBrowseForFolder. 
            pMalloc->Free(pidl); 
        } 
        // Release the shell's allocator. 
        pMalloc->Release(); 
    }

    return false;
}
