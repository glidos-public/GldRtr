/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * $Id$
 *
 ************************************************************/

#ifndef _VXD_H_
#define _VXD_H_

#include "pipe.h"


pipe_t *VxdOpen(void);

#endif
