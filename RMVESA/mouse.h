/************************************************************
 * Mouse driver for Glidos
 *
 * Copyright (c) Paul Gardiner.
 ************************************************************/

#ifndef _MOUSE_H_
#define _MOUSE_H_

Mouse_init(void);

Mouse_newState(long int xmickeys,
               long int ymickeys,
               long int buttons);


#endif
