// TexCache.cpp: implementation of the CTexCache class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "Box.h"
#include "TexCache.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define ABS(x) (((x) < 0) ? -(x) : (x))

CTexCache::CTexCache(CGlideDll *wrapper)
{
    m_ncache             = 0;
    m_source_is_small    = false;
    m_download_buffered  = false;
	m_download_unwritten = false;
    m_lfb_space          = 128*1024;
    m_wrapper            = wrapper;
}

CTexCache::~CTexCache()
{
}

void CTexCache::TexSource(FxU32 startAddress, FxU32 evenOdd, GrTexInfo *info)
{
    m_source_is_small = (info->largeLod >= GR_LOD_16);
    
    if(m_source_is_small)
    {
        m_sourceInfo.startAddress = startAddress;
        m_sourceInfo.evenOdd      = evenOdd;
        m_sourceInfo.info         = *info;
    }
	else
	{
        m_wrapper->grTexSource(GR_TMU0, startAddress, evenOdd, info);
	}
}

void CTexCache::TexDownloadMipMap(FxU32 startAddress, FxU32 evenOdd, GrTexInfo *info)
{
    if(info->largeLod >= GR_LOD_16)
    {
        int x, y, bpp;

		/* Flush buffered data, unless this call would overwrite it */
		if(m_download_unwritten && startAddress != m_downloadInfo.startAddress)
		{
            m_wrapper->grTexDownloadMipMap(GR_TMU0, m_downloadInfo.startAddress,
				                         m_downloadInfo.evenOdd,
										 &(m_downloadInfo.info));
		}

        DataSize(info, &x, &y, &bpp);

        m_downloadInfo.startAddress = startAddress;
        m_downloadInfo.evenOdd      = evenOdd;
        m_downloadInfo.info         = *info;

        memcpy(m_data, info->data, x*y*bpp);

		m_downloadInfo.info.data = m_data;

		m_download_buffered  = true;
		m_download_unwritten = true;
    }
    else
    {
		/* Flush buffered data to get it in before this call, in
		 * case there's an overlap */
		if(m_download_unwritten)
		{
            m_wrapper->grTexDownloadMipMap(GR_TMU0, m_downloadInfo.startAddress,
				                         m_downloadInfo.evenOdd,
										 &(m_downloadInfo.info));

			m_download_unwritten = false;
		}

        m_wrapper->grTexDownloadMipMap(GR_TMU0, startAddress, evenOdd, info);
    }
}

void CTexCache::PreRender(int n, GrVertex *vlist)
{
    if(m_source_is_small)
	{
		if(m_download_buffered
			&& m_sourceInfo.startAddress == m_downloadInfo.startAddress)
		{
			CBox box(n, vlist);
			int  i, y, xs, ys, bpp;
			
			DataSize(&(m_sourceInfo.info), &xs, &ys, &bpp);
			
			/* Adjust box to pixels */
			box.m_xmin = box.m_xmin * xs / 256;
			box.m_xmax = box.m_xmax * xs / 256;
			box.m_ymin = box.m_ymin * ys / 256;
			box.m_ymax = box.m_ymax * ys / 256;
			
			if(0 <= box.m_xmin && box.m_xmin < box.m_xmax && box.m_xmax <= 16
			    && 0 <= box.m_ymin && box.m_ymin < box.m_ymax && box.m_ymax <= 16)
			{
				/* Make everything work in bytes */
				xs         *= bpp;
				box.m_xmin *= bpp;
				box.m_xmax *= bpp;
				
				for(i = 0; i < m_ncache; i++)
				{
					GrTexInfo *info = &(m_cache[i].info);
					
					if(info->format      != m_sourceInfo.info.format
						|| info->aspectRatio != m_sourceInfo.info.aspectRatio
						|| info->largeLod    != m_sourceInfo.info.largeLod)
						continue;
					
					unsigned char *p = m_cache[i].data + box.m_ymin * xs;
					unsigned char *q = m_data          + box.m_ymin * xs;
					
					for(y = box.m_ymin; y < box.m_ymax; y++)
					{
						if(memcmp(p + box.m_xmin, q + box.m_xmin, box.m_xmax - box.m_xmin) != 0)
							break;
						
						p += xs;
						q += xs;
					}
					
					if(y == box.m_ymax)
						break;
				}
				
				/* i == NCACHE => m_ncache == NCACHE */
				if(i == m_ncache && m_ncache == NCACHE)
					i = m_ncache = 0;
				
				/* i == m_ncache => m_ncache < NCACHE */
				if(i == m_ncache && m_ncache < NCACHE)
				{
					m_cache[i].info = m_sourceInfo.info;
					memcpy(m_cache[i].data, m_data, xs * ys);
					
					m_sourceInfo.info.data = m_cache[i].data;
					m_wrapper->grTexDownloadMipMap(GR_TMU0, m_lfb_space + i * MAX_DATA_SIZE, m_sourceInfo.evenOdd, &(m_sourceInfo.info));
					m_ncache += 1;
				}
				
				/* i < m_ncache */
				if(i < m_ncache)
					m_wrapper->grTexSource(GR_TMU0, m_lfb_space + i * MAX_DATA_SIZE, m_sourceInfo.evenOdd, &(m_sourceInfo.info));
			}
			else
			{
				if(m_download_unwritten)
				{
					m_wrapper->grTexDownloadMipMap(GR_TMU0, m_downloadInfo.startAddress,
						m_downloadInfo.evenOdd,
						&(m_downloadInfo.info));
					
					m_download_unwritten = false;
				}
				
				m_wrapper->grTexSource(GR_TMU0, m_sourceInfo.startAddress, m_sourceInfo.evenOdd, &(m_sourceInfo.info));
			}
		}
		else
		{
			m_wrapper->grTexSource(GR_TMU0, m_sourceInfo.startAddress, m_sourceInfo.evenOdd, &(m_sourceInfo.info));
		}
	}
}

void CTexCache::DataSize(GrTexInfo *info, int *xp, int *yp, int *bpp)
{
    int x, y, asp;

    x = y = (256 >> info->largeLod);

    asp = info->aspectRatio - 3;

    if(asp > 0)
        x >>= asp;

    if(asp < 0)
        y >>= (-asp);

    *xp = x;
    *yp = y;

    *bpp = (info->format >= GR_TEXFMT_16BIT ? 2 : 1);
}
