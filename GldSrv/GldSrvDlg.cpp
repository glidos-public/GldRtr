/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide server
 * 
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.56  2007/11/30 10:39:46  paul
 * Update access to ProductID for Windows 2000, and improve debug info from STACKWALK version
 *
 * Revision 1.55  2007/11/03 14:19:24  paul
 * Make derivation of MAC address less sensitive to language setting.
 *
 * Revision 1.54  2007/05/29 19:46:37  paul
 * Account for variation in Miniport interface
 *
 * Revision 1.53  2007/05/08 19:04:37  paul
 * New Vista-safe GlidosID determination
 *
 * Revision 1.52  2007/04/06 08:08:40  paul
 * Update finding of Windows Product ID to work under Vista
 *
 * Revision 1.51  2006/12/05 15:20:10  paul
 * Disable help info on F1
 *
 * Revision 1.50  2006/03/25 13:06:14  paul
 * Extend settings using property sheets, and add "new game" feature.
 *
 * Revision 1.49  2006/03/24 14:51:57  paul
 * Make it server's responsibility to ask for game record rather than being informed when the start button is pressed
 *
 * Revision 1.48  2006/03/24 14:20:11  paul
 * Move ownership of the display screen from the main dialog to the server
 *
 * Revision 1.47  2004/10/12 13:59:53  paul
 * Role the server into the main thread.  Improves pumping of messages and cures the momentary freezes with D2 on ATI cards
 *
 * Revision 1.46  2004/01/01 13:52:16  paul
 * More confusion tactics against hackers.
 *
 * Revision 1.45  2003/11/18 22:00:07  paul
 * Allocate main classes randomly in the heap, and hide flags in rotating
 * patterns, in an attempt to confuse hackers.
 *
 * Revision 1.44  2003/05/25 13:54:28  paul
 * Tidy resources, including moving strings into the string table
 *
 * Revision 1.43  2003/04/02 09:25:00  paul
 * Add 1600 x 1200 option
 *
 * Revision 1.42  2002/07/18 15:17:47  paul
 * Apply some window style changes copied from jdoom
 *
 * Revision 1.41  2002/05/11 12:43:29  paul
 * Update UI to allow for multiple games
 *
 * Revision 1.40  2002/04/21 19:08:44  paul
 * Yet again change the parts of the rdata section that are used
 * in the signing process.
 *
 * Revision 1.39  2002/04/20 14:03:39  paul
 * Remove another 8 bytes from the checksummed section.
 * Needed since using beginTimerInterval
 *
 * Revision 1.38  2002/04/19 19:29:41  paul
 * First go at synchronising the DOS box clock under NT
 *
 * Dropped 4 bytes out of checked region (use of timeGetTime makes this
 * necessary).
 *
 * Added some debugging
 *
 * Revision 1.37  2002/04/10 18:21:00  paul
 * User snmp rather than netbios to get MAC address
 *
 * Bump version to 1.11
 *
 * Revision 1.36  2002/01/22 18:17:54  paul
 * Added warning if the wrong UB CD is in the main drive.
 *
 * Revision 1.35  2002/01/02 15:54:05  paul
 * Hide openning of authority file
 *
 * Revision 1.34  2002/01/02 14:40:45  paul
 * Add the NIC address to the ID calculation
 *
 * Revision 1.33  2001/12/31 17:57:40  paul
 * Use text based authority file
 *
 * Revision 1.32  2001/12/29 17:00:22  paul
 * Comment out some debugging, commited by accident
 *
 * Revision 1.31  2001/12/29 16:46:54  paul
 * Hash only the first 0x5000 bytes of the .rdata section because bits change
 * after that in W98
 *
 * Revision 1.30  2001/12/28 20:42:34  paul
 * Get the new signing strategy working for the release version.
 * Had to ignore the first 0x540 bytes of the rdata section; not sure
 * why.
 *
 * Revision 1.29  2001/12/28 12:35:48  paul
 * Hide some of the program's strings to discourage fiddling
 *
 * Revision 1.28  2001/12/28 10:54:55  paul
 * Removed a return statements that was accidently left in and was
 * disabling the Glidos ID button
 *
 * Revision 1.27  2001/12/27 18:38:33  paul
 * GldSrv app now protected by permutation array.
 *
 * Revision 1.26  2001/12/27 16:26:46  paul
 * Generate command permutation array from both the file and the memory
 * interface - permutation still displayed and not used.
 *
 * Revision 1.25  2001/12/27 13:51:32  paul
 * Construct permutation array from the program memory image - permutation
 * displayed but not used at present.
 *
 * Revision 1.24  2001/12/24 11:35:47  paul
 * Added forgotten setup of buffer length before call to RegQueryValueEx
 *
 * Revision 1.23  2001/12/16 17:27:54  paul
 * Added computer specific Glidos ID generation.
 *
 * Revision 1.22  2001/12/15 17:03:13  paul
 * Remove support-request dialog
 *
 * Revision 1.21  2001/12/02 20:21:34  paul
 * Merge v1.7 changes into main trunk
 *
 * Revision 1.20  2001/11/10 10:05:52  paul
 * Make TR and UB start up sensitive to the OS
 *
 * Revision 1.19  2001/11/10 04:36:02  paul
 * Control fullscreen/windowed property of DOS box in W2K by sending
 * Alt CR to the DOS window.  As part of this, handle out od sequence
 * grWinOpen and grWinClose, and add confirmation calls to stop the
 * DOS program continuing before the Alt CR is sent.
 *
 * Revision 1.18.2.1  2001/12/02 13:40:25  paul
 * Allow two sizes of tombub.exe file.
 *
 * Revision 1.18  2001/09/09 10:34:24  paul
 * Maintain settings between invokations using an ini file.
 * Alter parameters to ChangeDisplaySettings in attempt to avoid
 * the taskbar appearing.
 *
 * Revision 1.17  2001/08/11 14:22:59  paul
 * Add a "links" button.
 * Fixed the scaling of the antialiasing lines.
 *
 * Revision 1.16  2001/08/10 13:49:18  paul
 * Allow TR&UB to play with either CD.
 * Add support-request button.
 *
 * Revision 1.15  2001/08/01 09:06:50  paul
 * Distinguish between empty drives and non-existence drives, so that
 * people with their CDRom up at Q and the like can use the TR and UB
 * buttons.
 * Flip to version 1.4
 *
 * Revision 1.14  2001/07/28 13:49:31  paul
 * Invoke TR and UB via pif files.
 * Flipped to v1.3
 *
 * Revision 1.13  2001/07/28 11:57:03  paul
 * Hide mouse pointer in full screen mode.
 *
 * Revision 1.12  2001/07/26 20:43:09  paul
 * Full screen mode controllable from main window, with mode changes and all.
 *
 * Revision 1.11  2001/07/26 19:51:15  paul
 * Make new window for each call to grSstWinOpen.
 * Also provide two types of window for fullscreen and windowed.
 *
 * Revision 1.10  2001/07/18 19:37:07  paul
 * Add buttons for starting Tomb Raider and Unfinished Business, with tests
 * for not being installed or the CD not being present.
 * Also copy Glide2x.ovl to the C:\Tombraid folder.
 *
 * Revision 1.9  2001/07/15 19:31:32  paul
 * Hide DOS window and keep game active.
 *
 * Revision 1.8  2001/07/03 19:00:42  paul
 * Added 1280 x 1024 mode
 * Added client version check
 *
 * Revision 1.7  2001/07/03 15:23:55  paul
 * Added radio buttons for resolution control.
 * Passed control of GlideInit/Shutdown to server rather than client
 * Improved GUI's response to <CR> and <ESC>
 *
 * Revision 1.6  2001/06/30 10:19:40  paul
 * Provided for setting the screen resolution independently of the clients
 * request.
 *
 * Stopped all debug message generation, unless tracing.
 *
 * Revision 1.5  2001/06/28 10:55:46  paul
 * Provided method of capturing debug in memory and saving it to a file.
 * Disabled drawline because it interacts badly with chroma keying at
 * the moment.
 *
 * Revision 1.4  2001/06/25 18:23:35  paul
 * Added a few more methods
 * Changed DisplayMessage to DebugMessage for implemented methods
 * Added trace-toggle and pipe-status buttons
 *
 * Revision 1.3  2001/06/22 09:21:00  paul
 * Added game display window.
 *
 * Revision 1.2  2001/06/20 16:17:01  paul
 * Send "abort" command to VXD on shutdown to stop the server thread
 * hanging in a get call.
 *
 * Add buttons for shutdown, and saving the edit box.
 *
 * Us commands in protocol, only GrGlideInit at this stage.
 *
 ************************************************************/

// GldSrvDlg.cpp : implementation file
//

#include "stdafx.h"

#include <iphlpapi.h>

#include "openssl/md5.h"
#include "openssl/rsa.h"
#include "str_code.h"
#include "GldSrv.h"
#include "GldSrvDlg.h"
#include "GlideServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

#define SEED (0xfd)
#define MARKER_SIZE (12)

#define ROTR(x) (0xff & (((x) << 7) | ((x) >> 1)))


class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGldSrvDlg dialog

CGldSrvDlg::CGldSrvDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGldSrvDlg::IDD, pParent)
{
    PIMAGE_NT_HEADERS nt_header;
//    FILE *f;
	//{{AFX_DATA_INIT(CGldSrvDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    module_base = (unsigned char *) GetModuleHandle(NULL);
    nt_header = (PIMAGE_NT_HEADERS) (module_base + ((PIMAGE_DOS_HEADER) module_base)->e_lfanew);
    module_section_header = (PIMAGE_SECTION_HEADER) (nt_header + 1);
    server = NULL;
    m_kicked = false;
    m_server_running = false;
/*
    f = fopen("m_text.txt", "w");

    fwrite(module_base + module_section_header[0].VirtualAddress, 1,
           module_section_header[0].Misc.VirtualSize, f);

    fclose(f);

    f = fopen("m_rdata.txt", "w");

    fwrite(module_base + module_section_header[1].VirtualAddress, 1,
           module_section_header[1].Misc.VirtualSize, f);

    fclose(f);

    f = fopen("m_data.txt", "w");

    fwrite(module_base + module_section_header[2].VirtualAddress, 1,
           module_section_header[2].Misc.VirtualSize, f);

    fclose(f);
*/
}

void CGldSrvDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGldSrvDlg)
	DDX_Control(pDX, IDC_COMBO1, m_combo);
	DDX_Control(pDX, IDC_EDIT1, m_edit);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CGldSrvDlg, CDialog)
	//{{AFX_MSG_MAP(CGldSrvDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_Save, OnSave)
	ON_BN_CLICKED(IDC_TRACE, OnTrace)
	ON_BN_CLICKED(IDC_PIPE_STATE, OnPipeState)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_LINKS, OnLinks)
	ON_BN_CLICKED(IDC_ADJUST, OnAdjust)
	ON_BN_CLICKED(IDC_NEW, OnNewGame)
	ON_BN_CLICKED(IDC_START, OnStartGame)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnGameSelect)
	ON_WM_HELPINFO()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_KICKIDLE, OnKickIdle)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGldSrvDlg message handlers

BOOL CGldSrvDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

#ifdef STACKWALK
    try
    {
#endif
    server = new CGlideServer();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

    UpdateData(FALSE);
	
    m_game.Initialise(&m_combo);
    m_game.Load();
#ifdef STACKWALK
    }
    catch(CString s)
    {
        AfxMessageBox((LPCSTR)s, MB_OK);
    }
#endif

	return TRUE;  // return TRUE  unless you set the focus to a control
}

LRESULT CGldSrvDlg::OnKickIdle(WPARAM wparam, LPARAM lparam)
{
    if(!m_kicked)
    {
        m_kicked = true;
        m_server_running = true;
        server->Start(&m_game, this);
        m_server_running = false;
        PostMessage(WM_CLOSE);
    }

    return FALSE;
}

void CGldSrvDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CGldSrvDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CGldSrvDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CGldSrvDlg::OnSave() 
{
	CFileDialog save_box(false);
	save_box.m_ofn.lpstrInitialDir = "C:\\My Documents";
	save_box.m_ofn.lpstrFilter = "Test files\0*.txt\0All files\0*.*\0";
    strcpy(save_box.m_ofn.lpstrFile, "Glidecmd.txt");

    if(save_box.DoModal() == IDOK)
    {
        server->SaveTrace((LPCSTR)save_box.GetPathName());
    }
}

void CGldSrvDlg::OnTrace() 
{
	server->StartTrace();
}

void CGldSrvDlg::OnPipeState() 
{
	server->ShowPipeState();
}

void CGldSrvDlg::OnCancel() 
{
}

void CGldSrvDlg::OnOK() 
{
}

void CGldSrvDlg::OnClose() 
{
    if(m_server_running)
    {
        /*
         * Tell the server to shutdown, so that we
         * will return from OnKickIdle, which will
         * post another WM_CLOSE message
         */
        server->Shutdown();
    }
    else
    {
        /*
         * Reacting to OnClickIdle's WM_CLOSE message
         */
        delete server;
        server = NULL;

	    CDialog::OnCancel();
    }
}


void CGldSrvDlg::DisplayServerMessage(CString &mess)
{
    CString tmp;

    m_edit.GetWindowText(tmp);
    m_edit.SetWindowText(tmp + mess + "\r\n");
}

void CGldSrvDlg::OnLinks() 
{
    ::ShellExecute(0, 0, "https://www.glidos.net", 0, 0, SW_SHOW);
}

void CGldSrvDlg::OnAdjust() 
{
    m_game.Adjust();
}

void CGldSrvDlg::OnNewGame() 
{
    m_game.New();
}

void CGldSrvDlg::OnStartGame() 
{
    m_game.StartGame(server->GetOSType());
}

void CGldSrvDlg::OnGameSelect() 
{
    m_game.OnGameSelect();
}

BOOL CGldSrvDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
    return FALSE;
}
