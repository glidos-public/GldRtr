// GlideDll.cpp: implementation of the CGlideDll class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "gldsrv.h"
#include "GlideDll.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGlideDll::CGlideDll()
{
    m_lib = NULL;

#define GLIDE_ENTRY_POINT(ret, call, name, param, arg, pfix) \
    m_p##name = NULL;

#include "entry.h"

#undef GLIDE_ENTRY_POINT
}

CGlideDll::~CGlideDll()
{
    FreeLibrary(m_lib);
}

void CGlideDll::Load(LPCSTR path)
{
    FreeLibrary(m_lib);
    m_lib = LoadLibrary(path);
    if(m_lib == NULL)
        throw CString("Could not load ") + path;

#define GLIDE_ENTRY_POINT(ret, call, name, param, arg, pfix) \
    GetEntry((FARPROC *)&m_p##name, "_" #name pfix);

#include "entry.h"

#undef GLIDE_ENTRY_POINT
}

void CGlideDll::Unload()
{
    FreeLibrary(m_lib);
    m_lib = NULL;

#define GLIDE_ENTRY_POINT(ret, call, name, param, arg, pfix) \
    m_p##name = NULL;

#include "entry.h"

#undef GLIDE_ENTRY_POINT
}

#define GLIDE_ENTRY_POINT(ret, call, name, param, arg, pfix) \
ret CGlideDll::name param                                    \
{                                                            \
    if(m_p##name == NULL)                                    \
        throw CString("Undefined entry point " #name);       \
    call m_p##name arg;                                     \
}

#include "entry.h"

#undef GLIDE_ENTRY_POINT

void CGlideDll::GetEntry(FARPROC *pProc, LPCSTR name)
{
    *pProc = GetProcAddress(m_lib, name);
}
