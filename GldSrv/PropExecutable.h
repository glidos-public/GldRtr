#if !defined(AFX_PROPEXECUTABLE_H__729C3A68_834B_4D3F_B0B2_1D02AEF3551A__INCLUDED_)
#define AFX_PROPEXECUTABLE_H__729C3A68_834B_4D3F_B0B2_1D02AEF3551A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropExecutable.h : header file
//

#include "GameRecord.h"

/////////////////////////////////////////////////////////////////////////////
// CPropExecutable dialog

class CPropExecutable : public CPropertyPage
{
	DECLARE_DYNCREATE(CPropExecutable)

private:
    CGameRecord *m_rec;

// Construction
public:
	void Init(CGameRecord *rec);
	CPropExecutable();
	~CPropExecutable();

// Dialog Data
	//{{AFX_DATA(CPropExecutable)
	enum { IDD = IDD_PROP_EXEC };
	CString	m_exe_path;
	CString	m_sizes;
    int m_subfolders;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropExecutable)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	virtual BOOL OnWizardFinish();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropExecutable)
	afx_msg void OnButtonBrowseCd();
	afx_msg void OnButtonBrowseExec();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
    afx_msg void OnDeltaposSpin(NMHDR *pNMHDR, LRESULT *pResult);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPEXECUTABLE_H__729C3A68_834B_4D3F_B0B2_1D02AEF3551A__INCLUDED_)
