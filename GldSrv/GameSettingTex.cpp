// GameSettingTex.cpp: implementation of the CGameSettingTex class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "GameSettingTex.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CGameSettingTex::Init(CStoredStruct *st, const char *name)
{
	CGameSetting<CTexHandling>::Init(st, name);
	
	m_value = TEX_HANDLING_NONE;
};

CGameSettingTex &CGameSettingTex::operator=(const TexHandling &i)
{
	if(i != m_value)
		m_defined = true;
	
	m_value   = i;
	
	return *this;
};

bool CGameSettingTex::Parse(CString &val)
{
	if(val[0] == 'Y' || val[0] == 'y')
	{
		m_value = TEX_HANDLING_OVERRIDE;
		
		return true;
	}
	
	if(val[0] == 'N' || val[0] == 'n')
	{
		m_value = TEX_HANDLING_NONE;
		
		return true;
	}
	
	if(val[0] == 'C' || val[0] == 'c')
	{
		m_value = TEX_HANDLING_CAPTURE;
		
		return true;
	}
	
	return false;
};

CString CGameSettingTex::Print()
{
	switch(m_value)
	{
	case TEX_HANDLING_OVERRIDE:
		return "Yes";
		
	case TEX_HANDLING_CAPTURE:
		return "Capture";
		
	default:
		return "No";
	}
};
