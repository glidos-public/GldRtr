// PropertyPageList.cpp : implementation file
//

#include "stdafx.h"

#include <set>

#include "MappingFileCheck.h"
#include "genequivmap.h"
#include "PropertyPageList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageList property page

IMPLEMENT_DYNCREATE(CPropertyPageList, CPropertyPageBase)

CPropertyPageList::CPropertyPageList() : CPropertyPageBase(CPropertyPageList::IDD)
{
	//{{AFX_DATA_INIT(CPropertyPageList)
	m_folder = _T("");
	//}}AFX_DATA_INIT
}

CPropertyPageList::~CPropertyPageList()
{
}

void CPropertyPageList::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropertyPageList)
	DDX_Text(pDX, IDC_EDIT_FOLDER, m_folder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropertyPageList, CPropertyPageBase)
	//{{AFX_MSG_MAP(CPropertyPageList)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_FOLDER, OnButtonBrowseFolder)
	ON_BN_CLICKED(IDC_BUTTON_LIST, OnButtonList)
	//}}AFX_MSG_MAP
    ON_UPDATE_COMMAND_UI(IDC_BUTTON_LIST, OnUpdateListButton)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageList message handlers

void CPropertyPageList::OnButtonBrowseFolder() 
{
    if(GetFolder(m_folder))
        UpdateData(FALSE);	
}

typedef std::set<std::string> TexSet;

void CPropertyPageList::OnButtonList() 
{
    CMappingFile        map;
    CStringArray       *names;
    TexSet              texset;
    CString             ofile;
    FILE               *f;
    int                 i;

    ofile = GetOutputFileName("texture.log");
    if(ofile.IsEmpty())
        return;

    f = fopen((LPCSTR)ofile, "w");
    if(f == NULL)
    {
        AfxMessageBox("Could not open file for output", MB_OK);
        return;
    }

    fprintf(f, "Textures defined in %s\n\n", (LPCSTR)m_folder);

    map.Initialise(m_folder);
    names = map.GetNameSet();

    for(i = 0; i < names->GetSize(); i++)
    {
        TexElementInfoList            *list;
        TexElementInfoList::iterator   liter;

        list = map.GetElementInfoList((*names)[i]);
        for(liter = list->begin(); liter != list->end(); liter++)
        {
            CString s;
            CBox    box = liter->box;

            s.Format("%s(%d--%d)(%d--%d) \"%s\"", (*names)[i], box.m_xmin, box.m_xmax, box.m_ymin, box.m_ymax, (LPCSTR)liter->path.Mid(m_folder.GetLength()+1));

            texset.insert((LPCSTR)s);
        }
    }

    delete names;

    TexSet::iterator   titer;

    for(titer = texset.begin(); titer != texset.end(); titer++)
    {
        fprintf(f, "%s\n", titer->c_str());
    }

    fprintf(f, "\nEnd\n");

    fclose(f);

    AfxMessageBox("List file written");
	
}

void CPropertyPageList::OnUpdateListButton(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(!m_folder.IsEmpty());
}
