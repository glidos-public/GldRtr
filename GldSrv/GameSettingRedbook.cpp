// GameSettingRedbook.cpp: implementation of the CGameSettingRedbook class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "GameSettingRedbook.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CGameSettingRedbook::Init(CStoredStruct *st, const char *name)
{
	CGameSetting<CRedbookHandling>::Init(st, name);
	
	m_value = REDBOOK_HANDLING_NONE;
};

CGameSettingRedbook &CGameSettingRedbook::operator=(const RedbookHandling &i)
{
	if(i != m_value)
		m_defined = true;
	
	m_value   = i;
	
	return *this;
};

bool CGameSettingRedbook::Parse(CString &val)
{
	if(val[0] == 'C' || val[0] == 'c')
	{
		m_value = REDBOOK_HANDLING_CD;
		
		return true;
	}
	
	if(val[0] == 'N' || val[0] == 'n')
	{
		m_value = REDBOOK_HANDLING_NONE;
		
		return true;
	}
	
	if(val[0] == 'M' || val[0] == 'm')
	{
		m_value = REDBOOK_HANDLING_MP3;
		
		return true;
	}
	
	return false;
};

CString CGameSettingRedbook::Print()
{
	switch(m_value)
	{
	case REDBOOK_HANDLING_CD:
		return "CD";
		
	case REDBOOK_HANDLING_MP3:
		return "MP3";
		
	default:
		return "None";
	}
};
