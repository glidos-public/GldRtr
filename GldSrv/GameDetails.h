// GameDetails.h: interface for the CGameDetails class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAMEDETAILS_H__47EC6CA4_EE90_4D5E_AFC1_0C55FB768551__INCLUDED_)
#define AFX_GAMEDETAILS_H__47EC6CA4_EE90_4D5E_AFC1_0C55FB768551__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ostype.h"
#include "Resolution.h"
#include "GameRecord.h"

#define MAX_GAME_RECORDS (20)

class CGameDetails  
{
private:
	CTypedPtrArray<CPtrArray, CGameRecord *> m_rec;
    int                                      m_current;
	CComboBox                               *m_combo;

	CString CheckCdPresent(CString &file);
	void UpdateCombo();
	bool ReadSelected(FILE *f, CString &selected);
	void RemoveRecords();

public:
	void New();
	void GetGameRecord(CGameRecord &rec);
	void StartGame(OSType os);
	bool GetFullScreen();
    bool GetNoModeChange();
	CResolution &GetResolution();
	void Adjust();
	void OnGameSelect();
	void Initialise(CComboBox *combo);
	void Save();
	void Load();
	CGameDetails();
	virtual ~CGameDetails();
};

#endif // !defined(AFX_GAMEDETAILS_H__47EC6CA4_EE90_4D5E_AFC1_0C55FB768551__INCLUDED_)
