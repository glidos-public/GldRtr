#ifndef WRAPPER_FLAGS_H
#define WRAPPER_FLAGS_H

#define WRAPPER_FLAG_WINDOWED               (0x1)
#define WRAPPER_FLAG_MIPMAPS                (0x2)
#define WRAPPER_FLAG_PALETTED_TEXTURE_CACHE (0x4)
#define WRAPPER_FLAG_MANAGED_TEXTURES       (0x8)
#define WRAPPER_FLAG_16BIT_COLOR            (0x10)
#define WRAPPER_FLAG_NO_MODE_CHANGE         (0x20)

struct EyeSeparation
{
    float limit;
    float offset;
    float factor;
};

typedef enum
{
    StereoType_Interleaved,
    StereoType_Anaglyph,
    StereoType_AnaglyphGrey,
    StereoType_Checkerboard,
    StereoType_CheckerboardReversed
} StereoType;

struct StereoMap
{
    int           size;
    StereoType    type;
    int           nsep;
    EyeSeparation sep[3];
};

struct OldStereoMap
{
    int nsep;
    EyeSeparation sep[3];
};

#endif /* WRAPPER_FLAGS_H */
