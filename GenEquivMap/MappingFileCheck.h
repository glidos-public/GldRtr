// MappingFileCheck.h: interface for the CMappingFileCheck class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAPPINGFILECHECK_H__2C431614_2E4D_477E_81BE_59EF3D25F7C9__INCLUDED_)
#define AFX_MAPPINGFILECHECK_H__2C431614_2E4D_477E_81BE_59EF3D25F7C9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\GLDSRV\MappingFile.h"

class CMappingFileCheck : public CMappingFile  
{
public:
	CMappingFileCheck();
	virtual ~CMappingFileCheck();

	CString FollowLink(CString &path, CString &root);
};

#endif // !defined(AFX_MAPPINGFILECHECK_H__2C431614_2E4D_477E_81BE_59EF3D25F7C9__INCLUDED_)
