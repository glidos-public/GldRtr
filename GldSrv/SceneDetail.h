// SceneDetail.h: interface for the CSceneDetail class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCENEDETAIL_H__B79890EC_24AD_4C0B_9CB0_88218FBC20FD__INCLUDED_)
#define AFX_SCENEDETAIL_H__B79890EC_24AD_4C0B_9CB0_88218FBC20FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PolyCondition.h"
#include "SceneLocation.h"

class CSceneDetail  
{
private:
    CSceneLocation                             *m_location;
    int                                         m_index;
    CTypedPtrArray<CPtrArray, CPolyCondition *> m_conditions;

public:
	CSceneDetail(CSceneLocation *location, int index);
	virtual ~CSceneDetail();

    void AddCondition(const CString &expression);
    void Trigger(const CPolyCondition::CPoly &poly);
};

#endif // !defined(AFX_SCENEDETAIL_H__B79890EC_24AD_4C0B_9CB0_88218FBC20FD__INCLUDED_)
