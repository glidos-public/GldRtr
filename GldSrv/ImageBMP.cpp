// ImageBMP.cpp: implementation of the CImageBMP class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ImageBMP.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImageBMP::CImageBMP()
{
    m_f   = NULL;
    m_buf = NULL;
}

CImageBMP::~CImageBMP()
{
    if(m_f != NULL)
        fclose(m_f);

    delete [] m_buf;
}

bool CImageBMP::Load(FILE *f)
{
    int width;

    m_f = f;

    fseek(m_f, 0, SEEK_SET);

    fread(&m_hdr, 1, sizeof(m_hdr), m_f);

    if(((char *)&m_hdr.bfType)[0] != 'B' || ((char *)&m_hdr.bfType)[1] != 'M')
        return false;

    fread(&m_info, 1, sizeof(m_info), m_f);

    if(m_info.biSize != sizeof(m_info))
        return false;

    if(m_info.biBitCount != 24 && m_info.biBitCount != 8)
        return false;

    if(m_info.biBitCount == 8)
        fread(m_palette, 1, sizeof(m_palette), m_f);

    width = m_info.biWidth;

    m_y   = -2; /* Nothing cached */

    if(m_info.biBitCount == 24)
    {
        m_yskip = 3 * width + (width & 3);
    }
    else
    {
        m_yskip = width + ((3 * width) & 3);
    }

    m_buf   = new unsigned char[m_yskip];

    if(m_buf == NULL)
        return false;

    return true;
}

int CImageBMP::GetWidth()
{
    return m_info.biWidth;
}

int CImageBMP::GetHeight()
{
    return m_info.biHeight;
}

COLORREF CImageBMP::GetPixel(int x, int y)
{
    unsigned char r, g, b;

    if(y != m_y)
    {
        if(y != m_y +1)
        {
            fseek(m_f, m_hdr.bfOffBits + m_yskip * y, SEEK_SET);
        }

        fread(m_buf, 1, m_yskip, m_f);

        m_y = y;
    }

    if(m_info.biBitCount == 24)
    {
        r = m_buf[3*x];
        g = m_buf[3*x + 1];
        b = m_buf[3*x + 2];
        
        return (b << 16) | (g << 8) | r;
    }
    else
    {
        return m_palette[m_buf[x]];
    }
}


CImage *CImageBMP::TryLoad(FILE *f, unsigned char *buf, int size)
{
    CImageBMP *img = new CImageBMP;

    if(img == NULL)
        return NULL;

    if(buf[0] == 'B' && buf[1] == 'M' && img->Load(f))
        return img;

    delete img;

    return NULL;
}

