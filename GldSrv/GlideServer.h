/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide server
 * 
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.80  2007/10/02 18:36:47  paul
 * MP3 based support for Kevin Bracey's patched Tomb.exe
 *
 * Revision 1.79  2007/05/30 16:00:31  paul
 * Smooth TR FMVs
 *
 * Revision 1.78  2006/03/28 15:30:26  paul
 * Control OpenGLide and psVoodoo config from Glidos
 *
 * Revision 1.77  2006/03/27 19:09:46  paul
 * Explicitly load windows-side wrapper
 *
 * Revision 1.76  2006/03/24 14:51:57  paul
 * Make it server's responsibility to ask for game record rather than being informed when the start button is pressed
 *
 * Revision 1.75  2006/03/24 14:20:11  paul
 * Move ownership of the display screen from the main dialog to the server
 *
 * Revision 1.74  2004/10/12 13:59:53  paul
 * Role the server into the main thread.  Improves pumping of messages and cures the momentary freezes with D2 on ATI cards
 *
 * Revision 1.73  2004/10/10 09:08:37  paul
 * Tidy up formatting
 *
 * Revision 1.72  2004/08/06 14:06:03  paul
 * Multipart scene recognition
 *
 * Revision 1.71  2004/07/29 07:30:27  paul
 * First version of audio tracks triggered by screen content.  Just
 * presence of a single texture in this version.
 *
 * Revision 1.70  2004/01/10 12:00:35  paul
 * Add small-texture cache to speed up D2
 *
 * Revision 1.69  2004/01/01 13:52:16  paul
 * More confusion tactics against hackers.
 *
 * Revision 1.68  2003/12/19 16:44:32  paul
 * Pass glide state to texture handler so that it can do the restoring, thus
 * cleaning things up a bit.
 *
 * Fix bug in initialisation of glide state.
 *
 * Prevent chromakeying for overridden textures
 *
 * Make masked textures work as did chromakeying
 *
 * Delay transparent textures
 *
 * Revision 1.67  2003/11/18 22:00:07  paul
 * Allocate main classes randomly in the heap, and hide flags in rotating
 * patterns, in an attempt to confuse hackers.
 *
 * Revision 1.66  2003/05/18 16:17:12  paul
 * Merge texture-output-branch to trunk
 *
 * Revision 1.65.2.2  2003/05/16 16:13:06  paul
 * Add Texture replacement
 *
 * Revision 1.65.2.1  2003/05/14 10:50:32  paul
 * First version of texture capturing
 *
 * Revision 1.65  2003/05/13 14:13:18  paul
 * Move glide-state related info into a separate class
 *
 * Revision 1.64  2003/05/13 10:43:46  paul
 * Move details of pipe read caching into a separate class
 *
 * Revision 1.63  2002/12/03 20:43:22  paul
 * Patch small triangles together to give
 * bilinear interpolation in Blood
 *
 * Revision 1.62  2002/11/11 21:50:28  paul
 * Add grTexCombine
 *
 * Revision 1.61  2002/11/03 18:17:26  paul
 * Move the TR shadow hack out of OpenGLide into Glidos.
 *
 * Revision 1.60  2002/10/20 19:02:16  paul
 * Use depth buffering for the logo only if
 * the game uses w buffering
 *
 * Revision 1.59  2002/10/19 09:17:18  paul
 * Reenable GrClipWindow because it is now supported in OpenGLide.
 * Map through GuDrawTriangleWithClip.
 * Implement GuFogGenerateExp by requesting the
 * table from the server.
 *
 * Revision 1.58  2002/10/09 09:09:39  paul
 * Increase maximum number of gu textures.
 *
 * Allocate a gu texture on reset to make room for LFB tricks
 *
 * Revision 1.57  2002/08/28 18:49:50  paul
 * Introduce Glidos command for VESA frames with half-height mode.
 * Update game starter to use VESA support
 * Introduce VESA start/stop commands to aid server in detecting the
 * dos window.
 *
 * Revision 1.56  2002/08/03 13:34:36  paul
 * Remove nolonger used ChipTime class
 *
 * Revision 1.55  2002/08/03 13:26:56  paul
 * Mouse support for Descent II
 *
 * Revision 1.54  2002/07/25 18:16:59  paul
 * Provide user control of Gamma and forced texture smoothing
 *
 * Revision 1.53  2002/07/17 10:19:56  paul
 * When rendering LFB changes, use clamping to
 * avoid visible lines between the segments
 *
 * Revision 1.52  2002/07/07 09:39:30  paul
 * Handle grHints and ignore grTexDetailControl (for Carmageddon)
 *
 * Send buffer ref with LFB write requests.
 *
 * Revision 1.51  2002/07/03 20:14:49  paul
 * Put depth buffer right for LFB writes
 *
 * Revision 1.50  2002/07/03 16:30:28  paul
 * Make judder fix timer frequency controllable
 *
 * Bump to v1.14a
 *
 * Revision 1.49  2002/06/24 15:30:26  paul
 * Remove the reply thread which is no longer needed now
 * we have a polling interface on the DOS side.
 *
 * Revision 1.48  2002/06/03 18:39:31  paul
 * Store and reinstate TexSource after the logo and LFB textures
 *
 * Increment version
 *
 * Revision 1.47  2002/06/02 15:19:33  paul
 * Hold off checking the next 8 textures after a grLfbLock because
 * at least six are likely to be generated from it in TR1
 *
 * Revision 1.46  2002/06/02 12:13:48  paul
 * Send back frame to client on grLfbLock in read-only mode
 *
 * Revision 1.45  2002/06/01 09:24:26  paul
 * Map the guTex calls directly through rather than turning them
 * into grTex calls, now that I've fixed OpenGlide.  Makes little
 * difference to performance but tests the new OpenGLide code
 * and it is simpler.
 *
 * Revision 1.44  2002/05/27 18:13:25  paul
 * Disable fog mode for logo and LFB writes
 *
 * Revision 1.43  2002/05/21 16:15:31  paul
 * Map through a few more Glide calls, towards getting Red Guard going.
 *
 * Revision 1.42  2002/05/13 18:25:13  paul
 * Send ConvertAndDownloadRLE textures separately so as to
 * be able to avoid fingerprinting them
 *
 * Use bsearch to look up fingerprints.
 *
 * Revision 1.41  2002/05/12 11:58:16  paul
 * Make logo work for Descent II
 *
 * Revision 1.40  2002/05/11 16:12:21  paul
 * Add GrCullMode and GrFogColorValue for Descent II
 *
 * Revision 1.39  2002/05/11 15:53:29  paul
 * Make control-judder fix configurable.  Need for Descent II,
 * but makes TR run far too fast
 *
 * Revision 1.38  2002/05/11 12:43:29  paul
 * Update UI to allow for multiple games
 *
 * Revision 1.37  2002/05/08 17:28:55  paul
 * Do confirmation sending in a differnt thread so that a constant
 * stream of data can be sent to keep the Dosbox active and
 * prevent int 08s being lost
 *
 * Revision 1.36  2002/05/01 19:28:25  paul
 * Tailor texture size for small LFB updates
 *
 * Revision 1.35  2002/05/01 18:27:07  paul
 * Send partial LFB updates when smaller than 256x256
 *
 * Revision 1.34  2002/04/30 17:50:05  paul
 * Display LFB writes by turning them into textures, using
 * alpha to leave the unwritten areas unchanged.
 *
 * Revision 1.33  2002/04/25 18:18:08  paul
 * Add some debugging messages
 *
 * Revision 1.32  2002/04/21 19:05:41  paul
 * Pass a few more glide commands for Descent2.
 * Implement ConvertAndDownloadRLE by
 * decompressing locally
 *
 * Revision 1.31  2002/04/19 19:29:41  paul
 * First go at synchronising the DOS box clock under NT
 *
 * Dropped 4 bytes out of checked region (use of timeGetTime makes this
 * necessary).
 *
 * Added some debugging
 *
 * Revision 1.30  2002/04/16 10:42:34  paul
 * Pass through a few more Glide commands and temporarily turn
 * off alpha bending as first steps towards getting Descent II working
 *
 * Revision 1.29  2002/02/19 18:04:20  paul
 * CD Music now working in W2k
 *
 * Revision 1.28  2002/02/18 15:12:58  paul
 * Take on the responsiblility for CD Audio, so that it works in W2k and XP.
 * Nearly working.
 *
 * Revision 1.27  2001/12/31 17:57:40  paul
 * Use text based authority file
 *
 * Revision 1.26  2001/12/27 18:38:33  paul
 * GldSrv app now protected by permutation array.
 *
 * Revision 1.25  2001/12/27 16:26:46  paul
 * Generate command permutation array from both the file and the memory
 * interface - permutation still displayed and not used.
 *
 * Revision 1.24  2001/12/27 13:51:32  paul
 * Construct permutation array from the program memory image - permutation
 * displayed but not used at present.
 *
 * Revision 1.23  2001/11/30 17:23:37  paul
 * Base logo verticies on integers to match Tomb Raider,
 * so that they don't stand out.
 *
 * Revision 1.22  2001/11/29 18:48:24  paul
 * Complete logo rendering when random process leaves
 * polygons yet to be drawn.
 *
 * Revision 1.21  2001/11/29 16:59:16  paul
 * Interleave logo with resst of scene
 *
 * Revision 1.20  2001/11/29 13:35:41  paul
 * Remove the old, unused texture handling code.
 *
 * Revision 1.19  2001/11/26 12:54:22  paul
 * Merge gen-logo branch back into the trunk.
 * Branching turned out to be unnecessary because
 * nothing has happened on the trunk.
 *
 * Revision 1.18.2.1  2001/11/24 10:23:45  paul
 * Towards displaying a logo, make a square appear in the scene.
 *
 * Revision 1.18  2001/11/10 10:05:52  paul
 * Make TR and UB start up sensitive to the OS
 *
 * Revision 1.17  2001/11/10 04:36:02  paul
 * Control fullscreen/windowed property of DOS box in W2K by sending
 * Alt CR to the DOS window.  As part of this, handle out od sequence
 * grWinOpen and grWinClose, and add confirmation calls to stop the
 * DOS program continuing before the Alt CR is sent.
 *
 * Revision 1.16  2001/11/03 19:27:22  paul
 * Virtualise pipe access ready for W2k support
 *
 * Revision 1.15  2001/07/26 19:51:15  paul
 * Make new window for each call to grSstWinOpen.
 * Also provide two types of window for fullscreen and windowed.
 *
 * Revision 1.14  2001/07/25 13:29:25  paul
 * Flipped protocol version to 3.
 * Added GrAADrawLine.
 * Made the server rather than the glide wrapper responsible for sizing
 * the window.
 *
 * Revision 1.13  2001/07/21 20:25:22  paul
 * Flip protocol version to 2.
 * Use HWND rather than CWnd for communication between threads.
 *
 * Revision 1.12  2001/07/16 14:13:07  paul
 * Speed up DOS to Windows data flow using large buffers in all components
 *
 * Revision 1.11  2001/07/15 19:31:32  paul
 * Hide DOS window and keep game active.
 *
 * Revision 1.10  2001/07/13 08:21:34  paul
 * Flipped protocol to version 1.
 *
 * Added GrConstantColorValue4 and GrGlideShutdown
 *
 * Removed race condition in GrSstWinOpen.
 *
 * Implement GrDrawLine with a rectangle.
 *
 * Made window go away at game end, by closing on GrGlideShutdown,
 * and flushing pipe.
 *
 * Revision 1.9  2001/07/03 15:23:55  paul
 * Added radio buttons for resolution control.
 * Passed control of GlideInit/Shutdown to server rather than client
 * Improved GUI's response to <CR> and <ESC>
 *
 * Revision 1.8  2001/06/30 10:19:40  paul
 * Provided for setting the screen resolution independently of the clients
 * request.
 *
 * Stopped all debug message generation, unless tracing.
 *
 * Revision 1.7  2001/06/28 10:55:46  paul
 * Provided method of capturing debug in memory and saving it to a file.
 * Disabled drawline because it interacts badly with chroma keying at
 * the moment.
 *
 * Revision 1.6  2001/06/25 18:23:35  paul
 * Added a few more methods
 * Changed DisplayMessage to DebugMessage for implemented methods
 * Added trace-toggle and pipe-status buttons
 *
 * Revision 1.5  2001/06/23 16:07:22  paul
 * Implement gu texture methods using gr methods, to cope with glide wrappers
 * not providing the gu interface.
 *
 * Stop all messages to the debug window; now runs full speed.
 *
 * Revision 1.4  2001/06/22 15:35:22  paul
 * More debug messages.
 * Updated the reported hardware config to match OpenGlide
 * Refuse Gu3dfLoad to force TR I to generate mimmaps.
 *
 * Revision 1.3  2001/06/22 09:21:55  paul
 * First implementation of entry points used by Tomb Raider I
 *
 * Revision 1.2  2001/06/20 16:17:01  paul
 * Send "abort" command to VXD on shutdown to stop the server thread
 * hanging in a get call.
 *
 * Add buttons for shutdown, and saving the edit box.
 *
 * Us commands in protocol, only GrGlideInit at this stage.
 *
 ************************************************************/

// GlideServer.h: interface for the CGlideServer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GLIDESERVER_H__706B7700_61BA_11D5_B9B5_00C0DFF0F563__INCLUDED_)
#define AFX_GLIDESERVER_H__706B7700_61BA_11D5_B9B5_00C0DFF0F563__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GameRecord.h"
#include "Thread.h"
#include "sdk2_glide.h"
#include "sdk2_sst1vid.h"
#include "CDAudio.h"
#include "MP3Audio.h"
#include "Mouse.h"
#include "BloodFix.h"
#include "TexHandler.h"
#include "TexHandlerAudio.h"
#include "GlideState.h"
#include "CachedPipe.h"
#include "RenderStore.h"
#include "TexCache.h"
#include "ScreenDlg.h"
#include "GameDetails.h"
#include "GlideDll.h"
#include "ostype.h"

#define MAX_MM (1024)
#define PIPE_CACHE_SIZE (1024*1024)

class CGldSrvDlg;

class CGlideServer
{
private:
	bool                 m_vesa_active;
	int                  m_tex_check_holdoff;
	int                  m_server_height;
	int                  m_server_width;
	int                  m_game_height;
	int                  m_game_width;
    CGameRecord          m_game_record;
	OSType               m_OSType;
    HWND                 m_dos_window;
	float                m_yfactor;
	float                m_xfactor;
	GrScreenResolution_t m_server_resolution;
	CString              client_dir;
	bool                 m_tracing;
	bool                 m_trace_on_winopen;
    CGlideDll            m_wrapper;
    CGameDetails        *m_game;
    CGlideState          m_glide_state;
    CRenderStore         m_delay_store;
    CTexCache           *m_tex_cache;
    GrMipMapId_t         mipmap[MAX_MM];
    CCachedPipe          m_pipe;
    char                 app_file[MAX_PATH];
    char                *big_buf;
    char                *big_buf_ptr;
    CGldSrvDlg          *m_dlg;
    CString              m_message;
    CScreenDlg          *m_screen;
    bool                 m_window_open;
    CCDAudio             m_cd;
    CMp3Audio            m_mp3_audio;
    CMouse               m_mouse;
    CBloodFix           *m_blood_fix;
    CTexHandler         *m_tex_handler;
    CTexHandlerAudio    *m_audio_trigger;
    CString              m_fmv;

public:
	CGlideServer();
	virtual ~CGlideServer();

	void     Start(CGameDetails *game, CGldSrvDlg *dlg);

	OSType   GetOSType();
	HWND     GetDosWindow();
	void     SaveTrace(const char *fname);
	void     ShowPipeState();
	void     StartTrace();
	void     Shutdown();

private:
	virtual void Execute();

	void OpenWindow();
	void DestroyScreen();
	void GetScreenMode(int *xsize, int *ysize);
	void SetScreenMode(int xsize, int ysize);
	void ResetScreenMode();
	void HideCursor();
	void RestoreCursor();

	CString GetStringFromClient();
	float   GetFloatFromClient();
	int     GetIntFromClient();
	CString GetLineFromClient();
	char    GetCharFromClient();

	void displayLFBtex(unsigned short *tex, int xo, int yo, int xsize, int ysize, GrLOD_t lod, int fac);
	void displayLFBarea(GrTextureFormat_t fmt, unsigned short *lfb, int xo, int yo, int xsize, int ysize, int fac);

	void SetPerm();
	void SetTexSource(int id);
	void SendConfirmation();
	void ReleaseAppWindow();
	void GetWindowFromApp();
	void DisplayMessage(CString &mess);
	void DebugMessage(CString &mess);

    void doFMVOpen();
    void doFMVPlay();
	void doGrTexCombine();
	void doGuFogGenerateExp();
    void doGuFogTableIndexToW();
	void doGuDrawTriangleWithClip();
	void doVESAEnd(void);
	void doVESAStart(void);
	void doDisplayVESA(void);
	void doGrHints();
	void doGrTexDetailControl();
	void doGrLFBLock();
    void doGrLFBConstantAlpha();
    void doGrLFBConstantDepth();
	void doGrColorMask();
	void doGrFogTable();
	void doConvertAndDownloadRLE();
	void doGrFogColorValue();
	void doGrCullMode();
	void doDisplayLFBPart();
	void doDisplayLFB();
	void doGrClipWindow();
	void doGrAlphaTestFunction();
    void doGrAlphaTestReferenceValue();
	void doGrFogMode();
	void doGrAlphaCombine();
	void doGrTexClampMode();
	void doGrTexLodBiasValue();
	void doGrTexMipMapMode();
	void doGrDisableAllEffects();
	void doGrDrawTriangle();
	void doGrTexSource();
	void doGrTexDownloadMipMap();
	void doGrRenderBuffer();
	void doIoctl();
	void doAudioPoll();
	void doAudioStart();
	void doMp3Play();
	void doGrAADrawLine();
	void doGrGlideShutdown();
	void doGrConstantColorValue4();
	void doGrDrawLine();
	void doGrAlphaBlendFunction();
	void doGrColorCombine();
	void doGrDepthBiasLevel();
	void doGrSstWinClose();
	void doGrTexFilterMode();
	void doGrBufferSwap();
	void doGrDrawPolygonVertexList();
	void doGuTexSource();
	void doGuColorCombineFunction();
	void doGrBufferClear();
	void doGuTexDownloadMipMap();
	void doGuTexAllocateMemory();
	void doGu3dfLoad();
	void doGu3dfGetInfo();
	void doGuTexMemReset();
	void doGrTexDownloadTable();
	void doGrGammaCorrectionValue();
	void doGrChromaKeyValue();
	void doGrChromaKeyMode();
	void doGuAlphaSource();
	void doGrConstantColorValue();
	void doGrTexCombineFunction();
	void doGrDepthMask();
	void doGrDepthBufferFunction();
	void doGrDepthBufferMode();
	void doGrSstWinOpen();
	void doGrSstSelect();
	void doGrSstQueryHardware();
	void doGrGlideInit();
};

#endif // !defined(AFX_GLIDESERVER_H__706B7700_61BA_11D5_B9B5_00C0DFF0F563__INCLUDED_)
