// GameSettingTex.h: interface for the CGameSettingTex class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAMESETTINGTEX_H__0EA62AEC_CB58_4F4A_9A2C_142C163BBEA9__INCLUDED_)
#define AFX_GAMESETTINGTEX_H__0EA62AEC_CB58_4F4A_9A2C_142C163BBEA9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GameSetting.h"


class CTexHandling
{
public:
	enum TexHandling
    {
        TEX_HANDLING_NONE,
        TEX_HANDLING_CAPTURE,
        TEX_HANDLING_OVERRIDE
    };


protected:
	TexHandling m_value;

public:
	operator TexHandling()
	{
		return m_value;
	};
};


class CGameSettingTex : public CGameSetting<CTexHandling>
{
public:
	void             Init(CStoredStruct *st, const char *name);
	CGameSettingTex &operator=(const TexHandling &i);

private:
	bool    Parse(CString &val);
	CString Print();
};

#endif // !defined(AFX_GAMESETTINGTEX_H__0EA62AEC_CB58_4F4A_9A2C_142C163BBEA9__INCLUDED_)
