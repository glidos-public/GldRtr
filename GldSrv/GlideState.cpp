// GlideState.cpp: implementation of the CGlideState class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "GlideState.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGlideState::CGlideState(CGlideDll *wrapper, int lfb_tex_space) : m_alpha_combine(ColorCombineValue::ALPHA), m_color_combine(ColorCombineValue::COLOR)
{
    m_fog_mode        = GR_FOG_DISABLE;
    m_depth_mask      = 0;
    m_depth_func      = GR_CMP_LESS;
    m_depthbuffermode = GR_DEPTHBUFFER_DISABLE;
    m_render_buffer   = GR_BUFFER_BACKBUFFER;
    m_render_buffer_tmp = GR_BUFFER_BACKBUFFER;
    m_tex_value.is_id = true;
    m_tex_value.mmid  = 0;
    m_wrapper         = wrapper;
    m_lfb_tex_space   = lfb_tex_space;
    m_tex_source_dirty= false;
    m_shadow_hack     = false;
}

CGlideState::~CGlideState()
{

}

CGlideDll *CGlideState::GetWrapper()
{
    return m_wrapper;
}

void CGlideState::GrDepthBufferMode(GrDepthBufferMode_t mode)
{
    m_depthbuffermode = mode;
}

void CGlideState::GrDepthBufferFunction(GrCmpFnc_t func)
{
    m_depth_func = func;
}

void CGlideState::GrDepthMask(int mask)
{
    m_depth_mask = mask;
}

void CGlideState::GrConstantColorValue(GrColor_t col)
{
    m_constant_color = col;
}

void CGlideState::GrAlphaSource(GrAlphaSource_t src)
{
    m_shadow_color = m_constant_color;
}


void CGlideState::GuColorCombineFunction(GrColorCombineFnc_t func)
{
    m_color_combine.is_gu   = true;

    m_color_combine.gu_func = func;
}

bool CGlideState::IsGoodForLogo()
{
    return m_color_combine.is_gu
             ? m_color_combine.gu_func == GR_COLORCOMBINE_TEXTURE_TIMES_ITRGB
             : (m_color_combine.func == GR_COMBINE_FUNCTION_SCALE_OTHER
               && m_color_combine.fact == GR_COMBINE_FACTOR_LOCAL
               && m_color_combine.local == GR_COMBINE_LOCAL_ITERATED
               && m_color_combine.other == GR_COMBINE_OTHER_TEXTURE
               && m_color_combine.invert == FXFALSE);
}

void CGlideState::GuTexSource(int id)
{
    m_tex_value.is_id = true;
    m_tex_value.mmid = id;

    m_tex_source_dirty = false;
}

void CGlideState::SetDepthBufferForLogo()
{
    if(m_depthbuffermode == GR_DEPTHBUFFER_WBUFFER)
    {
        m_wrapper->grDepthMask(1);
    }
    else
    {
        m_wrapper->grDepthMask(0);
        m_wrapper->grDepthBufferFunction(GR_CMP_ALWAYS);
    }
}

void CGlideState::RestoreTexSource()
{
    if(m_tex_value.is_id)
    {
        m_wrapper->guTexSource(m_tex_value.mmid);
    }
    else
    {
        m_wrapper->grTexSource(0, m_tex_value.startAddress + m_lfb_tex_space,
            m_tex_value.evenOdd,
            &(m_tex_value.info));

        m_wrapper->grTexClampMode(GR_TMU0, m_tex_clamp.s_clampmode, m_tex_clamp.t_clampmode);
    }

    m_tex_source_dirty = false;
}

void CGlideState::RestoreDepthBuffer()
{
    m_wrapper->grDepthMask(m_depth_mask);
    m_wrapper->grDepthBufferFunction(m_depth_func);
}

void CGlideState::RestoreColorCombine()
{
    if(m_color_combine.is_gu)
    {
        m_wrapper->guColorCombineFunction(m_color_combine.gu_func);
    }
    else
    {
        m_wrapper->grColorCombine(m_color_combine.func,
                       m_color_combine.fact,
                       m_color_combine.local,
                       m_color_combine.other,
                       m_color_combine.invert);
    }
}

void CGlideState::RestoreAlphaBlendFunction()
{
    m_wrapper->grAlphaBlendFunction(m_alpha_blend.rgb_sf,
                         m_alpha_blend.rgb_df,
                         m_alpha_blend.alpha_sf,
                         m_alpha_blend.alpha_df);
}

void CGlideState::RestoreFogMode()
{
    m_wrapper->grFogMode(m_fog_mode);
}

void CGlideState::GrColorCombine(GrCombineFunction_t func, GrCombineFactor_t fact, GrCombineLocal_t local, GrCombineOther_t other, FxBool invert)
{
    m_color_combine.is_gu  = false;

    m_color_combine.func   = func;
    m_color_combine.fact   = fact;
    m_color_combine.local  = local;
    m_color_combine.other  = other;
    m_color_combine.invert = invert;
}

void CGlideState::GrAlphaBlendFunction(GrAlphaBlendFnc_t rgb_sf, GrAlphaBlendFnc_t rgb_df, GrAlphaBlendFnc_t alpha_sf, GrAlphaBlendFnc_t alpha_df)
{
    m_alpha_blend.rgb_sf   = rgb_sf;
    m_alpha_blend.rgb_df   = rgb_df;
    m_alpha_blend.alpha_sf = alpha_sf;
    m_alpha_blend.alpha_df = alpha_df;

    if(m_shadow_hack && rgb_sf == GR_BLEND_SRC_ALPHA && rgb_df == GR_BLEND_ONE_MINUS_SRC_ALPHA)
        m_wrapper->grConstantColorValue(m_shadow_color);
}

void CGlideState::GrRenderBuffer(GrBuffer_t buffer)
{
    m_render_buffer     = buffer;
    m_render_buffer_tmp = buffer;
}

void CGlideState::GrTexSource(FxU32 start, FxU32 evenOdd, GrTexInfo *info)
{
    m_tex_value.is_id = false;
    m_tex_value.startAddress = start;
    m_tex_value.evenOdd = evenOdd;
    m_tex_value.info = *info;
}

void CGlideState::GrTexClampMode(GrTextureClampMode_t s, GrTextureClampMode_t t)
{
    m_tex_clamp.s_clampmode = s;
    m_tex_clamp.t_clampmode = t;
}

void CGlideState::GrAlphaCombine(GrCombineFunction_t func, GrCombineFactor_t fact, GrCombineLocal_t local, GrCombineOther_t other, FxBool invert)
{
    m_alpha_combine.func   = func;
    m_alpha_combine.fact   = fact;
    m_alpha_combine.local  = local;
    m_alpha_combine.other  = other;
    m_alpha_combine.invert = invert;
}

void CGlideState::GrFogMode(GrFogMode_t mode)
{
    m_fog_mode = mode;
}

void CGlideState::TemporaryRenderBuffer(GrBuffer_t buffer)
{
    if(buffer != m_render_buffer)
    {
        m_render_buffer_tmp = buffer;
        m_wrapper->grRenderBuffer(buffer);
    }
}

void CGlideState::RestoreRenderBuffer()
{
    if(m_render_buffer_tmp != m_render_buffer)
        m_wrapper->grRenderBuffer(m_render_buffer);
}

void CGlideState::RestoreAlphaCombine()
{
    m_wrapper->grAlphaCombine(m_alpha_combine.func,
                   m_alpha_combine.fact,
                   m_alpha_combine.local,
                   m_alpha_combine.other,
                   m_alpha_combine.invert);
}

bool CGlideState::IsTexturing()
{
    if(m_color_combine.is_gu)
    {
        switch(m_color_combine.gu_func)
        {
        case GR_COLORCOMBINE_DECAL_TEXTURE:
        case GR_COLORCOMBINE_TEXTURE_TIMES_CCRGB:
        case GR_COLORCOMBINE_TEXTURE_TIMES_ITRGB:
        case GR_COLORCOMBINE_TEXTURE_TIMES_ITRGB_DELTA0:
        case GR_COLORCOMBINE_TEXTURE_TIMES_ALPHA:
            return true;
        default:
            return false;
        }
    }
    else
    {
        return (m_color_combine.func == GR_COMBINE_FUNCTION_SCALE_OTHER
                && m_color_combine.other == GR_COMBINE_OTHER_TEXTURE);
    }
}

void CGlideState::MarkTexSourceDirty()
{
    m_tex_source_dirty = true;
}

void CGlideState::RestoreTexSourceIfDirty()
{
    if(m_tex_source_dirty)
        RestoreTexSource();
}

void CGlideState::SetShadowHack(bool enable)
{
    m_shadow_hack = enable;
}
