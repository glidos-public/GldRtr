/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide router component
 * 
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.10  2004/12/11 18:38:21  paul
 * Rewrite of vxd pipe to support overlapped IO. This
 * is part of the work necessary to restoring Glidos to
 * working under Window 98, since the two main
 * threads were combined into one.  Overlapped IO
 * is necessary so that Glidos can pump Windows
 * messages while waiting for incoming Glide commands
 * from the DOS side.  This also removes one of the
 * buffering stages, since the DOS side can now write
 * directly into the Windows side memory, hence making
 * the system more efficient.  Makefile also updated to
 * work with NTDDK.
 *
 * Revision 1.9  2002/06/02 15:16:25  paul
 * Wake up both ends of the pipe on an abort
 *
 * Revision 1.8  2001/07/28 12:06:17  paul
 * Revert back to previous synchronisation flags.
 *
 * Revision 1.7  2001/07/21 20:21:04  paul
 * Removed BLOCK_THREAD_IDLE from semaphore waits.
 *
 * Revision 1.6  2001/07/16 14:13:07  paul
 * Speed up DOS to Windows data flow using large buffers in all components
 *
 * Revision 1.5  2001/06/26 14:02:01  paul
 * Corrected stupid bugs
 *
 * Revision 1.4  2001/06/25 18:34:28  paul
 * Changed synchronisation to stop deadlocks
 * Added flush (triggered by zero input)
 *
 * Revision 1.3  2001/06/20 16:12:17  paul
 * Use count for buffer size in PM API to allow other than strings to be passed.
 *
 * Provided an abort method to stop the server hanging in the get call.
 *
 * Revision 1.2  2001/06/20 09:17:09  paul
 * Added synchronisation and in/out indexes to give pipe-like behaviour
 *
 * Revision 1.1  2001/06/19 16:08:42  paul
 * Initial version
 *
 ************************************************************/

#define WANTVXDWRAPS

#include <basedef.h>
#include <vmm.h>
#include <debug.h>
#include <vxdwraps.h>
#include <vwin32.h>
#include <winerror.h>

typedef DIOCPARAMETERS *LPDIOC;

#pragma VxD_LOCKED_CODE_SEG
#pragma VxD_LOCKED_DATA_SEG

#define MIN(x,y) ((x) < (y) ? (x) : (y))

DWORD _stdcall GLDPIPE_W32_DeviceIOControl(DWORD, DWORD, DWORD, LPDIOC);
DWORD _stdcall GLDPIPE_CleanUp(void);

void _stdcall DIOCCompletionRoutine(ULONG internal)
{
    _asm pushf;
    _asm push eax;
    _asm push ebx;
    _asm push edx;
    _asm push ecx;
    _asm push esi;
    _asm push edi;
    _asm  mov ebx,internal;
    VxDCall(VWIN32_DIOCCompletionRoutine);
    _asm pop edi;
    _asm pop esi;
    _asm pop ecx;
    _asm pop edx;
    _asm pop ebx;
    _asm pop eax;
    _asm popf;    
}

void memcpy(char *t, char *s, int c)
{
    while(c--)
        *t++ = *s++;
}

VMM_SEMAPHORE lock_avaiable;
VMM_SEMAPHORE space_available;
int           aborted;
OVERLAPPED   *lastOverlapped;
char         *lastBuffer;
int           lastBufferCount;
int           usedInLastBuffer;

void initialise_pipe(void)
{
    lock_avaiable   = Create_Semaphore(1);
    space_available = Create_Semaphore(0);
    aborted         = 0;
    lastOverlapped  = NULL;
    lastBuffer      = NULL;
    lastBufferCount = 0;
    usedInLastBuffer= 0;
}

void finalise_pipe(void)
{
    Destroy_Semaphore(lock_avaiable);
    Destroy_Semaphore(space_available);
}

ULONG page_lock(ULONG lpMem, ULONG cbSize)
{
    ULONG LinPageNum, LinOffset, nPages;

    LinOffset = lpMem & 0xfff; // page offset of memory to map
    LinPageNum = lpMem >> 12;  // generate page number

    // Calculate # of pages to map globally
    nPages = ((lpMem + cbSize) >> 12) - LinPageNum + 1;

    //
    // Return global mapping of passed in pointer, as this new pointer
    // is how the memory must be accessed out of context.
    //
    return (_LinPageLock(LinPageNum, nPages, PAGEMAPGLOBAL) + LinOffset);
}

void page_unlock(ULONG lpMem, ULONG cbSize)
{
    ULONG LinPageNum, nPages;

    LinPageNum = lpMem >> 12;
    nPages = ((lpMem + cbSize) >> 12) - LinPageNum + 1;

    // Free globally mapped memory
    _LinPageUnLock(LinPageNum, nPages, PAGEMAPGLOBAL);
}

void clear_buffers(void)
{
    /* always called with the lock taken */
    if(lastBuffer != NULL)
    {
        page_unlock((ULONG)lastBuffer, lastBufferCount);
        lastBuffer      = NULL;
        lastBufferCount = 0;
    }

    if(lastOverlapped != NULL)
    {
        page_unlock((ULONG)lastOverlapped, sizeof(OVERLAPPED));
        lastOverlapped = NULL;
    }

    usedInLastBuffer = 0;
}

void signal_buffer_filled(void)
{
    /* always called with the lock taken */
    if(lastOverlapped != NULL)
    {
        lastOverlapped->O_InternalHigh = usedInLastBuffer;
        DIOCCompletionRoutine(lastOverlapped->O_Internal);
    }

    clear_buffers();
}

void wait_for_space(char **buf, int *count)
{
    /* always called with the lock taken */
    while(lastOverlapped == NULL)
    {
        Signal_Semaphore(lock_avaiable);
        Wait_Semaphore(space_available, BLOCK_SVC_INTS|BLOCK_THREAD_IDLE);
        Wait_Semaphore(lock_avaiable, BLOCK_SVC_INTS|BLOCK_THREAD_IDLE);
    }

    *buf   = lastBuffer + usedInLastBuffer;
    *count = lastBufferCount - usedInLastBuffer;
}

void put_bytes(char *buf, int count)
{
    Wait_Semaphore(lock_avaiable, BLOCK_SVC_INTS|BLOCK_THREAD_IDLE);

    if(!aborted)
    {
        if(count == 0)
        {
            /* Sender uses count of 0 to signal that
             * all data should be passed on to receiver
             * even if it is less than the receiver
             * asked for */
            signal_buffer_filled();
        }
        else
        {
            while(count > 0)
            {
                char *obuf;
                int   ocount;
                int   ncopy;

                wait_for_space(&obuf, &ocount);
                /* wait_for_space may release the lock for
                 * a while, so we may have been aborted */
                if(aborted)
                    break;

                ncopy = MIN(count, ocount);

                memcpy(obuf, buf, ncopy);
                usedInLastBuffer += ncopy;
                buf              += ncopy;
                count            -= ncopy;

                /* If the buffer is now full, tell receiver */
                if(ncopy == ocount)
                    signal_buffer_filled();
            }
        }
    }

    Signal_Semaphore(lock_avaiable);
}

void get_bytes(LPDIOC lpDIOCParms)
{
    Wait_Semaphore(lock_avaiable, BLOCK_SVC_INTS|BLOCK_THREAD_IDLE);

    clear_buffers();

    lastOverlapped = (OVERLAPPED *)page_lock((ULONG)lpDIOCParms->lpoOverlapped, sizeof(OVERLAPPED));
    lastBufferCount= lpDIOCParms->cbOutBuffer;
    lastBuffer     = (char *)page_lock(lpDIOCParms->lpvOutBuffer, lastBufferCount);

    /* Signal put thread that there is new space available */
    Signal_Semaphore(space_available);
        
    Signal_Semaphore(lock_avaiable);
}

void abort_get(void)
{
    Wait_Semaphore(lock_avaiable, BLOCK_SVC_INTS|BLOCK_THREAD_IDLE);
    aborted = 1;
    signal_buffer_filled();
    Signal_Semaphore(lock_avaiable);

    Signal_Semaphore(space_available);
}

DWORD _stdcall GLDPIPE_W32_DeviceIOControl(DWORD  dwService,
                                        DWORD  dwDDB,
                                        DWORD  hDevice,
                                        LPDIOC lpDIOCParms)
{
    DWORD dwRetVal = 0;

    // DIOC_OPEN is sent when VxD is loaded w/ CreateFile 
    //  (this happens just after SYS_DYNAMIC_INIT)
    if(dwService == DIOC_OPEN)
    {
        // Must return 0 to tell WIN32 that this VxD supports DEVIOCTL
        dwRetVal = 0;
    }
    // DIOC_CLOSEHANDLE is sent when VxD is unloaded w/ CloseHandle
    //  (this happens just before SYS_DYNAMIC_EXIT)
    else if(dwService == DIOC_CLOSEHANDLE)
    {
        // Dispatch to cleanup proc
        dwRetVal = GLDPIPE_CleanUp();
    }
    else if(dwService == 1)
    {
        get_bytes(lpDIOCParms);
    
        dwRetVal = ERROR_IO_PENDING;
    }
    else if(dwService == 2)
    {
        abort_get();
    }
    else if(dwService == 3)
    {
        int *vals = (int *) lpDIOCParms->lpvOutBuffer;
        vals[0] = 0;
        vals[1] = 0;
        vals[2] = 0;
        vals[3] = 0;
        vals[4] = 0;
        vals[5] = 0;

        dwRetVal = NO_ERROR;
    }
    else
    {
        // Returning a positive value will cause the WIN32 DeviceIOControl
        // call to return FALSE, the error code can then be retrieved
        // via the WIN32 GetLastError
        dwRetVal = ERROR_NOT_SUPPORTED;
    }
    return(dwRetVal);
}


DWORD _stdcall GLDPIPE_Dynamic_Exit(void)
{
    finalise_pipe();

    return(VXD_SUCCESS);
}

DWORD _stdcall GLDPIPE_CleanUp(void)
{
    return(VXD_SUCCESS);
}

/****************************************************************************
 *                CVXD_VMAPI
 *
 *    ENTRY: function - the function number (passed in eax)
 *        parm1 - parameter 1 (passed in ebx)
 *        parm2 - parameter 2 (passed in ecx)
 *
 *    EXIT:    NONE
 ***************************************************************************/
int _stdcall GLDPIPE_VMAPI(char *function,
                        unsigned int parm1,
                        unsigned int parm2)
{
    return 10;
}

/****************************************************************************
 *                CVXD_PMAPI
 *
 *    ENTRY: function - the function number (passed in eax)
 *        parm1 - parameter 1 (passed in ebx)
 *        parm2 - parameter 2 (passed in ecx)
 *
 *    EXIT:    NONE
 ***************************************************************************/
int _stdcall GLDPIPE_PMAPI(char *function,
                        unsigned int parm1,
                        unsigned int parm2)
{
    put_bytes(function, parm1);

    return 20;
}

#pragma VxD_ICODE_SEG
#pragma VxD_IDATA_SEG

DWORD _stdcall GLDPIPE_Dynamic_Init(void)
{
    initialise_pipe();

    return(VXD_SUCCESS);
}



