// TexElementAct.cpp: implementation of the CTexElementAct class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "TexElementAct.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexElementAct::CTexElementAct()
{

}

CTexElementAct::CTexElementAct(CBox &b, CString &name) : CTexElement(b, name)
{

}

CTexElementAct::~CTexElementAct()
{

}
