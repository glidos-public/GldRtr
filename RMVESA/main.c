#include <dos.h>
#include <stdio.h>
#include <string.h>

#include "xms.h"
#include "pipe.h"
#include "vesa.h"
#include "mouse.h"
#include "cd.h"

void main(int argc, const char *argv[])
{
    unsigned int maxblock;
    int          i;
    int          cd_enabled   = 0;
    int          vesa_enabled = 0;
    int          fmv_enabled  = 0;
    Xms         *xms          = NULL;

    for(i = 1; i < argc; i++)
    {
        if(strcmp(argv[i], "vesa") == 0)
        {
            vesa_enabled = 1;
        }
        else if(strcmp(argv[i], "cd") == 0)
        {
            cd_enabled = 1;
        }
        else if(strcmp(argv[i], "fmv") == 0)
        {
            fmv_enabled = 1;
            /* Enable vesa driver, but don't send
             * anything to Glidos (stops flickering
             * of multiple windows opening) */
            vesa_enabled = 1;
        }
        else
        {
            printf("usage: rmvesa.exe [vesa][cd]\n");
            return;
        }
    }

    if(vesa_enabled)
    {
        if(!Xms_present())
        {
            printf("Glidos VESA support failed: XMS unavailable\n");
            return;
        }

        xms = Xms_open();
        maxblock = Xms_maxblock(xms);
        printf("XMS Ver %x, Max block %d%02dK\n", Xms_version(xms), maxblock/100, maxblock%100);
    }

    /* Open communication with Glidos, arranging that
     * cd commands are sent whenever the dos-side glide
     * driver synchronises */
    Pipe_init(CD_sendCommands, Mouse_newState, fmv_enabled);

    Mouse_init();

    if(vesa_enabled)
        VESA_init(xms, fmv_enabled);

    if(cd_enabled)
        CD_init();

    printf("Glidos VESA support for DOSBox running\n");

    _dos_keep(0, 3*1024);
}
