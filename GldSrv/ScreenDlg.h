#if !defined(AFX_SCREENDLG_H__1E7476E0_65B6_11D5_B9B6_00C0DFF0F563__INCLUDED_)
#define AFX_SCREENDLG_H__1E7476E0_65B6_11D5_B9B6_00C0DFF0F563__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScreenDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CScreenDlg dialog

class CGlideServer;

class CScreenDlg : public CDialog
{
// Construction
public:
	BOOL Create(CGlideServer *server);
	CScreenDlg(UINT idd, CWnd* pParent = NULL);   // standard constructor
    void SetFMVMode(bool fmv_mode);
    bool FMVIsDismissed();

// Dialog Data
	//{{AFX_DATA(CScreenDlg)
	enum { IDD = IDD_SCREEN };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScreenDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CScreenDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnWindowPosChanged(WINDOWPOS FAR* lpwndpos);
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CGlideServer *m_server;
    bool m_fmv_mode;
    bool m_fmv_dismissed;
    UINT m_idd;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCREENDLG_H__1E7476E0_65B6_11D5_B9B6_00C0DFF0F563__INCLUDED_)
