// TexElementReplace.h: interface for the CTexElementReplace class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXELEMENTREPLACE_H__0D287F28_186F_4DC3_8EE0_04B22632B7F3__INCLUDED_)
#define AFX_TEXELEMENTREPLACE_H__0D287F28_186F_4DC3_8EE0_04B22632B7F3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TexReplace.h"
#include "TexElementAct.h"

class CTexElementReplace : public CTexElementAct
{
private:
    static void CalcLodAspect(int w, int h, GrLOD_t *lod, GrAspectRatio_t *aspect);
    static int PowOfTwo(int s);

    CTexHandlerReplace *m_tex_replace;
    CGlideDll          *m_wrapper;
    int     m_width;
    int     m_height;
    float   m_s_scale;
    float   m_t_scale;
    int     m_id;
    bool    m_has_transparency;

public:
    void     Reset();
    void     Override(int n, GrVertex *vlist);
    bool     Load(GrLOD_t lod);

    CTexElementReplace(CTexHandlerReplace *tex_replace);
	CTexElementReplace(CBox &b, CString &name, CTexHandlerReplace *tex_replace);
	virtual ~CTexElementReplace();

};

#endif // !defined(AFX_TEXELEMENTREPLACE_H__0D287F28_186F_4DC3_8EE0_04B22632B7F3__INCLUDED_)
