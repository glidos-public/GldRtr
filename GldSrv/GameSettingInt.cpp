// GameSettingInt.cpp: implementation of the CGameSettingInt class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "GameSettingInt.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CGameSettingInt::Init(CStoredStruct * st, const char *name, int init)
{
	CGameSetting<CInt>::Init(st, name);
	
	m_value = init;
};

CGameSettingInt &CGameSettingInt::operator=(const int &i)
{
	if(i != m_value)
		m_defined = true;
	
	m_value   = i;
	
	return *this;
};

bool CGameSettingInt::Parse(CString &val)
{
	int i;
    char c;
	
	if(sscanf((LPCSTR)val, "%d%c", &i, &c) == 1)
	{
		m_value = i;
		
		return true;
	}
	
	return false;
};

CString CGameSettingInt::Print()
{
	CString res;
	
	res.Format("%d", m_value);
	
	return res;
};
