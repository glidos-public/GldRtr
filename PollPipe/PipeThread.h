// PipeThread.h: interface for the CPipeThread class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PIPETHREAD_H__73C8D9F4_222D_4313_912B_6A565E843327__INCLUDED_)
#define AFX_PIPETHREAD_H__73C8D9F4_222D_4313_912B_6A565E843327__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Thread.h"

class CPipeThread : public CThread  
{
private:
    enum Cmd
    {
        CMD_NONE,
        CMD_CLOSE,
        CMD_READ,
        CMD_WRITE,
    };

    HANDLE m_cmd_issued;
    HANDLE m_cmd_complete;
    Cmd m_cmd;

public:
	bool Poll();
	void Read(void *buf, int count);
	void Write(void *buf, int count);
	void Close();
	void Open();
	CPipeThread();
	virtual ~CPipeThread();

private:
	bool m_open;
	int m_count;
	char *m_buf;
	void Execute();
};

#endif // !defined(AFX_PIPETHREAD_H__73C8D9F4_222D_4313_912B_6A565E843327__INCLUDED_)
