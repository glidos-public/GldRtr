// MappingFile.h: interface for the CMappingFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAPPINGFILE_H__D52D6249_0B69_4CFA_B290_198D0E573959__INCLUDED_)
#define AFX_MAPPINGFILE_H__D52D6249_0B69_4CFA_B290_198D0E573959__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>
#include <vector>
#include <map>
#include "box.h"

struct TexElementInfo
{
    CBox    box;
    CString path;

    TexElementInfo(const CBox &b, const CString &p);
};

typedef std::vector<TexElementInfo> TexElementInfoList;

typedef std::vector<std::string> ImageFileSet;
typedef std::map<int, ImageFileSet> KeyToImageFileSet;
typedef std::map<std::string, int> ImageFileToKey;

typedef std::vector<std::string> NameSet;
typedef std::map<std::string, int> NameMap;

class CMappingFile  
{
private:
	void SplitAtSlash(const CString &name, CString &hash, CString &coord);
	void GetRootAndDir(const CString &name, CString &root, CString &dir);
	void ReadEquivFile(FILE *f, char *buf);
	void ReadMappingFile(FILE *f, char *buf);
	virtual CString FollowLink(CString &path, CString &root);
    CString            m_folder;
    CMapStringToString m_map_root;
    CMapStringToString m_map_dir;
    ImageFileToKey     m_elemtokey;
    KeyToImageFileSet  m_keytoset;

public:
	CMappingFile();
	virtual ~CMappingFile();

    void Initialise(const CString &folder);

    TexElementInfoList *GetElementInfoList(unsigned char hash[]);
    TexElementInfoList *GetElementInfoList(const CString &name);
    CStringArray *GetNameSet();
    CString NameFromHash(unsigned char hash[]);
    CString GetFolder(unsigned char hash[]);
};

#endif // !defined(AFX_MAPPINGFILE_H__D52D6249_0B69_4CFA_B290_198D0E573959__INCLUDED_)
