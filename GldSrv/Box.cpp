// Box.cpp: implementation of the CBox class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Box.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBox::CBox(int xmin, int xmax, int ymin, int ymax)
{
    m_xmin = xmin;
    m_xmax = xmax;
    m_ymin = ymin;
    m_ymax = ymax;
}

CBox::CBox()
{
    m_xmin = 0;
    m_xmax = -1;
    m_ymin = 0;
    m_ymax = -1;
}

CBox::CBox(int n, GrVertex *vlist)
{
    float xmin = 256.0;
    float ymin = 256.0;
    float xmax = 0.0;
    float ymax = 0.0;
    int   i;
    
    for(i = 0; i < n; i++)
    {
        float x = vlist[i].tmuvtx[0].sow / vlist[i].oow;
        float y = vlist[i].tmuvtx[0].tow / vlist[i].oow;
        
        if(x < xmin) xmin = x;
        if(y < ymin) ymin = y;
        if(x > xmax) xmax = x;
        if(y > ymax) ymax = y;
    }

    if(xmax > xmin && ymax > ymin)
    {
        m_xmin = (int)xmin;
        m_xmax = (int)xmax + 1;
        m_ymin = (int)ymin;
        m_ymax = (int)ymax + 1;
    }
    else
    {
        m_xmin = 0;
        m_xmax = -1;
        m_ymin = 0;
        m_ymax = -1;
    }
}

CBox::~CBox()
{

}

bool CBox::operator < (const CBox &b) const
{
    if(m_xmin < b.m_xmin)
        return true;

    if(m_xmin > b.m_xmin)
        return false;

    if(m_xmax < b.m_xmax)
        return true;

    if(m_xmax > b.m_xmax)
        return false;

    if(m_ymin < b.m_ymin)
        return true;

    if(m_ymin > b.m_ymin)
        return false;

    if(m_ymax < b.m_ymax)
        return true;

    return false;
}

bool CBox::Intersects(const CBox *b) const
{
    return    m_xmin < b->m_xmax
        && b->m_xmin <    m_xmax
        &&    m_ymin < b->m_ymax
        && b->m_ymin <    m_ymax;
}

bool CBox::IncludedBy(const CBox *b) const
{
    return b->m_xmin <= m_xmin && m_xmax <= b->m_xmax
        && b->m_ymin <= m_ymin && m_ymax <= b->m_ymax;
}

void CBox::Include(const CBox *b)
{
    if(b->m_xmin < m_xmin)
        m_xmin = b->m_xmin;

    if(b->m_xmax > m_xmax)
        m_xmax = b->m_xmax;

    if(b->m_ymin < m_ymin)
        m_ymin = b->m_ymin;

    if(b->m_ymax > m_ymax)
        m_ymax = b->m_ymax;
}

bool CBox::IsDefined() const
{
    return m_xmin <= m_xmax && m_ymin <= m_ymax;
}
