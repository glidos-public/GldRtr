// PlayBackground.h: interface for the CPlayBackground class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PLAYBACKGROUND_H__6461A582_66BB_49B0_B653_A521EB45F207__INCLUDED_)
#define AFX_PLAYBACKGROUND_H__6461A582_66BB_49B0_B653_A521EB45F207__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PlayAudioFile.h"

class CPlayBackground : public CPlayAudioFile  
{
private:
	CString m_current;

public:
	CPlayBackground();
	virtual ~CPlayBackground();

	void Start(CString &file);
	void Stop();
	void Pause();
	void Poll();
};

#endif // !defined(AFX_PLAYBACKGROUND_H__6461A582_66BB_49B0_B653_A521EB45F207__INCLUDED_)
