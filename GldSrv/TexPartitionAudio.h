// TexPartitionAudio.h: interface for the CTexPartitionAudio class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXPARTITIONAUDIO_H__F6656F42_FB46_4ACE_9820_CF3E27BD53AB__INCLUDED_)
#define AFX_TEXPARTITIONAUDIO_H__F6656F42_FB46_4ACE_9820_CF3E27BD53AB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PlayBackground.h"
#include "TexOverrider.h"
#include "MappingFile.h"

class CTexPartitionAudio : public CTexPartitionAct  
{
private:
	CPlayBackground *m_background;

public:
	CTexPartitionAudio(GrLOD_t lod, CTexElementActFactory *factory, CPlayBackground *background);
	virtual ~CTexPartitionAudio();

	void Initialise(CMappingFile *mapper, unsigned char hash[], int count, const CBox *areas);
};

#endif // !defined(AFX_TEXPARTITIONAUDIO_H__F6656F42_FB46_4ACE_9820_CF3E27BD53AB__INCLUDED_)
