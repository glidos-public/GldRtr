#include "stdafx.h"
#include "str_code.h"

static const char code[] = {(char)0x84,(char)0x79,(char)0x3c,(char)0x02};


void str_decode(char *out, const char *in)
{
    int i = 0;
    char c = (char)0xff; // Any non-zero value.

    while(c != 0)
    {
        c = in[i] ^ code[i%4];
        out[i++] = c;
    }
}
