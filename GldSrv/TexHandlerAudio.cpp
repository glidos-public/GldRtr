// TexHandlerAudio.cpp: implementation of the CTexHandlerAudio class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "TexPartitionAudio.h"
#include "TexElementAudioFactory.h"
#include "TexHandlerAudio.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexHandlerAudio::CTexHandlerAudio(bool background_enable)
{
	m_background_enable = background_enable;
}

CTexHandlerAudio::~CTexHandlerAudio()
{
    POSITION pos = m_locations.GetStartPosition();

    while(pos != NULL)
    {
        CString s;
        CSceneLocation *scene;

        m_locations.GetNextAssoc(pos, s, scene);
        delete scene;
    }
}

CTexPartition *CTexHandlerAudio::NewTexPartition(GrTextureFormat_t fmt, int width, int height, GrLOD_t lod)
{
    return new CTexPartitionAudio(lod, new CTexElementAudioFactory(this),
		           m_background_enable ? &m_background
				                       : NULL);
}

void CTexHandlerAudio::SetupTexPartition(CTexPartition *partition, unsigned char hash[], char *data)
{
    ((CTexPartitionAudio *)partition)->Initialise(GetMappingFile(), hash, 0, NULL);
}

CString CTexHandlerAudio::GetFolder()
{
    return "Audio";
}

CSceneLocation *CTexHandlerAudio::GetLocation(const CString &loc_name)
{
    CSceneLocation *scene;

    if(!m_locations.Lookup(loc_name, scene))
    {
        scene = new CSceneLocation();

        if(!scene->Load(GetFolder(), loc_name))
        {
            delete scene;
            scene = NULL;
        }
        else
        {
            m_locations[loc_name] = scene;
        }
    }

    return scene;
}

void CTexHandlerAudio::GrBufferSwap()
{
    POSITION pos = m_locations.GetStartPosition();

    while(pos != NULL)
    {
        CString s;
        CSceneLocation *scene;

        m_locations.GetNextAssoc(pos, s, scene);

        scene->TestPeriodEnd(&m_play_audio_file);
    }
	
	if(m_background_enable)
	{
		if(m_play_audio_file.Playing())
			m_background.Pause();
		else
			m_background.Poll();
	}
}

bool CTexHandlerAudio::Active()
{
	return m_background_enable && (CFileFind().FindFile((LPCSTR)(GetFolder() + "\\*.*")) == TRUE);
}
