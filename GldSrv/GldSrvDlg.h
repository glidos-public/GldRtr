/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide server
 * 
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.19  2006/12/05 15:20:09  paul
 * Disable help info on F1
 *
 * Revision 1.18  2006/03/24 14:20:11  paul
 * Move ownership of the display screen from the main dialog to the server
 *
 * Revision 1.17  2004/10/12 13:59:53  paul
 * Role the server into the main thread.  Improves pumping of messages and cures the momentary freezes with D2 on ATI cards
 *
 * Revision 1.16  2003/11/18 22:00:07  paul
 * Allocate main classes randomly in the heap, and hide flags in rotating
 * patterns, in an attempt to confuse hackers.
 *
 * Revision 1.15  2002/05/11 12:43:29  paul
 * Update UI to allow for multiple games
 *
 * Revision 1.14  2001/12/27 13:51:32  paul
 * Construct permutation array from the program memory image - permutation
 * displayed but not used at present.
 *
 * Revision 1.13  2001/12/16 17:27:54  paul
 * Added computer specific Glidos ID generation.
 *
 * Revision 1.12  2001/12/15 17:03:13  paul
 * Remove support-request dialog
 *
 * Revision 1.11  2001/08/11 14:22:59  paul
 * Add a "links" button.
 * Fixed the scaling of the antialiasing lines.
 *
 * Revision 1.10  2001/08/10 13:49:18  paul
 * Allow TR&UB to play with either CD.
 * Add support-request button.
 *
 * Revision 1.9  2001/07/28 11:57:04  paul
 * Hide mouse pointer in full screen mode.
 *
 * Revision 1.8  2001/07/26 20:43:09  paul
 * Full screen mode controllable from main window, with mode changes and all.
 *
 * Revision 1.7  2001/07/26 19:51:15  paul
 * Make new window for each call to grSstWinOpen.
 * Also provide two types of window for fullscreen and windowed.
 *
 * Revision 1.6  2001/07/18 19:37:07  paul
 * Add buttons for starting Tomb Raider and Unfinished Business, with tests
 * for not being installed or the CD not being present.
 * Also copy Glide2x.ovl to the C:\Tombraid folder.
 *
 * Revision 1.5  2001/07/03 15:23:55  paul
 * Added radio buttons for resolution control.
 * Passed control of GlideInit/Shutdown to server rather than client
 * Improved GUI's response to <CR> and <ESC>
 *
 * Revision 1.4  2001/06/25 18:23:35  paul
 * Added a few more methods
 * Changed DisplayMessage to DebugMessage for implemented methods
 * Added trace-toggle and pipe-status buttons
 *
 * Revision 1.3  2001/06/22 09:21:00  paul
 * Added game display window.
 *
 * Revision 1.2  2001/06/20 16:17:01  paul
 * Send "abort" command to VXD on shutdown to stop the server thread
 * hanging in a get call.
 *
 * Add buttons for shutdown, and saving the edit box.
 *
 * Us commands in protocol, only GrGlideInit at this stage.
 *
 ************************************************************/

// GldSrvDlg.h : header file
//

#if !defined(AFX_GLDSRVDLG_H__8CE9ACE6_6150_11D5_B9B5_00C0DFF0F563__INCLUDED_)
#define AFX_GLDSRVDLG_H__8CE9ACE6_6150_11D5_B9B5_00C0DFF0F563__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "stdafx.h"
#include "GameDetails.h"	// Added by ClassView
#include "GlideServer.h"

/////////////////////////////////////////////////////////////////////////////
// CGldSrvDlg dialog

class CGldSrvDlg : public CDialog
{
private:
	CGameDetails m_game;
	PIMAGE_SECTION_HEADER module_section_header;
	unsigned char *module_base;
    CGlideServer *server;
    bool m_kicked;
    bool m_server_running;
// Construction
public:
	CGldSrvDlg(CWnd* pParent = NULL);	// standard constructor

	void DisplayServerMessage(CString &mess);

// Dialog Data
	//{{AFX_DATA(CGldSrvDlg)
	enum { IDD = IDD_GLDSRV_DIALOG };
	CComboBox	m_combo;
	CEdit	m_edit;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGldSrvDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CGldSrvDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSave();
	afx_msg void OnTrace();
	afx_msg void OnPipeState();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnClose();
	afx_msg void OnLinks();
	afx_msg void OnAdjust();
	afx_msg void OnNewGame();
	afx_msg void OnStartGame();
	afx_msg void OnGameSelect();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	//}}AFX_MSG
	afx_msg LRESULT OnKickIdle(WPARAM, LPARAM);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GLDSRVDLG_H__8CE9ACE6_6150_11D5_B9B5_00C0DFF0F563__INCLUDED_)
