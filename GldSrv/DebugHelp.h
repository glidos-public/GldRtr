// DebugHelp.h: interface for the CDebugHelp class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEBUGHELP_H__DCDB34F8_D983_467D_8B01_C54332B59A2D__INCLUDED_)
#define AFX_DEBUGHELP_H__DCDB34F8_D983_467D_8B01_C54332B59A2D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CDebugHelp  
{
private:
    HANDLE m_process;
    HANDLE m_thread;

public:
	CString Trace(unsigned int u, EXCEPTION_POINTERS *pExp);
	CDebugHelp();
	virtual ~CDebugHelp();

};

#endif // !defined(AFX_DEBUGHELP_H__DCDB34F8_D983_467D_8B01_C54332B59A2D__INCLUDED_)
