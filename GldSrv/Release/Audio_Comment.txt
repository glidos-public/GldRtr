Title=Tomb Raider audio triggers v1.2 for Glidos
Text
{
The original Play Station release of Tomb Raider had many beautiful sound
tracks that were for some reason missed from the PC version. Glidos v1.33
and later can play audio tracks when certain scenes are encountered while
playing Tomb Raider. This package contains the audio tracks and the trigger
files
<p>
This is a new release compiled by my friends at http://vogons.zetafleet.com,
namely <b>Antonijadis</b>, <b>Kaminari</b>, <b>JC</b> and <b>Slube</b>.  It
has the triggers for the first six and a bit levels, plus background music
triggers for Unfinished Business.
<p>
You need v1.33 of Glidos http://www.glidos.net installed on your computer to
use this package.  Simply allow the archive to install the Audio folder in
your Glidos folder.
}
Path=Glidos\Audio
Overwrite=1

