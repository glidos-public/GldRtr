// TexHandlerAudio.h: interface for the CTexHandlerAudio class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXHANDLERAUDIO_H__CC0CA53C_EBE6_4B95_AC96_73D28EB09D86__INCLUDED_)
#define AFX_TEXHANDLERAUDIO_H__CC0CA53C_EBE6_4B95_AC96_73D28EB09D86__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PlayAudioFile.h"
#include "PlayBackground.h"
#include "SceneLocation.h"
#include "TexHandler.h"

class CTexHandlerAudio : public CTexHandler  
{
private:
	bool                                                     m_background_enable;
    CPlayAudioFile                                           m_play_audio_file;
	CPlayBackground                                          m_background;
    CTypedPtrMap<CMapStringToPtr, CString, CSceneLocation *> m_locations;

    CTexPartition *NewTexPartition(GrTextureFormat_t fmt, int width, int height, GrLOD_t lod);
    void SetupTexPartition(CTexPartition *partition, unsigned char hash[16], char *data);
	CString GetFolder();

public:
    void GrBufferSwap();

	CTexHandlerAudio(bool background_enable);
	virtual ~CTexHandlerAudio();

    CSceneLocation *GetLocation(const CString &loc_name);

	bool Active();
};

#endif // !defined(AFX_TEXHANDLERAUDIO_H__CC0CA53C_EBE6_4B95_AC96_73D28EB09D86__INCLUDED_)
