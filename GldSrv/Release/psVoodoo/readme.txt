psVoodoo can be used with Glide-based Windows games, without
Glidos (and hence without paying to unlock Glidos). Place Glide2x.dll
in the same folder as the game's executable and run the game.
psVoodooConfig.exe can be used to set up psVoodoo: e.g.,
to run the game on a stereo monitor.
