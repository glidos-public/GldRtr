/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * $Id$
 *
 ************************************************************/

#include "vxd.h"

static unsigned short cvxd_seg;
static unsigned short cvxd_off;

static unsigned short cvxd_seg2;
static unsigned short cvxd_off2;

extern void find_vxd(char *name);
    
#pragma aux find_vxd =           \
    "push es"                    \
    "mov ebx, 0h"                \
    "mov eax, 1684h"             \
    "int 2fh"                    \
    "mov cvxd_seg, es"           \
    "mov cvxd_off, di"           \
    "pop es"                     \
    parm [edi]                   \
    modify [eax ebx edi];


extern void find_vxd2(char *name);
    
#pragma aux find_vxd2 =          \
    "push es"                    \
    "mov ebx, 0h"                \
    "mov eax, 1684h"             \
    "int 2fh"                    \
    "mov cvxd_seg2, es"           \
    "mov cvxd_off2, di"           \
    "pop es"                     \
    parm [edi]                   \
    modify [eax ebx edi];



static void send(char *buf, int n)
{
    ((int (far *)(char *, int))(cvxd_seg:>cvxd_off))(buf, n);
}

static void flush(void)
{
    ((int (far *)(char *, int))(cvxd_seg:>cvxd_off))(0, 0);
}

static int readChar(void)
{
    unsigned char c;

    ((int (far *)(char *, int))(cvxd_seg2:>cvxd_off2))(&c, 1);

	return c;
}

static void get(char *buf, int n)
{
    ((int (far *)(char *, int))(cvxd_seg2:>cvxd_off2))(buf, n); 
}

static pipe_t pipe = {pipe_os_w98, send, flush, readChar, get};


pipe_t *VxdOpen(void)
{
    find_vxd("GLDPIPE1");
    find_vxd2("EPIPDLG1");

    return ((cvxd_seg || cvxd_off) && (cvxd_seg2 || cvxd_off2)) ? &pipe : 0;
}
