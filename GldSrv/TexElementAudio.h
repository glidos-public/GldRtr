// TexElementAudio.h: interface for the CTexElementAudio class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXELEMENTAUDIO_H__F7D74447_F290_4E45_95E0_1BDE35FBE2A5__INCLUDED_)
#define AFX_TEXELEMENTAUDIO_H__F7D74447_F290_4E45_95E0_1BDE35FBE2A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SceneDetail.h"
#include "TexHandlerAudio.h"
#include "TexElementAct.h"

class CTexElementAudio : public CTexElementAct  
{
private:
    CTypedPtrArray<CPtrArray, CSceneDetail *> m_details;
    CTexHandlerAudio                         *m_handler;
    CSceneDetail                             *m_current;

    bool ParseLoc(const CString &line, CString *loc_name, int *index);

public:
    void Reset();
    void Override(int n, GrVertex *vlist);
    bool Load(GrLOD_t lod);

	CTexElementAudio(CTexHandlerAudio *handler);
	CTexElementAudio(CBox &b, CString &name, CTexHandlerAudio *handler);
	virtual ~CTexElementAudio();
};

#endif // !defined(AFX_TEXELEMENTAUDIO_H__F7D74447_F290_4E45_95E0_1BDE35FBE2A5__INCLUDED_)
