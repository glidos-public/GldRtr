// TexExploder.cpp: implementation of the CTexPartitionOutput class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TexExploder.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexPartitionOutput::CTexPartitionOutput(GrTextureFormat_t fmt, int width, int height)
                  : m_bmp(fmt, width, height),
                    m_texset(new CTexElementFactory())
{
}

CTexPartitionOutput::~CTexPartitionOutput()
{
}

void CTexPartitionOutput::Finalise()
{

}

void CTexPartitionOutput::Initialise(CMappingFile *mapper, unsigned char hash[], GuTexPalette *pal, char *data)
{
    TexElementInfoList *list;
    CString             folder;

    ASSERT(data != NULL);

    folder = mapper->GetFolder(hash);
    list   = mapper->GetElementInfoList(hash);

    ::CreateDirectory((LPCSTR)folder, NULL);
    m_texset.Initialise(folder, list);
    delete list;

    m_bmp.SetPalette(pal);
    m_bmp.SetPixels(data);
    m_bmp.Write(folder + "\\Complete.bmp", NULL);
}

bool CTexPartitionOutput::UsedRectangle(int n, GrVertex *vlist)
{
    CBox box(n, vlist);
    
    if(box.IsDefined())
    {
        bool reorg = false;
        bool isnew = true;
        
        ASSERT(box.m_xmin < box.m_xmax && box.m_ymin < box.m_ymax);
        
        for(m_texset.Start(); m_texset.Going(); m_texset.Next())
        {
            CBox *b = m_texset.Here();
            if(box.Intersects(b))
            {
                isnew = false;
                
                if(!box.IncludedBy(b))
                {
                    reorg = true;
                    m_texset.Mark();
                }

                box.Include(b);
            }
        }
        
        if(reorg)
        {
            m_texset.DeleteMarked(true);
            m_texset.Add(&box, &m_bmp);
        }
        
        if(isnew)
            m_texset.Add(&box, &m_bmp);
    }

    return false;
}
