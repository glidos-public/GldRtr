#ifndef _PIPE_H_
#define _PIPE_H_

#define CMD_Message (1)
#define CMD_GrGlideInit (2)
#define CMD_SstWinOpen (5)
#define CMD_ConfirmationRequest (35)
#define CMD_DisplayLFB (51)
#define CMD_DisplayVESA (61)
#define CMD_VESAStart (62)
#define CMD_VESAEnd (63)
#define CMD_Mp3Play (67)
#define CMD_FMVOpen (72)
#define CMD_FMVPlay (73)

typedef void (Pipe_OnSyncFn)(void);
typedef void (Pipe_OnMouseEventFn)(long int xmickeys,
                                   long int ymickeys,
                                   long int buttons);


void Pipe_init(Pipe_OnSyncFn       *onSync,
               Pipe_OnMouseEventFn *onMouseEvent,
               int                  fmv_enabled);

void Pipe_open(void);

int Pipe_playStatus(void);

void Pipe_setPlayStatus(int s);

void Pipe_write(char *s, unsigned int n);

void Pipe_sendChar(char c);

void Pipe_sendInt(unsigned long int i);

void Pipe_synchronise(void);

void Pipe_printf(char *s, int a, int b, int c, int d, int e);

#endif
