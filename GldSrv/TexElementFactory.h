// TexElementFactory.h: interface for the CTexElementFactory class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXELEMENTFACTORY_H__310E000E_ADFE_4EEE_87AA_23708E6F24C4__INCLUDED_)
#define AFX_TEXELEMENTFACTORY_H__310E000E_ADFE_4EEE_87AA_23708E6F24C4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TexElement.h"

class CTexElementFactory  
{
public:
    virtual CTexElement *MakeTexElement();
    virtual CTexElement *MakeTexElement(CBox &b, CString &name);

	CTexElementFactory();
	virtual ~CTexElementFactory();

};

#endif // !defined(AFX_TEXELEMENTFACTORY_H__310E000E_ADFE_4EEE_87AA_23708E6F24C4__INCLUDED_)
