// Box.h: interface for the CBox class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BOX_H__8C26A645_1175_49B8_8393_19C30E477B7F__INCLUDED_)
#define AFX_BOX_H__8C26A645_1175_49B8_8393_19C30E477B7F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "sdk2_glide.h"

class CBox  
{
public:
    int m_xmin, m_xmax, m_ymin, m_ymax;

    /* Construct an undefined box */
    CBox();

    /* Construct a box of defined dimensions */
	CBox(int xmin, int xmax, int ymin, int ymax);

    /* Construct the bounding box within texture
     * space of a list of verticies */
	CBox(int n, GrVertex *vlist);

	virtual ~CBox();

    bool operator < (const CBox &b) const;

	bool IsDefined() const;
	void Include(const CBox *b);
	bool IncludedBy(const CBox *b) const;
	bool Intersects(const CBox *b) const;
};

#endif // !defined(AFX_BOX_H__8C26A645_1175_49B8_8393_19C30E477B7F__INCLUDED_)
