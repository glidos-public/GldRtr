// GlideState.h: interface for the CGlideState class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GLIDESTATE_H__AE0261B2_0BCE_4D43_8ABF_444827A94BF4__INCLUDED_)
#define AFX_GLIDESTATE_H__AE0261B2_0BCE_4D43_8ABF_444827A94BF4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "sdk2_glide.h"
#include "GlideDll.h"

struct ColorCombineValue
{
    bool                is_gu;
    GrColorCombineFnc_t gu_func;
    GrCombineFunction_t func;
    GrCombineFactor_t   fact;
    GrCombineLocal_t    local;
    GrCombineOther_t    other;
    FxBool              invert;

    enum Type
    {
        ALPHA,
        COLOR
    };

    ColorCombineValue(Type type)
    {
        switch(type)
        {
        case COLOR:
            is_gu  = false;
            func   = GR_COMBINE_FUNCTION_SCALE_OTHER;
            fact   = GR_COMBINE_FACTOR_ONE;
            local  = GR_COMBINE_LOCAL_ITERATED;
            other  = GR_COMBINE_OTHER_ITERATED;
            invert = false;
            break;
            
        case ALPHA:
            is_gu  = false;
            func   = GR_COMBINE_FUNCTION_SCALE_OTHER;
            fact   = GR_COMBINE_FACTOR_ONE;
            local  = GR_COMBINE_LOCAL_NONE;
            other  = GR_COMBINE_OTHER_CONSTANT;
            invert = false;
            break;
        };
    };
};
    
struct AlphaBlendFncValue
{
    GrAlphaBlendFnc_t rgb_sf;
    GrAlphaBlendFnc_t rgb_df;
    GrAlphaBlendFnc_t alpha_sf;
    GrAlphaBlendFnc_t alpha_df;

    AlphaBlendFncValue()
    {
        rgb_sf = GR_BLEND_ONE;
        rgb_df = GR_BLEND_ZERO;
        alpha_sf = GR_BLEND_ONE;
        alpha_df = GR_BLEND_ZERO;
    };
};

struct TextureValue
{
    bool is_id;
    GrMipMapId_t mmid;
    FxU32 startAddress;
    FxU32 evenOdd;
    GrTexInfo info;
};

struct TexClampModeValue
{
    GrTextureClampMode_t s_clampmode;
    GrTextureClampMode_t t_clampmode;
};

struct ColorVal
{
    float a, r, g, b;
};

class CGlideState  
{
private:
    CGlideDll          *m_wrapper;
    ColorCombineValue   m_color_combine;
    ColorCombineValue   m_alpha_combine;
    AlphaBlendFncValue  m_alpha_blend;
    GrFogMode_t         m_fog_mode;
    TextureValue        m_tex_value;
    TexClampModeValue   m_tex_clamp;
    int                 m_depth_mask;
    GrCmpFnc_t          m_depth_func;
    GrColor_t           m_constant_color;
    GrColor_t           m_shadow_color;
	GrDepthBufferMode_t m_depthbuffermode;
	GrBuffer_t          m_render_buffer;
	GrBuffer_t          m_render_buffer_tmp;
    int                 m_lfb_tex_space;
    bool                m_tex_source_dirty;
    bool                m_shadow_hack;

public:
	void SetShadowHack(bool enable);
    void MarkTexSourceDirty();
    void RestoreTexSourceIfDirty();
	bool IsTexturing();
	void RestoreAlphaCombine();
	void RestoreRenderBuffer();
	void TemporaryRenderBuffer(GrBuffer_t buffer);
	void GrFogMode(GrFogMode_t mode);
	void GrAlphaCombine(GrCombineFunction_t func, GrCombineFactor_t fact, GrCombineLocal_t local, GrCombineOther_t other, FxBool invert);
	void GrTexClampMode(GrTextureClampMode_t s, GrTextureClampMode_t t);
	void GrTexSource(FxU32 start, FxU32 evenOdd, GrTexInfo *info);
	void GrRenderBuffer(GrBuffer_t buffer);
	void GrAlphaBlendFunction(GrAlphaBlendFnc_t rgb_sf, GrAlphaBlendFnc_t rgb_df, GrAlphaBlendFnc_t alpha_sf, GrAlphaBlendFnc_t alpha_df);
	void GrColorCombine(GrCombineFunction_t func, GrCombineFactor_t fact, GrCombineLocal_t local, GrCombineOther_t other, FxBool invert);
	void RestoreFogMode();
	void RestoreAlphaBlendFunction();
	void RestoreColorCombine();
	void RestoreDepthBuffer();
	void RestoreTexSource();
	void SetDepthBufferForLogo();
	void GuTexSource(int id);
	bool IsGoodForLogo();
	void GuColorCombineFunction(GrColorCombineFnc_t func);
	void GrAlphaSource(GrAlphaSource_t src);
	void GrConstantColorValue(GrColor_t col);
	void GrDepthMask(int mask);
	void GrDepthBufferFunction(GrCmpFnc_t func);
	void GrDepthBufferMode(GrDepthBufferMode_t mode);

    CGlideDll *GetWrapper();

	CGlideState(CGlideDll *wrapper, int lfb_tex_space);
	virtual ~CGlideState();

};

#endif // !defined(AFX_GLIDESTATE_H__AE0261B2_0BCE_4D43_8ABF_444827A94BF4__INCLUDED_)
