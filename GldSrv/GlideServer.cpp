/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide server
 *
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.158  2007/10/02 19:18:43  paul
 * Change Redbook settting to an enum
 *
 * Revision 1.157  2007/10/02 18:36:47  paul
 * MP3 based support for Kevin Bracey's patched Tomb.exe
 *
 * Revision 1.156  2007/05/30 16:00:31  paul
 * Smooth TR FMVs
 *
 * Revision 1.155  2007/05/27 09:22:12  paul
 * Use backbuffer and swap for displaying VESA frames
 *
 * Revision 1.154  2007/05/10 20:17:50  paul
 * Associate mouse with screen correctly and hence stop
 * pointer straying onto second monitor. Also releases
 * mouse on <CTRL><ESC>
 *
 * Revision 1.153  2007/03/01 11:33:08  paul
 * Correctly position window for fullscreen case
 *
 * Revision 1.152  2007/02/03 19:18:08  paul
 * Allow Glidos to configure new psVoodoo features
 *
 * Revision 1.151  2007/02/03 08:44:38  paul
 * Fix bug: LFB lock call was ignoring stideInBytes
 *
 * Revision 1.150  2006/12/31 17:47:14  paul
 * Don't move the display window when adjusting its size
 *
 * Revision 1.149  2006/04/30 08:53:10  paul
 * Support texture pack being in subfolders of the "Textures" folder, with
 * user selection of the one to apply.
 *
 * Revision 1.148  2006/04/16 10:58:24  paul
 * Allow user control over Tomb Raider shadow hack, and improve the
 * glide state handling
 *
 * Revision 1.147  2006/04/09 11:12:46  paul
 * Create a version that displays a stack trace if it crashes
 *
 * Revision 1.146  2006/03/28 15:30:26  paul
 * Control OpenGLide and psVoodoo config from Glidos
 *
 * Revision 1.145  2006/03/27 19:56:03  paul
 * Provide choice of glide driver
 *
 * Revision 1.144  2006/03/27 19:09:46  paul
 * Explicitly load windows-side wrapper
 *
 * Revision 1.143  2006/03/24 14:51:57  paul
 * Make it server's responsibility to ask for game record rather than being informed when the start button is pressed
 *
 * Revision 1.142  2006/03/24 14:20:11  paul
 * Move ownership of the display screen from the main dialog to the server
 *
 * Revision 1.141  2005/11/05 14:04:05  paul
 * Add missing Unlock after Glidos grabs contents of frame buffer
 *
 * Revision 1.140  2004/10/12 13:59:53  paul
 * Role the server into the main thread.  Improves pumping of messages and cures the momentary freezes with D2 on ATI cards
 *
 * Revision 1.139  2004/08/22 15:43:08  paul
 * Neaten up the way the game details are handled, including
 * measures to stop explicit default values disappearing from
 * the glidos.ini file.
 *
 * Revision 1.138  2004/08/14 14:19:17  paul
 * Allow configuration of redbook emulation and mp3 background sounds
 *
 * Revision 1.137  2004/08/14 12:25:40  paul
 * Trigger background music tracks from texture loading.
 *
 * Inhibit CD playback
 *
 * Pause background tracks for special music tracks.
 *
 * Revision 1.136  2004/08/06 14:06:03  paul
 * Multipart scene recognition
 *
 * Revision 1.135  2004/08/01 10:07:02  paul
 * Position dependent audio triggering
 *
 * Revision 1.134  2004/07/29 07:30:27  paul
 * First version of audio tracks triggered by screen content.  Just
 * presence of a single texture in this version.
 *
 * Revision 1.133  2004/07/20 19:06:55  paul
 * More consistent naming of texture handling classes
 *
 * Revision 1.132  2004/07/18 18:55:10  paul
 * Restructured texture override classes, working towards audio track
 * triggering based on screen content
 *
 * Revision 1.131  2004/01/10 12:00:35  paul
 * Add small-texture cache to speed up D2
 *
 * Revision 1.130  2004/01/01 13:52:16  paul
 * More confusion tactics against hackers.
 *
 * Revision 1.129  2003/12/19 16:44:32  paul
 * Pass glide state to texture handler so that it can do the restoring, thus
 * cleaning things up a bit.
 *
 * Fix bug in initialisation of glide state.
 *
 * Prevent chromakeying for overridden textures
 *
 * Make masked textures work as did chromakeying
 *
 * Delay transparent textures
 *
 * Revision 1.128  2003/11/18 22:00:07  paul
 * Allocate main classes randomly in the heap, and hide flags in rotating
 * patterns, in an attempt to confuse hackers.
 *
 * Revision 1.127  2003/11/16 15:49:59  paul
 * Cut down the calls to guTexSource caused by texture overriding.
 *
 * Revision 1.126  2003/05/26 09:08:21  paul
 * Add French version of app
 *
 * Revision 1.125  2003/05/24 09:32:05  paul
 * Bump version
 *
 * Revision 1.124  2003/05/19 09:09:53  paul
 * Bump version
 *
 * Revision 1.123  2003/05/18 16:17:12  paul
 * Merge texture-output-branch to trunk
 *
 * Revision 1.122.2.5  2003/05/18 15:39:26  paul
 * Don't allow retexturing Lara's naughty bits in unregistered
 * copies.
 *
 * Revision 1.122.2.4  2003/05/17 09:55:05  paul
 * Control texture override with ini file
 *
 * Revision 1.122.2.3  2003/05/16 16:13:06  paul
 * Add Texture replacement
 *
 * Revision 1.122.2.2  2003/05/15 08:31:06  paul
 * Output textures in exploded form
 *
 * Revision 1.122.2.1  2003/05/14 10:50:32  paul
 * First version of texture capturing
 *
 * Revision 1.122  2003/05/13 14:13:18  paul
 * Move glide-state related info into a separate class
 *
 * Revision 1.121  2003/05/13 10:43:46  paul
 * Move details of pipe read caching into a separate class
 *
 * Revision 1.120  2003/05/12 14:16:55  paul
 * Setup the size field in the info structure before passing
 * it to grLfbLock
 *
 * Revision 1.119  2003/04/09 17:19:10  paul
 * Bump version
 *
 * Revision 1.118  2003/03/30 17:03:26  paul
 * Bump version
 *
 * Revision 1.117  2003/02/01 16:20:49  paul
 * Update program and protocol version
 *
 * Revision 1.116  2003/01/01 18:18:12  paul
 * Add DisableLfbReads and DelayLfbWrites settings for Screamer 2.
 * Control these settings, plus BloodFix via the .ini file.
 *
 * Revision 1.115  2002/12/04 14:51:35  paul
 * Flush Blood renderer at a few more Glide
 * commands.
 *
 * Revision 1.114  2002/12/03 20:43:22  paul
 * Patch small triangles together to give
 * bilinear interpolation in Blood
 *
 * Revision 1.113  2002/11/30 20:19:52  paul
 * Oops
 *
 * Revision 1.112  2002/11/30 20:16:42  paul
 * Use tracing dialog for debug builds
 *
 * Revision 1.111  2002/11/11 21:50:28  paul
 * Add grTexCombine
 *
 * Revision 1.110  200/2/11/03 18:17:26  paul
 * Move the TR shadow hack out of OpenGLide into Glidos.
 *
 * Revision 1.109  2002/11/02 13:59:15  paul
 * Map WIthClip type commands through to correct command
 * in wrapper, because OpenGLide now unsnaps for non-WithClip
 * commands.
 *
 * Revision 1.108  2002/10/20 19:02:15  paul
 * Use depth buffering for the logo only if
 * the game uses w buffering
 *
 * Revision 1.107  2002/10/19 17:47:51  paul
 * Bump version to 1.19
 *
 * Revision 1.106  2002/10/19 09:17:18  paul
 * Reenable GrClipWindow because it is now supported in OpenGLide.
 * Map through GuDrawTriangleWithClip.
 * Implement GuFogGenerateExp by requesting the
 * table from the server.
 *
 * Revision 1.105  2002/10/09 09:09:39  paul
 * Increase maximum number of gu textures.
 *
 * Allocate a gu texture on reset to make room for LFB tricks
 *
 * Revision 1.104  2002/09/02 18:14:44  paul
 * Add support for new VESA mode 0x111
 *
 * Revision 1.103  2002/08/29 21:31:47  paul
 * Bump to v1.18 and add gldvesa.exe to installer
 *
 * Revision 1.102  2002/08/28 18:49:50  paul
 * Introduce Glidos command for VESA frames with half-height mode.
 * Update game starter to use VESA support
 * Introduce VESA start/stop commands to aid server in detecting the
 * dos window.
 *
 * Revision 1.101  2002/08/03 13:49:17  paul
 * Bump version to 1.17
 *
 * Revision 1.100  2002/08/03 13:26:56  paul
 * Mouse support for Descent II
 *
 * Revision 1.99  2002/07/25 18:28:35  paul
 * Bump version to 1.16
 *
 * Revision 1.98  2002/07/25 18:16:59  paul
 * Provide user control of Gamma and forced texture smoothing
 *
 * Revision 1.97  2002/07/18 15:16:33  paul
 * Allow through gamma correction, now its implemented in OpenGlide
 *
 * Revision 1.96  2002/07/17 10:19:56  paul
 * When rendering LFB changes, use clamping to
 * avoid visible lines between the segments
 *
 * Revision 1.95  2002/07/07 14:26:24  paul
 * When displaying LFB changes, set tmuvtx.oow, so as to be independant
 * of STW hints.
 *
 * Bumb to version 1.15
 *
 * Revision 1.94  2002/07/07 09:39:30  paul
 * Handle grHints and ignore grTexDetailControl (for Carmageddon)
 *
 * Send buffer ref with LFB write requests.
 *
 * Revision 1.93  2002/07/03 20:14:49  paul
 * Put depth buffer right for LFB writes
 *
 * Revision 1.92  2002/07/03 16:30:28  paul
 * Make judder fix timer frequency controllable
 *
 * Bump to v1.14a
 *
 * Revision 1.91  2002/06/26 14:44:57  paul
 * Bump version to 1.14
 *
 * Revision 1.90  2002/06/24 15:30:26  paul
 * Remove the reply thread which is no longer needed now
 * we have a polling interface on the DOS side.
 *
 * Revision 1.89  2002/06/03 18:39:31  paul
 * Store and reinstate TexSource after the logo and LFB textures
 *
 * Increment version
 *
 * Revision 1.88  2002/06/02 15:19:33  paul
 * Hold off checking the next 8 textures after a grLfbLock because
 * at least six are likely to be generated from it in TR1
 *
 * Revision 1.87  2002/06/02 12:13:48  paul
 * Send back frame to client on grLfbLock in read-only mode
 *
 * Revision 1.86  2002/06/01 09:24:26  paul
 * Map the guTex calls directly through rather than turning them
 * into grTex calls, now that I've fixed OpenGlide.  Makes little
 * difference to performance but tests the new OpenGLide code
 * and it is simpler.
 *
 * Revision 1.85  2002/05/27 18:13:25  paul
 * Disable fog mode for logo and LFB writes
 *
 * Revision 1.84  2002/05/26 12:11:09  paul
 * Bring x and y coords into the range 0 to 2048.  Redguard does
 * something strange
 *
 * Record grColorCombine cache values in doGuColorCombine.  Needed
 * to stop the logo killing Redguard's sky.
 *
 * More debugging output
 *
 * Revision 1.83  2002/05/21 18:33:10  paul
 * Override security in the debug version
 *
 * Revision 1.82  2002/05/21 16:15:31  paul
 * Map through a few more Glide calls, towards getting Red Guard going.
 *
 * Revision 1.81  2002/05/14 19:29:49  paul
 * Update version
 *
 * Revision 1.80  2002/05/13 18:25:13  paul
 * Send ConvertAndDownloadRLE textures separately so as to
 * be able to avoid fingerprinting them
 *
 * Use bsearch to look up fingerprints.
 *
 * Revision 1.79  2002/05/12 11:58:16  paul
 * Make logo work for Descent II
 *
 * Revision 1.78  2002/05/11 19:28:29  paul
 * Make logo texture counter work with gr calls ready
 * for Descent II
 *
 * Revision 1.77  2002/05/11 16:12:21  paul
 * Add GrCullMode and GrFogColorValue for Descent II
 *
 * Revision 1.76  2002/05/11 15:53:29  paul
 * Make control-judder fix configurable.  Need for Descent II,
 * but makes TR run far too fast
 *
 * Revision 1.75  2002/05/11 12:43:29  paul
 * Update UI to allow for multiple games
 *
 * Revision 1.74  2002/05/08 17:28:55  paul
 * Do confirmation sending in a differnt thread so that a constant
 * stream of data can be sent to keep the Dosbox active and
 * prevent int 08s being lost
 *
 * Revision 1.73  2002/05/07 09:51:01  paul
 * Use cyan for bluescreening so as not to lose the "blue key" indicator
 * in Descent II.
 *
 * Revision 1.72  2002/05/05 14:25:55  paul
 * Avoid overwriting the texture space used to display LFB writes
 *
 * Revision 1.71  2002/05/01 19:28:25  paul
 * Tailor texture size for small LFB updates
 *
 * Revision 1.70  2002/05/01 18:27:07  paul
 * Send partial LFB updates when smaller than 256x256
 *
 * Revision 1.69  2002/04/30 17:50:05  paul
 * Display LFB writes by turning them into textures, using
 * alpha to leave the unwritten areas unchanged.
 *
 * Revision 1.68  2002/04/25 18:18:08  paul
 * Add some debugging messages
 *
 * Revision 1.67  2002/04/21 19:05:41  paul
 * Pass a few more glide commands for Descent2.
 * Implement ConvertAndDownloadRLE by
 * decompressing locally
 *
 * Revision 1.66  2002/04/19 19:29:41  paul
 * First go at synchronising the DOS box clock under NT
 *
 * Dropped 4 bytes out of checked region (use of timeGetTime makes this
 * necessary).
 *
 * Added some debugging
 *
 * Revision 1.65  2002/04/16 10:42:34  paul
 * Pass through a few more Glide commands and temporarily turn
 * off alpha bending as first steps towards getting Descent II working
 *
 * Revision 1.64  2002/04/10 18:21:00  paul
 * User snmp rather than netbios to get MAC address
 *
 * Bump version to 1.11
 *
 * Revision 1.63  2002/02/24 19:36:12  paul
 * Enlarge logo
 *
 * Revision 1.62  2002/02/19 18:26:49  paul
 * Removed some debugging and moved to next version of app
 * and protocol
 *
 * Revision 1.61  2002/02/19 18:04:20  paul
 * CD Music now working in W2k
 *
 * Revision 1.60  2002/02/18 15:12:58  paul
 * Take on the responsiblility for CD Audio, so that it works in W2k and XP.
 * Nearly working.
 *
 * Revision 1.59  2002/01/09 14:37:29  paul
 * Get rid of anti-aliased line drawing to avoid murky water in Tr1
 *
 * Revision 1.58  2002/01/09 14:34:04  paul
 * Bump the version to 1.9
 *
 * Revision 1.57  2002/01/06 18:58:40  paul
 * Use texture finger prints to repress logo for the first tow levels.
 *
 * Revision 1.56  2002/01/06 14:55:44  paul
 * Add texture fingerprint generation.
 *
 * Revision 1.55  2002/01/06 13:33:39  paul
 * Protect against the logo being detected by a bogus OpenGlide writing
 * values into the vertex structure and detecting its presence in future polygons.
 *
 * Revision 1.54  2001/12/31 17:57:40  paul
 * Use text based authority file
 *
 * Revision 1.53  2001/12/31 16:27:25  paul
 * Back out use of cbc communications to GldLogo.dll because they
 * slowed things down too much.
 *
 * Revision 1.52  2001/12/29 16:52:26  paul
 * Bump version
 *
 * Revision 1.51  2001/12/28 20:42:34  paul
 * Get the new signing strategy working for the release version.
 * Had to ignore the first 0x540 bytes of the rdata section; not sure
 * why.
 *
 * Revision 1.50  2001/12/28 19:01:11  paul
 * Use openssl random functions throughout.
 *
 * Revision 1.49  2001/12/27 18:38:33  paul
 * GldSrv app now protected by permutation array.
 *
 * Revision 1.48  2001/12/27 16:26:46  paul
 * Generate command permutation array from both the file and the memory
 * interface - permutation still displayed and not used.
 *
 * Revision 1.47  2001/12/27 13:51:32  paul
 * Construct permutation array from the program memory image - permutation
 * displayed but not used at present.
 *
 * Revision 1.46  2001/12/09 11:43:19  paul
 * Use DES-CBC in communication with GldLogo.dll
 *
 * Revision 1.45  2001/12/05 18:38:40  paul
 * More confusion tactics: buffer polygons so that its harder to distinguish
 * logo from game.
 *
 * Revision 1.44  2001/12/05 15:37:16  paul
 * Move all polygon rendering into the 3DScene calls, in the hope of
 * confusing hackers.
 *
 * Revision 1.43  2001/12/02 20:21:34  paul
 * Merge v1.7 changes into main trunk
 *
 * Revision 1.42  2001/11/30 17:58:06  paul
 * Randomise the logo texture position
 *
 * Revision 1.41  2001/11/30 17:23:37  paul
 * Base logo verticies on integers to match Tomb Raider,
 * so that they don't stand out.
 *
 * Revision 1.40  2001/11/30 16:42:49  paul
 * In the logo, use undefined vertex fields from the game so that they
 * don't stand out.
 *
 * Revision 1.39  2001/11/29 18:48:24  paul
 * Complete logo rendering when random process leaves
 * polygons yet to be drawn.
 *
 * Revision 1.38  2001/11/29 18:09:00  paul
 * Randomize the interleaving of logo polygons
 *
 * Revision 1.37  2001/11/29 16:59:16  paul
 * Interleave logo with resst of scene
 *
 * Revision 1.36  2001/11/29 13:35:41  paul
 * Remove the old, unused texture handling code.
 *
 * Revision 1.35  2001/11/29 11:41:35  paul
 * Moved the light source to brighten the logo, now
 * the lighting calculation has been corrected.
 *
 * Revision 1.34  2001/11/29 09:42:12  paul
 * Reposition the texture over the logo, now that it is again
 * defined relative to a (0,0) origin.
 *
 * Revision 1.33  2001/11/26 15:04:53  paul
 * Provide lighting for the logo.
 *
 * Revision 1.32  2001/11/26 13:24:43  paul
 * Scale and offset logo view to match screen size
 *
 * Revision 1.31  2001/11/26 12:54:22  paul
 * Merge gen-logo branch back into the trunk.
 * Branching turned out to be unnecessary because
 * nothing has happened on the trunk.
 *
 * Revision 1.30.2.7  2001/11/26 12:44:43  paul
 * Shortened the L in the logo
 *
 * Revision 1.30.2.6  2001/11/26 11:40:06  paul
 * Defined wire coordinates for GLIDOS logo
 *
 * Revision 1.30.2.5  2001/11/25 20:28:48  paul
 * Added square wire creating class, for logo letters
 *
 * Revision 1.30.2.4  2001/11/24 11:31:27  paul
 * Double the logo width and map it into the corner of the texture
 *
 * Revision 1.30.2.3  2001/11/24 11:19:08  paul
 * Make the logo spin
 *
 * Revision 1.30.2.2  2001/11/24 11:02:20  paul
 * Center the logo
 *
 * Revision 1.30.2.1  2001/11/24 10:23:45  paul
 * Towards displaying a logo, make a square appear in the scene.
 *
 * Revision 1.30  2001/11/11 08:41:51  paul
 * Send Alt-CR to DOS box only when running under W2K
 *
 * Revision 1.29  2001/11/10 10:05:52  paul
 * Make TR and UB start up sensitive to the OS
 *
 * Revision 1.28  2001/11/10 04:36:02  paul
 * Control fullscreen/windowed property of DOS box in W2K by sending
 * Alt CR to the DOS window.  As part of this, handle out od sequence
 * grWinOpen and grWinClose, and add confirmation calls to stop the
 * DOS program continuing before the Alt CR is sent.
 *
 * Revision 1.27  2001/11/04 12:42:15  paul
 * Added pipe access class for W2K
 *
 * Revision 1.26  2001/11/03 19:27:22  paul
 * Virtualise pipe access ready for W2k support
 *
 * Revision 1.25  2001/10/07 18:44:37  paul
 * Bumped version to 1.6
 *
 * Revision 1.24  2001/08/11 14:22:59  paul
 * Add a "links" button.
 * Fixed the scaling of the antialiasing lines.
 *
 * Revision 1.23  2001/08/10 13:49:18  paul
 * Allow TR&UB to play with either CD.
 * Add support-request button.
 *
 * Revision 1.22  2001/08/01 09:06:50  paul
 * Distinguish between empty drives and non-existence drives, so that
 * people with their CDRom up at Q and the like can use the TR and UB
 * buttons.
 * Flip to version 1.4
 *
 * Revision 1.21  2001/07/28 13:49:31  paul
 * Invoke TR and UB via pif files.
 * Flipped to v1.3
 *
 * Revision 1.20  2001/07/26 19:51:15  paul
 * Make new window for each call to grSstWinOpen.
 * Also provide two types of window for fullscreen and windowed.
 *
 * Revision 1.19  2001/07/25 13:29:25  paul
 * Flipped protocol version to 3.
 * Added GrAADrawLine.
 * Made the server rather than the glide wrapper responsible for sizing
 * the window.
 *
 * Revision 1.18  2001/07/21 20:25:22  paul
 * Flip protocol version to 2.
 * Use HWND rather than CWnd for communication between threads.
 *
 * Revision 1.17  2001/07/20 15:04:28  paul
 * Change version to 1.2
 * Open Window on grSstOpenWindow, so as not to rely on the Glide Wrapper.
 *
 * Revision 1.16  2001/07/16 14:13:07  paul
 * Speed up DOS to Windows data flow using large buffers in all components
 *
 * Revision 1.15  2001/07/15 19:31:32  paul
 * Hide DOS window and keep game active.
 *
 * Revision 1.14  2001/07/13 08:21:34  paul
 * Flipped protocol to version 1.
 *
 * Added GrConstantColorValue4 and GrGlideShutdown
 *
 * Removed race condition in GrSstWinOpen.
 *
 * Implement GrDrawLine with a rectangle.
 *
 * Made window go away at game end, by closing on GrGlideShutdown,
 * and flushing pipe.
 *
 * Revision 1.13  2001/07/07 11:20:11  paul
 * Added a little more trace info, and corrected type of depth bias.
 *
 * Revision 1.12  2001/07/03 19:00:42  paul
 * Added 1280 x 1024 mode
 * Added client version check
 *
 * Revision 1.11  2001/07/03 15:23:55  paul
 * Added radio buttons for resolution control.
 * Passed control of GlideInit/Shutdown to server rather than client
 * Improved GUI's response to <CR> and <ESC>
 *
 * Revision 1.10  2001/06/30 10:31:13  paul
 * Get back lost Log info.
 *
 * Revision 1.9  2001/06/30 10:19:40  paul
 * Provided for setting the screen resolution independently of the clients
 * request.
 *
 * Stopped all debug message generation, unless tracing.
 *
 * Revision 1.8  2001/06/28 18:30:16  paul
 * Transfer the correct amount of data for textures.
 *
 * Revision 1.7  2001/06/28 10:55:46  paul
 * Provided method of capturing debug in memory and saving it to a file.
 * Disabled drawline because it interacts badly with chroma keying at
 * the moment.
 *
 * Revision 1.6  2001/06/25 18:23:35  paul
 * Added a few more methods
 * Changed DisplayMessage to DebugMessage for implemented methods
 * Added trace-toggle and pipe-status buttons
 *
 * Revision 1.5  2001/06/23 16:07:22  paul
 * Implement gu texture methods using gr methods, to cope with glide wrappers
 * not providing the gu interface.
 *
 * Stop all messages to the debug window; now runs full speed.
 *
 * Revision 1.4  2001/06/22 15:35:22  paul
 * More debug messages.
 * Updated the reported hardware config to match OpenGlide
 * Refuse Gu3dfLoad to force TR I to generate mimmaps.
 *
 * Revision 1.3  2001/06/22 09:21:55  paul
 * First implementation of entry points used by Tomb Raider I
 *
 * Revision 1.2  2001/06/20 16:17:01  paul
 * Send "abort" command to VXD on shutdown to stop the server thread
 * hanging in a get call.
 *
 * Add buttons for shutdown, and saving the edit box.
 *
 * Us commands in protocol, only GrGlideInit at this stage.
 *
 ************************************************************/

// GlideServer.cpp: implementation of the CGlideServer class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <math.h>
#include "openssl/md5.h"
#include "openssl/rsa.h"
#include "GldSrv.h"
#include "BloodFix.h"
#include "TexOutput.h"
#include "TexReplace.h"
#include "TexHandlerAudio.h"
#include "MoviePlayer.h"
#include "sdk2_glide.h"
#include "GldSrvDlg.h"
#include "wrapper_config.h"
#include "Resolution.h"

#include "GlideServer.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define PI (3.1415926f)
#define MARKER_SIZE (12)

#define MIN(x,y) ((x) < (y) ? (x) : (y))
#define MAX(x,y) ((x) > (y) ? (x) : (y))

#define VERSION (12)

#define LFB_TEX_SPACE (128*1024)
#define CACHE_SPACE   (128*1024)
#define RESERVED_SPACE (LFB_TEX_SPACE+CACHE_SPACE)

#define CMD_Message (1)
#define CMD_GrGlideInit (2)
#define CMD_GrSstQueryHardware (3)
#define CMD_GrSstSelect (4)
#define CMD_GrSstWinOpen (5)
#define CMD_GrDepthBufferMode (6)
#define CMD_GrDepthBufferFunction (7)
#define CMD_GrDepthMask (8)
#define CMD_GrTexCombineFunction (9)
#define CMD_GrConstantColorValue (10)
#define CMD_GuAlphaSource (11)
#define CMD_GrChromaKeyMode (12)
#define CMD_GrChromaKeyValue (13)
#define CMD_GrGammaCorrectionValue (14)
#define CMD_GrTexDownloadTable (15)
#define CMD_GuTexMemReset (16)
#define CMD_Gu3dfGetInfo (17)
#define CMD_Gu3dfLoad (18)
#define CMD_GuTexAllocateMemory (19)
#define CMD_GuTexDownloadMipMap (20)
#define CMD_GrBufferClear (21)
#define CMD_GuColorCombineFunction (22)
#define CMD_GuTexSource (23)
#define CMD_GrDrawPolygonVertexList (24)
#define CMD_GrBufferSwap (25)
#define CMD_GrTexFilterMode (26)
#define CMD_GrSstWinClose (27)
#define CMD_GrDepthBiasLevel (28)
#define CMD_GrColorCombine (29)
#define CMD_GrAlphaBlendFunction (30)
#define CMD_GrDrawLine (31)
#define CMD_GrConstantColorValue4 (32)
#define CMD_GrGlideShutdown (33)
#define CMD_GrAADrawLine (34)
#define CMD_ConfirmationRequest (35)
#define CMD_AudioStart (36)
#define CMD_AudioPoll (37)
#define CMD_Ioctl (38)
#define CMD_GrRenderBuffer (39)
#define CMD_GrTexDownloadMipMap (40)
#define CMD_GrTexSource (41)
#define CMD_GrDrawTriangle (42)
#define CMD_GrDisableAllEffects (43)
#define CMD_GrTexMipMapMode (44)
#define CMD_GrTexLodBiasValue (45)
#define CMD_GrTexClampMode (46)
#define CMD_GrAlphaCombine (47)
#define CMD_GrFogMode (48)
#define CMD_GrAlphaTestFunction (49)
#define CMD_GrClipWindow (50)
#define CMD_DisplayLFB (51)
#define CMD_DisplayLFBPart (52)
#define CMD_GrCullMode (53)
#define CMD_GrFogColorValue (54)
#define CMD_ConvertAndDownloadRLE (55)
#define CMD_GrFogTable (56)
#define CMD_GrColorMask (57)
#define CMD_GrLFBLock (58)
#define CMD_GrTexDetailControl (59)
#define CMD_GrHints (60)
#define CMD_DisplayVESA (61)
#define CMD_VESAStart (62)
#define CMD_VESAEnd (63)
#define CMD_GuDrawTriangleWithClip (64)
#define CMD_GuFogGenerateExp (65)
#define CMD_GrTexCombine (66)
#define CMD_Mp3Play (67)
#define CMD_GuFogTableIndexToW (68)
#define CMD_GrAlphaTestReferenceValue (69)
#define CMD_GrLFBConstantAlpha (70)
#define CMD_GrLFBConstantDepth (71)
#define CMD_FMVOpen (72)
#define CMD_FMVPlay (73)

CGlideServer::CGlideServer() : m_glide_state(&m_wrapper, RESERVED_SPACE),
                               m_delay_store(&m_wrapper, &m_glide_state)
{
    m_dos_window = NULL;
    m_vesa_active = false;
    m_window_open = false;

    m_screen = NULL;

    client_dir = "";

    m_tracing = false;
    m_trace_on_winopen = false;

    big_buf = (char *)malloc(2*1024*1024);
    big_buf_ptr = big_buf;
    m_server_resolution = GR_RESOLUTION_1024x768;
    m_tex_check_holdoff = 0;

	m_OSType = OS_NONE;

    char *cmd_line = GetCommandLine();
    if(cmd_line[0] == '\"')
        cmd_line += 1;

    strcpy(app_file, cmd_line);

    char *p = strchr(app_file, '\"');
    if(p != NULL)
        p[0] = 0;

    m_blood_fix = NULL;
	m_tex_handler = NULL;
    m_tex_cache = NULL;
}

CGlideServer::~CGlideServer()
{
    Shutdown();

    free(big_buf);

    delete m_blood_fix;
	delete m_tex_handler;
    delete m_tex_cache;
}

void CGlideServer::Execute()
{
    CString message;

    message.LoadString(IDS_STARTUP);
    DisplayMessage(message);

    m_mouse.Initialise();

    switch(m_pipe.Create())
    {
    case CCachedPipe::OS_W2K:
        m_OSType = OS_W2K;
        break;

    case CCachedPipe::OS_W98:
        m_OSType = OS_W98;
        break;

    default:
        {
            CString s;

            s.Format("Pipe creation failed %d", GetLastError());
            DisplayMessage(s);
            return;
        }
        break;
    }

    DebugMessage(CString("Pipe created"));
	
    try
    {
        while(1)
        {
            MSG msg;

            while(::PeekMessage(&msg, NULL, NULL, NULL, PM_NOREMOVE))
                AfxGetApp()->PumpMessage();

            switch(GetCharFromClient())
            {
            case CMD_Message:
                DisplayMessage(GetLineFromClient());
                break;

            case CMD_GrGlideInit:
                doGrGlideInit();
                break;

            case CMD_GrSstQueryHardware:
                doGrSstQueryHardware();
                break;

            case CMD_GrSstSelect:
                doGrSstSelect();
                break;

            case CMD_GrSstWinOpen:
                doGrSstWinOpen();
                break;

            case CMD_GrDepthBufferMode:
                doGrDepthBufferMode();
                break;

            case CMD_GrDepthBufferFunction:
                doGrDepthBufferFunction();
                break;

            case CMD_GrDepthMask:
                doGrDepthMask();
                break;

            case CMD_GrTexCombineFunction:
                doGrTexCombineFunction();
                break;

            case CMD_GrConstantColorValue:
                doGrConstantColorValue();
                break;

            case CMD_GuAlphaSource:
                doGuAlphaSource();
                break;

            case CMD_GrChromaKeyMode:
                doGrChromaKeyMode();
                break;

            case CMD_GrChromaKeyValue:
                doGrChromaKeyValue();
                break;

            case CMD_GrGammaCorrectionValue:
                doGrGammaCorrectionValue();
                break;

            case CMD_GrTexDownloadTable:
                doGrTexDownloadTable();
                break;

            case CMD_GuTexMemReset:
                doGuTexMemReset();
                break;

            case CMD_Gu3dfGetInfo:
                doGu3dfGetInfo();
                break;

            case CMD_Gu3dfLoad:
                doGu3dfLoad();
                break;

            case CMD_GuTexAllocateMemory:
                doGuTexAllocateMemory();
                break;

            case CMD_GuTexDownloadMipMap:
                doGuTexDownloadMipMap();
                break;

            case CMD_GrBufferClear:
                doGrBufferClear();
                break;

            case CMD_GuColorCombineFunction:
                doGuColorCombineFunction();
                break;

            case CMD_GuTexSource:
                doGuTexSource();
                break;

            case CMD_GrDrawPolygonVertexList:
                doGrDrawPolygonVertexList();
                break;

            case CMD_GrBufferSwap:
                doGrBufferSwap();
                break;

            case CMD_GrTexFilterMode:
                doGrTexFilterMode();
                break;

            case CMD_GrSstWinClose:
                doGrSstWinClose();
                break;

            case CMD_GrDepthBiasLevel:
                doGrDepthBiasLevel();
                break;

            case CMD_GrColorCombine:
                doGrColorCombine();
                break;

            case CMD_GrAlphaBlendFunction:
                doGrAlphaBlendFunction();
                break;

            case CMD_GrDrawLine:
                doGrDrawLine();
                break;

            case CMD_GrConstantColorValue4:
                doGrConstantColorValue4();
                break;

            case CMD_GrGlideShutdown:
                doGrGlideShutdown();
                break;

            case CMD_GrAADrawLine:
                doGrAADrawLine();
                break;

            case CMD_ConfirmationRequest:
                SendConfirmation();
                break;

            case CMD_AudioStart:
                doAudioStart();
                break;

            case CMD_AudioPoll:
                doAudioPoll();
                break;

            case CMD_Mp3Play:
                doMp3Play();
                break;

            case CMD_Ioctl:
                doIoctl();
                break;

            case CMD_GrRenderBuffer:
                doGrRenderBuffer();
                break;

            case CMD_GrTexDownloadMipMap:
                doGrTexDownloadMipMap();
                break;

            case CMD_GrTexSource:
                doGrTexSource();
                break;

            case CMD_GrDrawTriangle:
                doGrDrawTriangle();
                break;

            case CMD_GrDisableAllEffects:
                doGrDisableAllEffects();
                break;

            case CMD_GrTexMipMapMode:
                doGrTexMipMapMode();
                break;

            case CMD_GrTexLodBiasValue:
                doGrTexLodBiasValue();
                break;

            case CMD_GrTexClampMode:
                doGrTexClampMode();
                break;

            case CMD_GrAlphaCombine:
                doGrAlphaCombine();
                break;

            case CMD_GrFogMode:
                doGrFogMode();
                break;

            case CMD_GrAlphaTestFunction:
                doGrAlphaTestFunction();
                break;

            case CMD_GrAlphaTestReferenceValue:
                doGrAlphaTestReferenceValue();
                break;

            case CMD_GrClipWindow:
                doGrClipWindow();
                break;

            case CMD_DisplayLFB:
                doDisplayLFB();
                break;

            case CMD_DisplayLFBPart:
                doDisplayLFBPart();
                break;

            case CMD_GrCullMode:
                doGrCullMode();
                break;

            case CMD_GrFogColorValue:
                doGrFogColorValue();
                break;

            case CMD_ConvertAndDownloadRLE:
                doConvertAndDownloadRLE();
                break;

            case CMD_GrFogTable:
                doGrFogTable();
                break;

            case CMD_GrColorMask:
                doGrColorMask();
                break;

            case CMD_GrLFBLock:
                doGrLFBLock();
                break;

            case CMD_GrLFBConstantAlpha:
                doGrLFBConstantAlpha();
                break;

            case CMD_GrLFBConstantDepth:
                doGrLFBConstantDepth();
                break;

            case CMD_GrTexDetailControl:
                doGrTexDetailControl();
                break;

            case CMD_GrHints:
                doGrHints();
                break;

            case CMD_DisplayVESA:
                doDisplayVESA();
                break;

            case CMD_VESAStart:
                doVESAStart();
                break;

            case CMD_VESAEnd:
                doVESAEnd();
                break;

            case CMD_GuDrawTriangleWithClip:
                doGuDrawTriangleWithClip();
                break;

            case CMD_GuFogGenerateExp:
                doGuFogGenerateExp();
                break;

            case CMD_GuFogTableIndexToW:
                doGuFogTableIndexToW();
                break;

            case CMD_GrTexCombine:
                doGrTexCombine();
                break;

            case CMD_FMVOpen:
                doFMVOpen();
                break;

            case CMD_FMVPlay:
                doFMVPlay();
                break;

            default:
                //DisplayMessage(CString("Protocol error"));
                break;
            }
        }
    }
    catch(CString s)
    {
        if(!s.IsEmpty())
            AfxMessageBox((LPCSTR)s, MB_OK);
    }

    if(m_window_open)
	{
        m_wrapper.grSstWinClose();
	    ReleaseAppWindow();
        m_window_open = false;
	}

    m_wrapper.Unload();

    // Dynamically UNLOAD the C Virtual Device sample.
    //FIXME: race condition.

    m_pipe.Destroy();
}

void CGlideServer::Start(CGameDetails *game, CGldSrvDlg *dlg)
{
    m_dlg  = dlg;
    m_game = game;

    Execute();
}

void CGlideServer::DisplayMessage(CString &mess)
{
#if defined(NDEBUG)
    m_dlg->DisplayServerMessage(mess);
#else
    DebugMessage(mess);
#endif
}

void CGlideServer::DebugMessage(CString &mess)
{
    if(strlen((LPCSTR)mess) + (big_buf_ptr-big_buf) + 3 < 2*1024*1024)
    {
        strcpy(big_buf_ptr, (LPCSTR)mess);
        big_buf_ptr += strlen(big_buf_ptr);
        *big_buf_ptr++ = '\n';
    }
}

char CGlideServer::GetCharFromClient()
{
    char c;

    m_pipe.ReadPipe(&c, 1);

    return c;
}

CString CGlideServer::GetLineFromClient()
{
    char buf[1024];
    int i;
    bool going = true;

    for(i = 0; going && i < 1023; i++)
    {
        buf[i] = GetCharFromClient();
        if(buf[i] == 0)
            going = false;
    }

    buf[i] = 0;

    return CString(buf);
}

void CGlideServer::Shutdown()
{
    m_pipe.Wake();
}

void CGlideServer::doGuTexDownloadMipMap()
{
    GuNccTable table;
    char *data;
    int size, id;
    unsigned char md[16];

    id = GetIntFromClient();

    if(m_tracing)
    {
        CString s;

        s.Format("GuTexDownloadMipMap %d", id);
        DebugMessage(s);
    }

    size = GetIntFromClient();
    data = (char *) malloc(size);

    if(data == NULL)
        throw CString("Out of memory");

    m_pipe.ReadPipe(data, size);

    MD5((unsigned char *)data, size, md);

	if(m_tex_handler)
		m_tex_handler->GuTexDownloadMipMap(id, md, data);

#ifndef _DEBUG
    if(m_tex_check_holdoff == 0)
    {
#ifdef FINGERPRINTS
        FILE *f;

        f = fopen("fingerprints", "ab");
        if(f)
        {
            fwrite(md, 1, 16, f);
            fclose(f);
        }
        else
        {
            throw CString("Fingerprint file wont open");
        }
#else
#endif
    }
    else
    {
        m_tex_check_holdoff -= 1;
    }
#endif
    int table_present = GetIntFromClient();

    if(table_present)
        m_pipe.ReadPipe((char *)&table, sizeof(GuNccTable));

    if(id >= 0 && id < MAX_MM)
        m_wrapper.guTexDownloadMipMap(mipmap[id], data, table_present ? &table : NULL);

    free(data);
}

void CGlideServer::doGrBufferClear()
{
    if(m_tracing) DebugMessage(CString("GrBufferClear"));

    GrColor_t color = GetIntFromClient();
    GrAlpha_t alpha = GetIntFromClient();
    FxU16 depth = GetIntFromClient();
    m_wrapper.grBufferClear(color, alpha, depth);
}

void CGlideServer::doGuColorCombineFunction()
{
    GrColorCombineFnc_t func = GetIntFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GuColorCombineFunction"));
        CString s;
        s.Format("func = %d", func);
        DebugMessage(s);
    }

    m_glide_state.GuColorCombineFunction(func);

    m_wrapper.guColorCombineFunction(func);
}

void CGlideServer::doGuTexSource()
{
    if(m_tracing) DebugMessage(CString("GuTexSource"));

    int id = GetIntFromClient();

    if(m_tex_handler != NULL)
        m_tex_handler->GuTexSource(id);

    SetTexSource(id);
}

void CGlideServer::doGrDrawPolygonVertexList()
{
    int nverts, i;
    GrVertex *vlist;
    nverts = GetIntFromClient();
    vlist = (GrVertex *)malloc(nverts * sizeof(GrVertex));
    m_pipe.ReadPipe((char *)vlist, nverts * sizeof(GrVertex));

    for(i = 0; i < nverts; i++)
    {
        vlist[i].x *= m_xfactor;
        vlist[i].y *= m_yfactor;
    }

    if(m_tracing)
    {
        CString s;

        s.Format("x = %f, y = %f, z = %f", vlist[0].x, vlist[0].y, vlist[0].z);
        DebugMessage(s);

        s.Format("r = %f, g = %f, b = %f", vlist[0].r, vlist[0].g, vlist[0].b);
        DebugMessage(s);

        s.Format("ooz = %f, a = %f, oow = %f", vlist[0].ooz, vlist[0].a, vlist[0].oow);
        DebugMessage(s);

        s.Format("sow = %f, tow = %f, oow = %f", vlist[0].tmuvtx[0].sow, vlist[0].tmuvtx[0].tow, vlist[0].tmuvtx[0].oow);
        DebugMessage(s);
    }

    bool drawn = false;

    if(m_glide_state.IsTexturing() && m_tex_handler != NULL)
        drawn = m_tex_handler->GrDrawPolygonVertexList(nverts, vlist);

    if(!drawn)
        m_wrapper.grDrawPolygonVertexList(nverts, vlist);

    free(vlist);
}

void CGlideServer::doGrBufferSwap()
{
    if(m_tracing) DebugMessage(CString("GrBufferSwap"));

    if(m_blood_fix)
        m_blood_fix->Flush();

    m_delay_store.Render();

    m_wrapper.grBufferSwap(GetIntFromClient());
}

void CGlideServer::doGrTexFilterMode()
{
    GrChipID_t tmu = GetIntFromClient();
    GrTextureFilterMode_t minfilter_mode = GetIntFromClient();
    GrTextureFilterMode_t magfilter_mode = GetIntFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GrTexFilterMode"));
        CString s;
        s.Format("min = &d, mag = %d", minfilter_mode, magfilter_mode);
        DebugMessage(s);
    }

    if(m_game_record.forced_smoothing)
    {
        minfilter_mode = GR_TEXTUREFILTER_BILINEAR;
        magfilter_mode = GR_TEXTUREFILTER_BILINEAR;
    }

    m_wrapper.grTexFilterMode(tmu, minfilter_mode, magfilter_mode);
}

void CGlideServer::doGrSstWinClose()
{
    if(m_tracing) DebugMessage(CString("GrSstWinClose"));

    DisplayMessage(CString("WinClose"));

    if(m_window_open)
	{
        m_wrapper.grSstWinClose();
	    ReleaseAppWindow();
        m_window_open = false;
	}
}

void CGlideServer::doGrDepthBiasLevel()
{
    int dbias = GetIntFromClient();

    if(m_tracing) DebugMessage(CString("GrDepthBiasLevel"));

    m_wrapper.grDepthBiasLevel((signed short) dbias);
}

void CGlideServer::doGrColorCombine()
{
    GrCombineFunction_t function = GetIntFromClient();
    GrCombineFactor_t factor = GetIntFromClient();
    GrCombineLocal_t local = GetIntFromClient();
    GrCombineOther_t other = GetIntFromClient();
    FxBool invert = GetIntFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GrColorCombine"));
        CString s;
        s.Format("function = %x", function);
        DebugMessage(s);
        s.Format("factor = %x", factor);
        DebugMessage(s);
        s.Format("local = %x", local);
        DebugMessage(s);
        s.Format("other = %x", other);
        DebugMessage(s);
    }

    m_glide_state.GrColorCombine(function, factor, local, other, invert);

    m_wrapper.grColorCombine(function, factor, local, other, invert);
}

void CGlideServer::doGrAlphaBlendFunction()
{
    if(m_blood_fix)
        m_blood_fix->Flush();

    GrAlphaBlendFnc_t rgb_sf = GetIntFromClient();
    GrAlphaBlendFnc_t rgb_df = GetIntFromClient();
    GrAlphaBlendFnc_t alpha_sf = GetIntFromClient();
    GrAlphaBlendFnc_t alpha_df = GetIntFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GrAlphaBlendFunction"));
        CString s;
        s.Format("rgb_sf = %x", rgb_sf);
        DebugMessage(s);
        s.Format("rgb_df = %x", rgb_df);
        DebugMessage(s);
        s.Format("alpha_sf = %x", alpha_sf);
        DebugMessage(s);
        s.Format("alpha_df = %x", alpha_df);
        DebugMessage(s);
    }

    m_glide_state.GrAlphaBlendFunction(rgb_sf, rgb_df, alpha_sf, alpha_df);

    m_wrapper.grAlphaBlendFunction(rgb_sf, rgb_df, alpha_sf, alpha_df);
}

void CGlideServer::doGrDrawLine()
{
    float xdiff, ydiff, absxdiff, absydiff;
    int i;

    GrVertex v[4];
    m_pipe.ReadPipe((char *) &v[0], sizeof(GrVertex));
    m_pipe.ReadPipe((char *) &v[1], sizeof(GrVertex));

    v[2] = v[1];
    v[3] = v[0];

    xdiff = v[0].x - v[1].x;
    ydiff = v[0].y - v[1].y;

    absxdiff = (float) fabs(xdiff);
    absydiff = (float) fabs(ydiff);

    if(absxdiff > absydiff)
    {
        v[2].y += xdiff/absxdiff;
        v[3].y += xdiff/absxdiff;
    }
    else
    {
        v[2].x += ydiff/absydiff;
        v[3].x += ydiff/absydiff;
    }

    for(i = 0; i < 4; i++)
    {
        v[i].x *= m_xfactor;
        v[i].y *= m_yfactor;
    }

    if(m_tracing)
    {
        DebugMessage(CString("GrDrawLine"));
        CString s;

        s.Format("x = %f, y = %f, z = %f", v[0].x, v[0].y, v[0].z);
        DebugMessage(s);

        s.Format("r = %f, g = %f, b = %f", v[0].r, v[0].g, v[0].b);
        DebugMessage(s);

        s.Format("ooz = %f, a = %f, oow = %f", v[0].ooz, v[0].a, v[0].oow);
        DebugMessage(s);

        s.Format("sow = %f, tow = %f, oow = %f", v[0].tmuvtx[0].sow, v[0].tmuvtx[0].tow, v[0].tmuvtx[0].oow);
        DebugMessage(s);

        s.Format("x = %f, y = %f, z = %f", v[1].x, v[1].y, v[1].z);
        DebugMessage(s);

        s.Format("r = %f, g = %f, b = %f", v[1].r, v[1].g, v[1].b);
        DebugMessage(s);

        s.Format("ooz = %f, a = %f, oow = %f", v[1].ooz, v[1].a, v[1].oow);
        DebugMessage(s);

        s.Format("sow = %f, tow = %f, oow = %f", v[1].tmuvtx[0].sow, v[1].tmuvtx[0].tow, v[1].tmuvtx[0].oow);
        DebugMessage(s);
    }

    m_wrapper.grDrawPolygonVertexList(4, v);
}

void CGlideServer::StartTrace()
{
    big_buf_ptr = big_buf;
    m_tracing = true;
    //m_trace_on_winopen = true;
}

void CGlideServer::ShowPipeState()
{
    throw CString("Device does not support the requested API\n");
}

void CGlideServer::SaveTrace(const char *fname)
{
    FILE *f;

    m_tracing = false;
    m_trace_on_winopen = false;

    f = fopen(fname, "w");

    if(f)
    {
        fwrite(big_buf, 1, big_buf_ptr-big_buf, f);
        fclose(f);
    }
}

void CGlideServer::doGrConstantColorValue4()
{
    float a = GetFloatFromClient();
    float r = GetFloatFromClient();
    float g = GetFloatFromClient();
    float b = GetFloatFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GrConstantColorValue4"));

        CString s;

        s.Format("a = %f, r = %f, g = %f, b = %f", a, r, g, b);
        DebugMessage(s);
    }

    m_wrapper.grConstantColorValue4(a, r, g, b);
}

void CGlideServer::doGrGlideShutdown()
{
    DisplayMessage(CString("GlideShutdown"));

    if(m_window_open)
	{
        m_wrapper.grSstWinClose();
	    ReleaseAppWindow();
        m_window_open = false;
	}

    m_cd.Close();
}

HWND CGlideServer::GetDosWindow()
{
    return m_dos_window;
}

void CGlideServer::doGrAADrawLine()
{
    GrVertex v1, v2;

    if(m_tracing)
        DebugMessage(CString("GrAADrawLine@8"));

    m_pipe.ReadPipe((char *) &v1, sizeof(GrVertex));
    m_pipe.ReadPipe((char *) &v2, sizeof(GrVertex));
    v1.x *= m_xfactor;
    v1.y *= m_yfactor;
    v2.x *= m_xfactor;
    v2.y *= m_yfactor;
    // Avoid the murky-water bug.
    //m_wrapper.grDrawLine(&v1, &v2);
}

void CGlideServer::GetWindowFromApp()
{
    if(m_game_record.dos_full_screen && m_OSType == OS_W2K)
	    ::SendMessage(m_dos_window, WM_SYSKEYDOWN, VK_RETURN, 0x20000000);

    OpenWindow();

    if(m_game_record.full_screen)
        m_mouse.Capture(m_screen->m_hWnd);
}

void CGlideServer::ReleaseAppWindow()
{
    if(m_game_record.full_screen)
        m_mouse.Release();

    DestroyScreen();

    if(m_game_record.dos_full_screen && m_OSType == OS_W2K)
	    ::SendMessage(m_dos_window, WM_SYSKEYDOWN, VK_RETURN, 0x20000000);
}

void CGlideServer::SendConfirmation()
{
    if(m_tracing)
        DebugMessage(CString("Confirm"));

    int coord[3] = {0, 0, 0};

    if(m_game_record.full_screen)
        m_mouse.Poll(&(coord[0]), &(coord[1]), &(coord[2]));

    coord[0] *= 5;
    coord[1] *= 5;

    m_pipe.Write((char *) coord, 12);

    int mp3_playing = m_mp3_audio.IsPlaying() ? 1 : 0;

    m_pipe.Write((char *) &mp3_playing, 4);
}

OSType CGlideServer::GetOSType()
{
	return m_OSType;
}

void CGlideServer::SetTexSource(int id)
{
    if(id >= 0 && id < MAX_MM)
    {
        m_wrapper.guTexSource(mipmap[id]);

        m_glide_state.GuTexSource(mipmap[id]);
    }
}

void CGlideServer::doAudioStart()
{
    int drive    = GetIntFromClient();
    int start    = GetIntFromClient();
    int duration = GetIntFromClient();

    m_cd.Play(start, duration);

    /*
    CString s;

    s.Format("Start: drive = %x, start = %x, duration = %x", drive, start, duration);
    DisplayMessage(s);
    */
}

void CGlideServer::doAudioPoll()
{
    m_cd.Poll();

    /*
    CString s;
    s.Format("Poll");
    DisplayMessage(s);
    */
}

void CGlideServer::doMp3Play()
{
    int start    = GetIntFromClient();

    if(start)
        m_mp3_audio.Play((start/75 + 2)/60);
    else
        m_mp3_audio.Stop();
}

void CGlideServer::doIoctl()
{
    int tseg = GetIntFromClient();
    int toff = GetIntFromClient();

    CString s;
    s.Format("Ioctl %x:%x", tseg, toff);

    DisplayMessage(s);
}

void CGlideServer::doGrRenderBuffer()
{
    GrBuffer_t buffer = GetIntFromClient();
    if(m_tracing)
    {
        CString s;
        s.Format("GrRenderBuffer@4 %d", buffer);
        DebugMessage(s);
    }

    m_glide_state.GrRenderBuffer(buffer);

    m_wrapper.grRenderBuffer(buffer);
}

void CGlideServer::doGrTexDownloadMipMap()
{
    if(m_tracing)
        DebugMessage(CString("GrTexDownloadMipMap@16"));

    GrChipID_t tmu      = GetIntFromClient();
    FxU32 startAddress  = GetIntFromClient();
    FxU32 evenOdd       = GetIntFromClient();
    GrTexInfo info;

    m_pipe.ReadPipe((char *) &info, sizeof(GrTexInfo));

    int size = GetIntFromClient();

    info.data = malloc(size);
    m_pipe.ReadPipe((char *) info.data, size);

#ifndef _DEBUG
    if(size >= 2 * 1024)
    {
        unsigned char md[16];

        MD5((unsigned char *)info.data, size, md);

#ifdef FINGERPRINTS
        FILE *f;

        f = fopen("fingerprints", "ab");
        if(f)
        {
            fwrite(md, 1, 16, f);
            fclose(f);
        }
        else
        {
            throw CString("Fingerprint file wont open");
        }
#else
#endif
    }
#endif

    if(m_tex_cache)
        m_tex_cache->TexDownloadMipMap(startAddress + RESERVED_SPACE, evenOdd, &info);
    else
        m_wrapper.grTexDownloadMipMap(tmu, startAddress + RESERVED_SPACE, evenOdd, &info);

    free(info.data);
}

void CGlideServer::doGrTexSource()
{
    if(m_tracing)
        DebugMessage(CString("GrTexSource@16"));

    if(m_blood_fix)
        m_blood_fix->Flush();

    GrChipID_t tmu      = GetIntFromClient();
    FxU32 startAddress  = GetIntFromClient();
    FxU32 evenOdd       = GetIntFromClient();
    GrTexInfo info;

    m_pipe.ReadPipe((char *) &info, sizeof(GrTexInfo));

    m_glide_state.GrTexSource(startAddress, evenOdd, &info);

    if(m_tracing)
    {
        CString s;
        s.Format("fmt = %d", info.format);
        DebugMessage(s);
    }

    if(m_tex_cache)
        m_tex_cache->TexSource(startAddress + RESERVED_SPACE, evenOdd, &info);
    else
        m_wrapper.grTexSource(tmu, startAddress + RESERVED_SPACE, evenOdd, &info);
}

void CGlideServer::doGrDrawTriangle()
{
    if(m_tracing)
        DebugMessage(CString("GrDrawTriangle@12"));

    GrVertex v[3];
    int i;

    m_pipe.ReadPipe((char *) &v[0], sizeof(GrVertex));
    m_pipe.ReadPipe((char *) &v[1], sizeof(GrVertex));
    m_pipe.ReadPipe((char *) &v[2], sizeof(GrVertex));

    if(m_tracing)
    {
        for(i = 0; i < 3; i++)
        {
            CString s;
            s.Format("(%f,%f)", v[i].x, v[i].y);
            DebugMessage(s);
        }

        CString s;
        s.Format("col0 (%f,%f,%f. %f)", v[0].r, v[0].g, v[0].b, v[0].a);
        DebugMessage(s);
        s.Format("oow = %f, ooz = %f", v[0].oow, v[0].ooz);
        DebugMessage(s);
        s.Format("oow = %f, sow = %f, tow = %f", v[0].tmuvtx[0].oow, v[0].tmuvtx[0].sow, v[0].tmuvtx[0].tow);
        DebugMessage(s);
    }

    if(m_blood_fix && m_blood_fix->Accept(v))
        return;

    for(i = 0; i < 3; i++)
    {
        v[i].x -= ((int)(v[i].x / 2048)) * 2048.0f;
        v[i].y -= ((int)(v[i].y / 2048)) * 2048.0f;
        if(m_tracing)
        {
            CString s;
            s.Format("(%f,%f)", v[i].x, v[i].y);
            DebugMessage(s);
        }
        v[i].x *= m_xfactor;
        v[i].y *= m_yfactor;
    }

    if(m_tex_cache && m_glide_state.IsTexturing())
        m_tex_cache->PreRender(3, v);

    m_wrapper.grDrawPolygonVertexList(3, v);
}

void CGlideServer::doGrDisableAllEffects()
{
    if(m_tracing)
        DebugMessage(CString("GrDisableAllEffects@0"));
    m_wrapper.grDisableAllEffects();
}

void CGlideServer::doGrTexMipMapMode()
{
    if(m_tracing)
        DebugMessage(CString("GrTexMipMapMode@12"));

    GrChipID_t     tmu      = GetIntFromClient();
    GrMipMapMode_t mode     = GetIntFromClient();
    FxBool         lodBlend = GetIntFromClient();

    m_wrapper.grTexMipMapMode(tmu, mode, lodBlend);
}

void CGlideServer::doGrTexLodBiasValue()
{
    if(m_tracing)
        DebugMessage(CString("GrTexLodBiasValue@8"));

    int a = GetIntFromClient();
    float b = GetFloatFromClient();

    m_wrapper.grTexLodBiasValue(a, b);
}

void CGlideServer::doGrTexClampMode()
{
    if(m_tracing)
        DebugMessage(CString("GrTexClampMode@12"));

    GrChipID_t           a = GetIntFromClient();
    GrTextureClampMode_t b = GetIntFromClient();
    GrTextureClampMode_t c = GetIntFromClient();

    m_glide_state.GrTexClampMode(b, c);

    m_wrapper.grTexClampMode(a, b, c);
}

void CGlideServer::doGrAlphaCombine()
{
    if(m_tracing)
        DebugMessage(CString("GrAlphaCombine@20"));

    if(m_blood_fix)
        m_blood_fix->Flush();

    GrCombineFunction_t a = GetIntFromClient();
    GrCombineFactor_t   b = GetIntFromClient();
    GrCombineLocal_t    c = GetIntFromClient();
    GrCombineOther_t    d = GetIntFromClient();
    FxBool              e = GetIntFromClient();

    if(m_tracing)
    {
        CString s;
        s.Format("fun = %d, fac = %d, loc = %d, oth = %d, inv = %d",
                 a, b, c, d, e);
        DebugMessage(s);
    }

    m_glide_state.GrAlphaCombine(a, b, c, d, e);

    m_wrapper.grAlphaCombine(a, b, c, d, e);
}

void CGlideServer::doGrFogMode()
{
    if(m_tracing)
        DebugMessage(CString("GrFogMode@4"));

    GrFogMode_t a = GetIntFromClient();

    m_glide_state.GrFogMode(a);

    m_wrapper.grFogMode(a);
}

void CGlideServer::doGrAlphaTestFunction()
{
    int a = GetIntFromClient();

    if(m_tracing)
    {
        CString s;
        s.Format("GrAlphaTestFunction@4 %d", a);
        DebugMessage(s);
    }

    m_wrapper.grAlphaTestFunction(a);
}

void CGlideServer::doGrAlphaTestReferenceValue()
{
    int a = GetIntFromClient();

    if(m_tracing)
    {
        CString s;
        s.Format("GrAlphaTestReferenceValue@4 %d", a);
        DebugMessage(s);
    }

    m_wrapper.grAlphaTestReferenceValue(a);
}

void CGlideServer::doGrClipWindow()
{
    if(m_tracing)
        DebugMessage(CString("GrClipWindow@16"));

    int a = (int)(GetIntFromClient() * m_xfactor);
    int b = (int)(GetIntFromClient() * m_yfactor);
    int c = (int)(GetIntFromClient() * m_xfactor);
    int d = (int)(GetIntFromClient() * m_yfactor);

    m_wrapper.grClipWindow(a, b, c, d);
}

void CGlideServer::doDisplayLFB()
{
    if(m_tracing)
        DebugMessage(CString("DisplayLFB"));

    GrBuffer_t buffer = GetIntFromClient();
    unsigned short *lfb;
    lfb = (unsigned short *) malloc(640*480*sizeof(unsigned short));

    m_pipe.ReadPipe((char *)lfb, 640*480*sizeof(unsigned short));

    m_glide_state.TemporaryRenderBuffer(buffer);

    displayLFBarea(GR_TEXFMT_RGB_565, lfb,   0,   0, 210, 240, 1);
    displayLFBarea(GR_TEXFMT_RGB_565, lfb, 210,   0, 220, 240, 1);
    displayLFBarea(GR_TEXFMT_RGB_565, lfb, 430,   0, 210, 240, 1);
    displayLFBarea(GR_TEXFMT_RGB_565, lfb,   0, 240, 210, 240, 1);
    displayLFBarea(GR_TEXFMT_RGB_565, lfb, 210, 240, 220, 240, 1);
    displayLFBarea(GR_TEXFMT_RGB_565, lfb, 430, 240, 210, 240, 1);

    m_glide_state.RestoreRenderBuffer();

    free(lfb);
}

#define BLUE_SCREEN (0x7ff)

void CGlideServer::displayLFBarea(GrTextureFormat_t fmt, unsigned short *lfb, int xo, int yo, int xsize, int ysize, int fac)
{
    unsigned short *lfb_area, *src, *tgt;
    int x, y;

    lfb_area = (unsigned short *) malloc(256*256*sizeof(unsigned short));

    src = lfb + yo * 640 + xo * fac;
    tgt = lfb_area;

    switch(fmt)
    {
    case GR_TEXFMT_RGB_565:
        for(y = 0; y < 256; y++)
        {
            for(x = 0; x < 256; x++)
            {
                int lx = MIN(x, xsize-1) * fac;

                tgt[x] = (src[lx] != BLUE_SCREEN) ? (0x8000 |((src[lx] & 0xFFC0)>>1) | (src[lx] & 0x1f)) : 0;
            }

            if(y < ysize - 1)
                src += 640;

            tgt += 256;
        }
        break;

    case GR_TEXFMT_ARGB_1555:
        for(y = 0; y < 256; y++)
        {
            for(x = 0; x < 256; x++)
            {
                int lx = MIN(x, xsize-1) * fac;

                tgt[x] = (src[lx] | 0x8000);
            }

            if(y < ysize - 1)
                src += 640;

            tgt += 256;
        }
        break;
    }

    displayLFBtex(lfb_area, xo, yo, xsize, ysize, GR_LOD_256, fac);

    free(lfb_area);
}

void CGlideServer::doDisplayLFBPart()
{
    if(m_tracing)
        DebugMessage(CString("DisplayLFBPart"));

    unsigned short *lfb_area, *tex, *src, *tgt;
    int x, y;
    GrBuffer_t buffer = GetIntFromClient();
    int xmin = GetIntFromClient();
    int ymin = GetIntFromClient();
    int xsize = GetIntFromClient();
    int ysize = GetIntFromClient();
    int maxsize, texsize;
    GrLOD_t lod;

    maxsize = MAX(xsize, ysize);
    if(maxsize > 128)
    {
        lod = GR_LOD_256;
        texsize = 256;
    }
    else if(maxsize > 64)
    {
        lod = GR_LOD_128;
        texsize = 128;
    }
    else if(maxsize > 32)
    {
        lod = GR_LOD_64;
        texsize = 64;
    }
    else if(maxsize > 16)
    {
        lod = GR_LOD_32;
        texsize = 32;
    }
    else
    {
        lod = GR_LOD_16;
        texsize = 16;
    }

    lfb_area = (unsigned short *)malloc(xsize * ysize * sizeof(short));

    m_pipe.ReadPipe((char *) lfb_area, xsize * ysize * sizeof(short));

    tex = (unsigned short *)malloc(texsize*texsize*sizeof(short));

    src = lfb_area;
    tgt = tex;

    for(y = 0; y < texsize; y++)
    {
        for(x = 0; x < texsize; x++)
        {
            tgt[x] = (x < xsize && y < ysize && src[x] != BLUE_SCREEN) ? (0x8000 |
                                                                          ((src[x] & 0xFFC0)>>1) |
                                                                          (src[x] & 0x1f))
                                                                       : 0;
        }

        src += xsize;
        tgt += texsize;
    }

    free(lfb_area);

    m_glide_state.TemporaryRenderBuffer(buffer);

    displayLFBtex(tex, xmin, ymin, xsize, ysize, lod, 1);

    m_glide_state.RestoreRenderBuffer();

    free(tex);
}

void CGlideServer::displayLFBtex(unsigned short *tex, int xo, int yo, int xsize, int ysize, GrLOD_t lod, int fac)
{
    GrTexInfo info;
    GrVertex v[4];

    info.smallLod    = lod;
    info.largeLod    = lod;
    info.aspectRatio = GR_ASPECT_1x1;
    info.format      = GR_TEXFMT_ARGB_1555;
    info.data        = tex;

    m_wrapper.grTexDownloadMipMap(GR_TMU0, 8, GR_MIPMAPLEVELMASK_BOTH, &info);
    m_wrapper.grTexSource(GR_TMU0, 8, GR_MIPMAPLEVELMASK_BOTH, &info);

    m_wrapper.grTexClampMode(GR_TMU0, GR_TEXTURECLAMP_CLAMP, GR_TEXTURECLAMP_CLAMP);

    m_wrapper.grColorCombine(GR_COMBINE_FUNCTION_SCALE_OTHER,
                   GR_COMBINE_FACTOR_ONE,
                   GR_COMBINE_LOCAL_NONE,
                   GR_COMBINE_OTHER_TEXTURE,
                   FXFALSE);

    m_wrapper.grAlphaCombine(GR_COMBINE_FUNCTION_SCALE_OTHER,
                   GR_COMBINE_FACTOR_ONE,
                   GR_COMBINE_LOCAL_NONE,
                   GR_COMBINE_OTHER_TEXTURE,
                   FXFALSE);

    m_wrapper.grAlphaBlendFunction(GR_BLEND_SRC_ALPHA,
                         GR_BLEND_ONE_MINUS_SRC_ALPHA,
                         GR_BLEND_ONE,
                         GR_BLEND_ZERO);

    m_wrapper.grFogMode(GR_FOG_DISABLE);

    m_wrapper.grDepthBufferFunction(GR_CMP_ALWAYS);

    m_wrapper.grDepthMask(0);

    v[0].x = m_xfactor * fac * (float)xo;
    v[0].y = m_yfactor * fac * (float)yo;
    v[0].oow = 1.0f;
    v[0].tmuvtx[0].oow = 1.0f;
    v[0].tmuvtx[0].sow = 0.0f;
    v[0].tmuvtx[0].tow = 0.0f;

    v[1].x = m_xfactor * fac * (float)(xo + xsize);
    v[1].y = m_yfactor * fac * (float)yo;
    v[1].oow = 1.0f;
    v[1].tmuvtx[0].oow = 1.0f;
    v[1].tmuvtx[0].sow = (float)(xsize<<lod);
    v[1].tmuvtx[0].tow = 0.0f;

    v[2].x = m_xfactor * fac * (float)(xo + xsize);
    v[2].y = m_yfactor * fac * (float)(yo + ysize);
    v[2].oow = 1.0f;
    v[2].tmuvtx[0].oow = 1.0f;
    v[2].tmuvtx[0].sow = (float)(xsize<<lod);
    v[2].tmuvtx[0].tow = (float)(ysize<<lod);

    v[3].x = m_xfactor * fac * (float)xo;
    v[3].y = m_yfactor * fac * (float)(yo + ysize);
    v[3].oow = 1.0f;
    v[3].tmuvtx[0].oow = 1.0f;
    v[3].tmuvtx[0].sow = 0.0f;
    v[3].tmuvtx[0].tow = (float)(ysize<<lod);

    m_wrapper.grDrawPolygonVertexList(4, v);

    m_glide_state.RestoreDepthBuffer();

    m_glide_state.RestoreTexSource();

    m_glide_state.RestoreFogMode();

    m_glide_state.RestoreColorCombine();

    m_glide_state.RestoreAlphaCombine();

    m_glide_state.RestoreAlphaBlendFunction();
}

void CGlideServer::doGrCullMode()
{
    if(m_tracing)
        DebugMessage(CString("GrCullMode"));

    m_wrapper.grCullMode(GetIntFromClient());
}

void CGlideServer::doGrFogColorValue()
{
    if(m_tracing)
        DebugMessage(CString("GrFogColorValue"));

    if(m_blood_fix)
        m_blood_fix->Flush();

    int v = GetIntFromClient();
    m_wrapper.grFogColorValue(v);
}

void CGlideServer::doConvertAndDownloadRLE()
{
    if(m_tracing)
        DebugMessage(CString("ConvertAndDownloadRLE@16"));

    GrChipID_t tmu      = GetIntFromClient();
    FxU32 startAddress  = GetIntFromClient();
    FxU32 evenOdd       = GetIntFromClient();
    GrTexInfo info;

    m_pipe.ReadPipe((char *) &info, sizeof(GrTexInfo));

    int size = GetIntFromClient();

    info.data = malloc(size);
    m_pipe.ReadPipe((char *) info.data, size);

    m_wrapper.grTexDownloadMipMap(tmu, startAddress + RESERVED_SPACE, evenOdd, &info);

    free(info.data);
}

void CGlideServer::doGrFogTable()
{
    if(m_tracing)
        DebugMessage(CString("GrFogTable@8"));

    GrFog_t fog[GR_FOG_TABLE_SIZE];
    m_pipe.ReadPipe((char *)fog, GR_FOG_TABLE_SIZE * sizeof(GrFog_t));
    m_wrapper.grFogTable(fog);
}

void CGlideServer::doGrColorMask()
{
    if(m_tracing)
        DebugMessage(CString("GrColorMask@8"));

    FxBool rgb = GetIntFromClient();
    FxBool a = GetIntFromClient();
    m_wrapper.grColorMask(rgb, a);
}

void CGlideServer::doGrLFBLock()
{
    if(m_tracing)
        DebugMessage(CString("GrLFBLock@24"));

    GrBuffer_t buffer = GetIntFromClient();
    GrLfbInfo_t info;
    WORD *buf = new WORD[640 * 480];
    FxU32 x, y;
    FxU32 xinc, yinc, xacc, yacc;
    WORD *src, *dest;

    info.size = sizeof(GrLfbInfo_t);

    m_wrapper.grLfbLock(GR_LFB_READ_ONLY, buffer, GR_LFBWRITEMODE_565,
        GR_ORIGIN_UPPER_LEFT, FXFALSE, &info);

    yacc = 0;
    xinc = m_server_width * 0x10000 / 640;
    yinc = m_server_height * 0x10000 / 480;

    for(y = 0; y < 480; y++)
    {
        src = ((WORD *)info.lfbPtr) + (yacc>>16) * (info.strideInBytes>>1);
        dest = buf + y * 640;

        xacc = 0;

        for(x = 0; x < 640; x++)
        {
            dest[x] = src[xacc>>16];

            xacc += xinc;
        }

        yacc += yinc;
    }

    m_wrapper.grLfbUnlock(GR_LFB_READ_ONLY, buffer);

    m_pipe.Write((char *) buf, 640*480*2);

    delete [] buf;

    m_tex_check_holdoff = 8;
}

void CGlideServer::doGrLFBConstantAlpha()
{
    if(m_tracing)
        DebugMessage(CString("GrLFBConstantAlpha@8"));

    int a = GetIntFromClient();

    m_wrapper.grLfbConstantAlpha(a);
}

void CGlideServer::doGrLFBConstantDepth()
{
    if(m_tracing)
        DebugMessage(CString("GrLFBConstantDepth@8"));

    int a = GetIntFromClient();

    m_wrapper.grLfbConstantDepth(a);
}

void CGlideServer::doGrTexDetailControl()
{
    if(m_tracing)
        DebugMessage(CString("GrTexDetailControl@16"));
}

void CGlideServer::doGrHints()
{
    if(m_tracing)
        DebugMessage(CString("GrHints@8"));

    int a = GetIntFromClient();
    int b = GetIntFromClient();

    m_wrapper.grHints(a, b);
}

void CGlideServer::doDisplayVESA()
{
    if(m_tracing)
        DebugMessage(CString("DisplayVESA"));

    int scale = GetIntFromClient();
    GrTextureFormat_t fmt = GetIntFromClient();
    unsigned short *lfb;

    if(scale)
    {
        lfb = (unsigned short *) malloc(640*240*sizeof(unsigned short));

        m_pipe.ReadPipe((char *)lfb, 640*240*sizeof(unsigned short));
    }
    else
    {
        lfb = (unsigned short *) malloc(640*480*sizeof(unsigned short));

        m_pipe.ReadPipe((char *)lfb, 640*480*sizeof(unsigned short));
    }

    m_glide_state.TemporaryRenderBuffer(GR_BUFFER_BACKBUFFER);

    m_wrapper.grTexFilterMode(0, GR_TEXTUREFILTER_BILINEAR, GR_TEXTUREFILTER_BILINEAR);

    if(scale)
    {
        displayLFBarea(fmt, lfb,   0,   0, 160, 240, 2);
        displayLFBarea(fmt, lfb, 160,   0, 160, 240, 2);
    }
    else
    {
        displayLFBarea(fmt, lfb,   0,   0, 210, 240, 1);
        displayLFBarea(fmt, lfb, 210,   0, 220, 240, 1);
        displayLFBarea(fmt, lfb, 430,   0, 210, 240, 1);
        displayLFBarea(fmt, lfb,   0, 240, 210, 240, 1);
        displayLFBarea(fmt, lfb, 210, 240, 220, 240, 1);
        displayLFBarea(fmt, lfb, 430, 240, 210, 240, 1);
    }

    m_wrapper.grBufferSwap(0);

    m_glide_state.RestoreRenderBuffer();

    free(lfb);
}

void CGlideServer::doVESAStart()
{
    if(m_window_open)
	{
        m_wrapper.grSstWinClose();
	    ReleaseAppWindow();
        m_window_open = false;
	}

    m_game->GetGameRecord(m_game_record);
    m_wrapper.Load(m_game_record.driver.Path());

    try
    {
        m_wrapper.setConfig(m_game_record.ConfigFlags());
    }
    catch(CString s)
    {
        DisplayMessage(s);
    }

    m_wrapper.grGlideInit();
    m_dos_window = ::GetForegroundWindow();
    m_vesa_active = true;
}

void CGlideServer::doVESAEnd()
{
    if(m_window_open)
	{
        m_wrapper.grSstWinClose();
	    ReleaseAppWindow();
        m_window_open = false;
	}

    m_vesa_active = false;
}

void CGlideServer::doGuDrawTriangleWithClip()
{
    if(m_tracing)
        DebugMessage(CString("GuDrawTriangleWithClip@12"));

    GrVertex v[3];
    int i;

    m_pipe.ReadPipe((char *) &v[0], sizeof(GrVertex));
    m_pipe.ReadPipe((char *) &v[1], sizeof(GrVertex));
    m_pipe.ReadPipe((char *) &v[2], sizeof(GrVertex));

    for(i = 0; i < 3; i++)
    {
        if(m_tracing)
        {
            CString s;
            s.Format("(%f,%f)", v[i].x, v[i].y);
            DebugMessage(s);
        }
        v[i].x *= m_xfactor;
        v[i].y *= m_yfactor;
    }

    if(m_tracing)
    {
        CString s;
        s.Format("col0 (%f,%f,%f. %f)", v[0].r, v[0].g, v[0].b, v[0].a);
        DebugMessage(s);
        s.Format("oow = %f, ooz = %f", v[0].oow, v[0].ooz);
        DebugMessage(s);
        s.Format("oow = %f, sow = %f, tow = %f", v[0].tmuvtx[0].oow, v[0].tmuvtx[0].sow, v[0].tmuvtx[0].tow);
        DebugMessage(s);
    }

    m_wrapper.guDrawPolygonVertexListWithClip(3, v);
}

void CGlideServer::doGuFogGenerateExp()
{
    float density = GetFloatFromClient();
    GrFog_t table[GR_FOG_TABLE_SIZE];

    if(m_tracing)
    {
        CString s;
        s.Format("GuFogGenerateExp %f", density);
        DebugMessage(s);
    }

    m_wrapper.guFogGenerateExp(table, density);

    m_pipe.Write((char *) table, GR_FOG_TABLE_SIZE * sizeof(GrFog_t));

    if(m_tracing)
    {
        for(int i = 0; i < GR_FOG_TABLE_SIZE; i += 4)
        {
            CString s;
            s.Format("%02x %02x %02x %02x", table[i], table[i+1], table[i+2], table[i+3]);
            DebugMessage(s);
        }
    }
}

void CGlideServer::doGuFogTableIndexToW()
{
    if(m_tracing)
        DebugMessage(CString("GuFogTableIndexToW"));

    int     i = GetIntFromClient();
    float res = (float)pow(2.0, 3.0+(double)(i>>2)) / (8-(i&3));

    m_pipe.Write((char *)&res, sizeof(res));
}

void CGlideServer::doGrTexCombine()
{
    if(m_tracing)
        DebugMessage(CString("GrTexCombine"));

    GrChipID_t tmu = GetIntFromClient();
    GrCombineFunction_t rgb_function = GetIntFromClient();
    GrCombineFactor_t rgb_factor = GetIntFromClient();
    GrCombineFunction_t alpha_function = GetIntFromClient();
    GrCombineFactor_t alpha_factor = GetIntFromClient();
    FxBool rgb_invert = GetIntFromClient();
    FxBool alpha_invert = GetIntFromClient();

    m_wrapper.grTexCombine(tmu, rgb_function, rgb_factor,
        alpha_function, alpha_factor,
        rgb_invert, alpha_invert);
}

void CGlideServer::doFMVOpen()
{
    CString path;
    CString msg;
    int     i;

    if(m_tracing)
        DebugMessage(CString("FMVOpen"));

    path = GetLineFromClient();
    i = path.ReverseFind('\\');
    m_fmv = (i == -1 ? path
                     : path.Mid(i+1));

    i = m_fmv.ReverseFind('.');
    if(i != -1)
        m_fmv = m_fmv.Left(i);

    m_fmv += ".avi";

    msg.Format("Open %s", (LPCSTR)m_fmv);

    DisplayMessage(msg);
}

void CGlideServer::doFMVPlay()
{
    char         c = 0;
    CString      path;

    if(m_tracing)
        DebugMessage(CString("FMVPlay"));

    DisplayMessage(CString("Play FMV"));

    path.Format("FMV\\%s", (LPCSTR)m_fmv);

    doGrSstWinClose();
    OpenWindow();
    if(m_game->GetFullScreen())
    {
        int xsize = ::GetSystemMetrics(SM_CXSCREEN);
        int ysize = ::GetSystemMetrics(SM_CYSCREEN);

        m_screen->MoveWindow(0, 0, xsize, ysize);
    }

    m_screen->SetFMVMode(true);

    {
        CMoviePlayer player;
        HRESULT      hr;
        HANDLE       event;
        bool         is_playing = true;

        hr = player.Create(m_screen);
        if(FAILED(hr))
            goto exit;

        hr = player.Play(path);
        if(FAILED(hr))
            goto exit;

        hr = player.GetEvent(&event);
        if(FAILED(hr))
            goto exit;

        while(is_playing)
        {
            MSG  msg;
            bool state_change;

            while(::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
                AfxGetApp()->PumpMessage();

            if(m_screen->FMVIsDismissed())
            {
                is_playing = false;
            }
            else
            {
                state_change = (MsgWaitForMultipleObjects(1, &event, false, INFINITE, QS_ALLINPUT) == 0);

                if(state_change)
                    is_playing = player.IsPlaying();
            }
        }
exit:
        if(FAILED(hr) && hr != 0x80040216)
        {
            AfxMessageBox(IDS_FMV_FAILED, MB_OK);
        }
    }

    m_screen->SetFMVMode(false);

    DestroyScreen();

    m_pipe.Write(&c, 1);
}

void CGlideServer::OpenWindow()
{
    int xsize, ysize;

    CResolution &res = m_game->GetResolution();
    m_server_resolution = res;
    CResolution::Size size = res;
    xsize = size.width;
    ysize = size.height;

    if(m_game->GetFullScreen())
        m_screen = new CScreenDlg(IDD_SCREEN_FULL);
    else
        m_screen = new CScreenDlg(IDD_SCREEN);

    m_screen->Create(this);

    if(m_game->GetFullScreen())
    {
        if (m_game->GetNoModeChange())
        {
            int x, y;
            GetScreenMode(&x, &y);
            m_screen->MoveWindow(0, 0, x, y);
        }
        else
        {
            m_screen->MoveWindow(0, 0, xsize, ysize);
        }
    }
    else
    {
        RECT inside, outside;

        m_screen->GetWindowRect(&outside);
        m_screen->GetClientRect(&inside);
        int adjust = (outside.bottom - outside.top) - (inside.bottom - inside.top);

        m_screen->MoveWindow(outside.left, outside.top, xsize, ysize + adjust);
    }

	if(GetOSType() == OS_W2K)
        m_screen->SetWindowPos(&CWnd::wndTopMost, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);

    m_screen->ShowWindow(SW_NORMAL);

    if(m_game->GetFullScreen())
    {
        if (!m_game->GetNoModeChange())
            SetScreenMode(xsize, ysize);

        HideCursor();
    }

    if(m_game->GetFullScreen())
        SetWindowLong(m_screen->m_hWnd, GWL_STYLE, WS_POPUP|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS);
    else
        SetWindowLong(m_screen->m_hWnd, GWL_STYLE, WS_VISIBLE|WS_CAPTION|WS_CLIPCHILDREN|WS_CLIPSIBLINGS);
}

void CGlideServer::DestroyScreen()
{
    if(m_screen)
    {
        m_screen->DestroyWindow();
        delete m_screen;
        m_screen = NULL;
        //::ShowWindow(server->GetDosWindow(), SW_NORMAL);
    }

    RestoreCursor();

    ResetScreenMode();
}

void CGlideServer::GetScreenMode(int *xsize, int *ysize)
{
    *xsize = ::GetSystemMetrics(SM_CXSCREEN);
    *ysize = ::GetSystemMetrics(SM_CYSCREEN);
}

void CGlideServer::SetScreenMode(int xsize, int ysize)
{
    HDC hdc;
    DWORD bits_per_pixel;
    bool found;
    DEVMODE DevMode;

    hdc = ::GetDC(m_screen->m_hWnd);
    bits_per_pixel = GetDeviceCaps(hdc, BITSPIXEL);
    ::ReleaseDC(m_screen->m_hWnd, hdc);

    found = false;
    DevMode.dmSize = sizeof(DEVMODE);

    for(int i = 0; !found && EnumDisplaySettings(NULL, i, &DevMode) != FALSE; i++ )
    {
        if((DevMode.dmPelsWidth == (DWORD)xsize) && (DevMode.dmPelsHeight == (DWORD)ysize) &&
            (DevMode.dmBitsPerPel == bits_per_pixel))
            found = true;
    }

    if(!found || ChangeDisplaySettings( &DevMode, CDS_RESET|CDS_FULLSCREEN ) != DISP_CHANGE_SUCCESSFUL)
        AfxMessageBox(IDS_BAD_SCREEN_MODE, MB_OK);
}

void CGlideServer::ResetScreenMode()
{
    ChangeDisplaySettings(NULL, 0);
}

void CGlideServer::HideCursor()
{
    while(ShowCursor(TRUE) < 0)
        ;
    while(ShowCursor(FALSE) >= 0)
        ;
}

void CGlideServer::RestoreCursor()
{
    while(ShowCursor(FALSE) >= 0)
        ;
    while(ShowCursor(TRUE) < 0)
        ;
}

void CGlideServer::doGrGlideInit()
{
    char c;

    if(m_tracing) DebugMessage(CString("GrGlideInit"));

    if(GetIntFromClient() != VERSION)
        throw CString("Client/Server version mismatch");

    client_dir = GetStringFromClient();

    if(m_window_open)
	{
        m_wrapper.grSstWinClose();
	    ReleaseAppWindow();
        m_window_open = false;
	}

    m_game->GetGameRecord(m_game_record);

    m_wrapper.Load(m_game_record.driver.Path());

    try
    {
        m_wrapper.setConfig(m_game_record.ConfigFlags());

        if(m_game_record.driver == CGlideDriver::GLIDE_DRIVER_D3D)
            m_wrapper.setStereoMap(&((const StereoMap)m_game_record.stereo));
    }
    catch(CString s)
    {
        DisplayMessage(s);
    }

    m_glide_state.SetShadowHack(m_game_record.shadow_hack != FALSE);

    m_wrapper.grGlideInit();

    if(!m_vesa_active)
        m_dos_window = ::GetForegroundWindow();

    c = m_game_record.control_judder_fix;
    m_pipe.Write(&c, 1);

    c = m_game_record.disable_lfb_reads;
    m_pipe.Write(&c, 1);

    c = m_game_record.delay_lfb_writes;
    m_pipe.Write(&c, 1);

    c = (m_game_record.dos_emu != CDosEmu::DOSEMU_DOSBOX)
                     ? m_game_record.redbook_emulation
                     : 0;

    m_pipe.Write(&c, 1);

	m_window_open = false;
}

void CGlideServer::doGrSstQueryHardware()
{
    GrHwConfiguration hwconfig;
    GrVoodooConfig_t *vdConfig;
    GrTMUConfig_t *tmuConfig;


    hwconfig.num_sst = 0;
    m_wrapper.grSstQueryHardware(&hwconfig);

    if(m_tracing)
    {
        DebugMessage(CString("GrSstQueryHardware"));

        CString s;
        s.Format("num_sst = %d", hwconfig.num_sst);
        DebugMessage(s);

        s.Format("type = %d", hwconfig.SSTs[0].type);
        DebugMessage(s);
        vdConfig = &(hwconfig.SSTs[0].sstBoard.VoodooConfig);

        s.Format("fbRam = %d", vdConfig->fbRam);
        DebugMessage(s);

        s.Format("fbiRev = %d", vdConfig->fbiRev);
        DebugMessage(s);

        s.Format("nTexelfx = %d", vdConfig->nTexelfx);
        DebugMessage(s);

        tmuConfig = &(vdConfig->tmuConfig[0]);

        s.Format("tmuRev = %d", tmuConfig->tmuRev);
        DebugMessage(s);

        s.Format("tmuRamv = %d", tmuConfig->tmuRam);
        DebugMessage(s);
    }
}

void CGlideServer::doGrSstSelect()
{
    DebugMessage(CString("GrSstSelect"));

    m_wrapper.grSstSelect(0);
}

void CGlideServer::doGrSstWinOpen()
{
    char *cptr;
    CString s;

    if(m_window_open)
	{
        m_wrapper.grSstWinClose();
	    ReleaseAppWindow();
        m_window_open = false;
	}

    if(m_trace_on_winopen)
        m_tracing = true;

    GetWindowFromApp();
	m_window_open = true;

   /*
    * Reinitialise the debug message store
    */
    big_buf_ptr = big_buf;

    GrScreenResolution_t screen_resolution;
    GrScreenResolution_t server_resolution;
    GrScreenRefresh_t    refresh_rate;
    GrColorFormat_t      color_format;
    GrOriginLocation_t   origin_location;
    int                  nColBuffers;
    int                  nAuxBuffers;

    DebugMessage(CString("GrSstWinOpen"));

    server_resolution = m_server_resolution;

    screen_resolution = GetIntFromClient();
    refresh_rate      = GetIntFromClient();
    color_format      = GetIntFromClient();
    origin_location   = GetIntFromClient();
    nColBuffers       = GetIntFromClient();
    nAuxBuffers       = GetIntFromClient();

    switch(screen_resolution)
    {
    case GR_RESOLUTION_320x200:   cptr = "320x200";   m_game_width =  320; m_game_height =  200; break;
    case GR_RESOLUTION_320x240:   cptr = "320x240";   m_game_width =  320; m_game_height =  240; break;
    case GR_RESOLUTION_400x256:   cptr = "400x256";   m_game_width =  400; m_game_height =  256; break;
    case GR_RESOLUTION_512x384:   cptr = "512x384";   m_game_width =  512; m_game_height =  384; break;
    case GR_RESOLUTION_640x200:   cptr = "640x200";   m_game_width =  640; m_game_height =  200; break;
    case GR_RESOLUTION_640x350:   cptr = "640x350";   m_game_width =  640; m_game_height =  350; break;
    case GR_RESOLUTION_640x400:   cptr = "640x400";   m_game_width =  640; m_game_height =  400; break;
    case GR_RESOLUTION_640x480:   cptr = "640x480";   m_game_width =  640; m_game_height =  480; break;
    case GR_RESOLUTION_800x600:   cptr = "800x600";   m_game_width =  800; m_game_height =  600; break;
    case GR_RESOLUTION_960x720:   cptr = "960x720";   m_game_width =  960; m_game_height =  720; break;
    case GR_RESOLUTION_856x480:   cptr = "856x480";   m_game_width =  856; m_game_height =  480; break;
    case GR_RESOLUTION_512x256:   cptr = "512x256";   m_game_width =  512; m_game_height =  256; break;
    case GR_RESOLUTION_1024x768:  cptr = "1024x768";  m_game_width = 1024; m_game_height =  768; break;
    case GR_RESOLUTION_1280x1024: cptr = "1280x1024"; m_game_width = 1280; m_game_height = 1024; break;
    case GR_RESOLUTION_1600x1200: cptr = "1600x1200"; m_game_width = 1600; m_game_height = 1200; break;
    case GR_RESOLUTION_400x300:   cptr = "400x300";   m_game_width =  400; m_game_height =  300; break;
    case GR_RESOLUTION_NONE: throw CString("Client asked for resolution NONE");
    default: throw CString("Client asked for unknown resolution");
    };

    s.Format("Resolution = %s", cptr);
    DisplayMessage(s);

    switch(refresh_rate)
    {
    case GR_REFRESH_60Hz:  cptr = "60Hz";  break;
    case GR_REFRESH_70Hz:  cptr = "70Hz";  break;
    case GR_REFRESH_72Hz:  cptr = "72Hz";  break;
    case GR_REFRESH_75Hz:  cptr = "75Hz";  break;
    case GR_REFRESH_80Hz:  cptr = "80Hz";  break;
    case GR_REFRESH_90Hz:  cptr = "90Hz";  break;
    case GR_REFRESH_100Hz: cptr = "100Hz"; break;
    case GR_REFRESH_85Hz:  cptr = "85Hz";  break;
    case GR_REFRESH_120Hz: cptr = "120Hz"; break;
    default: cptr ="<unknown>"; break;
    };

    s.Format("Refresh rate = %s", cptr);
    DisplayMessage(s);

    switch(color_format)
    {
    case GR_COLORFORMAT_ARGB: cptr = "ARGB"; break;
    case GR_COLORFORMAT_ABGR: cptr = "ABGR"; break;
    case GR_COLORFORMAT_RGBA: cptr = "RGBA"; break;
    case GR_COLORFORMAT_BGRA: cptr = "BGRA"; break;
    };

    s.Format("Color format = %s", cptr);
    DisplayMessage(s);

    s.Format("Number of color buffers = %d", nColBuffers);
    DisplayMessage(s);

    s.Format("Number of aux buffers = %d", nAuxBuffers);
    DisplayMessage(s);

    CResolution res(server_resolution);
    if(!res.IsOkay())
        throw CString("Server asked for unknown resolution");
    CResolution::Size size = res;
    m_server_width = size.width;
    m_server_height = size.height;

    m_xfactor = ((float)m_server_width) / ((float)m_game_width);
    m_yfactor = ((float)m_server_height) / ((float)m_game_height);

    m_wrapper.grSstWinOpen((unsigned long)m_screen->m_hWnd, server_resolution,
        refresh_rate,
        color_format,
        origin_location,
        nColBuffers,
        nAuxBuffers);

    m_wrapper.grGammaCorrectionValue((float) m_game_record.gamma);

    if(m_blood_fix)
    {
        delete m_blood_fix;
        m_blood_fix = NULL;
    }

    if(m_game_record.blood_fix)
        m_blood_fix = new CBloodFix(&m_wrapper, m_xfactor, m_yfactor);

    switch(m_game_record.tex_handling)
    {
    case CTexHandling::TEX_HANDLING_OVERRIDE:
        delete m_tex_handler;
	    m_tex_handler = new CTexHandlerReplace(&m_glide_state, &m_delay_store, m_game_record.tex_folder);
        break;

    case CTexHandling::TEX_HANDLING_CAPTURE:
        delete m_tex_handler;
        m_tex_handler = new CTexHandlerOutput;
        break;

    default:
        delete m_tex_handler;
        m_tex_handler = NULL;
        break;
    }

    if(m_tex_cache)
    {
        delete m_tex_cache;
        m_tex_cache = NULL;
    }

    if(m_game_record.tex_cache)
        m_tex_cache = new CTexCache(&m_wrapper);
}

int CGlideServer::GetIntFromClient()
{
    int i;

    m_pipe.ReadPipe((char *) &i, 4);

    return i;
}

void CGlideServer::doGrDepthBufferMode()
{
    GrDepthBufferMode_t depthbuffermode = GetIntFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GrDepthBufferMode"));
        CString s;
        s.Format("mode = %d", depthbuffermode);
        DebugMessage(s);
    }

    m_glide_state.GrDepthBufferMode(depthbuffermode);

    m_wrapper.grDepthBufferMode(depthbuffermode);
}

void CGlideServer::doGrDepthBufferFunction()
{
    GrCmpFnc_t depth_func = GetIntFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GrDepthBufferFunction"));
        CString s;
        s.Format("func = %d", depth_func);
        DebugMessage(s);
    }

    m_glide_state.GrDepthBufferFunction(depth_func);

    m_wrapper.grDepthBufferFunction(depth_func);
}

void CGlideServer::doGrDepthMask()
{
    int mask = GetIntFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GrDepthMask"));
        CString s;
        s.Format("mask = %x", mask);
        DebugMessage(s);
    }

    m_glide_state.GrDepthMask(mask);

    m_wrapper.grDepthMask(mask);
}

void CGlideServer::doGrTexCombineFunction()
{
    GrChipID_t tmu = GetIntFromClient();
    GrTextureCombineFnc_t fnc = GetIntFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GrTexCombineFunction"));
        CString s;
        s.Format("fnc = %d", fnc);
        DebugMessage(s);
    }

    m_wrapper.grTexCombineFunction(tmu, fnc);
}

void CGlideServer::doGrConstantColorValue()
{
    GrColor_t col = GetIntFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GrConstantColorValue"));
        CString s;
        s.Format("col = %08x", col);
        DebugMessage(s);
    }

    m_glide_state.GrConstantColorValue(col);

    m_wrapper.grConstantColorValue(col);
}

void CGlideServer::doGuAlphaSource()
{
    GrAlphaSource_t src = GetIntFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GuAlphaSource"));
        CString s;
        s.Format("src = %d", src);
        DebugMessage(s);
    }

    m_glide_state.GrAlphaSource(src);

    m_wrapper.guAlphaSource(src);
}

void CGlideServer::doGrChromaKeyMode()
{
    int mode = GetIntFromClient();

    if(m_tracing)
    {
        CString s;
        s.Format("GrChromaKeyMode %d", mode);
        DebugMessage(s);
    }

    m_wrapper.grChromakeyMode(mode);
}

void CGlideServer::doGrChromaKeyValue()
{
    int val = GetIntFromClient();

    if(m_tracing)
    {
        DebugMessage(CString("GrChromaKeyValue"));
        CString s;
        s.Format("CKey = %08x", val);
        DebugMessage(s);
    }

    m_wrapper.grChromakeyValue(val);
}

void CGlideServer::doGrGammaCorrectionValue()
{
    float f = GetFloatFromClient();

    if(m_tracing)
    {
        CString s;
        s.Format("GrGammaCorrectionValue %f", f);
        DebugMessage(s);
    }

    m_wrapper.grGammaCorrectionValue((float)(f * m_game_record.gamma));
}

float CGlideServer::GetFloatFromClient()
{
    float f;

    m_pipe.ReadPipe((char *) &f, sizeof(float));
    return f;
}

void CGlideServer::doGrTexDownloadTable()
{
    GrChipID_t tmu;
    GrTexTable_t type;
    GuTexTable table;
    CString s;
    int i;

    if(m_tracing) DebugMessage(CString("GrTexDownloadTable"));

    if(m_blood_fix)
        m_blood_fix->Flush();

    tmu = GetIntFromClient();
    type = GetIntFromClient();
    switch(type)
    {
    case GR_TEXTABLE_NCC0:
    case GR_TEXTABLE_NCC1:
        if(m_tracing)
        {
            s.Format("NCC tab = %d", sizeof(GuNccTable));
            DebugMessage(s);
        }

        m_pipe.ReadPipe((char *) &table, sizeof(GuNccTable));
        m_wrapper.grTexDownloadTable(tmu, type, &table);
        break;

    case GR_TEXTABLE_PALETTE:
        m_pipe.ReadPipe((char *) &table, sizeof(GuTexPalette));
		if(m_tex_handler)
			m_tex_handler->GrTexDownloadTable(&table.palette);

        m_wrapper.grTexDownloadTable(tmu, type, &table);

        if(m_tracing)
        {
            s.Format("Palette = %d", sizeof(GuTexPalette));
            DebugMessage(s);

            for(i = 0; i < 256; i += 4)
            {
                s.Format("%08x, %08x, %08x, %08x", ((int *)&table)[i], ((int *)&table)[i+1], ((int *)&table)[i+2], ((int *)&table)[i+3]);
                DebugMessage(s);
            }
        }
        break;

    default:
        DisplayMessage(CString("Bad table type"));
        break;
    }
}

void CGlideServer::doGuTexMemReset()
{
    if(m_tracing)
    {
        DebugMessage(CString("GuTexMemReset"));
    }

    m_wrapper.guTexMemReset();

   /*
    * Allocate texture to reserve room for LFB tricks
    */
    m_wrapper.guTexAllocateMemory(GR_TMU0, GR_MIPMAPLEVELMASK_BOTH,
                        256, 256, GR_TEXFMT_RGB_565, GR_MIPMAP_DISABLE,
                        GR_LOD_256, GR_LOD_256, GR_ASPECT_1x1,
                        GR_TEXTURECLAMP_CLAMP, GR_TEXTURECLAMP_CLAMP,
                        GR_TEXTUREFILTER_BILINEAR,
                        GR_TEXTUREFILTER_BILINEAR,
                        0.0, FXFALSE);
}

void CGlideServer::doGu3dfGetInfo()
{
    throw CString("Error: Gu3dfGetInfo called");
}

CString CGlideServer::GetStringFromClient()
{
    CString s;
    int len = GetIntFromClient();
    char *p = s.GetBuffer(len);
    m_pipe.ReadPipe(p, len);
    s.ReleaseBuffer(len);
    return s;
}


void CGlideServer::doGu3dfLoad()
{
    throw CString("Error: Gu3dfLoad called");
}

void CGlideServer::doGuTexAllocateMemory()
{
    int id = GetIntFromClient();

    if(m_tracing)
    {
        CString s;

        s.Format("GuTexAllocateMemory %d", id);
        DebugMessage(s);
    }

    GrChipID_t tmu = GetIntFromClient();
    FxU8 odd_even_mask = GetIntFromClient();
    int width = GetIntFromClient();
    int height = GetIntFromClient();
    GrTextureFormat_t fmt = GetIntFromClient();
    GrMipMapMode_t mm_mode = GetIntFromClient();
    GrLOD_t smallest_lod = GetIntFromClient();
    GrLOD_t largest_lod = GetIntFromClient();
    GrAspectRatio_t aspect = GetIntFromClient();
    GrTextureClampMode_t s_clamp_mode = GetIntFromClient();
    GrTextureClampMode_t t_clamp_mode = GetIntFromClient();
    GrTextureFilterMode_t minfilter_mode = GetIntFromClient();
    GrTextureFilterMode_t magfilter_mode = GetIntFromClient();
    int lod_bias = GetIntFromClient();
    FxBool trilinear = GetIntFromClient();

    if(m_game_record.forced_smoothing)
    {
        minfilter_mode = GR_TEXTUREFILTER_BILINEAR;
        magfilter_mode = GR_TEXTUREFILTER_BILINEAR;
    }

    if(id < 0 || id >= MAX_MM)
        return;

	if(m_tex_handler)
		m_tex_handler->GuTexAllocateMemory(id, fmt, width, height, largest_lod);

    mipmap[id] = m_wrapper.guTexAllocateMemory(tmu, odd_even_mask,
                             width, height, fmt, mm_mode,
                             smallest_lod, largest_lod,
                             aspect,
                             s_clamp_mode, t_clamp_mode,
                             minfilter_mode, magfilter_mode,
                             0.0f, trilinear);
}
