// TexReplace.cpp: implementation of the CTexHandlerReplace class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "TexReplace.h"
#include "LaraTextures.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "TexElementReplaceFactory.h"
#include "TexOverrider.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexHandlerReplace::CTexHandlerReplace(CGlideState *glide_state, CRenderStore *delay_store, CString folder)
    : m_subfolder(folder)
{
    m_restricted  = false;
    m_glide_state = glide_state;
    m_delay_store = delay_store;
}

CTexHandlerReplace::~CTexHandlerReplace()
{

}

void CTexHandlerReplace::Restrict()
{
    m_restricted = true;
}

CTexPartition *CTexHandlerReplace::NewTexPartition(GrTextureFormat_t fmt, int width, int height, GrLOD_t lod)
{
    return new CTexPartitionAct(lod, new CTexElementReplaceFactory(this));
}

static int __cdecl bad_tex_compare(const void *arg1, const void *arg2)
{
    return memcmp(arg1, arg2, 16);
}


void CTexHandlerReplace::SetupTexPartition(CTexPartition *partition, unsigned char hash[], char *data)
{
    int         count = 0;
    const CBox *areas = NULL;

    if(m_restricted)
    {
        BadTexture *bt = (BadTexture *) bsearch(hash, bad_textures, NUM_BAD_TEXTURES, sizeof(BadTexture), bad_tex_compare);

        if(bt != NULL)
        {
            count = bt->count;
            areas = bt->areas;
        }
    }

    ((CTexPartitionAct *)partition)->Initialise(GetMappingFile(), hash, count, areas);
}

CString CTexHandlerReplace::GetFolder()
{
    return m_subfolder.IsEmpty() ? "Textures"
        : CString("Textures\\") + m_subfolder;
}

CRenderStore *CTexHandlerReplace::GetStore()
{
    return m_delay_store;
}

CGlideState *CTexHandlerReplace::GetGlideState()
{
    return m_glide_state;
}
