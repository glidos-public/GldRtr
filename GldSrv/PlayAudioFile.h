// PlayAudioFile.h: interface for the CPlayAudioFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PLAYAUDIOFILE_H__F5C6B807_695A_4A71_A185_B87FFF5B08A5__INCLUDED_)
#define AFX_PLAYAUDIOFILE_H__F5C6B807_695A_4A71_A185_B87FFF5B08A5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "objbase.h"
#include "uuids.h"
#include "control.h"
#include "strmif.h"
#include "atlbase.h"

struct __declspec(uuid("{56A868B1-0AD4-11CE-B03A-0020AF0BA770}")) IMediaControl;

class CPlayAudioFile  
{
private:
    CComPtr<IGraphBuilder> m_builder;
	CComPtr<IFilterGraph>  m_graph;
    CComPtr<IMediaControl> m_control;
	CComPtr<IMediaSeeking> m_seeking;
	CComPtr<IBaseFilter>   m_input;

	bool                   m_might_be_running;

public:
    void Start(CString &file);
    void Stop();
	bool Playing();

	CPlayAudioFile();
	virtual ~CPlayAudioFile();

};

#endif // !defined(AFX_PLAYAUDIOFILE_H__F5C6B807_695A_4A71_A185_B87FFF5B08A5__INCLUDED_)
