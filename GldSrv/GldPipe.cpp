// GldPipe.cpp: implementation of the CGldPipe class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "GldPipe.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGldPipe::CGldPipe()
{
    m_shutdown = false;
}

CGldPipe::~CGldPipe()
{

}

void CGldPipe::Pump()
{
	MSG  msg;

    while(::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
        AfxGetApp()->PumpMessage();
}

void CGldPipe::PumpWhileWaiting(HANDLE event)
{
    bool going = true;

    while(!m_shutdown && going)
    {
        Pump();

		if(MsgWaitForMultipleObjects(1, &event, false, INFINITE, QS_ALLINPUT) == 0)
			going = false;
    }
}
