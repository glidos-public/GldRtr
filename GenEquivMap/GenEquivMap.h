// GenEquivMap.h : main header file for the GENEQUIVMAP application
//

#if !defined(AFX_GENEQUIVMAP_H__5A1D6437_7A5F_4EFC_B79B_7BB4F8DDB604__INCLUDED_)
#define AFX_GENEQUIVMAP_H__5A1D6437_7A5F_4EFC_B79B_7BB4F8DDB604__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CGenEquivMapApp:
// See GenEquivMap.cpp for the implementation of this class
//

class CGenEquivMapApp : public CWinApp
{
public:
	CGenEquivMapApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGenEquivMapApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CGenEquivMapApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GENEQUIVMAP_H__5A1D6437_7A5F_4EFC_B79B_7BB4F8DDB604__INCLUDED_)
