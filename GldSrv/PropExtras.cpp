// PropExtras.cpp : implementation file
//

#include "stdafx.h"
#include "gldsrv.h"
#include "PropExtras.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropExtras property page

IMPLEMENT_DYNCREATE(CPropExtras, CPropertyPage)

CPropExtras::CPropExtras() : CPropertyPage(CPropExtras::IDD)
{
	//{{AFX_DATA_INIT(CPropExtras)
	m_blood_fix = FALSE;
	m_tex_cache = FALSE;
	m_delay_writes = FALSE;
	m_disable_reads = FALSE;
	m_paletted_texture_cache = FALSE;
	m_shadow_hack = FALSE;
	//}}AFX_DATA_INIT
}

CPropExtras::~CPropExtras()
{
}

void CPropExtras::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropExtras)
	DDX_Check(pDX, IDC_BLOOD_FIX, m_blood_fix);
	DDX_Check(pDX, IDC_CACHE, m_tex_cache);
	DDX_Check(pDX, IDC_DELAY_WRITES, m_delay_writes);
	DDX_Check(pDX, IDC_DISABLE_READS, m_disable_reads);
	DDX_Check(pDX, IDC_PALETTED_TEXTURE_CACHE, m_paletted_texture_cache);
	DDX_Check(pDX, IDC_SHADOW_HACK, m_shadow_hack);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropExtras, CPropertyPage)
	//{{AFX_MSG_MAP(CPropExtras)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropExtras message handlers

void CPropExtras::Init(CGameRecord *rec)
{
    m_rec = rec;
}

BOOL CPropExtras::OnSetActive() 
{
    m_blood_fix     = m_rec->blood_fix;
    m_shadow_hack   = m_rec->shadow_hack;
    m_delay_writes  = m_rec->delay_lfb_writes;
    m_disable_reads = m_rec->disable_lfb_reads;
    m_tex_cache     = m_rec->tex_cache;
    m_paletted_texture_cache = m_rec->palette_tricks_cache;

    UpdateData(FALSE);

	return CPropertyPage::OnSetActive();
}

BOOL CPropExtras::OnKillActive() 
{
    UpdateData();

    m_rec->blood_fix          = (m_blood_fix != FALSE);
    m_rec->shadow_hack        = (m_shadow_hack != FALSE);
    m_rec->delay_lfb_writes   = (m_delay_writes != FALSE);
    m_rec->disable_lfb_reads  = (m_disable_reads != FALSE);
    m_rec->tex_cache          = (m_tex_cache != FALSE);
    m_rec->palette_tricks_cache = (m_paletted_texture_cache != 0);

	return CPropertyPage::OnKillActive();
}
