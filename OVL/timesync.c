#include "misc.h"
#include "vdd.h"

#include "timesync.h"

#define NULL (0)

static volatile int tick_count;
static int *volatile predicted_count;
static int divider;
static int active = 0;

static void (__interrupt __far *old_08handler)();

static void __interrupt __far new_08handler(void)
{
    if(tick_count - *predicted_count < 0)
    {
        tick_count += divider;

        chain(old_08handler);
    }

    int08end();
}

static int get_timer_divider(void)
{
    int decs = 10;
    int max = 0;
    int lasti = 0;
    int i;

    /*
     * Watch the timer go around ten times, and
     * record its maximum value.  This is a good
     * approximation to the divider, and tells us
     * how fast the timer should be ticking.
     */
    while(decs)
    {
        i = get_timer_count();
        if(i > max)
            max = i;

        if(i > lasti)
            decs -= 1;

        lasti = i;
    }

    return max;
}

void timesync_init(void)
{
    if(!active)
    {
        divider    = get_timer_divider();
        set_timer_divider(divider/10);
        predicted_count = PicPredict();
        tick_count = *predicted_count;
        
        old_08handler = getvect(0x8);
        setvect(0x8, new_08handler);

        active = 1;
    }
}

void timesync_fin(void)
{
    if(active)
    {
        setvect(0x8, old_08handler);

        set_timer_divider(divider);

        active = 0;
    }
}

void timesync_poll(void)
{
    if(active && tick_count - *predicted_count < 0)
        do_int08();
}
