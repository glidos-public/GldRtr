// TimerThread.cpp: implementation of the CTimerThread class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

extern "C"
{
#define i386
#include "vddsvc.h"
}

#include "TimerThread.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTimerThread::CTimerThread()
{
    m_pic_time = 0;
}

CTimerThread::~CTimerThread()
{

}

void CTimerThread::Execute()
{
    int pic_time = 0;
    int mm_time;

    timeBeginPeriod(1);

    SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);
    m_poll_time = mm_time = timeGetTime();

    SetPriorityClass(GetCurrentProcess(), BELOW_NORMAL_PRIORITY_CLASS);
    m_high_pri = false;

    while(!Dying())
    {
        int mm_inc, i;
        int time = timeGetTime();

        Sleep(m_delay);

        if(time - m_poll_time > 1000)
        {
            if(m_high_pri)
            {
                SetPriorityClass(GetCurrentProcess(), BELOW_NORMAL_PRIORITY_CLASS);
                m_high_pri = false;
            }
        }
        else
        {
            if(!m_high_pri)
            {
                SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);
                m_high_pri = true;
            }
        }

        mm_inc = time - mm_time;

        for(i = 0; i < mm_inc; i++)
            pic_time += 1193;

        mm_time += mm_inc;

        m_pic_time = pic_time;

        VDDSimulateInterrupt(ICA_MASTER, 0, 1);
    }

    timeEndPeriod(1);
}

void CTimerThread::Start(int val)
{
    m_delay = val;
    if(m_delay > 1)
    {
        m_delay -= 1;
    }

    CThread::Start();
}

void CTimerThread::Poll()
{
    m_poll_time = timeGetTime();
}
