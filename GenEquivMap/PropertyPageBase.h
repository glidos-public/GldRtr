#if !defined(AFX_PROPERTYPAGEBASE_H__271F128D_7792_4844_9AE2_3D471276AC4B__INCLUDED_)
#define AFX_PROPERTYPAGEBASE_H__271F128D_7792_4844_9AE2_3D471276AC4B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropertyPageBase.h : header file
//

#include <string>
#include <map>
#include <vector>
#include "image.h"

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageBase dialog

typedef std::vector<std::string>            ImageFileSet;
typedef std::map<std::string, ImageFileSet> ImageToImageFileSet;
typedef std::map<int, ImageFileSet>         KeyToImageFileSet;
typedef std::map<std::string, int>          ImageFileToKey;

struct ReadState
{
    KeyToImageFileSet keyToSet;
    ImageFileToKey    elemToKey;
    int               key;
};


class CPropertyPageBase : public CPropertyPage
{
	DECLARE_DYNCREATE(CPropertyPageBase)

// Construction
public:
    CPropertyPageBase():CPropertyPage() {};
	CPropertyPageBase(UINT id);
	~CPropertyPageBase();

// Dialog Data
	//{{AFX_DATA(CPropertyPageBase)
	enum { IDD = IDD_ABOUTBOX };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropertyPageBase)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyPageBase)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

    bool GetFolder(CString &folder);
	bool ReadEquiv(ReadState &state, const CString &file);
	CString DropExtension(CString &name);
	CString GetOutputFileName(LPCSTR fname);
	CString HashImage(CImage *img);
    CString NameFromHash(unsigned char hash[]);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTYPAGEBASE_H__271F128D_7792_4844_9AE2_3D471276AC4B__INCLUDED_)
