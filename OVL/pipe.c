/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * $Id$
 *
 ************************************************************/

#include "pipe.h"
#include "vxd.h"
#include "npipe.h"
#include "vdd.h"

int detect_os(char *name);

#pragma aux detect_os =          \
    "mov cx, es"                 \
    "mov dx, di"                 \
    "mov ebx, 0h"                \
    "mov eax, 1684h"             \
    "int 2fh"                    \
    "mov bx, es"                 \
    "or bx, di"                  \
    "jz nt"                      \
    "mov bx, es"                 \
    "sub bx, cx"                 \
    "sub dx, di"                 \
    "or bx, dx"                  \
    "jz dosbox"                  \
    "mov eax, 0h"                \
    "jp exit"                    \
"nt: mov eax, 1h"                \
    "jp exit"                    \
"dosbox: mov eax, 2h"            \
"exit: mov es, cx"               \
    parm [edi]                   \
    modify [eax ebx ecx edx edi] \
    value [eax];

pipe_t *pipe_open(void)
{
    pipe_os_t os = detect_os("GLDPIPE1");

    switch(os)
    {
        case pipe_os_w98:
            return VxdOpen();

        case pipe_os_nt:
            return VDDOpen();

        case pipe_os_dosbox:
            return NPipeOpen();

        default:
            return 0;
    }
}
