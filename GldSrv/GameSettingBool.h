// GameSettingBool.h: interface for the CGameSettingBool class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAMESETTINGBOOL_H__5C2A3E10_8136_4EF4_81A5_069FAF4E4F07__INCLUDED_)
#define AFX_GAMESETTINGBOOL_H__5C2A3E10_8136_4EF4_81A5_069FAF4E4F07__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GameSetting.h"

class CBool
{
protected:
	bool m_value;

public:
	CBool()
	{
		m_value = false;
	};

	operator bool()
	{
		return m_value;
	};
};


class CGameSettingBool : public CGameSetting<CBool>
{
public:
	CGameSettingBool &operator=(const bool &val);

private:
	bool    Parse(CString &val);
	CString Print();
};


#endif // !defined(AFX_GAMESETTINGBOOL_H__5C2A3E10_8136_4EF4_81A5_069FAF4E4F07__INCLUDED_)
