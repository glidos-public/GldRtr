// TexHandler.h: interface for the CTexHandler class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXHANDLER_H__77DE8B66_827C_4094_A699_618363426447__INCLUDED_)
#define AFX_TEXHANDLER_H__77DE8B66_827C_4094_A699_618363426447__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "sdk2_glide.h"
#include "MappingFile.h"
#include "TexPartition.h"

class CTexHandler  
{
	enum
	{
		MAX_MM = 1024
	};

    
    CTexPartition *m_tex_partition[MAX_MM];
    CTexPartition *m_selected_partition;

    bool               m_initialised;
    CMappingFile       m_mapper;

private:
    virtual CTexPartition *NewTexPartition(GrTextureFormat_t fmt, int width, int height, GrLOD_t lod) = 0;
    virtual void SetupTexPartition(CTexPartition *partition, unsigned char hash[16], char *data) = 0;
	virtual CString GetFolder() = 0;

public:
	CMappingFile *GetMappingFile();
    /* Returns indication as to whether the polygon has been
     * dealt with by the call.  Otherwise, the caller better
     * deal with it. */
	bool GrDrawPolygonVertexList(int n, GrVertex *vlist);
	void GuTexSource(int id);
	void GuTexDownloadMipMap(int id, unsigned char hash[16], char *data);
	void GuTexAllocateMemory(int id, GrTextureFormat_t fmt, int width, int height, GrLOD_t lod);
	virtual void GrTexDownloadTable(GuTexPalette *pal);
    virtual void Restrict();
	CTexHandler();
	virtual ~CTexHandler();

};

#endif // !defined(AFX_TEXHANDLER_H__77DE8B66_827C_4094_A699_618363426447__INCLUDED_)
