// StoredStruct.h: interface for the CStoredStruct class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STOREDSTRUCT_H__506F44EC_5D45_4D47_AAD7_FB234651920D__INCLUDED_)
#define AFX_STOREDSTRUCT_H__506F44EC_5D45_4D47_AAD7_FB234651920D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CStoredStructField;

class CStoredStruct  
{
protected:
	CTypedPtrArray<CPtrArray, CStoredStructField *> m_fields;

public:
	CStoredStruct &operator=(const CStoredStruct &v);
	void           AddField(CStoredStructField *field);
};

class CStoredStructField
{
public:
	void         Init(CStoredStruct *st);
	virtual bool Read(CString &key, CString &val) = 0;
	virtual      CString Write()                  = 0;
};


#endif // !defined(AFX_STOREDSTRUCT_H__506F44EC_5D45_4D47_AAD7_FB234651920D__INCLUDED_)
