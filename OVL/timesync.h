/*
 * Routines to keep the ntvdm clock in sync.  It falls behind,
 * presumably because it doesn't tick when not the current process,
 * so the game running thinks its doing really well and generates
 * more frames, which makes things worse.
 */

#ifndef _TIMESYNC_H_
#define _TIMESYNC_H_

void timesync_init(void);

void timesync_fin(void);

void timesync_poll(void);

#endif
