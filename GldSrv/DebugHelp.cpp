// DebugHelp.cpp: implementation of the CDebugHelp class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "gldsrv.h"
#include "imagehlp.h"
#include "DebugHelp.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define MAXNAMELEN (30)

CDebugHelp::CDebugHelp()
{
    SymSetOptions(SYMOPT_UNDNAME|SYMOPT_DEFERRED_LOADS);

    m_process = GetCurrentProcess();
    m_thread  = GetCurrentThread();
    SymInitialize(m_process, NULL, TRUE);
}

CDebugHelp::~CDebugHelp()
{
    SymCleanup(m_process);
}

CString CDebugHelp::Trace(unsigned int u, EXCEPTION_POINTERS *pExp)
{
    CString          res;
    STACKFRAME       frame;
    char             buf[sizeof(IMAGEHLP_SYMBOL) + sizeof(TCHAR) * (MAXNAMELEN - 1)];
    PIMAGEHLP_SYMBOL sym = (PIMAGEHLP_SYMBOL) buf;
    IMAGEHLP_LINE    line;
    DWORD            disp;
    bool             first = true;

    memset(&frame, 0, sizeof(frame));

    frame.AddrPC.Mode    = AddrModeFlat;
    frame.AddrFrame.Mode = AddrModeFlat;
    frame.AddrStack.Mode = AddrModeFlat;

    frame.AddrPC.Offset  = pExp->ContextRecord->Eip;
    frame.AddrFrame.Offset  = pExp->ContextRecord->Ebp;
    frame.AddrStack.Offset  = pExp->ContextRecord->Esp;

    while(StackWalk(IMAGE_FILE_MACHINE_I386,
                      m_process,
                      m_thread,
                      &frame,
                      pExp->ContextRecord,
                      NULL,
                      SymFunctionTableAccess,
                      SymGetModuleBase,
                      NULL))
    {
        if(!first)
            res += " <- ";

        sym->SizeOfStruct  = sizeof(IMAGEHLP_SYMBOL);
        sym->MaxNameLength = MAXNAMELEN;
        if(SymGetSymFromAddr(m_process, frame.AddrPC.Offset, &disp, sym))
            res += sym->Name;
        else
            res += "<Unknown>";

        if(first)
        {
            line.SizeOfStruct = sizeof(IMAGEHLP_LINE);
            if(SymGetLineFromAddr(m_process, frame.AddrPC.Offset, &disp, &line))
            {
                CString s;
                s.Format("(%d)", line.LineNumber);
                res += s;
            }
        }

        first = false;
    }

    return res;
}
