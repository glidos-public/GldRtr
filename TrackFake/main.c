#include <stdio.h>
#include <string.h>
#include <dos.h>
#include <process.h>
#include <i86.h>

void (__interrupt __far *old2f)();

void __interrupt __far new2f(union INTPACK r)
{
    if(r.x.ax == 0x1510)
    {
        unsigned char __far *req;
        unsigned char __far *ctrl;

        req = (char __far *)(r.w.es:>r.w.bx);

        /* Return success indication */
        req[3] = 0;
        req[4] = 1;

        switch(req[2])
        {
            case 3:
                ctrl = *((unsigned char __far **)(req+14));
                switch(ctrl[0])
                {
                    case 6: /* Divice status */
                        ctrl[1] = 2;
                        ctrl[2] = 2;
                        ctrl[3] = 0;
                        ctrl[4] = 0;
                        break;

                    case 9: /* Media changed */
                        ctrl[1] = 255;
                        break;

                    case 10: /* Audio disc info */
                        ctrl[1] = 1;
                        ctrl[2] = 65;
                        ctrl[3] = 0;
                        ctrl[4] = 0;
                        ctrl[5] = 65;
                        ctrl[6] = 0;
                        break;

                    case 11: /* Audio track info */
                        ctrl[2] = 0;
                        ctrl[3] = 0;
                        ctrl[4] = ctrl[1] - 1;
                        ctrl[5] = 0;
                        ctrl[6] = (ctrl[1] == 1) ? 4 : 0;
                        break;
                }
                break;
        }
    }
    else
    {
        _chain_intr(old2f);
    }
}

void main(int argc, const char *argv[])
{
    if(argc != 1)
    {
        printf("usage: trackfake.exe\n");
        return;
    }

    printf("Installing track faker\n");

    old2f = _dos_getvect(0x2f);

    _dos_setvect(0x2f, new2f);

    _dos_keep(0, 4*1024);
}

