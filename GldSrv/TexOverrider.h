// TexOverrider.h: interface for the CTexPartitionAct class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXOVERRIDER_H__52BCE016_B0F8_44E1_BDFE_CE530533C8F4__INCLUDED_)
#define AFX_TEXOVERRIDER_H__52BCE016_B0F8_44E1_BDFE_CE530533C8F4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TexElementAct.h"
#include "TexElementActFactory.h"
#include "TexPartition.h"
#include "TexSet.h"
#include "MappingFile.h"

class CTexPartitionAct : public CTexPartition
{
private:
    GrLOD_t m_lod;
    CTexSet<CTexElementAct, CTexElementActFactory> m_texset;

public:
    bool UsedRectangle(int n, GrVertex *vlist);
	void Initialise(CMappingFile *mapper, unsigned char hash[], int count, const CBox *areas);
	void Finalise();
	CTexPartitionAct(GrLOD_t lod, CTexElementActFactory *factory);
	virtual ~CTexPartitionAct();

};

#endif // !defined(AFX_TEXOVERRIDER_H__52BCE016_B0F8_44E1_BDFE_CE530533C8F4__INCLUDED_)
