// W2KPipe.h: interface for the CW2KPipe class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_W2KPIPE_H__9429CB60_D091_11D5_B9FE_00C0DFF0F563__INCLUDED_)
#define AFX_W2KPIPE_H__9429CB60_D091_11D5_B9FE_00C0DFF0F563__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GldPipe.h"

class CW2KPipe : public CGldPipe  
{
private:
	bool m_connected;
    HANDLE m_pipe;
    OVERLAPPED overlapped;
    OVERLAPPED overlapped_write;

public:
	virtual bool Write(char *buf, int n);
	static CW2KPipe * NewPipe();
	virtual void Wake();
	virtual bool Read(char *buf, int requested, unsigned long *returned);
	virtual bool Create();
	CW2KPipe();
	virtual ~CW2KPipe();

};

#endif // !defined(AFX_W2KPIPE_H__9429CB60_D091_11D5_B9FE_00C0DFF0F563__INCLUDED_)
