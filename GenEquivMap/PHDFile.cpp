// PHDFile.cpp: implementation of the CPHDFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GenEquivMap.h"
#include "PHDFile.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define VERSION_TR1 (0x20)

CPHDFile::CPHDFile()
{
    m_f       = NULL;
    m_tub     = false;
    m_tile    = NULL;
    m_sprtex  = NULL;
    m_objtex  = NULL;
    m_ntile   = 0;
    m_nsprtex = 0;
    m_nobjtex = 0;
}

CPHDFile::~CPHDFile()
{
    delete [] m_tile;
    delete [] m_sprtex;
    delete [] m_objtex;
    fclose(m_f);
}

bool CPHDFile::Open(const CString &file)
{
    try
    {
        int ver;
        int i;

        i = file.ReverseFind('.');
        m_tub = (i != -1 && file.Mid(i+1).CompareNoCase("tub") == 0);

        m_f = fopen((LPCSTR)file, "rb");
        if(m_f == NULL)
            throw "Cannot open file";
        
        ver = ReadInt();
        if(ver != VERSION_TR1)
            throw "Not a Tomb Raider 1 level";

        m_ntile = ReadInt();

        m_tile = new Tile[m_ntile];
        if(m_tile == NULL)
            throw "Insufficient memory to hold tiles";

        fread(m_tile, sizeof(Tile), m_ntile, m_f);

        Skip(sizeof(int));

        int nrooms = ReadShort();

        for(i = 0; i < nrooms; i++)
            ReadRoom();

        SkipIntCountedData(2);        //Floor data
        SkipIntCountedData(2);        //Mesh data
        SkipIntCountedData(4);        //Mesh pointers
        SkipIntCountedData(32);       //Animations
        SkipIntCountedData(6);        //State changes
        SkipIntCountedData(8);        //Animation dispatches
        SkipIntCountedData(2);        //Animation commands
        SkipIntCountedData(4);        //Mesh trees
        SkipIntCountedData(2);        //Frames
        SkipIntCountedData(18);       //Moveables
        SkipIntCountedData(32);       //Static meshes

        ReadObjectTextures();
        ReadSpriteTextures();

        SkipIntCountedData(8);        //Sprite sequences
        if(m_tub)
            ReadPalette();
        SkipIntCountedData(16);       //Camera data
        SkipIntCountedData(16);       //Sound source
        int nbox = ReadInt();
        Skip(nbox * 20);              //Box
        SkipIntCountedData(2);        //Overlaps
        Skip(nbox * 6 * sizeof(short));//Zones
        SkipIntCountedData(2);        //Animated textures
        SkipIntCountedData(22);       //Items
        Skip(32 * 256);               //Light map
        if(!m_tub)
            ReadPalette();
    }
    catch(LPCSTR mess)
    {
        AfxMessageBox(mess, MB_OK);

        return false;
    }
    
    return true;
}

int CPHDFile::GetNumTiles()
{
    return m_ntile;
}

Tile *CPHDFile::GetTile(int i)
{
    ASSERT(i < m_ntile);

    return &(m_tile[i]);
}

void CPHDFile::ReadRoom()
{
    Skip(16);                          //Room Info
    SkipIntCountedData(sizeof(short)); //Room Data
    SkipShortCountedData(32);          //Doors

    {                                  //Sectors
        int nzsect = ReadShort();
        int nxsect = ReadShort();

        Skip(nzsect * nxsect * 8);
    }

    Skip(sizeof(short));               //Intensity1
    SkipShortCountedData(18);          //Lights
    SkipShortCountedData(18);          //Static Meshes
    Skip(2 * sizeof(short));           //AlternateRoom,Flags
}
 

void CPHDFile::Skip(int n)
{
    fseek(m_f, n, SEEK_CUR);
}

void CPHDFile::SkipShortCountedData(int n)
{
    int count;

    count = ReadShort();
    Skip(count *n);
}

void CPHDFile::SkipIntCountedData(int n)
{
    int count;

    count = ReadInt();
    Skip(count *n);
}

int CPHDFile::ReadByte()
{
    unsigned char v;

    fread(&v, sizeof(v), 1, m_f);

    return v;
}

int CPHDFile::ReadShort()
{
    unsigned short v;

    fread(&v, sizeof(v), 1, m_f);

    return v;
}

int CPHDFile::ReadInt()
{
    int v;

    fread(&v, sizeof(v), 1, m_f);

    return v;
}

void CPHDFile::ReadPalette()
{
    int i;

    for(i = 0; i < 256; i++)
        m_pal[i] = ReadColor();

    m_pal[0] = 0xFFFF;
}

int CPHDFile::ReadColor()
{
    unsigned char c[3];

    fread(c, 3, 1, m_f);

    return ((c[2] << 2) | (c[1] << 10) | (c[0] << 18));
}

int *CPHDFile::GetPalette()
{
    return m_pal;
}

void CPHDFile::ReadSpriteTextures()
{
    int i;

    m_nsprtex = ReadInt();

    m_sprtex = new TexUse[m_nsprtex];
    if(m_sprtex == NULL)
        throw "Insufficient memory to hold sprite textures";

    for(i = 0; i < m_nsprtex; i++)
    {
        TexUse *use = &(m_sprtex[i]);

        use->texid = ReadShort();
        use->xlow  = ReadByte();
        use->ylow  = ReadByte();
        use->xhigh = use->xlow + (ReadShort() >> 8);
        use->yhigh = use->ylow + (ReadShort() >> 8);

        Skip(8);
    }
}

void CPHDFile::ReadObjectTextures()
{
    int i;

    m_nobjtex = ReadInt();

    m_objtex = new TexUse[m_nobjtex];
    if(m_objtex == NULL)
        throw "Insufficient memory to hold object textures";

    for(i = 0; i < m_nobjtex; i++)
    {
        int     j;
        TexUse *use = &(m_objtex[i]);

        Skip(sizeof(short));
        use->texid = ReadShort();
        use->xlow  = 255;
        use->ylow  = 255;
        use->xhigh = 0;
        use->yhigh = 0;


        for(j = 0; j < 4; j++)
        {
            int x = ReadShort();
            int y = ReadShort();
            if(x && y)
            {
                x >>= 8;
                y >>= 8;
                
                if(use->xlow > x)
                    use->xlow = x;
                
                if(use->ylow > y)
                    use->ylow = y;
                
                if(use->xhigh < x)
                    use->xhigh = x;
                
                if(use->yhigh < y)
                    use->yhigh = y;
            }
        }
    }
}

int CPHDFile::GetNumTexUses()
{
    return m_nsprtex + m_nobjtex;
}

TexUse *CPHDFile::GetTexUse(int i)
{
    ASSERT(i < m_nsprtex + m_nobjtex);

    return (i < m_nsprtex
                 ? &(m_sprtex[i])
                 : &(m_objtex[i - m_nsprtex]));
}
