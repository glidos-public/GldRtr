// TexReplace.h: interface for the CTexHandlerReplace class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXREPLACE_H__9AC6E156_F168_49C5_9425_91707570A31C__INCLUDED_)
#define AFX_TEXREPLACE_H__9AC6E156_F168_49C5_9425_91707570A31C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GlideState.h"
#include "RenderStore.h"
#include "TexHandler.h"

class CTexHandlerReplace : public CTexHandler
{
private:
    bool           m_restricted;
    CGlideState   *m_glide_state;
    CRenderStore  *m_delay_store;
    CString        m_subfolder;
    CTexPartition *NewTexPartition(GrTextureFormat_t fmt, int width, int height, GrLOD_t lod);
    void           SetupTexPartition(CTexPartition *partition, unsigned char hash[], char *data);
	CString        GetFolder();

public:
    CRenderStore  *GetStore();
    CGlideState   *GetGlideState();
	CTexHandlerReplace(CGlideState *glide_state, CRenderStore *delay_store, CString folder);
    void           Restrict();
	virtual ~CTexHandlerReplace();

};

#endif // !defined(AFX_TEXREPLACE_H__9AC6E156_F168_49C5_9425_91707570A31C__INCLUDED_)
