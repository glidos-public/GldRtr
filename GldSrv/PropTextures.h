#if !defined(AFX_PROPTEXTURES_H__8BA6C111_2B3B_4016_B232_6EB20B145BF9__INCLUDED_)
#define AFX_PROPTEXTURES_H__8BA6C111_2B3B_4016_B232_6EB20B145BF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropTextures.h : header file
//

#include "GameRecord.h"

/////////////////////////////////////////////////////////////////////////////
// CPropTextures dialog

class CPropTextures : public CPropertyPage
{
	DECLARE_DYNCREATE(CPropTextures)

private:
    CGameRecord *m_rec;

// Construction
public:
	void Init(CGameRecord *rec);
	CPropTextures();
	~CPropTextures();

// Dialog Data
	//{{AFX_DATA(CPropTextures)
	enum { IDD = IDD_PROP_TEX };
	CButton	m_oldpack;
	CStatic	m_label;
	CButton	m_static_select;
	CComboBox	m_packname;
	int		m_override;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropTextures)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropTextures)
	afx_msg void OnOldtex();
	afx_msg void OnOverride();
	afx_msg void OnOverride2();
	afx_msg void OnOverride3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPTEXTURES_H__8BA6C111_2B3B_4016_B232_6EB20B145BF9__INCLUDED_)
