// PropCD.cpp : implementation file
//

#include "stdafx.h"
#include "GldSrv.h"
#include "PropCD.h"


// CPropCD dialog

IMPLEMENT_DYNAMIC(CPropCD, CPropertyPage)

CPropCD::CPropCD()
	: CPropertyPage(CPropCD::IDD)
	, m_check_path(_T(""))
    , m_cd_or_iso(0)
{

}

CPropCD::~CPropCD()
{
}

void CPropCD::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DRIVE, m_drive);
	DDX_Control(pDX, IDC_ISO_PATH, m_iso_path);
	DDX_Control(pDX, IDC_BROWSE, m_browse_iso_path);
    DDX_Radio(pDX, IDC_CD, m_cd_or_iso);
    DDX_Text(pDX, IDC_CD_PATH, m_check_path);
}


BEGIN_MESSAGE_MAP(CPropCD, CPropertyPage)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_CD, &CPropCD::OnBnClickedButtonBrowseCd)
	ON_BN_CLICKED(IDC_BROWSE, &CPropCD::OnBnClickedBrowseISO)
    ON_BN_CLICKED(IDC_CD, &CPropCD::OnBnClickedCd)
    ON_BN_CLICKED(IDC_ISO, &CPropCD::OnBnClickedIso)
END_MESSAGE_MAP()


void CPropCD::Init(CGameRecord *rec)
{
    m_rec = rec;
}

void CPropCD::UpdateButtons()
{
    m_drive.EnableWindow(m_cd_or_iso == 0);
    m_iso_path.EnableWindow(m_cd_or_iso == 1);
    m_browse_iso_path.EnableWindow(m_cd_or_iso == 1);
}

// CPropCD message handlers

BOOL CPropCD::OnSetActive()
{
    m_check_path = m_rec->cd_file;

    m_drive.ResetContent();
    m_drive.AddString("Auto");
    for(int i = 0; i < 26; i++)
    {
        CString cdroot;
        cdroot.Format("%c:\\", 'A' + i);

        if(GetDriveType((LPCSTR)cdroot) == DRIVE_CDROM)
            m_drive.AddString((LPCSTR)cdroot);
    }

    if(m_rec->cd_path.GetLength() <= 3)
    {
        m_cd_or_iso = 0;
        int pos = m_drive.FindStringExact(-1, (LPCSTR)m_rec->cd_path);

        m_drive.SetCurSel(pos == CB_ERR ? 0 : pos);
        m_iso_path.SetWindowText("");
    }
    else
    {
        m_drive.SetCurSel(0);
        m_iso_path.SetWindowText(m_rec->cd_path);
        m_cd_or_iso = 1;
    }

    m_check_path = m_rec->cd_file;

    UpdateData(FALSE);
    UpdateButtons();

    CPropertySheet *sheet = (CPropertySheet *) GetParent();
    sheet->SetWizardButtons(PSWIZB_FINISH|PSWIZB_BACK);

	return CPropertyPage::OnSetActive();
}

BOOL CPropCD::OnKillActive()
{
    CString cd_path;

    UpdateData();

    switch(m_cd_or_iso)
    {
    case 0:
        if(m_drive.GetCurSel() == 0)
            m_rec->cd_path = CString("");
        else
            m_drive.GetWindowText(cd_path);
        break;

    case 1:
        m_iso_path.GetWindowText(cd_path);
        break;
    }

    m_rec->cd_path = cd_path;

    m_rec->cd_file = m_check_path;

	return CPropertyPage::OnKillActive();
}

void CPropCD::OnBnClickedButtonBrowseCd()
{
	CFileDialog dlg(TRUE, NULL, NULL, OFN_NOCHANGEDIR);

    if(dlg.DoModal() == IDOK)
    {
        m_check_path = dlg.GetPathName().Mid(3);
        UpdateData(FALSE);
    }
}

void CPropCD::OnBnClickedBrowseISO()
{
    CFileDialog dlg(TRUE, NULL, NULL, OFN_NOCHANGEDIR);

    if(dlg.DoModal() == IDOK)
    {
        m_iso_path.SetWindowText(dlg.GetPathName());
        UpdateData(FALSE);
    }
}

void CPropCD::OnBnClickedCd()
{
    UpdateData();
    UpdateButtons();
}

void CPropCD::OnBnClickedIso()
{
    UpdateData();
    UpdateButtons();
}
