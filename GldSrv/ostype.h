#ifndef OSTYPE_H
#define OSTYPE_H

enum OSType
{
    OS_NONE,
    OS_W2K,
    OS_W98
};

#endif // OSTYPE_H
