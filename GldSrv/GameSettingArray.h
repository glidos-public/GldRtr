// GameSettingArray.h: interface for the CGameSettingArray class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(GAMESETTINGARRAY_H)
#define GAMESETTINGARRAY_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GameSetting.h"
#include "wrapper_flags.h"

class CTextableInt
{
private:
    unsigned int m_val;

public:
    operator unsigned int()
    {
        return m_val;
    };

    bool Read(const char *buf)
    {
        char c;

        return sscanf(buf, "%d%c", &m_val, &c) == 1;
    };

    CString Write()
    {
        CString v;

        v.Format("%d", m_val);

        return v;
    };
};

class CTextableFloat
{
private:
    float m_val;

public:
    CTextableFloat()        : m_val(0.0f) {};
    CTextableFloat(float v) : m_val(v)    {};

    operator float()
    {
        return m_val;
    };

    bool Read(const char *buf)
    {
        char c;

        return sscanf(buf, "%f%c", &m_val, &c) == 1;
    };

    CString Write()
    {
        CString v;

        v.Format("%.6g", m_val);

        return v;
    };
};

template<class T>
class CGameSettingArray : public CGameSetting<CArray<T, const T&> >
{
public:
	CGameSettingArray<T> &operator=(const CGameSettingArray<T> &a);

	bool    Parse(CString &val);
	CString Print();
};

class CGameSettingArrayInt : public CGameSettingArray<CTextableInt>
{
};

class CGameSettingStereo : public CGameSettingArray<CTextableFloat>
{
public:
    const CGameSettingStereo &operator=(const StereoMap &sm)
    {
        int       i;

        RemoveAll();

        if(sm.nsep > 0)
        {
            Add((float)sm.nsep);

            for(i = 0; i < sm.nsep; i++)
            {
                Add(sm.sep[i].limit);
                Add(sm.sep[i].offset);
                Add(sm.sep[i].factor);
            }

            Add((float)sm.type);
        }

        return *this;
    };

    operator const StereoMap()
    {
        StereoMap map;
        INT_PTR   s;
        INT_PTR   i, j;

        map.nsep = 0;
        s = GetSize();
        if(s > 0 && s > (int)GetAt(0) * 3)
            map.nsep = (int)GetAt(0);

        j = 1;
        for(i = 0; i < map.nsep; i++)
        {
            map.sep[i].limit  = GetAt(j++);
            map.sep[i].offset = GetAt(j++);
            map.sep[i].factor = GetAt(j++);
        }

        map.type = (s > j) ? (StereoType)(int)GetAt(j)
                           : (StereoType)0;

        map.size = sizeof(map);

        return map;
    }
};

#endif // !defined(GAMESETTINGARRAY_H)
#include "GameSettingArray.cpp"
