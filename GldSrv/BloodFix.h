// BloodFix.h: interface for the CBloodFix class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BLOODFIX_H__7891A743_A491_4CC7_98C2_B9E70DD904AB__INCLUDED_)
#define AFX_BLOODFIX_H__7891A743_A491_4CC7_98C2_B9E70DD904AB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "sdk2_glide.h"
#include "GlideDll.h"

class CBloodFix  
{
    enum
    {
        MaxLines = 800
    };

    enum TType
    {
        None,
        Vertical,
        Horizontal
    };

    struct GrLine
    {
        GrVertex v1, v2;
    };

private:
	void reverse();
	bool wall_or_floor(GrLine *lines, int nlines);
	void lnudge_texture(GrLine *lines, int nlines);
	void rnudge_texture(GrLine *lines, int nlines);
	bool split(GrLine *lines, int nlines, int *cut);
	void render(GrLine *lines, int nlines);
    float  m_xfac;
    float  m_yfac;
    TType  m_holding;
    GrLine m_lines[MaxLines];
    int    m_nlines;
    CGlideDll *m_wrapper;
public:
	void Flush();
	bool Accept(GrVertex *v);
	CBloodFix(CGlideDll *wrapper, float xfac, float yfac);
	virtual ~CBloodFix();

};

#endif // !defined(AFX_BLOODFIX_H__7891A743_A491_4CC7_98C2_B9E70DD904AB__INCLUDED_)
