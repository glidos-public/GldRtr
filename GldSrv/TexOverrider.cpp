// TexOverrider.cpp: implementation of the CTexPartitionAct class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "TexElementAct.h"
#include "TexElementActFactory.h"
#include "TexOverrider.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexPartitionAct::CTexPartitionAct(GrLOD_t lod, CTexElementActFactory *factory)
                            : m_texset(factory)
{
    m_lod    = lod;
}

CTexPartitionAct::~CTexPartitionAct()
{

}

void CTexPartitionAct::Finalise()
{
}

void CTexPartitionAct::Initialise(CMappingFile *mapper, unsigned char hash[], int count, const CBox *areas)
{
    TexElementInfoList *list;

    list = mapper->GetElementInfoList(hash);
    m_texset.Initialise(mapper->GetFolder(hash), list);
    delete list;

    for(m_texset.Start(); m_texset.Going(); m_texset.Next())
    {
        bool rude = false;

        for(int i = 0; i < count; i++)
            if(m_texset.Here()->Intersects(&areas[i]))
                rude = true;

        if(rude || !m_texset.Here()->Load(m_lod))
        {
            m_texset.Mark();
        }
    }

    m_texset.DeleteMarked(false);
}

bool CTexPartitionAct::UsedRectangle(int n, GrVertex *vlist)
{
    CBox box(n, vlist);

    for(m_texset.Start(); m_texset.Going(); m_texset.Next())
    {
        CTexElementAct *e = m_texset.Here();

        if(box.IncludedBy(e))
        {
            e->Override(n, vlist);

            return true;
        }
    }

    m_texset.Any()->Reset();

    return false;
}
