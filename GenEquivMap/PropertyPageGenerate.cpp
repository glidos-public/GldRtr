// PropertyPageGenerate.cpp : implementation file
//

#include "stdafx.h"
#include "genequivmap.h"
#include "PropertyPageGenerate.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageGenerate property page

IMPLEMENT_DYNCREATE(CPropertyPageGenerate, CPropertyPageBase)

CPropertyPageGenerate::CPropertyPageGenerate() : CPropertyPageBase(CPropertyPageGenerate::IDD)
{
	//{{AFX_DATA_INIT(CPropertyPageGenerate)
	m_folder = _T("");
	//}}AFX_DATA_INIT
}

CPropertyPageGenerate::~CPropertyPageGenerate()
{
}

void CPropertyPageGenerate::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropertyPageGenerate)
	DDX_Text(pDX, IDC_EDIT_FOLDER, m_folder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropertyPageGenerate, CPropertyPageBase)
	//{{AFX_MSG_MAP(CPropertyPageGenerate)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_FOLDER, OnButtonBrowseFolder)
	ON_BN_CLICKED(IDC_BUTTON_RUN, OnButtonRun)
	//}}AFX_MSG_MAP
    ON_UPDATE_COMMAND_UI(IDC_BUTTON_RUN, OnUpdateRunButton)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageGenerate message handlers

void CPropertyPageGenerate::OnButtonBrowseFolder() 
{
    if(GetFolder(m_folder))
        UpdateData(FALSE);
}

void CPropertyPageGenerate::OnButtonRun() 
{
    ImageToImageFileSet map;
    CFileFind rootfind, subfind;
    BOOL      rootgoing, subgoing;

    rootgoing = rootfind.FindFile((LPCSTR)(m_folder + "\\*.*"));
    while(rootgoing)
    {
        rootgoing = rootfind.FindNextFile();

        if(rootfind.IsDirectory() && !rootfind.IsDots())
        {
            CString subfolder = rootfind.GetFileName();

            subgoing = subfind.FindFile((LPCSTR)(m_folder + "\\" + subfolder + "\\*.*"));
            while(subgoing)
            {
                int dummy;

                subgoing = subfind.FindNextFile();
                CString name = subfind.GetFileName();
                if(sscanf((LPCSTR)name, "(%*d--%*d)(%*d--%d).", &dummy) == 1)
                {
                    CString path = m_folder + "\\" + subfolder + "\\" + name;
                    
                    CImage *img = CImage::Load(path);
                    if(img)
                    {
                        CString       hash = HashImage(img);
                        ImageFileSet &set  = map[(LPCSTR)hash];
                        
                        set.push_back((LPCSTR)(subfolder + "\\" + DropExtension(name)));
                        
                        delete img;
                    }
                }
            }

            subfind.Close();
        }
    }

    rootfind.Close();

    CString file = GetOutputFileName("equiv.txt");

    FILE *f;

    f = file.IsEmpty() ? NULL
                       : fopen((LPCSTR)file, "w");

    if(f)
    {
        fprintf(f, "GLIDOS TEXTURE EQUIV\n");

        ImageToImageFileSet::iterator miter;
        
        for(miter = map.begin(); miter != map.end(); miter++)
        {
            ImageFileSet &set = miter->second;
            
            if(set.size() > 1)
            {
                ImageFileSet::iterator  siter;

                fprintf(f, "\nBeginEquiv\n");
                
                for(siter = set.begin(); siter != set.end(); siter++)
                    fprintf(f, "%s\n", siter->c_str());

                fprintf(f, "EndEquiv\n");
            }
        }

        fclose(f);

        AfxMessageBox("File generated", MB_OK);
    }
    else
    {
        AfxMessageBox("File generation failed", MB_OK);
    }
}

void CPropertyPageGenerate::OnUpdateRunButton(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(!m_folder.IsEmpty());
}
