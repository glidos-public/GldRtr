// GameSettingDouble.h: interface for the CGameSettingDouble class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAMESETTINGDOUBLE_H__A4346DA5_7AD7_4037_A96A_66F959FA6A40__INCLUDED_)
#define AFX_GAMESETTINGDOUBLE_H__A4346DA5_7AD7_4037_A96A_66F959FA6A40__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GameSetting.h"


class CDouble
{
protected:
	double m_value;

public:
	operator double()
	{
		return m_value;
	};
};


class CGameSettingDouble : public CGameSetting<CDouble>
{
public:
	void                Init(CStoredStruct *st, const char *name, double init);
	CGameSettingDouble &operator=(const double &i);

private:
	bool    Parse(CString &val);
	CString Print();
};


#endif // !defined(AFX_GAMESETTINGDOUBLE_H__A4346DA5_7AD7_4037_A96A_66F959FA6A40__INCLUDED_)
