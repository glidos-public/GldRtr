/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide router component
 * 
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.3  2002/06/24 15:40:51  paul
 * Change synchronisation of server to client vxd pipe
 * so that writes return before reads block.
 *
 * Revision 1.2  2002/06/02 15:16:25  paul
 * Wake up both ends of the pipe on an abort
 *
 * Revision 1.1  2002/06/02 09:45:55  paul
 * Backward vxd pipe for sending from server to client under W98
 *
 * Revision 1.8  2001/07/28 12:06:17  paul
 * Revert back to previous synchronisation flags.
 *
 * Revision 1.7  2001/07/21 20:21:04  paul
 * Removed BLOCK_THREAD_IDLE from semaphore waits.
 *
 * Revision 1.6  2001/07/16 14:13:07  paul
 * Speed up DOS to Windows data flow using large buffers in all components
 *
 * Revision 1.5  2001/06/26 14:02:01  paul
 * Corrected stupid bugs
 *
 * Revision 1.4  2001/06/25 18:34:28  paul
 * Changed synchronisation to stop deadlocks
 * Added flush (triggered by zero input)
 *
 * Revision 1.3  2001/06/20 16:12:17  paul
 * Use count for buffer size in PM API to allow other than strings to be passed.
 *
 * Provided an abort method to stop the server hanging in the get call.
 *
 * Revision 1.2  2001/06/20 09:17:09  paul
 * Added synchronisation and in/out indexes to give pipe-like behaviour
 *
 * Revision 1.1  2001/06/19 16:08:42  paul
 * Initial version
 *
 ************************************************************/

#define WANTVXDWRAPS

#include <basedef.h>
#include <vmm.h>
#include <debug.h>
#include <vxdwraps.h>
#include <vwin32.h>
#include <winerror.h>

typedef DIOCPARAMETERS *LPDIOC;

#pragma VxD_LOCKED_CODE_SEG
#pragma VxD_LOCKED_DATA_SEG


DWORD _stdcall GLDPIPE_W32_DeviceIOControl(DWORD, DWORD, DWORD, LPDIOC);
DWORD _stdcall GLDPIPE_CleanUp(void);


void memcpy(char *t, char *s, int c)
{
    while(c--)
        *t++ = *s++;
}


#define BUFSIZE (1024*1024)

char buffer[BUFSIZE];

volatile int in_ptr;
volatile int out_ptr;
volatile int aborted;
volatile int put_entered;
volatile int get_entered;
volatile int nf, ne;

VMM_SEMAPHORE not_full;
VMM_SEMAPHORE not_empty;

void initialise_pipe(void)
{
    not_full = Create_Semaphore(0);
    not_empty = Create_Semaphore(0);
    in_ptr = out_ptr = aborted = 0;
}

void finalise_pipe(void)
{
    Destroy_Semaphore(not_full);
    Destroy_Semaphore(not_empty);
}

void put_bytes(char *buf, int count)
{
    int free;

    put_entered = 1;

    while(count)
    {
        free = out_ptr - in_ptr - 1;
        if(free < 0)
        {
            free = BUFSIZE - in_ptr;

            if(out_ptr == 0)
                free -= 1;
        }

        if(free > 0)
        {
            if(free > count)
                free = count;

            memcpy(buffer+in_ptr, buf, free);

            in_ptr += free;
            if(in_ptr == BUFSIZE)
                in_ptr = 0;
            
            buf += free;
            count -= free;
        }
        else
        {
            nf = 0;
            Wait_Semaphore(not_full, BLOCK_SVC_INTS|BLOCK_THREAD_IDLE);
            nf = 1;
        }
    }

    Signal_Semaphore(not_empty);

    put_entered = 0;
}

int get_bytes(char *buf, int count)
{
    int free, accumulated;

    get_entered = 1;

    accumulated = 0;

    while(count && !aborted)
    {
        free = in_ptr - out_ptr;
        if(free < 0)
            free = BUFSIZE - out_ptr;

        if(free > 0)
        {
            if(free > count)
                free = count;

            memcpy(buf, buffer+out_ptr, free);

            out_ptr += free;
            if(out_ptr == BUFSIZE)
                out_ptr = 0;
            
            buf += free;
            count -= free;
            accumulated += free;
        }
        else
        {
            ne = 0;
            Wait_Semaphore(not_empty, BLOCK_SVC_INTS|BLOCK_THREAD_IDLE);
            ne = 1;
        }
    }

    Signal_Semaphore(not_full);

    get_entered = 0;

    return accumulated;
}

void abort_get(void)
{
    aborted = 1;
    Signal_Semaphore(not_empty);
    Signal_Semaphore(not_full);
}

DWORD _stdcall GLDPIPE_W32_DeviceIOControl(DWORD  dwService,
                                        DWORD  dwDDB,
                                        DWORD  hDevice,
                                        LPDIOC lpDIOCParms)
{
    DWORD dwRetVal = 0;

    // DIOC_OPEN is sent when VxD is loaded w/ CreateFile 
    //  (this happens just after SYS_DYNAMIC_INIT)
    if(dwService == DIOC_OPEN)
    {
        // Must return 0 to tell WIN32 that this VxD supports DEVIOCTL
        dwRetVal = 0;
    }
    // DIOC_CLOSEHANDLE is sent when VxD is unloaded w/ CloseHandle
    //  (this happens just before SYS_DYNAMIC_EXIT)
    else if(dwService == DIOC_CLOSEHANDLE)
    {
        // Dispatch to cleanup proc
        dwRetVal = GLDPIPE_CleanUp();
    }
    else if(dwService == 1)
    {
        put_bytes((char *)lpDIOCParms->lpvOutBuffer, lpDIOCParms->cbOutBuffer);
        *((int *)lpDIOCParms->lpcbBytesReturned) = 0;
    
        dwRetVal = NO_ERROR;
    }
    else if(dwService == 2)
    {
        abort_get();
    }
    else if(dwService == 3)
    {
        int *vals = (int *) lpDIOCParms->lpvOutBuffer;
        vals[0] = get_entered;
        vals[1] = put_entered;
        vals[2] = in_ptr;
        vals[3] = out_ptr;
        vals[4] = ne;
        vals[5] = nf;

        dwRetVal = NO_ERROR;
    }
    else
    {
        // Returning a positive value will cause the WIN32 DeviceIOControl
        // call to return FALSE, the error code can then be retrieved
        // via the WIN32 GetLastError
        dwRetVal = ERROR_NOT_SUPPORTED;
    }
    return(dwRetVal);
}


DWORD _stdcall GLDPIPE_Dynamic_Exit(void)
{
    finalise_pipe();

    return(VXD_SUCCESS);
}

DWORD _stdcall GLDPIPE_CleanUp(void)
{
    return(VXD_SUCCESS);
}

/****************************************************************************
 *                CVXD_VMAPI
 *
 *    ENTRY: function - the function number (passed in eax)
 *        parm1 - parameter 1 (passed in ebx)
 *        parm2 - parameter 2 (passed in ecx)
 *
 *    EXIT:    NONE
 ***************************************************************************/
int _stdcall GLDPIPE_VMAPI(char *function,
                        unsigned int parm1,
                        unsigned int parm2)
{
    return 10;
}

/****************************************************************************
 *                CVXD_PMAPI
 *
 *    ENTRY: function - the function number (passed in eax)
 *        parm1 - parameter 1 (passed in ebx)
 *        parm2 - parameter 2 (passed in ecx)
 *
 *    EXIT:    NONE
 ***************************************************************************/
int _stdcall GLDPIPE_PMAPI(char *function,
                        unsigned int parm1,
                        unsigned int parm2)
{
    get_bytes(function, parm1);

    return 20;
}

#pragma VxD_ICODE_SEG
#pragma VxD_IDATA_SEG

DWORD _stdcall GLDPIPE_Dynamic_Init(void)
{
    initialise_pipe();

    return(VXD_SUCCESS);
}



