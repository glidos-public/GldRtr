// PollPipe.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include <stdio.h>

extern "C"
{
#define i386
#include "vddsvc.h"
}

#include "PipeThread.h"
#include "TimerThread.h"

#include "PollPipe.h"

static CPipeThread pipe_thread;
static CTimerThread timer_thread;

BOOL DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}


void DispatchCall(void)
{
    switch(getEDX())
    {
    case PP_OPEN:
        pipe_thread.Open();
        break;

    case PP_CLOSE:
        pipe_thread.Close();
        break;

    case PP_READ:
        pipe_thread.Read((void *) getEBX(), getECX());
        break;

    case PP_WRITE:
        pipe_thread.Write((void *) getEBX(), getECX());
        break;

    case PP_POLL:
        timer_thread.Poll();
        setECX(pipe_thread.Poll());
        break;

    case PP_JUDFIX:
        timer_thread.Start(getECX());
        break;

    case PP_PICPREDICT:
        setECX((int)&(timer_thread.m_pic_time));
        break;
    }
}
