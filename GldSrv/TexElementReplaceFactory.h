// TexElementReplaceFactory.h: interface for the CTexElementReplaceFactory class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXELEMENTREPLACEFACTORY_H__0026FC5E_D04B_4B2B_8918_D38AD6E4BC7C__INCLUDED_)
#define AFX_TEXELEMENTREPLACEFACTORY_H__0026FC5E_D04B_4B2B_8918_D38AD6E4BC7C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TexReplace.h"
#include "TexElementActFactory.h"
#include "TexElementFactory.h"

class CTexElementReplaceFactory : public CTexElementActFactory
{
private:
    CTexHandlerReplace *m_tex_replace;
public:
    CTexElementAct *MakeTexElement();
    CTexElementAct *MakeTexElement(CBox &b, CString &name);

	CTexElementReplaceFactory(CTexHandlerReplace *tex_replace);
	virtual ~CTexElementReplaceFactory();

};

#endif // !defined(AFX_TEXELEMENTREPLACEFACTORY_H__0026FC5E_D04B_4B2B_8918_D38AD6E4BC7C__INCLUDED_)
