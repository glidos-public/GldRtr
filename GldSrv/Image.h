// Image.h: interface for the CImage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IMAGE_H__688C6D5D_C6A9_4DC2_9303_0EE88A031EB7__INCLUDED_)
#define AFX_IMAGE_H__688C6D5D_C6A9_4DC2_9303_0EE88A031EB7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CImage  
{
public:
	virtual bool     HasAlpha();
	virtual COLORREF GetPixel(int x, int y) = 0;
	virtual int      GetHeight()            = 0;
	virtual int      GetWidth()             = 0;
	static  CImage  *Load(CString &path);

	CImage();
	virtual ~CImage();
};

#endif // !defined(AFX_IMAGE_H__688C6D5D_C6A9_4DC2_9303_0EE88A031EB7__INCLUDED_)
