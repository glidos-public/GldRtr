// ImagePNG.h: interface for the CImagePNG class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IMAGEPNG_H__35473D9B_113B_4DDD_B8FA_0C7E2DE09018__INCLUDED_)
#define AFX_IMAGEPNG_H__35473D9B_113B_4DDD_B8FA_0C7E2DE09018__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Image.h"
extern "C"
{
#include "png.h"
}

class CImagePNG : public CImage  
{
private:
    png_structp m_png_ptr;
    png_infop   m_info_ptr;
    png_bytep  *m_row_pointers;
    int         m_width;
    int         m_height;
    int         m_channels;
    bool        Load(FILE *f, int size);

public:
    virtual bool     HasAlpha();
    virtual COLORREF GetPixel(int x, int y);
    virtual int      GetHeight();
    virtual int      GetWidth();

    virtual ~CImagePNG();

    static  CImage  *TryLoad(FILE *f, unsigned char *buf, int size);

private:
    CImagePNG();

};

#endif // !defined(AFX_IMAGEPNG_H__35473D9B_113B_4DDD_B8FA_0C7E2DE09018__INCLUDED_)
