// BloodFix.cpp: implementation of the CBloodFix class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <math.h>
#include "GldSrv.h"
#include "BloodFix.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBloodFix::CBloodFix(CGlideDll *wrapper, float xfac, float yfac) : m_xfac(xfac), m_yfac(yfac)
{
    m_holding = None;
    m_nlines  = 0;
    m_wrapper = wrapper;
}

CBloodFix::~CBloodFix()
{

}

bool CBloodFix::Accept(GrVertex *v)
{
    if(v[0].x == v[2].x && v[0].y == v[1].y && v[0].x + 1.0 == v[1].x)
    {
        if(m_nlines >= MaxLines
            || m_holding != Vertical
            || v[0].x != m_lines[m_nlines-1].v1.x + 1.0
            || fabs(v[0].tmuvtx[0].sow - m_lines[m_nlines-1].v2.tmuvtx[0].sow) > 20.0
            || fabs(v[0].tmuvtx[0].tow - m_lines[m_nlines-1].v2.tmuvtx[0].tow) > 20.0)
        {
            Flush();
        }

        m_lines[m_nlines].v1 = v[2];
        m_lines[m_nlines].v2 = v[0];

        m_nlines += 1;

        m_holding = Vertical;

        return true;
    }
    else if(v[2].x == v[1].x && v[2].y == v[0].y && v[2].y + 1.0 == v[1].y)
    {
        if(m_nlines >= MaxLines
            || m_holding != Horizontal
            || (v[2].y != m_lines[m_nlines-1].v1.y + 1.0 && v[2].y != m_lines[m_nlines-1].v1.y - 1.0)
            || fabs(v[2].tmuvtx[0].sow - m_lines[m_nlines-1].v1.tmuvtx[0].sow) > 20.0
            || fabs(v[2].tmuvtx[0].tow - m_lines[m_nlines-1].v1.tmuvtx[0].tow) > 20.0)
        {
            Flush();
        }

        m_lines[m_nlines].v1 = v[2];
        m_lines[m_nlines].v2 = v[0];

        m_nlines += 1;

        m_holding = Horizontal;

        return true;
    }
    else
    {
        Flush();

        return false;
    }
}

void CBloodFix::Flush()
{
    if(m_holding != None)
    {
        if(m_holding == Horizontal && m_lines[0].v1.y > m_lines[m_nlines-1].v1.y)
            reverse();

        render(m_lines, m_nlines);

        m_holding = None;
        m_nlines  = 0;
    }
}

void CBloodFix::render(GrLine *lines, int nlines)
{
    int cut;
    int i;
    
    if(split(lines, nlines, &cut))
    {
        render(lines, cut);
        render(lines + cut, nlines - cut);
    }
    else if(wall_or_floor(lines, nlines))
    {
        /* Correct the perspective */
        if(nlines >= 3)
        {
            GrVertex vl[2], vr[2], vm[2];
            int    m_i;
            double m_ydiff, m_sdiff, m_tdiff;
            double l_ydiff, l_sdiff, l_tdiff;
            double r_ydiff, r_sdiff, r_tdiff;
            float  fac;
            
            m_i = (nlines-1)/2;
            
            vl[0] = lines[0].v1;
            vl[1] = lines[0].v2;
            
            vr[0] = lines[nlines-1].v1;
            vr[1] = lines[nlines-1].v2;
            
            vm[0] = lines[m_i].v1;
            vm[1] = lines[m_i].v2;
            
            if(m_holding == Vertical)
            {
                l_ydiff = vl[0].y - vl[1].y;
                r_ydiff = vr[0].y - vr[1].y;
                m_ydiff = vm[0].y - vm[1].y;
            }
            else
            {
                l_ydiff = vl[0].x - vl[1].x;
                r_ydiff = vr[0].x - vr[1].x;
                m_ydiff = vm[0].x - vm[1].x;
            }
            
            m_sdiff = (vm[0].tmuvtx[0].sow - vm[1].tmuvtx[0].sow) / m_ydiff;
            m_tdiff = (vm[0].tmuvtx[0].tow - vm[1].tmuvtx[0].tow) / m_ydiff;
            
            if(fabs(l_ydiff) > fabs(r_ydiff))
            {
                l_sdiff = (vl[0].tmuvtx[0].sow - vl[1].tmuvtx[0].sow) / l_ydiff;
                l_tdiff = (vl[0].tmuvtx[0].tow - vl[1].tmuvtx[0].tow) / l_ydiff;
                
                fac = (float)sqrt((l_tdiff*l_tdiff+l_sdiff*l_sdiff)
                                    / (m_tdiff*m_tdiff+m_sdiff*m_sdiff));
                
                fac = (float)((fac - 1.0) / m_i);
                
                for(i = 0; i < nlines; i++)
                {
                    float ifac = (float)(1.0 + fac * i);
                    
                    lines[i].v1.oow           *= ifac;
                    lines[i].v1.tmuvtx[0].sow *= ifac;
                    lines[i].v1.tmuvtx[0].tow *= ifac;
                    
                    lines[i].v2.oow           *= ifac;
                    lines[i].v2.tmuvtx[0].sow *= ifac;
                    lines[i].v2.tmuvtx[0].tow *= ifac;
                }

                lnudge_texture(lines, nlines);
            }
            else
            {
                r_sdiff = (vr[0].tmuvtx[0].sow - vr[1].tmuvtx[0].sow) / r_ydiff;
                r_tdiff = (vr[0].tmuvtx[0].tow - vr[1].tmuvtx[0].tow) / r_ydiff;
                
                fac = (float)sqrt((r_tdiff*r_tdiff+r_sdiff*r_sdiff)
                                    / (m_tdiff*m_tdiff+m_sdiff*m_sdiff));
                
                fac = (float)((fac - 1.0) / (nlines - 1 - m_i));
                
                for(i = 0; i < nlines; i++)
                {
                    float ifac = (float)(1.0 + fac * (nlines - 1 - i));
                    
                    lines[i].v1.oow           *= ifac;
                    lines[i].v1.tmuvtx[0].sow *= ifac;
                    lines[i].v1.tmuvtx[0].tow *= ifac;
                    
                    lines[i].v2.oow           *= ifac;
                    lines[i].v2.tmuvtx[0].sow *= ifac;
                    lines[i].v2.tmuvtx[0].tow *= ifac;
                }

                rnudge_texture(lines, nlines);
            }
        }

        GrVertex v[4];
        v[0] = lines[0].v1;
        v[1] = lines[0].v2;
        v[2] = lines[nlines-1].v2;
        v[3] = lines[nlines-1].v1;

        if(m_holding == Vertical)
        {
            v[2].x += 1.0;
            v[3].x += 1.0;
        }
        else
        {
            v[2].y += 1.0;
            v[3].y += 1.0;
        }
        
        for(i = 0; i < 4; i++)
        {
            v[i].x *= m_xfac;
            v[i].y *= m_yfac;
        }

        m_wrapper->grDrawPolygonVertexList(4, v);
    }
    else
    {
        for(i = 0; i < nlines; i++)
        {
            int j;
            GrVertex v[4];
            v[0] = lines[i].v1;
            v[1] = lines[i].v2;
            v[2] = lines[i].v2;
            v[3] = lines[i].v1;

            if(m_holding == Vertical)
            {
                v[2].x += 1.0;
                v[3].x += 1.0;
            }
            else
            {
                v[2].y += 1.0;
                v[3].y += 1.0;
            }
            
            for(j = 0; j < 4; j++)
            {
                v[j].x *= m_xfac;
                v[j].y *= m_yfac;
            }
            
            m_wrapper->grDrawPolygonVertexList(4, v);
        }
    }
}

bool CBloodFix::split(GrLine *lines, int nlines, int *cut)
{
    double grad;
    int    i;
    int    max_i;
    double max_v;

    if(nlines <= 2)
        return false;

    if(m_holding == Vertical)
    {
        /* calculate gradient of triangle top */
        grad = (lines[nlines-1].v1.y - lines[0].v1.y)
            / (lines[nlines-1].v1.x - lines[0].v1.x);
        
        max_i = 1;
        max_v = 0.0;
        
        /* Find maximum vertical displacement from straight line */
        for(i = 1; i < nlines-1; i++)
        {
            double v;
            
            v = fabs(lines[i].v1.y - lines[0].v1.y - (lines[i].v1.x - lines[0].v1.x) * grad);
            
            if(v > max_v)
            {
                max_v = v;
                max_i = i;
            }
        }
        
        if(max_v > 1.0)
        {
            *cut = max_i;
            return true;
        }
        
        /* calculate gradient of triangle bottom */
        grad = (lines[nlines-1].v2.y - lines[0].v2.y)
            / (lines[nlines-1].v2.x - lines[0].v2.x);
        
        max_i = 1;
        max_v = 0.0;
        
        /* Find maximum vertical displacement from straight line */
        for(i = 1; i < nlines-1; i++)
        {
            double v;
            
            v = fabs(lines[i].v2.y - lines[0].v2.y - (lines[i].v2.x - lines[0].v2.x) * grad);
            
            if(v > max_v)
            {
                max_v = v;
                max_i = i;
            }
        }
        
        if(max_v > 1.0)
        {
            *cut = max_i;
            return true;
        }
    }
    else
    {
        /* calculate gradient of triangle top */
        grad = (lines[nlines-1].v1.x - lines[0].v1.x)
            / (lines[nlines-1].v1.y - lines[0].v1.y);
        
        max_i = 1;
        max_v = 0.0;
        
        /* Find maximum vertical displacement from straight line */
        for(i = 1; i < nlines-1; i++)
        {
            double v;
            
            v = fabs(lines[i].v1.x - lines[0].v1.x - (lines[i].v1.y - lines[0].v1.y) * grad);
            
            if(v > max_v)
            {
                max_v = v;
                max_i = i;
            }
        }
        
        if(max_v > 1.0)
        {
            *cut = max_i;
            return true;
        }
        
        /* calculate gradient of triangle bottom */
        grad = (lines[nlines-1].v2.x - lines[0].v2.x)
            / (lines[nlines-1].v2.y - lines[0].v2.y);
        
        max_i = 1;
        max_v = 0.0;
        
        /* Find maximum vertical displacement from straight line */
        for(i = 1; i < nlines-1; i++)
        {
            double v;
            
            v = fabs(lines[i].v2.x - lines[0].v2.x - (lines[i].v2.y - lines[0].v2.y) * grad);
            
            if(v > max_v)
            {
                max_v = v;
                max_i = i;
            }
        }
        
        if(max_v > 1.0)
        {
            *cut = max_i;
            return true;
        }
    }

    return false;
}

void CBloodFix::lnudge_texture(GrLine *lines, int nlines)
{
    double x  = 0.0;
    double x2 = 0.0;
    double s  = 0.0;
    double sx = 0.0;
    double t  = 0.0;
    double tx = 0.0;
    double n  = (double)nlines;
    int i;
    double ms, mt;
    double bs, bt;
    double s0, t0;
    double sn, tn;
    float  snudge_1;
    float  tnudge_1;
    float  snudge_n;
    float  tnudge_n;

    sn = lines[nlines-1].v1.tmuvtx[0].sow;
    tn = lines[nlines-1].v1.tmuvtx[0].tow;

    for(i = 0; i < nlines; i++)
    {
        /* For each i, extrapolate s & t increment from lines[nlines-1].v1.y */
        double si;
        double ti;
        double rat;
        double div;

        if(m_holding == Vertical)
        {
            div = lines[i].v2.y - lines[i].v1.y;
            rat = (lines[nlines-1].v1.y - lines[i].v1.y) / div;
        }
        else
        {
            div = lines[i].v2.x - lines[i].v1.x;
            rat = (lines[nlines-1].v1.x - lines[i].v1.x) / div;
        }

        si = lines[i].v1.tmuvtx[0].sow + (lines[i].v2.tmuvtx[0].sow - lines[i].v1.tmuvtx[0].sow) * rat;
        ti = lines[i].v1.tmuvtx[0].tow + (lines[i].v2.tmuvtx[0].tow - lines[i].v1.tmuvtx[0].tow) * rat;

        if(i == 0)
        {
            s0 = si;
            t0 = ti;
        }

        x  += (double) i;
        x2 += ((double) i) * ((double) i);
        s  += si;
        sx += si * (double) i;
        t  += ti;
        tx += ti * (double) i;
    }

    /* Work out best fit lines (best x to s&t mapping) */
    ms = (s*x - n*sx) / (x*x - n*x2);
    bs = (x*sx - x2*s) / (x*x - n*x2);

    mt = (t*x - n*tx) / (x*x - n*x2);
    bt = (x*tx - x2*t) / (x*x - n*x2);

    /* Calculate the difference from best at the end points */
    snudge_1 = (float)(s0 - bs);
    tnudge_1 = (float)(t0 - bt);

    snudge_n = (float)(sn - bs - ms * (double) (nlines-1));
    tnudge_n = (float)(tn - bt - mt * (double) (nlines-1));

    /* Nudge the texture */
    lines[0].v1.tmuvtx[0].sow -= snudge_1;
    lines[0].v1.tmuvtx[0].tow -= tnudge_1;
    lines[0].v2.tmuvtx[0].sow -= snudge_1;
    lines[0].v2.tmuvtx[0].tow -= tnudge_1;

    lines[nlines-1].v1.tmuvtx[0].sow -= snudge_n;
    lines[nlines-1].v1.tmuvtx[0].tow -= tnudge_n;
    lines[nlines-1].v2.tmuvtx[0].sow -= snudge_n;
    lines[nlines-1].v2.tmuvtx[0].tow -= tnudge_n;

    /* Allow for the 1 pixel stretch */
    lines[nlines-1].v1.tmuvtx[0].sow += (float)ms;
    lines[nlines-1].v1.tmuvtx[0].tow += (float)mt;
    lines[nlines-1].v2.tmuvtx[0].sow += (float)ms;
    lines[nlines-1].v2.tmuvtx[0].tow += (float)mt;

    double oowrate = (lines[nlines-1].v1.oow - lines[0].v1.oow) / (double) nlines;

    lines[nlines-1].v1.oow += (float)oowrate;
    lines[nlines-1].v2.oow += (float)oowrate;
}

void CBloodFix::rnudge_texture(GrLine *lines, int nlines)
{
    double x  = 0.0;
    double x2 = 0.0;
    double s  = 0.0;
    double sx = 0.0;
    double t  = 0.0;
    double tx = 0.0;
    double n  = (double)nlines;
    int i;
    double ms, mt;
    double bs, bt;
    double s0, t0;
    double sn, tn;
    float  snudge_1;
    float  tnudge_1;
    float  snudge_n;
    float  tnudge_n;

    s0 = lines[0].v1.tmuvtx[0].sow;
    t0 = lines[0].v1.tmuvtx[0].tow;

    for(i = 0; i < nlines; i++)
    {
        /* For each i, extrapolate s & t increment from lines[0].v1.y */
        double si;
        double ti;
        double rat;
        double div;

        if(m_holding == Vertical)
        {
            div = lines[i].v2.y - lines[i].v1.y;
            rat = (lines[0].v1.y - lines[i].v1.y) / div;
        }
        else
        {
            div = lines[i].v2.x - lines[i].v1.x;
            rat = (lines[0].v1.x - lines[i].v1.x) / div;
        }

        si = lines[i].v1.tmuvtx[0].sow + (lines[i].v2.tmuvtx[0].sow - lines[i].v1.tmuvtx[0].sow) * rat;
        ti = lines[i].v1.tmuvtx[0].tow + (lines[i].v2.tmuvtx[0].tow - lines[i].v1.tmuvtx[0].tow) * rat;

        if(i == nlines-1)
        {
            sn = si;
            tn = ti;
        }

        x  += (double) i;
        x2 += ((double) i) * ((double) i);
        s  += si;
        sx += si * (double) i;
        t  += ti;
        tx += ti * (double) i;
    }

    /* Work out best fit lines (best x to s&t mapping) */
    ms = (s*x - n*sx) / (x*x - n*x2);
    bs = (x*sx - x2*s) / (x*x - n*x2);

    mt = (t*x - n*tx) / (x*x - n*x2);
    bt = (x*tx - x2*t) / (x*x - n*x2);

    /* Calculate the difference from best at the end points */
    snudge_1 = (float)(s0 - bs);
    tnudge_1 = (float)(t0 - bt);

    snudge_n = (float)(sn - bs - ms * (double) (nlines-1));
    tnudge_n = (float)(tn - bt - mt * (double) (nlines-1));

    /* Nudge the texture */
    lines[0].v1.tmuvtx[0].sow -= snudge_1;
    lines[0].v1.tmuvtx[0].tow -= tnudge_1;
    lines[0].v2.tmuvtx[0].sow -= snudge_1;
    lines[0].v2.tmuvtx[0].tow -= tnudge_1;

    lines[nlines-1].v1.tmuvtx[0].sow -= snudge_n;
    lines[nlines-1].v1.tmuvtx[0].tow -= tnudge_n;
    lines[nlines-1].v2.tmuvtx[0].sow -= snudge_n;
    lines[nlines-1].v2.tmuvtx[0].tow -= tnudge_n;

    /* Allow for the 1 pixel stretch */
    lines[nlines-1].v1.tmuvtx[0].sow += (float)ms;
    lines[nlines-1].v1.tmuvtx[0].tow += (float)mt;
    lines[nlines-1].v2.tmuvtx[0].sow += (float)ms;
    lines[nlines-1].v2.tmuvtx[0].tow += (float)mt;

    double oowrate = (lines[nlines-1].v1.oow - lines[0].v1.oow) / (double) nlines;

    lines[nlines-1].v1.oow += (float)oowrate;
    lines[nlines-1].v2.oow += (float)oowrate;
}

bool CBloodFix::wall_or_floor(GrLine *lines, int nlines)
{
    int mid;
    double l_ydiff;
    double r_ydiff;
    double m_ydiff;
    double sdiff1;
    double tdiff1;
    double sdiff2;
    double tdiff2;

    if(nlines <= 2)
        return true;

    /*
     * Compare the mid line with the the longest of the two
     * edges, and check that the two vectors through the 
     * texture have the same direction
     */
    mid = (nlines-1) / 2;

    if(m_holding == Vertical)
    {
        l_ydiff = lines[0].v1.y        - lines[0].v2.y;
        r_ydiff = lines[nlines-1].v1.y - lines[nlines-1].v2.y;
        m_ydiff = lines[mid].v1.y      - lines[mid].v2.y;
    }
    else
    {
        return true;
        l_ydiff = lines[0].v1.x        - lines[0].v2.x;
        r_ydiff = lines[nlines-1].v1.x - lines[nlines-1].v2.x;
        m_ydiff = lines[mid].v1.x      - lines[mid].v2.x;
    }

    if(fabs(l_ydiff) > fabs(r_ydiff))
    {
        sdiff1 = lines[0].v1.tmuvtx[0].sow - lines[0].v2.tmuvtx[0].sow;
        tdiff1 = lines[0].v1.tmuvtx[0].tow - lines[0].v2.tmuvtx[0].tow;
        sdiff2 = lines[mid].v1.tmuvtx[0].sow - lines[mid].v2.tmuvtx[0].sow;
        tdiff2 = lines[mid].v1.tmuvtx[0].tow - lines[mid].v2.tmuvtx[0].tow;
    }
    else
    {
        sdiff1 = lines[mid].v1.tmuvtx[0].sow - lines[mid].v2.tmuvtx[0].sow;
        tdiff1 = lines[mid].v1.tmuvtx[0].tow - lines[mid].v2.tmuvtx[0].tow;
        sdiff2 = lines[nlines-1].v1.tmuvtx[0].sow - lines[nlines-1].v2.tmuvtx[0].sow;
        tdiff2 = lines[nlines-1].v1.tmuvtx[0].tow - lines[nlines-1].v2.tmuvtx[0].tow;
    }

    return sdiff1 * tdiff2 == sdiff2 * tdiff1;
}

void CBloodFix::reverse()
{
    int i;

    for(i = 0; i < m_nlines/2; i++)
    {
        GrLine tmp = m_lines[i];
        m_lines[i] = m_lines[m_nlines - 1 - i];
        m_lines[m_nlines - 1 - i] = tmp;
    }
}
