#if !defined(AFX_PROPERTYPAGEGENERATE_H__8211382F_769B_451B_AB83_F856C9E0CA55__INCLUDED_)
#define AFX_PROPERTYPAGEGENERATE_H__8211382F_769B_451B_AB83_F856C9E0CA55__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropertyPageGenerate.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageGenerate dialog

#include "PropertyPageBase.h"

class CPropertyPageGenerate : public CPropertyPageBase
{
	DECLARE_DYNCREATE(CPropertyPageGenerate)

// Construction
public:
	CPropertyPageGenerate();
	~CPropertyPageGenerate();

// Dialog Data
	//{{AFX_DATA(CPropertyPageGenerate)
	enum { IDD = IDD_PROPPAGE_GENERATE };
	CString	m_folder;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropertyPageGenerate)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyPageGenerate)
	afx_msg void OnButtonBrowseFolder();
	afx_msg void OnButtonRun();
	//}}AFX_MSG
    void OnUpdateRunButton(CCmdUI *pCmdUI);
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTYPAGEGENERATE_H__8211382F_769B_451B_AB83_F856C9E0CA55__INCLUDED_)
