// W98Pipe.h: interface for the CW98Pipe class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_W98PIPE_H__839ACEE2_CFC7_11D5_B9FC_00C0DFF0F563__INCLUDED_)
#define AFX_W98PIPE_H__839ACEE2_CFC7_11D5_B9FC_00C0DFF0F563__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GldPipe.h"

class CW98Pipe : public CGldPipe  
{
private:
    HANDLE m_pipe;
    HANDLE m_epip;
    OVERLAPPED overlapped;
public:
	virtual bool Write(char *buf, int n);
	static CW98Pipe * NewPipe();
	virtual void Wake();
	virtual bool Read(char *buf, int requested, unsigned long *returned);
	virtual bool Create();
	CW98Pipe();
	virtual ~CW98Pipe();

};

#endif // !defined(AFX_W98PIPE_H__839ACEE2_CFC7_11D5_B9FC_00C0DFF0F563__INCLUDED_)
