// PlayAudioFile.cpp: implementation of the CPlayAudioFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "PlayAudioFile.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPlayAudioFile::CPlayAudioFile()
{
	m_builder.CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER);

	if(m_builder != NULL)
		m_builder.QueryInterface(&m_graph);

	if(m_graph != NULL)
        m_builder.QueryInterface(&m_control);
    
	if(m_control != NULL)
        m_builder.QueryInterface(&m_seeking);

	if(m_seeking == NULL)
	{
		m_control = NULL;
		m_graph   = NULL;
		m_seeking = NULL;
		m_builder = NULL;
	}

	m_might_be_running = false;
}

CPlayAudioFile::~CPlayAudioFile()
{
}

void CPlayAudioFile::Start(CString &file)
{
	CComPtr<IPin>   pin;
    wchar_t         buf[256];
    int             len;
	LONGLONG        pos = 0;

	if(m_builder == NULL)
		return;
	
	len = MultiByteToWideChar(CP_ACP, 0, (LPCSTR)file, file.GetLength(), buf, 256);
	buf[len] = 0;
	
	if(m_input != NULL)
	{
	    m_control->Stop();
		m_graph->RemoveFilter(m_input);
		m_input = NULL;
	}

	m_builder->AddSourceFilter(buf, L"input", &m_input);
	if(m_input == NULL)
		return;

	m_input->FindPin(L"Output", &pin);
	if(pin == NULL)
		return;

	m_builder->Render(pin);
	m_seeking->SetPositions(&pos, AM_SEEKING_AbsolutePositioning,
		                    &pos, AM_SEEKING_NoPositioning);
	m_control->Run();

	m_might_be_running = true;
}

void CPlayAudioFile::Stop()
{
    if(m_builder == NULL)
		return;
		
	if(m_might_be_running)
	{
		LONGLONG pos = 0;
		
		m_seeking->SetPositions(&pos, AM_SEEKING_AbsolutePositioning,
			&pos, AM_SEEKING_AbsolutePositioning);
		
		m_might_be_running = false;
	}
}

bool CPlayAudioFile::Playing()
{
	if(m_builder == NULL)
		return false;

	if(m_might_be_running)
	{
		HRESULT         res;
		LONGLONG        current;
		LONGLONG        stop;
		
		res = m_seeking->GetPositions(&current, &stop);
		
		m_might_be_running = (res == S_OK ? current < stop : false);
	}

	return m_might_be_running;
}
