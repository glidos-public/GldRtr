#if !defined(AFX_PROPERTYPAGEJOIN_H__C1FDEB1C_0E3C_4E37_A7D3_AA8E0BF17A50__INCLUDED_)
#define AFX_PROPERTYPAGEJOIN_H__C1FDEB1C_0E3C_4E37_A7D3_AA8E0BF17A50__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropertyPageJoin.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageJoin dialog

#include "PropertyPageBase.h"

class CPropertyPageJoin : public CPropertyPageBase
{
	DECLARE_DYNCREATE(CPropertyPageJoin)

// Construction
public:
	CPropertyPageJoin();
	~CPropertyPageJoin();

// Dialog Data
	//{{AFX_DATA(CPropertyPageJoin)
	enum { IDD = IDD_PROPPAGE_JOIN };
	CString	m_file1;
	CString	m_file2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropertyPageJoin)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyPageJoin)
	afx_msg void OnButtonBrowseFile1();
	afx_msg void OnButtonBrowseFile2();
	afx_msg void OnButtonJoin();
	//}}AFX_MSG
    void OnUpdateJoinButton(CCmdUI *pCmdUI);
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTYPAGEJOIN_H__C1FDEB1C_0E3C_4E37_A7D3_AA8E0BF17A50__INCLUDED_)
