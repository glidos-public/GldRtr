// TexElementFactory.cpp: implementation of the CTexElementFactory class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TexElementFactory.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexElementFactory::CTexElementFactory()
{

}

CTexElementFactory::~CTexElementFactory()
{

}

CTexElement *CTexElementFactory::MakeTexElement()
{
    return new CTexElement();
}

CTexElement *CTexElementFactory::MakeTexElement(CBox &b, CString &name)
{
    return new CTexElement(b, name);
}
