

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GameSetting.h"
#include "GameSettingString.h"
#include "GameSettingArray.h"
#include "GameSettingBool.h"
#include "GameSettingInt.h"
#include "GameSettingDouble.h"
#include "GameSettingTex.h"
#include "GameSettingDriver.h"
#include "GameSettingRedbook.h"
#include "GameSettingDosEmu.h"
#include "GameSettingResolution.h"
#include "sdk2_3dfx.h"


class CGameRecord : public CStoredStruct
{
public:
    FxU32 ConfigFlags();
    CGameSettingString     name;
    CGameSettingString     executable;
    CGameSettingInt        subfolders;
    CGameSettingArrayInt   sizes;
    CGameSettingString     cd_path;
    CGameSettingString     cd_file;
    CGameSettingDriver     driver;
    CGameSettingBool       mipmaps;
    CGameSettingBool       palette_tricks_cache;
    CGameSettingBool       managed_textures;
    CGameSettingBool       no_mode_change;
    CGameSettingBool       colour_16bit;
    CGameSettingBool       dos_graphics;
    CGameSettingBool       vesa_support;
    CGameSettingInt        control_judder_fix;
    CGameSettingResolution resolution;
    CGameSettingBool       full_screen;
    CGameSettingDouble     gamma;
    CGameSettingBool       forced_smoothing;
    CGameSettingBool       disable_lfb_reads;
    CGameSettingBool       delay_lfb_writes;
    CGameSettingBool       blood_fix;
    CGameSettingBool       shadow_hack;
    CGameSettingBool       tex_cache;
    CGameSettingTex        tex_handling;
    CGameSettingString     tex_folder;
    CGameSettingRedbook    redbook_emulation;
    CGameSettingBool       fmv_pack;
    CGameSettingDosEmu     dos_emu;
    CGameSettingStereo     stereo;
    bool                   dos_full_screen;

    CGameRecord()
    {
		name.Init(this, "Name");
		executable.Init(this, "Executable");
        subfolders.Init(this, "Subfolders", 0);
		sizes.Init(this, "ExeSizes");
		cd_path.Init(this, "CDPath");
		cd_file.Init(this, "CDCheckFile");
        driver.Init(this, "Driver");
        mipmaps.Init(this, "MipMapping");
        palette_tricks_cache.Init(this, "PaletteTricksCache");
        managed_textures.Init(this, "ManagedTextures");
        no_mode_change.Init(this, "NoModeChange");
        colour_16bit.Init(this, "16BitColour");
    	dos_graphics.Init(this, "DosGraphics");
    	vesa_support.Init(this, "VESASupport");
		control_judder_fix.Init(this, "ControlJudderFix", 0);
		resolution.Init(this, "Resolution", (CResolution)2);
    	full_screen.Init(this, "FullScreen");
		gamma.Init(this, "GammaAdjustment", 1.0);
    	forced_smoothing.Init(this, "ForcedTextureSmoothing");
    	disable_lfb_reads.Init(this, "DisableLfbReads");
    	delay_lfb_writes.Init(this, "DelayLfbWrites");
    	blood_fix.Init(this, "BloodFix");
    	shadow_hack.Init(this, "TrShadowHack");
    	tex_cache.Init(this, "TextureCache");
		tex_handling.Init(this, "TextureOverride");
        tex_folder.Init(this, "TextureFolder");
		redbook_emulation.Init(this, "RedbookEmulation");
        fmv_pack.Init(this, "FMVPack");
        dos_emu.Init(this, "DosEmulation");
        stereo.Init(this, "StereoMap");
    };

	void                Write(FILE *f);
	static CGameRecord *CGameRecord::Read(FILE *f);
};

