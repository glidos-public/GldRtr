#if !defined(AFX_PROPDISPLAY_H__859BA365_EB0E_41A3_AF65_104D392CBA72__INCLUDED_)
#define AFX_PROPDISPLAY_H__859BA365_EB0E_41A3_AF65_104D392CBA72__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropDisplay.h : header file
//

#include "GameRecord.h"

/////////////////////////////////////////////////////////////////////////////
// CPropDisplay dialog

class CPropDisplay : public CPropertyPage
{
	DECLARE_DYNCREATE(CPropDisplay)

private:
	void UpdateButtons();
	double       m_gamma;
    CGameRecord *m_rec;

// Construction
public:
	void Init(CGameRecord *rec);
	CPropDisplay();
	~CPropDisplay();

// Dialog Data
	//{{AFX_DATA(CPropDisplay)
	enum { IDD = IDD_PROP_DISPLAY };
	CButton	m_16bit_colour;
	CComboBox	m_resolution;
	CButton	m_no_mode_change;
	CButton	m_managed_textures;
	CSliderCtrl	m_slider;
	BOOL	m_full_screen;
	CString	m_gamma_display;
	CString	m_name;
	BOOL	m_smooth;
	int		m_driver;
	BOOL	m_mipmaps;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropDisplay)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropDisplay)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnDriver2();
	afx_msg void OnDriver1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPDISPLAY_H__859BA365_EB0E_41A3_AF65_104D392CBA72__INCLUDED_)
