// TexElementAudioFactory.h: interface for the CTexElementAudioFactory class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXELEMENTAUDIOFACTORY_H__1BB87091_BF24_4FE0_8FB4_3AE95E8F8E7D__INCLUDED_)
#define AFX_TEXELEMENTAUDIOFACTORY_H__1BB87091_BF24_4FE0_8FB4_3AE95E8F8E7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TexHandlerAudio.h"
#include "TexElementActFactory.h"

class CTexElementAudioFactory : public CTexElementActFactory  
{
private:
    CTexHandlerAudio *m_handler;

public:
    CTexElementAct *MakeTexElement();
    CTexElementAct *MakeTexElement(CBox &b, CString &name);

	CTexElementAudioFactory(CTexHandlerAudio *handler);
	virtual ~CTexElementAudioFactory();

};

#endif // !defined(AFX_TEXELEMENTAUDIOFACTORY_H__1BB87091_BF24_4FE0_8FB4_3AE95E8F8E7D__INCLUDED_)
