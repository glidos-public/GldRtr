/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * $Id$
 *
 ************************************************************/

#ifndef _NPIPE_H_
#define _NPIPE_H_

#include "pipe.h"


pipe_t *NPipeOpen(void);

#endif
