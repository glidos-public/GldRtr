#ifndef WRAPPER_CONFIG_H
#define WRAPPER_CONFIG_H

#include "sdk2_3dfx.h"
#include "wrapper_flags.h"


FX_ENTRY void FX_CALL setConfig(FxU32 flags);

FX_ENTRY void FX_CALL setStereoMap(const StereoMap *map);

#endif /* WRAPPER_CONFIG_H */
