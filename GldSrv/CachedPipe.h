// CachedPipe.h: interface for the CCachedPipe class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CACHEDPIPE_H__C5116B30_60D5_4F03_A986_1C2675A5929C__INCLUDED_)
#define AFX_CACHEDPIPE_H__C5116B30_60D5_4F03_A986_1C2675A5929C__INCLUDED_

#include "GldPipe.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCachedPipe  
{
private:
    enum
    {
        PIPE_CACHE_SIZE = 1024*1024
    };

    CGldPipe *m_pipe;
	char     *m_pipe_cache;
	int       m_pipe_used;
	int       m_pipe_index;
	bool      m_shutting_down;

public:
	enum OSType
	{
		OS_NONE,
		OS_W2K,
		OS_W98
	};

	CCachedPipe();
	virtual ~CCachedPipe();

	OSType Create();
	void   Destroy();

	void   Wake();
	bool   Write(char *buf, int n);
	void   ReadPipe(char *buf, int count);
};

#endif // !defined(AFX_CACHEDPIPE_H__C5116B30_60D5_4F03_A986_1C2675A5929C__INCLUDED_)
