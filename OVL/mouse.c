/************************************************************
 * Mouse driver for Glidos
 *
 * Copyright (c) Paul Gardiner.
 ************************************************************/

#include <i86.h>
#include "misc.h"
#include "mouse.h"

enum
{
    ButtonPress_None,
    ButtonPress_LDown,
    ButtonPress_LUp,
    ButtonPress_RDown,
    ButtonPress_RUp
};

enum
{
    ButtonState_Left  = 1,
    ButtonState_Right = 2
};

enum
{
    Mouse_Moved           = 0x01,
    Mouse_LButtonPressed  = 0x02,
    Mouse_LButtonReleased = 0x04,
    Mouse_RButtonPressed  = 0x08,
    Mouse_RButtonReleased = 0x10
};

static int installed = 0;

static int button_state = 0;

static int xmin   = 0;
static int ymin   = 0;
static int xmax   = 640;
static int ymax   = 200;
static int xpos   = 640/2;
static int ypos   = 200/2;
static int xrate  = 8;
static int yrate  = 16;

static int xmove = 0;
static int ymove = 0;

static lcount = 0;
static lxpos  = 0;
static lypos  = 0;

static rcount = 0;
static rxpos  = 0;
static rypos  = 0;

static int mouse_callback_selector = 0;
static int mouse_callback_offset   = 0;

static int mouse_mask   = 0;

extern void call_mouse(int selector, int offset, int flags, int bstate, int xpos, int ypos);

#pragma aux call_mouse =           \
    "push ebp"                     \
    "mov ebp, esp"                 \
    "sub esp, 0Ch"                 \
    "mov -8[ebp],di"               \
    "mov -0Ch[ebp],esi"            \
    "mov esi, ecx"                 \
    "mov edi, edx"                 \
    "call pword ptr -0Ch[ebp]"     \
    "mov esp, ebp"                 \
    "pop ebp"                      \
    parm [edi] [esi] [eax] [ebx] [ecx] [edx]\
    modify [eax ebx ecx edx esi edi];

static mouse_button_simulate(int button_info)
{
    char *presses = (char *)&button_info;
    int   i;

    for(i = 0; i < 4; i++)
    {
        switch(presses[i])
        {
            case ButtonPress_LDown:
                button_state |= ButtonState_Left;
                lxpos = xpos;
                lypos = ypos;
                lcount += 1;
                if(mouse_callback_selector && (mouse_mask & Mouse_LButtonPressed))
                    call_mouse(mouse_callback_selector, mouse_callback_offset, Mouse_LButtonPressed, button_state, xpos, ypos);
                break;

            case ButtonPress_LUp:
                button_state &= ~ButtonState_Left;
                if(mouse_callback_selector && (mouse_mask & Mouse_LButtonReleased))
                    call_mouse(mouse_callback_selector, mouse_callback_offset, Mouse_LButtonReleased, button_state, xpos, ypos);
                break;

            case ButtonPress_RDown:
                button_state |= ButtonState_Right;
                rxpos = xpos;
                rypos = ypos;
                rcount += 1;
                if(mouse_callback_selector && (mouse_mask & Mouse_RButtonPressed))
                    call_mouse(mouse_callback_selector, mouse_callback_offset, Mouse_RButtonPressed, button_state, xpos, ypos);
                break;

            case ButtonPress_RUp:
                button_state &= ~ButtonState_Right;
                if(mouse_callback_selector && (mouse_mask & Mouse_RButtonReleased))
                    call_mouse(mouse_callback_selector, mouse_callback_offset, Mouse_RButtonReleased, button_state, xpos, ypos);
                break;
        }
    }
}

static void (__interrupt __far *old_int33_handler)();

static void __interrupt __far int33_handler(union INTPACK r)
{
    switch(r.x.eax & 0xffff)
    {
    case 0: /* Reset driver and read status */
        r.x.eax = 0xffff;
        r.x.ebx = 2;
        break;

    case 0x3: /* Return position and button status */
        r.x.ebx = button_state;
        r.x.ecx = xpos;
        r.x.edx = ypos;
        break;

    case 0x4: /* Position mouse cursor */
        xpos = r.w.cx;
        ypos = r.w.dx;
        break;

    case 0x5: /* Return button press data */
        if(r.w.bx == 1)
        {
            r.x.eax = button_state;
            r.x.ebx = rcount;
            r.x.ecx = rxpos;
            r.x.edx = rypos;
            rcount = 0;
        }
        else
        {
            r.x.eax = button_state;
            r.x.ebx = lcount;
            r.x.ecx = lxpos;
            r.x.edx = lypos;
            lcount = 0;
        }
        break;

    case 0x7:
        xmin = r.w.cx;
        xmax = r.w.dx;
        break;

    case 0x8:
        ymin = r.w.cx;
        ymax = r.w.dx;
        break;

    case 0xb: /* Read motion counters */
        r.x.ecx = xmove/2;
        r.x.edx = ymove/2;

        xmove -= r.x.ecx;
        ymove -= r.x.edx;
        break;

    case 0xc: /* Define interrupt subroutine parameters */
        mouse_mask = r.x.ecx;
        mouse_callback_selector = r.x.es;
        mouse_callback_offset   = r.x.edx;
        break;

    case 0xf:
        xrate = r.w.cx;
        yrate = r.w.dx;
        break;

    case 0x15: /* Return driver storage requirements */
        r.x.ebx = 0x10;
        break;

    default:
        chain(old_int33_handler);
        break;
    }
}

Mouse_install(void)
{
    if(!installed)
    {
        installed = 1;

        old_int33_handler = getvect(0x33);
        setvect(0x33, int33_handler);
    }
}

Mouse_uninstall(void)
{
    if(installed)
    {
        installed = 0;
        setvect(0x33, old_int33_handler);
    }
}

Mouse_newState(int xmickeys, int ymickeys, int buttons)
{
    xmove += xmickeys;
    ymove += ymickeys;
    xpos += xmickeys * 8 / xrate;
    ypos += ymickeys * 8 / yrate;
    if(xpos < xmin) xpos = xmin;
    if(xpos >= xmax) xpos = xmax - 1;
    if(ypos < ymin) ypos = ymin;
    if(ypos >= ymax) ypos = ymax - 1;

    mouse_button_simulate(buttons);
}

