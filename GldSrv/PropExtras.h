#if !defined(AFX_PROPEXTRAS_H__9B8510F4_5125_42DF_AEDE_1C053193302B__INCLUDED_)
#define AFX_PROPEXTRAS_H__9B8510F4_5125_42DF_AEDE_1C053193302B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropExtras.h : header file
//

#include "GameRecord.h"

/////////////////////////////////////////////////////////////////////////////
// CPropExtras dialog

class CPropExtras : public CPropertyPage
{
	DECLARE_DYNCREATE(CPropExtras)

private:
    CGameRecord *m_rec;

// Construction
public:
	void Init(CGameRecord *rec);
	CPropExtras();
	~CPropExtras();

// Dialog Data
	//{{AFX_DATA(CPropExtras)
	enum { IDD = IDD_PROP_EXTRAS };
	BOOL	m_blood_fix;
	BOOL	m_tex_cache;
	BOOL	m_delay_writes;
	BOOL	m_disable_reads;
	BOOL	m_paletted_texture_cache;
	BOOL	m_shadow_hack;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropExtras)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropExtras)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPEXTRAS_H__9B8510F4_5125_42DF_AEDE_1C053193302B__INCLUDED_)
