// TexHandler.cpp: implementation of the CTexHandler class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TexHandler.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define MASTER_SIG "GLIDOS TEXTURE MAPPING"
#define BASE_MARKER "BASE:"
#define ROOT_MARKER "ROOT:"
#define WHITE_SPACE " \t\r\n"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexHandler::CTexHandler()
{
    int i;

    m_selected_partition = NULL;
    m_initialised        = false;

    for(i = 0; i < MAX_MM; i++)
        m_tex_partition[i] = NULL;
}

CTexHandler::~CTexHandler()
{
    int i;

    for(i = 0; i < MAX_MM; i++)
        delete m_tex_partition[i];

}

void CTexHandler::GrTexDownloadTable(GuTexPalette *pal)
{
}

void CTexHandler::GuTexAllocateMemory(int id, GrTextureFormat_t fmt, int width, int height, GrLOD_t lod)
{
	if(0 <= id && id < MAX_MM)
	{
		if(m_tex_partition[id] != NULL)
		{
            m_tex_partition[id]->Finalise();
			delete m_tex_partition[id];
			m_tex_partition[id] = NULL;
		}

        if(fmt == GR_TEXFMT_P_8)
		    m_tex_partition[id] = NewTexPartition(fmt, width, height, lod);
	}
}

void CTexHandler::GuTexDownloadMipMap(int id, unsigned char hash[], char *data)
{
	if(0 <= id && id < MAX_MM && m_tex_partition[id] != NULL)
        SetupTexPartition(m_tex_partition[id], hash, data);
}

void CTexHandler::GuTexSource(int id)
{
    m_selected_partition = ((0 <= id && id < MAX_MM) ? m_tex_partition[id] : NULL);
}

bool CTexHandler::GrDrawPolygonVertexList(int n, GrVertex *vlist)
{
    return (m_selected_partition != NULL)
                ? m_selected_partition->UsedRectangle(n, vlist)
                : false;
}

void CTexHandler::Restrict()
{
}

CMappingFile *CTexHandler::GetMappingFile()
{
    if(!m_initialised)
    {
        m_mapper.Initialise(GetFolder());

        m_initialised = true;
    }

    return &m_mapper;
}
