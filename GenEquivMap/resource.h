//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by GenEquivMap.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_GENEQUIVMAP_DIALOG          102
#define IDD_PROPPAGE_CAPTURE            107
#define IDR_MAINFRAME                   128
#define IDD_PROPPAGE_GENERATE           129
#define IDD_PROPPAGE_JOIN               130
#define IDD_PROPPAGE_CHECK              131
#define IDD_PROPPAGE_LIST               132
#define IDC_EDIT_FOLDER                 1000
#define IDC_BUTTON_BROWSE_FOLDER        1001
#define IDC_BUTTON_RUN                  1002
#define IDC_EDIT_TEX_FOLDER             1003
#define IDC_BUTTON_LIST                 1003
#define IDC_BUTTON_BROWSE_TEX_FOLDER    1004
#define IDC_BUTTON_CHECK                1005
#define IDC_EDIT_FILE2                  1006
#define IDC_BUTTON_BROWSE_FILE2         1007
#define IDC_BUTTON_JOIN                 1008
#define IDC_EDIT_FILE1                  1009
#define IDC_BUTTON_BROWSE_FILE1         1010
#define IDC_EDIT_FOLDER_OUT             1011
#define IDC_BUTTON_BROWSE_FOLDER_OUT    1012
#define IDC_BUTTON_CAPTURE              1013
#define IDC_EDIT_LEVEL                  1014
#define IDC_BUTTON_BROWSE_LEVEL         1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
