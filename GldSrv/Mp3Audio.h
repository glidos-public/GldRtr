// Mp3Audio.h: interface for the CMp3Audio class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MP3AUDIO_H__D630AC17_9F9B_49B7_BE5C_FD1541469913__INCLUDED_)
#define AFX_MP3AUDIO_H__D630AC17_9F9B_49B7_BE5C_FD1541469913__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <map>
#include <string>
#include "PlayAudioFile.h"

class CMp3Audio  
{
private:
    std::map<int, std::string> m_files;
    CPlayAudioFile             m_player;

public:
	bool IsPlaying();
	void Stop();
	void Play(int i);
	CMp3Audio();
	virtual ~CMp3Audio();

};

#endif // !defined(AFX_MP3AUDIO_H__D630AC17_9F9B_49B7_BE5C_FD1541469913__INCLUDED_)
