/************************************************************
 * Mouse driver for Glidos
 *
 * Copyright (c) Paul Gardiner.
 ************************************************************/

#ifndef _MOUSE_H_
#define _MOUSE_H_

Mouse_install(void);

Mouse_uninstall(void);

Mouse_newState(int xmickeys, int ymickeys, int buttons);


#endif
