// GldPipe.h: interface for the CGldPipe class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GLDPIPE_H__839ACEE1_CFC7_11D5_B9FC_00C0DFF0F563__INCLUDED_)
#define AFX_GLDPIPE_H__839ACEE1_CFC7_11D5_B9FC_00C0DFF0F563__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGldPipe  
{
public:
	virtual bool Write(char *buf, int n)=0;
	virtual void Wake()=0;
	virtual bool Create()=0;
	virtual bool Read(char *buf, int requested, unsigned long *returned)=0;
	CGldPipe();
	virtual ~CGldPipe();

protected:
	bool m_shutdown;
    void PumpWhileWaiting(HANDLE event);
    void Pump();
};

#endif // !defined(AFX_GLDPIPE_H__839ACEE1_CFC7_11D5_B9FC_00C0DFF0F563__INCLUDED_)
