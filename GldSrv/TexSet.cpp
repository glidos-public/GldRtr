// TexSet.cpp: implementation of the CTexSet class.
//
//////////////////////////////////////////////////////////////////////

#ifndef TEXSET_CPP
#define TEXSET_CPP

#include "stdafx.h"
#include "TexSet.h"

#ifndef TEXSET_H
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

template <class TElement, class TFactory>
CTexSet<TElement, TFactory>::CTexSet(TFactory *factory)
{
    m_factory = factory;
    m_list    = NULL;
    m_here    = NULL;
    m_any     = factory->MakeTexElement();
}

template <class TElement, class TFactory>
CTexSet<TElement, TFactory>::~CTexSet()
{
    while(m_list != NULL)
    {
        m_here = m_list;
        m_list = m_list->m_next;
        delete m_here;
    }

    delete m_any;
    delete m_factory;
}


template <class TElement, class TFactory>
void CTexSet<TElement, TFactory>::Initialise(CString &folder, TexElementInfoList *list)
{
    TexElementInfoList::iterator iter;

    m_folder = folder;

    for(iter = list->begin(); iter != list->end(); iter++)
    {
        TexElementInfo info = *iter;

        CLink *l = new CLink(m_factory->MakeTexElement(
                              info.box, info.path));
        l->m_next = m_list;
        m_list = l;
    }
}

template <class TElement, class TFactory>
TElement *CTexSet<TElement, TFactory>::Any()
{
    return m_any;
}

template <class TElement, class TFactory>
void CTexSet<TElement, TFactory>::Start()
{
    m_here = m_list;
}

template <class TElement, class TFactory>
bool CTexSet<TElement, TFactory>::Going()
{
    return m_here != NULL;
}

template <class TElement, class TFactory>
void CTexSet<TElement, TFactory>::Next()
{
    ASSERT(m_here != NULL);

    m_here = m_here->m_next;
}

template <class TElement, class TFactory>
TElement *CTexSet<TElement, TFactory>::Here()
{
    return m_here->m_element;
}

template <class TElement, class TFactory>
void CTexSet<TElement, TFactory>::Mark()
{
    ASSERT(m_here != NULL);

    m_here->m_marked = true;
}

template <class TElement, class TFactory>
void CTexSet<TElement, TFactory>::DeleteMarked(bool delete_file)
{
    CLink **p;
    CLink *tmp;

    p = &m_list;

    while(*p)
    {
        tmp = *p;

        if(tmp->m_marked)
        {
            *p = tmp->m_next;

            if(delete_file)
                ::DeleteFile((LPCSTR)(tmp->m_element->GetName()));

            delete tmp;
        }
        else
        {
            p = &(tmp->m_next);
        }
    }
}

template <class TElement, class TFactory>
void CTexSet<TElement, TFactory>::Add(CBox *b, CBmpWriter *bmp)
{
    CString name;
    CString path;

    name.Format("(%d--%d)(%d--%d).bmp", b->m_xmin, b->m_xmax, b->m_ymin, b->m_ymax);

    path = m_folder + "\\" + name;

    bmp->Write(path, b);

    CLink *p = new CLink(m_factory->MakeTexElement(*b, path));
    p->m_next = m_list;
    m_list = p;

    Write();
}


template <class TElement, class TFactory>
void CTexSet<TElement, TFactory>::Write()
{
    /* This method used to write out a key file,
     * but now we don't use key files, so it does
     * nothing. */
}

#endif
