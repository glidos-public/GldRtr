     .386

     _TEXT16 SEGMENT BYTE PUBLIC USE16 'CODE'
         ASSUME  cs:_TEXT16

         PUBLIC  rmhandler_
         PUBLIC  _ormhandler
         PUBLIC  _pmcallback
     rmhandler_:
         push    bx
         push    ax
         and     ax, 0ff00h
         cmp     ax, 04f00h
         je      vesa
         ;cmp     ax, 04f00h
         ;je      vesa
         ;cmp     ax, 0f00h
         ;je      vesa
         cmp     ax, 0
         je      vesa
         push    bp
         mov     bp, sp
         mov     ax, cs:[_ormhandler]
         mov     bx, cs:[_ormhandler+2]
         xchg    ax, [bp+2]
         xchg    bx, [bp+4]
         pop     bp
         retf
     vesa:
         pop     ax
         pop     bx
         call    dword ptr cs:[_pmcallback]
         iret
     _ormhandler:
         dw 0
         dw 0
     _pmcallback:
         dw 0
         dw 0
    _TEXT16 ENDS
     END
     
