// TexPartitionAudio.cpp: implementation of the CTexPartitionAudio class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "TexPartitionAudio.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexPartitionAudio::CTexPartitionAudio(GrLOD_t lod, CTexElementActFactory *factory, CPlayBackground *background) : CTexPartitionAct(lod, factory)
{
	m_background = background;
}

CTexPartitionAudio::~CTexPartitionAudio()
{

}

void CTexPartitionAudio::Initialise(CMappingFile *mapper, unsigned char hash[], int count, const CBox *areas)
{
	if(m_background != NULL)
	{
        CString folder = mapper->GetFolder(hash);

		char  buf[256];
		FILE *f = fopen((LPCSTR)(folder + "\\Background"), "rb");
		if(f != NULL)
		{
			while(fgets(buf, 256, f) != NULL)
			{
				if(memcmp(buf, "Background:", 11) == 0)
				{
					CString file = buf+11;
					
					file.TrimLeft();
					file.TrimRight();
					
					int pos = folder.ReverseFind('\\');
					if(pos != -1)
					{
						file = folder.Left(pos) + '\\' + file;
						m_background->Start(file);
						break;
					}
				}
			}
		}
	}

	CTexPartitionAct::Initialise(mapper, hash, count, areas);
}
