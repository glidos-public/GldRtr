// PolyCondition.cpp: implementation of the CPolyCondition class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "PolyCondition.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CPolyCondition::CPoly::CPoly(int n, GrVertex *vlist)
{
    int i;

    m_xmin = m_xmax = (int) vlist[0].x;
    m_ymin = m_ymax = (int) vlist[0].y;

    for(i = 1; i < n; i++)
    {
        int x = (int) vlist[i].x;
        int y = (int) vlist[i].y;

        if(x < m_xmin)
            m_xmin = x;
        else if(x > m_xmax)
            m_xmax = x;

        if(y < m_ymin)
            m_ymin = y;
        else if(y > m_ymax)
            m_ymax = y;
    }
}

int CPolyCondition::CPoly::Eval(CPolyCondition::Var var) const
{
    switch(var)
    {
    case CPolyCondition::VarXMin:
        return m_xmin;

    case CPolyCondition::VarXMax:
        return m_xmax;

    case CPolyCondition::VarYMin:
        return 479 - m_ymax;

    case CPolyCondition::VarYMax:
        return 479 - m_ymin;

	case CPolyCondition::VarWidth:
		return m_xmax - m_xmin;

	case CPolyCondition::VarHeight:
		return m_ymax - m_ymin;

    case CPolyCondition::VarNone:
        return 0;
    }

    return 0;
}

bool CPolyCondition::ParseVar(CString s, CPolyCondition::Var *var)
{
    s.TrimLeft();
    s.TrimRight();

    if(s == "left")
    {
        *var = CPolyCondition::VarXMin;
        return true;
    }
    else if(s == "right")
    {
        *var = CPolyCondition::VarXMax;
        return true;
    }
    else if(s == "bottom")
    {
        *var = CPolyCondition::VarYMin;
        return true;
    }
    else if(s == "top")
    {
        *var = CPolyCondition::VarYMax;
        return true;
    }
    else if(s == "width")
    {
        *var = CPolyCondition::VarWidth;
        return true;
    }
    else if(s == "height")
    {
        *var = CPolyCondition::VarHeight;
        return true;
    }
    else
    {
        return false;
    }
}

bool CPolyCondition::ParseInt(CString s, int *i)
{
    s.TrimLeft();
    s.TrimRight();

    if(s.GetLength() == s.SpanIncluding("0123456789").GetLength())
    {
        *i = atoi((LPCSTR)s);
        return true;
    }
    else
    {
        return false;
    }
}

bool CPolyCondition::ParseExp(CString s, CPolyCondition::Exp *exp)
{
    int pos;

    pos = s.Find('+');

    if(pos != -1)
    {
        CString l(s.Left(pos));
        CString r(s.Mid(pos+1));

        return (ParseVar(l, &(exp->var)) && ParseInt(r, &(exp->summand)))
            || (ParseVar(r, &(exp->var)) && ParseInt(l, &(exp->summand)));
    }
    else
    {
        if(ParseVar(s, &(exp->var)))
        {
            exp->summand = 0;
            return true;
        }
        else if(ParseInt(s, &(exp->summand)))
        {
            exp->var = VarNone;
            return true;
        }
        else
        {
            return false;
        }
    }
}

CPolyCondition::CPoly::~CPoly()
{
}

CPolyCondition::CPolyCondition(const CString &exp)
{
    int pos;

    pos = exp.Find('<');
    if(pos != -1)
    {
        valid = (ParseExp(exp.Left(pos), &lesser)
                  && ParseExp(exp.Mid(pos+1), &greater));

        return;
    }

    pos = exp.Find('>');
    if(pos != -1)
    {
        valid = (ParseExp(exp.Left(pos), &greater)
                  && ParseExp(exp.Mid(pos+1), &lesser));

        return;
    }

    valid = false;
}

CPolyCondition::~CPolyCondition()
{

}

bool CPolyCondition::Test(const CPolyCondition::CPoly &pos)
{
    return valid && (pos.Eval(lesser.var) + lesser.summand
                       < pos.Eval(greater.var) + greater.summand);
}
