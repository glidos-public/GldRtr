/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * $Id$
 *
 ************************************************************/

#ifndef _VDD_H_
#define _VDD_H_

#include "pipe.h"


pipe_t *VDDOpen(void);


/*
 * The VDD used for polled comunication, also
 * does the judder fix, so we tack the interface
 * on here.
 */
void JudderFixOn(int val);

/*
 * And it provides the address of a location
 * that keeps time in PIC timer units
 */
int *PicPredict(void);

#endif
