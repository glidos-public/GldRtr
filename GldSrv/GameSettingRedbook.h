// GameSettingRedbook.h: interface for the CGameSettingRedbook class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAMESETTINGREDBOOK_H__0EA62AEC_CB58_4F4A_9A2C_142C163BBEA9__INCLUDED_)
#define AFX_GAMESETTINGREDBOOK_H__0EA62AEC_CB58_4F4A_9A2C_142C163BBEA9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GameSetting.h"


class CRedbookHandling
{
public:
	enum RedbookHandling
    {
        REDBOOK_HANDLING_NONE,
        REDBOOK_HANDLING_CD,
        REDBOOK_HANDLING_MP3
    };


protected:
	RedbookHandling m_value;

public:
	operator RedbookHandling()
	{
		return m_value;
	};
};


class CGameSettingRedbook : public CGameSetting<CRedbookHandling>
{
public:
	void             Init(CStoredStruct *st, const char *name);
	CGameSettingRedbook &operator=(const RedbookHandling &i);

private:
	bool    Parse(CString &val);
	CString Print();
};

#endif // !defined(AFX_GAMESETTINGREDBOOK_H__0EA62AEC_CB58_4F4A_9A2C_142C163BBEA9__INCLUDED_)
