// Mp3Audio.cpp: implementation of the CMp3Audio class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "gldsrv.h"
#include "Mp3Audio.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMp3Audio::CMp3Audio()
{
    CFileFind finder;

    BOOL working = finder.FindFile("Audio\\Tracks\\*.*");
    while(working)
    {
        working = finder.FindNextFile();

        if(!finder.IsDirectory())
        {
            int i;

            if(sscanf((LPCSTR)finder.GetFileName(),
                "%d", &i) == 1)
            {
                m_files[i] = (LPCSTR)finder.GetFilePath();
            }

        }
    }
}

CMp3Audio::~CMp3Audio()
{

}

void CMp3Audio::Play(int  i)
{
     m_player.Stop();

    if(m_files.count(i) == 1)
    {
        CString s(m_files[i].c_str());
        m_player.Start(s);
    }
}

void CMp3Audio::Stop()
{
    m_player.Stop();
}

bool CMp3Audio::IsPlaying()
{
    return m_player.Playing();
}
