// GameSettingString.cpp: implementation of the CGameSettingString class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "GameSettingString.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CGameSettingString &CGameSettingString::operator=(CString &val)
{
	m_defined          = !val.IsEmpty();
	*((CString *)this) = val;
	
	return *this;
}

bool CGameSettingString::Parse(CString &val)
{
	*((CString *)this) = val;
	
	return true;
}

CString CGameSettingString::Print()
{
	return *this;
}
