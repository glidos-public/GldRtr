/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * $Id$
 *
 ************************************************************/

#include "timesync.h"

#include "vdd.h"

#define NULL (0)

static unsigned short dos_segment;
static unsigned short dos_selector;
static unsigned short dispatch_handle;

static struct rminfo {
    long EDI;
    long ESI;
    long EBP;
    long reserved_by_system;
    long EBX;
    long EDX;
    long ECX;
    long EAX;
    short flags;
    short ES,DS,FS,GS,IP,CS,SP,SS;
} RMI;


extern void allocate_dos_memory(void);

#pragma aux allocate_dos_memory =  \
    "mov eax, 100h"                \
    "mov ebx, 32"                  \
    "int 31h"                      \
    "mov dos_segment, ax"          \
    "mov dos_selector, dx"         \
    modify [eax ebx edx];


extern void do_real_mode_call(const struct rminfo far *rmi);

#pragma aux do_real_mode_call =    \
    "push es"                      \
    "mov es, dx"                   \
    "mov edi, eax"                 \
    "mov eax, 301h"                \
    "mov ebx, 0"                   \
    "mov ecx, 0"                   \
    "int 31h"                      \
    "pop es"                       \
    modify [edi ebx ecx]           \
    parm [dx eax];


static void farstrcpy(char far *so, const char far *si)
{
    while(*si)
        *so++ = *si++;
        
    *so = *si;
}


static int strlen(const char *s)
{
    int len = 0;

    while(*s++ != 0)
        len += 1;

    return len;
}


static void memset(void *buf, int val, int count)
{
    char *cbuf = (char *) buf;

    while(count--)
        *cbuf++ = val;
}


static void load_vdd(const char *dllname, const char *dispname)
{
    char far *dos_mem = dos_selector:>0;
    int dllname_index;
    int dispname_index;

    dllname_index = 5;
    dispname_index = 5 + strlen(dllname) + 1;
    
    /* Set up little program in 16bit DOS area */
    dos_mem[0] = 0xC4; /* Register Module */
    dos_mem[1] = 0xC4;
    dos_mem[2] = 0x58;
    dos_mem[3] = 0;
    dos_mem[4] = 0xcb; /* Far return */

    /* Copy strings into 16bit DOS area */
    farstrcpy(dos_mem + dllname_index, dllname);
    farstrcpy(dos_mem + dispname_index, dispname);

    /* Prepare 16bit register block */
    memset(&RMI, 0, sizeof(RMI));
    RMI.DS  = dos_segment;
    RMI.ESI = dllname_index;
    RMI.EBX = dispname_index;
    RMI.ES  = 0;              /* Tell "RegisterModule" no init */
    RMI.EDI = 0;
    RMI.CS  = dos_segment;    /* Start of little program */
    RMI.IP  = 0;
    RMI.SS  = 0;              /* Let DPMI set up the stack */
    RMI.SP  = 0;

    do_real_mode_call(&RMI);
    
    dispatch_handle = RMI.EAX;
}

static int dispatch_call(int cmd, const char *buf, int len)
{
    char far *dos_mem = dos_selector:>0;

    /* Set up little program in 16bit DOS area */
    dos_mem[0] = 0xC4; /* Dispatch Call */
    dos_mem[1] = 0xC4;
    dos_mem[2] = 0x58;
    dos_mem[3] = 2;
    dos_mem[4] = 0xcb; /* Far return */

    /* Prepare 16bit register block */
    memset(&RMI, 0, sizeof(RMI));
    RMI.EAX = dispatch_handle;
    RMI.EBX = (int) buf;
    RMI.ECX = len;
    RMI.EDX = cmd;
    RMI.CS  = dos_segment;    /* Start of little program */
    RMI.IP  = 0;
    RMI.SS  = 0;              /* Let DPMI set up the stack */
    RMI.SP  = 0;

    do_real_mode_call(&RMI);

    return RMI.ECX;
}

#define PP_OPEN   (1)
#define PP_CLOSE  (2)
#define PP_READ   (3)
#define PP_WRITE  (4)
#define PP_POLL   (5)
#define PP_JUDFIX (6)
#define PP_PICPREDICT (7)

static void pp_close(void)
{
    dispatch_call(PP_CLOSE, NULL, 0);
}

static void send(char *buf, int c)
{
    dispatch_call(PP_WRITE, buf, c);

    while(!dispatch_call(PP_POLL, NULL, 0))
        timesync_poll();
}

static void get(char *buf, int c)
{
    dispatch_call(PP_READ, buf, c);

    while(!dispatch_call(PP_POLL, NULL, 0))
        timesync_poll();
}

static void flush(void)
{
}

static int readChar(void)
{
	unsigned char c;

	get(&c, 1);
	return c;
}

static pipe_t pipe = {pipe_os_nt, send, flush, readChar, get};


pipe_t *VDDOpen(void)
{
    allocate_dos_memory();
    load_vdd("PollPipe.dll", "DispatchCall");
    dispatch_call(PP_OPEN, NULL, 0);

    return &pipe;
}


void JudderFixOn(int val)
{
    static int initialised = 0;

    if(!initialised)
    {
        dispatch_call(PP_JUDFIX, NULL, val);
        initialised = 1;
    }
}

int *PicPredict(void)
{
    return (int *) dispatch_call(PP_PICPREDICT, NULL, 0);
}
