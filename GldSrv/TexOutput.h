// TexOutput.h: interface for the CTexHandlerOutput class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXOUTPUT_H__4638CB32_EA57_4A4B_90A9_4786E3BFFF8E__INCLUDED_)
#define AFX_TEXOUTPUT_H__4638CB32_EA57_4A4B_90A9_4786E3BFFF8E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TexHandler.h"

class CTexHandlerOutput : public CTexHandler
{
private:
    CString       m_folder;
	GuTexPalette  m_pal;

    CTexPartition *NewTexPartition(GrTextureFormat_t fmt, int width, int height, GrLOD_t lod);
    void           SetupTexPartition(CTexPartition *partition, unsigned char hash[], char *data);
	CString        GetFolder();

public:
	void GrTexDownloadTable(GuTexPalette *pal);
	CTexHandlerOutput(LPCSTR folder = "Capture");
	virtual ~CTexHandlerOutput();

};

#endif // !defined(AFX_TEXOUTPUT_H__4638CB32_EA57_4A4B_90A9_4786E3BFFF8E__INCLUDED_)
