// GameSettingDosEmu.cpp: implementation of the CGameSettingDosEmu class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "gldsrv.h"
#include "GameSettingDosEmu.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CGameSettingDosEmu::Init(CStoredStruct *st, const char *name)
{
    CGameSetting<CDosEmu>::Init(st, name);

    m_value = DOSEMU_WINDOWS;
};

CGameSettingDosEmu &CGameSettingDosEmu::operator=(const DosEmu &i)
{
    if(i != m_value)
        m_defined = true;

    m_value = i;

    return *this;
};

bool CGameSettingDosEmu::Parse(CString &val)
{
    if(val[0] == 'W' || val[0] == 'w')
    {
        m_value = DOSEMU_WINDOWS;

        return true;
    }

    if(val[0] == 'V' || val[0] == 'v')
    {
        m_value = DOSEMU_VDOS32;

        return true;
    }

    if(val[0] == 'D' || val[0] == 'd')
    {
        m_value = DOSEMU_DOSBOX;

        return true;
    }

    return false;
}

CString CGameSettingDosEmu::Print()
{
    switch(m_value)
    {
        default:
        case DOSEMU_WINDOWS:
            return "Windows";

        case DOSEMU_VDOS32:
            return "VDos32";

        case DOSEMU_DOSBOX:
            return "DOSBox";
    }
}
