#pragma once
#include "GameSetting.h"
#include "Resolution.h"

class CGameSettingResolution : public CGameSetting<CResolution>
{
public:
    void                    Init(CStoredStruct *st, const char *name, CResolution &init);
    CGameSettingResolution &operator=(const CResolution &val);

private:
    bool    Parse(CString &val);
    CString Print();
};
