// PHDFile.h: interface for the CPHDFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PHDFILE_H__63667038_87D8_4194_99CD_47FD7462E562__INCLUDED_)
#define AFX_PHDFILE_H__63667038_87D8_4194_99CD_47FD7462E562__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef unsigned char Tile[256][256];

struct TexUse
{
    int texid;
    int xlow, xhigh, ylow, yhigh;
};

class CPHDFile  
{
private:
    FILE     *m_f;
    bool      m_tub;

    int       m_ntile;
    Tile     *m_tile;

    int       m_pal[256];

    int       m_nsprtex;
    TexUse   *m_sprtex;
    int       m_nobjtex;
    TexUse   *m_objtex;

	int  ReadByte();
	int  ReadShort();
	int  ReadInt();

	void Skip(int n);
	void SkipShortCountedData(int n);
	void SkipIntCountedData(int n);

	int  ReadColor();
	void ReadPalette();

	void ReadObjectTextures();
	void ReadSpriteTextures();

	void ReadRoom();

public:
	CPHDFile();
	virtual ~CPHDFile();

	bool    Open(const CString &file);

	int     GetNumTexUses();
	TexUse *GetTexUse(int i);

	int    *GetPalette();
	int     GetNumTiles();

	Tile   *GetTile(int i);
};

#endif // !defined(AFX_PHDFILE_H__63667038_87D8_4194_99CD_47FD7462E562__INCLUDED_)
