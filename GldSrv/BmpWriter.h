// BmpWriter.h: interface for the CBmpWriter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BMPWRITER_H__E8FCA050_E6E7_4119_8A25_72B2B2C2DDA3__INCLUDED_)
#define AFX_BMPWRITER_H__E8FCA050_E6E7_4119_8A25_72B2B2C2DDA3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "sdk2_glide.h"
#include "Box.h"

class CBmpWriter  
{
private:
    GrTextureFormat_t m_fmt;
    int               m_width;
    int               m_height;
    GuTexPalette      m_pal;
    char             *m_data;

public:
	void Write(CString &path, CBox *box);
	void SetPixels(char *data);
	void SetPalette(GuTexPalette *pal);
	CBmpWriter(GrTextureFormat_t fmt, int width, int height);
	virtual ~CBmpWriter();

};

#endif // !defined(AFX_BMPWRITER_H__E8FCA050_E6E7_4119_8A25_72B2B2C2DDA3__INCLUDED_)
