// PolyCondition.h: interface for the CPolyCondition class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_POLYCONDITION_H__3E96E28D_CA0A_4CCC_A17E_3FA94BA5BCD8__INCLUDED_)
#define AFX_POLYCONDITION_H__3E96E28D_CA0A_4CCC_A17E_3FA94BA5BCD8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "sdk2_glide.h"
#include "box.h"

class CPolyCondition  
{
public:
    enum Var
    {
        VarNone,
        VarXMin,
        VarXMax,
        VarYMin,
        VarYMax,
		VarWidth,
		VarHeight
    };

    struct Exp
    {
        Var var;
        int summand;
    };

private:
    bool valid;
    Exp  lesser;
    Exp  greater;

public:
    class CPoly : public CBox
    {
    public:
        CPoly(int n, GrVertex *vlist);

        int Eval(Var var) const;

        virtual ~CPoly();
    };

    static bool ParseVar(CString s, Var *var);
    static bool ParseInt(CString s, int *i);
    static bool ParseExp(CString s, Exp *exp);

	CPolyCondition(const CString &exp);
	virtual ~CPolyCondition();

    bool Test(const CPoly &pos);
};

#endif // !defined(AFX_POLYCONDITION_H__3E96E28D_CA0A_4CCC_A17E_3FA94BA5BCD8__INCLUDED_)
