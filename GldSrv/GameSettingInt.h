// GameSettingInt.h: interface for the CGameSettingInt class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAMESETTINGINT_H__B2A2560C_A576_4AAC_AE72_0A7A45CBEE81__INCLUDED_)
#define AFX_GAMESETTINGINT_H__B2A2560C_A576_4AAC_AE72_0A7A45CBEE81__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "GameSetting.h"


class CInt
{
protected:
	int m_value;

public:
	operator int()
	{
		return m_value;
	};
};


class CGameSettingInt : public CGameSetting<CInt>
{
public:
	void             Init(CStoredStruct * st, const char *name, int init);
	CGameSettingInt &operator=(const int &i);

	bool    Parse(CString &val);
	CString Print();
};


#endif // !defined(AFX_GAMESETTINGINT_H__B2A2560C_A576_4AAC_AE72_0A7A45CBEE81__INCLUDED_)
