/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * DOS/4GW dll stub to replace Glide2x.ovl, and implement
 * the calls by passing them via a VXD to windows.
 *
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.67  2007/10/02 18:36:47  paul
 * MP3 based support for Kevin Bracey's patched Tomb.exe
 *
 * Revision 1.66  2006/03/22 09:19:36  paul
 * Request lfb from windows side only when necessary
 *
 * Revision 1.65  2006/03/21 21:37:52  paul
 * Get rid of tabs and trailing whitespace
 *
 * Revision 1.64  2004/08/14 14:19:18  paul
 * Allow configuration of redbook emulation and mp3 background sounds
 *
 * Revision 1.63  2003/03/30 16:56:35  paul
 * When ControlJudderFix is set to 1, use new accurate ticker
 * timing.
 *
 * This is done by causing vast numbers of int 8's, but sitting
 * on the vector so as to allow only the correct number to chain through.
 * Timing is monitored by interogating an address where an accurate
 * PIT counter is simulated.
 *
 * Revision 1.62  2003/02/01 16:20:49  paul
 * Update program and protocol version
 *
 * Revision 1.61  2003/01/01 18:18:13  paul
 * Add DisableLfbReads and DelayLfbWrites settings for Screamer 2.
 * Control these settings, plus BloodFix via the .ini file.
 *
 * Revision 1.60  2002/12/29 18:00:03  paul
 * Include OVL in the main build rather than via separate .bat files
 *
 * Revision 1.59  2002/11/24 20:08:06  paul
 * Add int 33h ax=bh to mouse driver, for Blood.
 *
 * Revision 1.58  2002/11/11 21:50:09  paul
 * Add grTexCombine and grTexCalcMemRequired
 *
 * Revision 1.57  2002/10/19 09:17:18  paul
 * Reenable GrClipWindow because it is now supported in OpenGLide.
 * Map through GuDrawTriangleWithClip.
 * Implement GuFogGenerateExp by requesting the
 * table from the server.
 *
 * Revision 1.56  2002/10/09 09:06:56  paul
 * Increase maximum number of gu textures.
 *
 * Support guDrawTriangleWithClip.
 *
 * Support guTexMemQueryAvail
 *
 * Speed up update area calculation for writable LFB
 *
 * Revision 1.55  2002/08/28 18:49:50  paul
 * Introduce Glidos command for VESA frames with half-height mode.
 * Update game starter to use VESA support
 * Introduce VESA start/stop commands to aid server in detecting the
 * dos window.
 *
 * Revision 1.54  2002/08/06 18:28:03  paul
 * Use separate arrays for reading from and writing to the LFB.
 *
 * Make LFB row stride 1024 for games that assume it.
 *
 * Give back constant depths when reading the aux buffer.
 *
 * All this makes Extream Assult work mostly
 *
 * Revision 1.53  2002/08/03 13:49:17  paul
 * Bump version to 1.17
 *
 * Revision 1.52  2002/08/03 13:26:56  paul
 * Mouse support for Descent II
 *
 * Revision 1.51  2002/07/07 09:38:31  paul
 * Handle grHints and ignore grTexDetailControl (for Carmageddon)
 *
 * Send buffer ref with LFB write requests.
 *
 * Send buffer updates if a buffer swap happens with the frame
 * buffer locked.
 *
 * Revision 1.50  2002/07/03 16:30:28  paul
 * Make judder fix timer frequency controllable
 *
 * Bump to v1.14a
 *
 * Revision 1.49  2002/06/26 14:46:48  paul
 * Remove some references to timesync and bump protocol
 * version
 *
 * Revision 1.48  2002/06/24 15:39:20  paul
 * Access pipe via a VDD to allow polling
 *
 * Revision 1.47  2002/06/03 18:39:31  paul
 * Store and reinstate TexSource after the logo and LFB textures
 *
 * Increment version
 *
 * Revision 1.46  2002/06/02 12:10:08  paul
 * Fix C warning
 *
 * Revision 1.45  2002/06/02 09:50:36  paul
 * Get copy of frame buffer from client in lfblock for read only
 *
 * Revision 1.44  2002/05/25 14:58:11  paul
 * Remove ConvertAndDownloadRLE hacks.
 *
 * No longer needed since I realised that what looked like corrupted
 * 1555 was AP88
 *
 * Revision 1.43  2002/05/21 16:15:31  paul
 * Map through a few more Glide calls, towards getting Red Guard going.
 *
 * Revision 1.42  2002/05/18 14:27:10  paul
 * Pick the higher of the two tlut addresses supplied by Descent II
 *
 * Revision 1.41  2002/05/14 19:29:49  paul
 * Update version
 *
 * Revision 1.40  2002/05/13 18:25:13  paul
 * Send ConvertAndDownloadRLE textures separately so as to
 * be able to avoid fingerprinting them
 *
 * Use bsearch to look up fingerprints.
 *
 * Revision 1.39  2002/05/11 16:12:21  paul
 * Add GrCullMode and GrFogColorValue for Descent II
 *
 * Revision 1.38  2002/05/11 15:57:50  paul
 * Turn of CD music fix for Win 98
 *
 * Revision 1.37  2002/05/11 15:53:29  paul
 * Make control-judder fix configurable.  Need for Descent II,
 * but makes TR run far too fast
 *
 * Revision 1.36  2002/05/09 16:24:45  paul
 * Remove synchonisation on ConvertRLEAndDownload
 *
 * Revision 1.35  2002/05/08 17:32:16  paul
 * Expect a stream of confirmation replies from the server.
 *
 * Also remove maybe_sync
 *
 * Revision 1.34  2002/05/07 09:51:01  paul
 * Use cyan for bluescreening so as not to lose the "blue key" indicator
 * in Descent II.
 *
 * Revision 1.33  2002/05/07 09:48:30  paul
 * Fill in only 1/3 of the missing clock ticks, but always do at least one per
 * frame.  The one per frame improves Descent II controls significantly.
 *
 * Revision 1.32  2002/05/01 18:27:07  paul
 * Send partial LFB updates when smaller than 256x256
 *
 * Revision 1.31  2002/04/30 17:50:05  paul
 * Display LFB writes by turning them into textures, using
 * alpha to leave the unwritten areas unchanged.
 *
 * Revision 1.30  2002/04/25 18:12:08  paul
 * Force immediate sending of draw commands when writing to the front buffer
 *
 * Revision 1.29  2002/04/24 16:47:10  paul
 * Remove const on the Descent II tlut array to avoid
 * compilation problems
 *
 * Revision 1.28  2002/04/22 18:21:43  paul
 * Build into ConvertAndDownloadRLE the tlut for Descent II.
 * Needed because Descent II sometimes sends garbage.
 * Acceptable because ConvertAndDownloadRLE is probably
 * not called by any other game.
 *
 * Revision 1.27  2002/04/21 19:05:42  paul
 * Pass a few more glide commands for Descent2.
 * Implement ConvertAndDownloadRLE by
 * decompressing locally
 *
 * Revision 1.26  2002/04/20 14:13:34  paul
 * Call int 8 directly rather than temporarily reving the timer chip.
 * Seems to more or less work, but something goes a bit wrong
 * when the guidebot is released. Telling it to stay away helps.
 *
 * Also fiddled about with synchronisation points to try and minimize the
 * guidbot problem
 *
 * Revision 1.25  2002/04/19 19:29:40  paul
 * First go at synchronising the DOS box clock under NT
 *
 * Dropped 4 bytes out of checked region (use of timeGetTime makes this
 * necessary).
 *
 * Added some debugging
 *
 * Revision 1.24  2002/04/16 15:29:58  paul
 * Move some of the assembly coded DOS calls out into
 * a seperate file to improve structure a little.
 *
 * Revision 1.23  2002/04/16 10:42:34  paul
 * Pass through a few more Glide commands and temporarily turn
 * off alpha bending as first steps towards getting Descent II working
 *
 * Revision 1.22  2002/02/19 18:26:49  paul
 * Removed some debugging and moved to next version of app
 * and protocol
 *
 * Revision 1.21  2002/02/19 18:04:20  paul
 * CD Music now working in W2k
 *
 * Revision 1.20  2002/02/18 15:12:58  paul
 * Take on the responsiblility for CD Audio, so that it works in W2k and XP.
 * Nearly working.
 *
 * Revision 1.19  2002/01/02 11:44:04  paul
 * Synchronise server with game on every frame to stop lag in low
 * complexity scenes.  Two line change.
 *
 * Revision 1.18  2001/11/10 04:07:29  paul
 * Pick between vxd and npipe automatically.
 *
 * Send bufferload, rather than one char at a time.
 *
 * Add reverse data path for confirmation (stub only in vxd).  This is needed
 * in the W2k build to force tomb.exe to wait until Glidos has pushed it into
 * fullscreen mode, before running FMV sequences.
 *
 * Add confirmation-request message (part of the above).
 *
 * Add forgotten file pipe.h
 *
 * Revision 1.17  2001/11/05 09:59:32  paul
 * Virtualise pipe access, and add "named pipe" version for W2K (which
 * doesn't work).
 *
 * Revision 1.16  2001/07/25 13:27:03  paul
 * Flipped protocol version to 3.
 * Added GrAADrawLine.
 *
 * Revision 1.15  2001/07/21 20:23:40  paul
 * Flip protocol version to 2.
 *
 * Revision 1.14  2001/07/16 14:13:07  paul
 * Speed up DOS to Windows data flow using large buffers in all components
 *
 * Revision 1.13  2001/07/13 08:21:34  paul
 * Flipped protocol to version 1.
 *
 * Added GrConstantColorValue4 and GrGlideShutdown
 *
 * Removed race condition in GrSstWinOpen.
 *
 * Implement GrDrawLine with a rectangle.
 *
 * Made window go away at game end, by closing on GrGlideShutdown,
 * and flushing pipe.
 *
 * Revision 1.12  2001/07/03 19:01:38  paul
 * Added version check
 *
 * Revision 1.11  2001/07/03 15:24:30  paul
 * A few more printf -> debug_printf
 *
 * Revision 1.10  2001/06/28 18:30:16  paul
 * Transfer the correct amount of data for textures.
 *
 * Revision 1.9  2001/06/28 10:57:18  paul
 * Hacky implementation of GrLfbLock
 *
 * Revision 1.8  2001/06/25 18:13:20  paul
 * Keep mimmap id in bounds used by server.
 * Change printf to debug_printf for implemented methods
 * Added a few more methods
 * Send flush command (empty buffer) on GrBufferSwap
 *
 * Revision 1.7  2001/06/23 16:07:22  paul
 * Implement gu texture methods using gr methods, to cope with glide wrappers
 * not providing the gu interface.
 *
 * Stop all messages to the debug window; now runs full speed.
 *
 * Revision 1.6  2001/06/22 15:35:22  paul
 * More debug messages.
 * Updated the reported hardware config to match OpenGlide
 * Refuse Gu3dfLoad to force TR I to generate mimmaps.
 *
 * Revision 1.5  2001/06/22 09:21:55  paul
 * First implementation of entry points used by Tomb Raider I
 *
 * Revision 1.4  2001/06/20 16:14:10  paul
 * Using counted buffer to send to the VXD.
 * Sending commands, only GrGlideInit at this stage.
 *
 * Revision 1.3  2001/06/20 09:20:48  paul
 * Made to communicate with Windows server via the gldpipe VXD
 *
 * Revision 1.2  2001/06/15 18:03:59  paul
 * Made to write to file instead of the screen.
 *
 * Revision 1.1.1.1  2001/06/13 14:43:51  paul
 * Initial version: just enough to fool Tomb Raider I to run.
 *
 ************************************************************/

#include <i86.h>
#include "glide.h"
#include "misc.h"
#include "vxd.h"
#include "npipe.h"
#include "timesync.h"
#include "mouse.h"

#define BUF_SIZE (1024*1024)
#define MAX_MM (1024)
#define VERSION (12)

#define GR_BUFFER_NONE (-1)

#define MIN(x, y) ((x) < (y) ? (x) : (y))

#define CMD_Message (1)
#define CMD_GrGlideInit (2)
#define CMD_GrSstQueryHardware (3)
#define CMD_GrSstSelect (4)
#define CMD_GrSstWinOpen (5)
#define CMD_GrDepthBufferMode (6)
#define CMD_GrDepthBufferFunction (7)
#define CMD_GrDepthMask (8)
#define CMD_GrTexCombineFunction (9)
#define CMD_GrConstantColorValue (10)
#define CMD_GuAlphaSource (11)
#define CMD_GrChromaKeyMode (12)
#define CMD_GrChromaKeyValue (13)
#define CMD_GrGammaCorrectionValue (14)
#define CMD_GrTexDownloadTable (15)
#define CMD_GuTexMemReset (16)
#define CMD_Gu3dfGetInfo (17)
#define CMD_Gu3dfLoad (18)
#define CMD_GuTexAllocateMemory (19)
#define CMD_GuTexDownloadMipMap (20)
#define CMD_GrBufferClear (21)
#define CMD_GuColorCombineFunction (22)
#define CMD_GuTexSource (23)
#define CMD_GrDrawPolygonVertexList (24)
#define CMD_GrBufferSwap (25)
#define CMD_GrTexFilterMode (26)
#define CMD_GrSstWinClose (27)
#define CMD_GrDepthBiasLevel (28)
#define CMD_GrColorCombine (29)
#define CMD_GrAlphaBlendFunction (30)
#define CMD_GrDrawLine (31)
#define CMD_GrConstantColorValue4 (32)
#define CMD_GrGlideShutdown (33)
#define CMD_GrAADrawLine (34)
#define CMD_ConfirmationRequest (35)
#define CMD_AudioPlay (36)
#define CMD_AudioPoll (37)
#define CMD_Ioctl (38)
#define CMD_GrRenderBuffer (39)
#define CMD_GrTexDownloadMipMap (40)
#define CMD_GrTexSource (41)
#define CMD_GrDrawTriangle (42)
#define CMD_GrDisableAllEffects (43)
#define CMD_GrTexMipMapMode (44)
#define CMD_GrTexLodBiasValue (45)
#define CMD_GrTexClampMode (46)
#define CMD_GrAlphaCombine (47)
#define CMD_GrFogMode (48)
#define CMD_GrAlphaTestFunction (49)
#define CMD_GrClipWindow (50)
#define CMD_DisplayLFB (51)
#define CMD_DisplayLFBPart (52)
#define CMD_GrCullMode (53)
#define CMD_GrFogColorValue (54)
#define CMD_ConvertAndDownloadRLE (55)
#define CMD_GrFogTable (56)
#define CMD_GrColorMask (57)
#define CMD_GrLFBLock (58)
#define CMD_GrTexDetailControl (59)
#define CMD_GrHints (60)
#define CMD_DisplayVESA (61)
#define CMD_VESAStart (62)
#define CMD_VESAEnd (63)
#define CMD_GuDrawTriangleWithClip (64)
#define CMD_GuFogGenerateExp (65)
#define CMD_GrTexCombine (66)
#define CMD_Mp3Play (67)
#define CMD_GuFogTableIndexToW (68)
#define CMD_GrAlphaTestReferenceValue (69)
#define CMD_GrLFBConstantAlpha (70)
#define CMD_GrLFBConstantDepth (71)


static pipe_t *pipe = 0;
static int running_nt = 0;
static int judder_fix = 0;

char buffer[BUF_SIZE];
int buf_index = 0;

void mp3_audio_update_state(int playing);

void send_buffer(char *buf, int count)
{
    timesync_poll();
    if(count == 0 || buf_index + count > BUF_SIZE)
    {
        pipe->send(buffer, buf_index);
        buf_index = 0;

        if(count == 0)
            pipe->flush();
    }

    if(count)
    {
        memcpy(buffer+buf_index, buf, count);
        buf_index += count;
    }
}

void wait_for_confirmation()
{
    int coord[3];
    int mp3_playing;

    pipe->get((char *) coord, 12);

    pipe->get((char *) &mp3_playing, 4);

    mp3_audio_update_state(mp3_playing);

    if(pipe->os != pipe_os_dosbox)
        Mouse_newState(coord[0], coord[1], coord[2]);
}


void send_context_switch(void)
{
    send_buffer(0, 0);
}

void synchronise_with_server()
{
    send_char(CMD_ConfirmationRequest);
    send_context_switch();
    wait_for_confirmation();
}


char prnt_buf[1024];
int buf_i;

void printc(char c)
{
    prnt_buf[buf_i++] = c;
}

void printdigit(int i)
{
    printc(i > 9 ? (i - 10 + 'A') : (i + '0'));
}

void print_int(int i, int base)
{
    char buf[20];
    int d = 0;

    if(i < 0)
    {
        printc('-');
        i = -i; /*FIXME: Doesn't work for MIN_INT */
    }

    if(i == 0)
    {
        printc('0');
    }
    else
    {
        while(i)
        {
            buf[d++] = i%base;
            i /= base;
        }

        while(d)
        {
            printdigit(buf[--d]);
        }
    }
}

#if 1
void debug_printf(char *x, ...)
{
}
#else
#define debug_printf printf
#endif

void printf(char *s, ...)
{
   /*
    * Assumes that arguments are on the stack,
    * and that each are of integer size
    */
    int *p = (int *)&s;
    buf_i = 0;
    prnt_buf[buf_i++] = CMD_Message;

    while(*s)
    {
        switch(*s)
        {
        case '%':
            ++s;
            switch(*s)
            {
            case 'x':
                ++s;
                print_int(*(++p), 16);
                break;
            case 'd':
                ++s;
                print_int(*(++p), 10);
                break;
            }
            break;

        case '\n':
            ++s;
            break;

        default:
            printc(*s++);
            break;
        }
    }

    prnt_buf[buf_i++] = 0;
    send_buffer(prnt_buf, buf_i);
}

void send_char(char c)
{
    send_buffer(&c, 1);
}

void send_int(int i)
{
    send_buffer((char *) &i, 4);
}

void send_counted_string(char *s)
{
    int i;

    for(i = 0; s[i]; i++)
        ;

    send_int(i);
    send_buffer(s, i);
}

void open_communication(void)
{
    pipe = pipe_open();

    running_nt = (pipe->os == pipe_os_nt);
}





#define NCDCMD (32)

struct real_call
{
    int edi;
    int esi;
    int ebp;
    int reserved;
    int ebx;
    int edx;
    int ecx;
    int eax;
    short flags;
    short es;
    short ds;
    short fs;
    short gs;
    short ip;
    short cs;
    short sp;
    short ss;
};
struct cd_cmd
{
    int drive;
    int start;
    int count;
};

struct cd_cmd cd_cmd_buf[NCDCMD];
int cd_cmd_n = 0;
int play_status = 1;

void (__interrupt __far *old_int31_handler)();

void __interrupt __far int31_mp3(union INTPACK r)
{
    if((r.x.eax & 0xffff) == 0x0300 && r.x.ebx == 0x2f)
    {
        /* We have picked up a simulated real mode int 2fh
         * Lets have a look at the call structure */
        struct real_call *rc = (struct real_call *) r.x.edi;

        if(rc->eax == 0x1510)
        {
            /* Its a device request aimed at mscdex
             * es:bx is the real mode address of the
             * request header */
            unsigned char *req = (unsigned char *) ((rc->es)<<4 | rc->ebx);

            /* Return success indication */
            req[3] = 0;
            req[4] = 1;

            switch(req[2])
            {
            case 3: /* IOCTL input */
                {
                    unsigned int   p    = *((unsigned int *)(req + 14));
                    unsigned char *ctrl =  (unsigned char *)((p>>16) * 0x10 + (p & 0xffff));

                    switch(ctrl[0])
                    {
                    case 6: /* Divice status */
                        ctrl[1] = 2;
                        ctrl[2] = 2;
                        ctrl[3] = 0;
                        ctrl[4] = 0;
                        break;

                    case 9: /* Media changed */
                        ctrl[1] = 255;
                        break;

                    case 10: /* Audio disc info */
                        ctrl[1] = 1;
                        ctrl[2] = 10;
                        ctrl[3] = 0;
                        ctrl[4] = 0;
                        ctrl[5] = 10;
                        ctrl[6] = 0;
                        break;

                    case 11: /* Audio track info */
                        ctrl[2] = 0;
                        ctrl[3] = 0;
                        ctrl[4] = ctrl[1] - 1;
                        ctrl[5] = 0;
                        ctrl[6] = (ctrl[1] == 1) ? 4 : 0;
                        break;

                    case 12: /* Q channel */
                        req[4] |= (play_status ? 2 : 0);
                        break;
                    }
                }
                break;

            case 131: /* SEEK */
                cd_cmd_buf[cd_cmd_n].drive = -1;
                cd_cmd_buf[cd_cmd_n].start = 0;
                cd_cmd_n += 1;

                play_status = 1;
                break;

            case 132: /* PLAY */
                cd_cmd_buf[cd_cmd_n].drive = -1;
                cd_cmd_buf[cd_cmd_n].start = (req[14] | (req[15] << 8) | (req[16] << 16) | (req[17] << 24));
                cd_cmd_n += 1;

                play_status = 1;
                req[4] = 3;
                break;

            case 133: /* STOP */
                cd_cmd_buf[cd_cmd_n].drive = -1;
                cd_cmd_buf[cd_cmd_n].start = 0;
                cd_cmd_n += 1;
                break;
            }

            return; /* Don't chain */
        }
    }

    /* In cases where we don't deal with request,
     * we chain to the original handler */
    chain(old_int31_handler);
}


void __interrupt __far int31_cd(union INTPACK r)
{
    if((r.x.eax & 0xffff) == 0x0300 && r.x.ebx == 0x2f)
    {
        /* We have picked up a simulated real mode int 2fh
         * Lets have a look at the call structure */
        struct real_call *rc = (struct real_call *) r.x.edi;

        if(rc->eax == 0x1510)
        {
            /* Its a device request aimed at mscdex
             * es:bx is the real mode address of the
             * request header */
            unsigned char *req = (unsigned char *) ((rc->es)<<4 | rc->ebx);


            if(req[2] == 131) /* Seek */
            {
                /* Return success indication */
                req[3] = 0;
                req[4] = 1;

                return;
            }

            if(req[2] == 132 && cd_cmd_n < NCDCMD) /* Play Audio */
            {
                cd_cmd_buf[cd_cmd_n].drive = r.x.ecx;
                cd_cmd_buf[cd_cmd_n].start = req[14] | (req[15] << 8) | (req[16] << 16) | (req[17] << 24);
                cd_cmd_buf[cd_cmd_n].count = req[18] | (req[19] << 8) | (req[20] << 16) | (req[21] << 24);

                cd_cmd_n += 1;

                /* Return success indication */
                req[3] = 0;
                req[4] = 1;

                return;
            }

            /* Stop Audio */
            /* Let TR handle the stop command */
            /*
            if(req[2] == 133 && cd_cmd_n < NCDCMD)
            {
                chain();
            }
            */

            if(req[2] == 3 && cd_cmd_n < NCDCMD) /* IOCTL READ */
            {
                cd_cmd_buf[cd_cmd_n].drive = r.x.ecx;
                cd_cmd_buf[cd_cmd_n].start = 0;
                cd_cmd_buf[cd_cmd_n].count = 0;

                cd_cmd_n += 1;

                /* Return success indication */
                req[3] = 0;
                req[4] = 1;

                return;
            }
        }
    }

    /* In cases where we don't deal with request,
     * we chain to the original handler */
    chain(old_int31_handler);
}

void mp3_audio_update_state(int playing)
{
    play_status = playing;
}

static void send_cd_cmds(void)
{
    int i;

    for(i = 0; i < cd_cmd_n; i++)
    {
        if(cd_cmd_buf[i].drive < 0)
        {
            send_char(CMD_Mp3Play);
            send_int(cd_cmd_buf[i].start);
        }
        else if(cd_cmd_buf[i].start == 0)
        {
            send_char(CMD_AudioPoll);
        }
        else
        {
            send_char(CMD_AudioPlay);
            send_int(cd_cmd_buf[i].drive);
            send_int(cd_cmd_buf[i].start);
            send_int(cd_cmd_buf[i].count);
        }
    }

    cd_cmd_n = 0;
}


/* Stuff for hiding ds selector in the
 * code page */


static int sat_on_31 = 0;

static void sit_on_31(int type)
{
    if(!sat_on_31)
    {
        sat_on_31 = 1;

        old_int31_handler = getvect(0x31);

        switch(type)
        {
            case 1:
                setvect(0x31, int31_cd);
                break;

            case 2:
                setvect(0x31, int31_mp3);
                break;
        }
    }
}

static void get_off_31(void)
{
    if(sat_on_31)
    {
        sat_on_31 = 0;
        setvect(0x31, old_int31_handler);
    }
}


int texture_size(int Lod, int aspectRatio, int format )
{
    static int nLength, nBytes;

    switch(Lod)
    {
    case GR_LOD_256:      nLength = 256;    break;
    case GR_LOD_128:      nLength = 128;    break;
    case GR_LOD_64:       nLength = 64;     break;
    case GR_LOD_32:       nLength = 32;     break;
    case GR_LOD_16:       nLength = 16;     break;
    case GR_LOD_8:        nLength = 8;      break;
    case GR_LOD_4:        nLength = 4;      break;
    case GR_LOD_2:        nLength = 2;      break;
    case GR_LOD_1:        nLength = 1;      break;
    }

    switch(aspectRatio)
    {
    case GR_ASPECT_1x1:        nBytes = nLength * nLength;               break;
    case GR_ASPECT_1x2:
    case GR_ASPECT_2x1:        nBytes = (nLength >> 1) * nLength;        break;
    case GR_ASPECT_1x4:
    case GR_ASPECT_4x1:        nBytes = (nLength >> 2) * nLength;        break;
    case GR_ASPECT_1x8:
    case GR_ASPECT_8x1:        nBytes = (nLength >> 3) * nLength;        break;
    }

    switch(format)
    {
    case GR_TEXFMT_RGB_565:
    case GR_TEXFMT_ARGB_8332:
    case GR_TEXFMT_AYIQ_8422:
    case GR_TEXFMT_ARGB_1555:
    case GR_TEXFMT_ARGB_4444:
    case GR_TEXFMT_ALPHA_INTENSITY_88:
    case GR_TEXFMT_AP_88:
        nBytes <<= 1;
        break;
    }

    return nBytes;
}

static int mipmap_id = 0;
static int mipmap_size[MAX_MM];

static int disable_lfb_read     = 1;
static int delay_lfb_write      = 1;
static int lfb_read_contents    = GR_BUFFER_NONE;

static int lfb_write_pending    = 0;
static int frame_started        = 0;
static int buffer_is_locked     = 0;
static GrBuffer_t render_buffer = GR_BUFFER_BACKBUFFER;
static GrBuffer_t locked_buffer = GR_BUFFER_BACKBUFFER;

static void setup_blue_screen(void);
static void send_blue_screen(GrBuffer_t buffer);
static void flush_lfb_writes(void);

static void react_to_render(void)
{
    frame_started = 1;

    if(lfb_read_contents == render_buffer)
        lfb_read_contents = GR_BUFFER_NONE;
}

static void react_to_swap(void)
{
    frame_started = 0;

    switch(lfb_read_contents)
    {
        case GR_BUFFER_BACKBUFFER:
            lfb_read_contents = GR_BUFFER_FRONTBUFFER;
            break;

        case GR_BUFFER_FRONTBUFFER:
            lfb_read_contents = GR_BUFFER_BACKBUFFER;
            break;
    }
}

void __export __stdcall _GRTEXDOWNLOAD_DEFAULT_4_8(int a, int b, int c, int d, int e, int f)
{
    printf("_GRTEXDOWNLOAD_DEFAULT_4_8@24\n");
}

void __export __stdcall _GRTEXDOWNLOAD_DEFAULT_4_WIDES(int a, int b, int c, int d, int e, int f)
{
    printf("_GRTEXDOWNLOAD_DEFAULT_4_WIDES@24\n");
}

void __export __stdcall _GRTEXDOWNLOAD_DEFAULT_8_1(int a, int b, int c, int d, int e, int f)
{
    printf("_GRTEXDOWNLOAD_DEFAULT_8_1@24\n");
}

void __export __stdcall _GRTEXDOWNLOAD_DEFAULT_8_2(int a, int b, int c, int d, int e, int f)
{
    printf("_GRTEXDOWNLOAD_DEFAULT_8_2@24\n");
}

void __export __stdcall _GRTEXDOWNLOAD_DEFAULT_8_4(int a, int b, int c, int d, int e, int f)
{
    printf("_GRTEXDOWNLOAD_DEFAULT_8_4@24\n");
}

void __export __stdcall _GRTEXDOWNLOAD_DEFAULT_8_WIDES(int a, int b, int c, int d, int e, int f)
{
    printf("_GRTEXDOWNLOAD_DEFAULT_8_WIDES@24\n");
}

void __export __stdcall _GRTEXDOWNLOAD_DEFAULT_16_1(int a, int b, int c, int d, int e, int f)
{
    printf("_GRTEXDOWNLOAD_DEFAULT_16_1@24\n");
}

void __export __stdcall _GRTEXDOWNLOAD_DEFAULT_16_2(int a, int b, int c, int d, int e, int f)
{
    printf("_GRTEXDOWNLOAD_DEFAULT_16_2@24\n");
}

void __export __stdcall _GRTEXDOWNLOAD_DEFAULT_16_WIDES(int a, int b, int c, int d, int e, int f)
{
    printf("_GRTEXDOWNLOAD_DEFAULT_16_WIDES@24\n");
}

void __export __stdcall _GRTEXDOWNLOAD_DEFAULT_32_1(int a, int b, int c, int d, int e, int f)
{
    printf("_GRTEXDOWNLOAD_DEFAULT_32_1@24\n");
}

void __export __stdcall _GRTEXDOWNLOAD_DEFAULT_32_WIDES(int a, int b, int c, int d, int e, int f)
{
    printf("_GRTEXDOWNLOAD_DEFAULT_32_WIDES@24\n");
}

void __export __stdcall GRSPLASH(int a, int b, int c, int d, int e)
{
    printf("GRSPLASH@20\n");
}

FxBool __export __stdcall GU3DFGETINFO(char *filename, Gu3dfInfo *file_info)
{
    debug_printf("GU3DFGETINFO@8\n");
   /*
    * Pretend file doesn't exist to get TRI to
    * generate textures on the fly.
    * FIXME: wont work on most games, probably
    */
    return FXFALSE;

    send_char(CMD_Gu3dfGetInfo);
    send_counted_string(filename);
    send_int((int) file_info);
    file_info->mem_required = 1024;

    return FXTRUE;
}

FxBool __export __stdcall GU3DFLOAD(char *filename, Gu3dfInfo *file_info)
{
    debug_printf("GU3DFLOAD@8\n");

    return FXFALSE;

    send_char(CMD_Gu3dfLoad);
    send_counted_string(filename);
    send_int((int) file_info);
    send_int((int) file_info->data);

    return FXTRUE;
}

void __export __stdcall GUALPHASOURCE(int a)
{
    debug_printf("GUALPHASOURCE@4\n");
    send_char(CMD_GuAlphaSource);
    send_int(a);
}

void __export __stdcall GUCOLORCOMBINEFUNCTION(int a)
{
    debug_printf("GUCOLORCOMBINEFUNCTION@4\n");
    send_char(CMD_GuColorCombineFunction);
    send_int(a);
}

void __export __stdcall GUENDIANSWAPWORDS(int a)
{
    printf("GUENDIANSWAPWORDS@4\n");
}

void __export __stdcall GUENDIANSWAPBYTES(int a)
{
    printf("GUENDIANSWAPBYTES@4\n");
}

float __export __stdcall GUFOGTABLEINDEXTOW(int i)
{
    float res = 0;

    /* Get this done by the server because we cannot do
     * floating point arithmetic here */
    send_char(CMD_GuFogTableIndexToW);
    send_int(i);
    send_context_switch();

    pipe->get((char *)&res, sizeof(res));
    return res;
}

void __export __stdcall GUFOGGENERATEEXP(GrFog_t *table, int density)
{
    debug_printf("GUFOGGENERATEEXP@8\n");

    send_char(CMD_GuFogGenerateExp);
    send_int(density);
    send_context_switch();

    pipe->get(table, GR_FOG_TABLE_SIZE * sizeof(GrFog_t));
}

void __export __stdcall GUFOGGENERATEEXP2(int a, int b)
{
    printf("GUFOGGENERATEEXP2@8\n");
}

void __export __stdcall GUFOGGENERATELINEAR(int a, int b, int c)
{
    printf("GUFOGGENERATELINEAR@12\n");
}

void __export __stdcall GUTEXCREATECOLORMIPMAP(void)
{
    printf("GUTEXCREATECOLORMIPMAP@0\n");
}

void __export __stdcall GUENCODERLE16(int a, int b, int c, int d)
{
    printf("GUENCODERLE16@16\n");
}

void __export __stdcall GUDRAWTRIANGLEWITHCLIP(GrVertex *v1, GrVertex *v2, GrVertex *v3)
{
    debug_printf("GUDRAWTRIANGLEWITHCLIP@12\n");

    send_char(CMD_GuDrawTriangleWithClip);
    send_buffer((char *) v1, sizeof(GrVertex));
    send_buffer((char *) v2, sizeof(GrVertex));
    send_buffer((char *) v3, sizeof(GrVertex));

    if(render_buffer == GR_BUFFER_FRONTBUFFER)
        synchronise_with_server();
}

void __export __stdcall GUAADRAWTRIANGLEWITHCLIP(int a, int b, int c)
{
    printf("GUAADRAWTRIANGLEWITHCLIP@12\n");
}

void __export __stdcall GUDRAWPOLYGONVERTEXLISTWITHCLIP(int a, int b)
{
    printf("GUDRAWPOLYGONVERTEXLISTWITHCLIP@8\n");
}

void __export __stdcall GUMPINIT(void)
{
    printf("GUMPINIT@0\n");
}

void __export __stdcall GUMPTEXCOMBINEFUNCTION(int a)
{
    printf("GUMPTEXCOMBINEFUNCTION@4\n");
}

void __export __stdcall GUMPTEXSOURCE(int a, int b)
{
    printf("GUMPTEXSOURCE@8\n");
}

void __export __stdcall GUFBREADREGION(int a, int b, int c, int d, int e, int f)
{
    printf("GUFBREADREGION@24\n");
}

void __export __stdcall GUFBWRITEREGION(int a, int b, int c, int d, int e, int f)
{
    printf("GUFBWRITEREGION@24\n");
}

void __export __stdcall GRGLIDEGETVERSION(char version[80])
{
    debug_printf("GRGLIDEGETVERSION@4\n");
    memcpy(version, "2.48.00.0455", 13);
}

void __export __stdcall GRGLIDEGETSTATE(int a)
{
    printf("GRGLIDEGETSTATE@4\n");
}

void __export __stdcall GRHINTS(int a, int b)
{
    debug_printf("GRHINTS@8\n");
    send_char(CMD_GrHints);
    send_int(a);
    send_int(b);
}

void __export __stdcall GRGLIDEINIT(void)
{
    char buf[64];
    int  redbook_emulation;

    misc_init();
    open_communication();

    if(pipe->os != pipe_os_dosbox)
        Mouse_install();

    debug_printf("GRGLIDEINIT@0\n");
    send_char(CMD_GrGlideInit);
    send_int(VERSION);
   /*
    * Send current dir to server so that
    * it can load .3df files.
    */
    get_current_dir(buf);
    send_counted_string(buf);

    send_context_switch();

    judder_fix = pipe->readChar();
    if(running_nt && judder_fix)
        JudderFixOn(judder_fix);

    disable_lfb_read = pipe->readChar();

    delay_lfb_write = pipe->readChar();

    redbook_emulation = pipe->readChar();

    if(running_nt && redbook_emulation)
        sit_on_31(redbook_emulation);
}

void __export __stdcall GRGLIDESHAMELESSPLUG(int a)
{
    printf("GRGLIDESHAMELESSPLUG@4\n");
}

void __export __stdcall GRRESETTRISTATS(void)
{
    printf("GRRESETTRISTATS@0\n");
}

void __export __stdcall GRTRISTATS(int a, int b)
{
    printf("GRTRISTATS@8\n");
}

void __export __stdcall GRSSTQUERYBOARDS(int a)
{
    printf("GRSSTQUERYBOARDS@4\n");
}

FxBool __export __stdcall GRSSTQUERYHARDWARE(GrHwConfiguration *hwConfig)
{
    GrVoodooConfig_t *vdConfig;
    GrTMUConfig_t *tmuConfig;

    debug_printf("GRSSTQUERYHARDWARE@4\n");
    debug_printf("  num_sst = %d\n", hwConfig->num_sst);
    send_char(CMD_GrSstQueryHardware);

    hwConfig->num_sst = 1;

    hwConfig->SSTs[0].type = GR_SSTTYPE_VOODOO;

    vdConfig = &(hwConfig->SSTs[0].sstBoard.VoodooConfig);
    vdConfig->fbRam = 4;
    vdConfig->fbiRev = 2;
    vdConfig->nTexelfx = 1;
    vdConfig->sliDetect = FXFALSE;

    tmuConfig = &(vdConfig->tmuConfig[0]);
    tmuConfig->tmuRev = 1;
    tmuConfig->tmuRam = 2;

    return FXTRUE;
}

void __export __stdcall GRSSTSELECT(int which_sst)
{
    int i;

    debug_printf("GRSSTSELECT@4\n");
    debug_printf("  sst = %d\n", which_sst);
    send_char(CMD_GrSstSelect);
}

void __export __stdcall GRSSTSCREENWIDTH(void)
{
    printf("GRSSTSCREENWIDTH@0\n");
}

void __export __stdcall GRSSTSCREENHEIGHT(void)
{
    printf("GRSSTSCREENHEIGHT@0\n");
}

void __export __stdcall GRSSTVIDMODE(int a, int b)
{
    printf("GRSSTVIDMODE@8\n");
}

FxU32 __export __stdcall GRTEXCALCMEMREQUIRED(GrLOD_t lodmin, GrLOD_t lodmax, GrAspectRatio_t aspect, GrTextureFormat_t fmt)
{
    int size, i;
    debug_printf("GRTEXCALCMEMREQUIRED@16\n");

    size = 0;

    for(i = lodmax; i <= lodmin; i++)
        size += texture_size(i, aspect, fmt);

    debug_printf("size = %x\n", size);

    return size;
}

void __export __stdcall GRTEXDETAILCONTROL(int a, int b, int c, int d)
{
    debug_printf("GRTEXDETAILCONTROL@16\n");
    send_char(CMD_GrTexDetailControl);
}

FxU32 __export __stdcall GRTEXMINADDRESS(int a)
{
    debug_printf("GRTEXMINADDRESS@4\n");

    return 8;
}

FxU32 __export __stdcall GRTEXMAXADDRESS(int a)
{
    debug_printf("GRTEXMAXADDRESS@4\n");

    return 8*1024*1024 - 128*1024;
}

int __export __stdcall GRTEXTEXTUREMEMREQUIRED(FxU32 evenOdd,
                                       GrTexInfo *info)
{
    int size, i;
    debug_printf("GRTEXTEXTUREMEMREQUIRED@8\n");

    size = 0;

    for(i = info->largeLod; i <= info->smallLod; i++)
        size += texture_size(i, info->aspectRatio, info->format);

    return size;
}

void __export __stdcall GRTEXDOWNLOADMIPMAP(GrChipID_t tmu, FxU32 startAddress, FxU32 evenOdd, GrTexInfo *info)
{
    int size, i;
    debug_printf("GRTEXDOWNLOADMIPMAP@16\n");

    size = 0;

    for(i = info->largeLod; i <= info->smallLod; i++)
        size += texture_size(i, info->aspectRatio, info->format);

    send_char(CMD_GrTexDownloadMipMap);
    send_int(tmu);
    send_int(startAddress);
    send_int(evenOdd);
    send_buffer((char *) info, sizeof(GrTexInfo));
    send_int(size);
    send_buffer(info->data, size);
}

void __export __stdcall GRTEXDOWNLOADTABLEPARTIAL(int a, int b, int c, int d, int e)
{
    printf("GRTEXDOWNLOADTABLEPARTIAL@20\n");
}

void __export __stdcall GRTEXDOWNLOADMIPMAPLEVEL(GrChipID_t        tmu,
                          FxU32             startAddress,
                          GrLOD_t           thisLod,
                          GrLOD_t           largeLod,
                          GrAspectRatio_t   aspectRatio,
                          GrTextureFormat_t format,
                          FxU32             evenOdd,
                          void              *data)
{
    GrTexInfo info;

    debug_printf("GRTEXDOWNLOADMIPMAPLEVEL@32");

    if(thisLod == largeLod)
    {
        info.aspectRatio = aspectRatio;
        info.data        = data;
        info.format      = format;
        info.largeLod    = largeLod;
        info.smallLod    = largeLod;

        GRTEXDOWNLOADMIPMAP(tmu, startAddress, evenOdd, &info);
    }
}

void __export __stdcall GRERRORSETCALLBACK(int a)
{
    debug_printf("GRERRORSETCALLBACK@4\n");
}

void __export __stdcall GUMOVIESTART(void)
{
    printf("GUMOVIESTART@0\n");
}

void __export __stdcall GUMOVIESTOP(void)
{
    printf("GUMOVIESTOP@0\n");
}

void __export __stdcall GUMOVIESETNAME(int a)
{
    printf("GUMOVIESETNAME@4\n");
}

int __export __stdcall GUTEXALLOCATEMEMORY(
                    GrChipID_t tmu,
                    FxU8 odd_even_mask,
                    int width, int height,
                    GrTextureFormat_t fmt,
                    GrMipMapMode_t mm_mode,
                    GrLOD_t smallest_lod, GrLOD_t largest_lod,
                    GrAspectRatio_t aspect,
                    GrTextureClampMode_t s_clamp_mode,
                    GrTextureClampMode_t t_clamp_mode,
                    GrTextureFilterMode_t minfilter_mode,
                    GrTextureFilterMode_t magfilter_mode,
                    int lod_bias, /* a float really */
                    FxBool trilinear)
{
    int id, i, size;;

    debug_printf("GUTEXALLOCATEMEMORY@60\n");

    if(mipmap_id >= MAX_MM)
    {
        printf("TEXTURE ID OVERFLOW\n");
        return GR_NULL_MIPMAP_HANDLE;
    }

    id = mipmap_id++;

    size = 0;

    for(i = largest_lod; i <= smallest_lod; i++)
        size += texture_size(i, aspect, fmt);

    mipmap_size[id] = size;
    debug_printf("%d -> %d", id, mipmap_size[id]);

    send_char(CMD_GuTexAllocateMemory);
    send_int(id);

    send_int(tmu);
    send_int(odd_even_mask);
    send_int(width);
    send_int(height);
    send_int(fmt);
    send_int(mm_mode);
    send_int(smallest_lod);
    send_int(largest_lod);
    send_int(aspect);
    send_int(s_clamp_mode);
    send_int(t_clamp_mode);
    send_int(minfilter_mode);
    send_int(magfilter_mode);
    send_int(lod_bias);
    send_int(trilinear);

    return id + 1;
}

void __export __stdcall GUTEXCHANGEATTRIBUTES(int a, int b, int c, int d,
                           int e, int f, int g, int h,
                           int i, int j, int k, int l)
{
    printf("GUTEXCHANGEATTRIBUTES@48\n");
}

void __export __stdcall GRTEXCOMBINEFUNCTION(int a, int b)
{
    debug_printf("GRTEXCOMBINEFUNCTION@8\n");
    send_char(CMD_GrTexCombineFunction);
    send_int(a);
    send_int(b);
}

void __export __stdcall GUTEXCOMBINEFUNCTION(int a, int b)
{
    debug_printf("GUTEXCOMBINEFUNCTION@8\n");
    send_char(CMD_GrTexCombineFunction);
    send_int(a);
    send_int(b);
}

void __export __stdcall GUTEXDOWNLOADMIPMAP(GrMipMapId_t mmid, void *src, GuNccTable *table)
{
    int id;

    debug_printf("GUTEXDOWNLOADMIPMAP@12\n");
    send_char(CMD_GuTexDownloadMipMap);

    id = mmid-1;

    send_int(id);
    send_int(mipmap_size[id]);
    send_buffer((char *) src, mipmap_size[id]);

    send_int((int)table);
    if(table)
        send_buffer((char *) table, sizeof(GuNccTable));
}

void __export __stdcall GUTEXDOWNLOADMIPMAPLEVEL(int a, int b, int c)
{
    printf("GUTEXDOWNLOADMIPMAPLEVEL@12\n");
}

void __export __stdcall GUTEXGETCURRENTMIPMAP(int a)
{
    printf("GUTEXGETCURRENTMIPMAP@4\n");
}

void __export __stdcall GUTEXGETMIPMAPINFO(int a)
{
    printf("GUTEXGETMIPMAPINFO@4\n");
}

FxU32 __export __stdcall GUTEXMEMQUERYAVAIL(int a)
{
    debug_printf("GUTEXMEMQUERYAVAIL@4\n");
    return 16 * 1024 * 1024;
}

void __export __stdcall GUTEXMEMRESET(void)
{
    debug_printf("GUTEXMEMRESET@0\n");
    send_char(CMD_GuTexMemReset);
    mipmap_id = 0;
}

void __export __stdcall _GUMPTEXCOMBINEFUNCTION(int a)
{
    printf("_GUMPTEXCOMBINEFUNCTION@4\n");
}

void __export __stdcall GUMPDRAWTRIANGLE(int a, int b, int c)
{
    printf("GUMPDRAWTRIANGLE@12\n");
}

void __export __stdcall GRAADRAWPOINT(int a)
{
    printf("GRAADRAWPOINT@4\n");
}

void __export __stdcall GRAADRAWLINE(GrVertex *v1, GrVertex *v2)
{
    debug_printf("GRAADRAWLINE@8\n");
    send_char(CMD_GrAADrawLine);
    send_buffer((char *) v1, sizeof(GrVertex));
    send_buffer((char *) v2, sizeof(GrVertex));
}

void __export __stdcall GRAADRAWTRIANGLE(int a, int b, int c, int d, int e, int f)
{
    printf("GRAADRAWTRIANGLE@24\n");
}

void __export __stdcall GRAADRAWPOLYGON(int a, int b, int c)
{
    printf("GRAADRAWPOLYGON@12\n");
}

void __export __stdcall GRAADRAWPOLYGONVERTEXLIST(int nverts, GrVertex vlist[])
{
    printf("GRAADRAWPOLYGONVERTEXLIST@8\n");
}

void __export __stdcall GRDRAWPOINT(int a)
{
    printf("GRDRAWPOINT@4\n");
}

void __export __stdcall GRDRAWLINE(GrVertex *v1, GrVertex *v2)
{
    debug_printf("GRDRAWLINE@8\n");

    react_to_render();

    send_char(CMD_GrDrawLine);
    send_buffer((char *) v1, sizeof(GrVertex));
    send_buffer((char *) v2, sizeof(GrVertex));

    if(render_buffer == GR_BUFFER_FRONTBUFFER)
        synchronise_with_server();
}

void __export __stdcall GRDRAWTRIANGLE(GrVertex *v1, GrVertex *v2, GrVertex *v3)
{
    debug_printf("GRDRAWTRIANGLE@12\n");

    react_to_render();

    send_char(CMD_GrDrawTriangle);
    send_buffer((char *) v1, sizeof(GrVertex));
    send_buffer((char *) v2, sizeof(GrVertex));
    send_buffer((char *) v3, sizeof(GrVertex));

    if(render_buffer == GR_BUFFER_FRONTBUFFER)
        synchronise_with_server();
}

void __export __stdcall GRDRAWPLANARPOLYGON(int a, int b, int c)
{
    printf("GRDRAWPLANARPOLYGON@12\n");
}

void __export __stdcall GRDRAWPLANARPOLYGONVERTEXLIST(int a, int b)
{
    printf("GRDRAWPLANARPOLYGONVERTEXLIST@8\n");
}

void __export __stdcall GRDRAWPOLYGON(int a, int b, int c)
{
    printf("GRDRAWPOLYGON@12\n");
}

void __export __stdcall GRDRAWPOLYGONVERTEXLIST(int nverts, GrVertex vlist[])
{
    debug_printf("GRDRAWPOLYGONVERTEXLIST@8\n");

    react_to_render();

    send_char(CMD_GrDrawPolygonVertexList);
    send_int(nverts);
    send_buffer((char *) vlist, nverts * sizeof(GrVertex));
}

void __export __stdcall _GRCOLORCOMBINEDELTA0MODE(int a)
{
    printf("_GRCOLORCOMBINEDELTA0MODE@4\n");
}

void __export __stdcall GRALPHABLENDFUNCTION(int a, int b, int c, int d)
{
    debug_printf("GRALPHABLENDFUNCTION@16\n");
    send_char(CMD_GrAlphaBlendFunction);
    send_int(a);
    send_int(b);
    send_int(c);
    send_int(d);
}

void __export __stdcall GRALPHACOMBINE(int a, int b, int c, int d, int e)
{
    debug_printf("GRALPHACOMBINE@20\n");
    send_char(CMD_GrAlphaCombine);
    send_int(a);
    send_int(b);
    send_int(c);
    send_int(d);
    send_int(e);
}

void __export __stdcall GRALPHACONTROLSITRGBLIGHTING(int a)
{
    printf("GRALPHACONTROLSITRGBLIGHTING@4\n");
}

void __export __stdcall GRALPHATESTFUNCTION(int a)
{
    debug_printf("GRALPHATESTFUNCTION@4\n");
    send_char(CMD_GrAlphaTestFunction);
    send_int(a);
}

void __export __stdcall GRALPHATESTREFERENCEVALUE(int a)
{
    debug_printf("GRALPHATESTREFERENCEVALUE@4\n");
    send_char(CMD_GrAlphaTestReferenceValue);
    send_int(a);
}

void __export __stdcall GRBUFFERCLEAR(int a, int b, int c)
{
    debug_printf("GRBUFFERCLEAR@12\n");
    send_char(CMD_GrBufferClear);
    send_int(a);
    send_int(b);
    send_int(c);
}

void __export __stdcall GRBUFFERSWAP(int a)
{
    debug_printf("GRBUFFERSWAP@4\n");

    send_cd_cmds();

    flush_lfb_writes();
    react_to_swap();

    if(running_nt && judder_fix == 1)
        timesync_init();

    if(buffer_is_locked)
    {
        send_blue_screen(locked_buffer);
        setup_blue_screen();
    }

    send_char(CMD_GrBufferSwap);
    send_int(a);

    synchronise_with_server();
}

int __export __stdcall GRBUFFERNUMPENDING(void)
{
    debug_printf("GRBUFFERNUMPENDING@0\n");

    synchronise_with_server();

    return 0;
}

void __export __stdcall GRCHROMAKEYMODE(int a)
{
    debug_printf("GRCHROMAKEYMODE@4\n");
    send_char(CMD_GrChromaKeyMode);
    send_int(a);
}

void __export __stdcall GRCHROMAKEYVALUE(int a)
{
    debug_printf("GRCHROMAKEYVALUE@4\n");
    send_char(CMD_GrChromaKeyValue);
    send_int(a);
}

void __export __stdcall GRCLIPWINDOW(int a, int b, int c, int d)
{
    debug_printf("GRCLIPWINDOW@16\n");
    send_char(CMD_GrClipWindow);
    send_int(a);
    send_int(b);
    send_int(c);
    send_int(d);
}

void __export __stdcall GRCOLORCOMBINE(int a, int b, int c, int d, int e)
{
    debug_printf("GRCOLORCOMBINE@20\n");
    send_char(CMD_GrColorCombine);
    send_int(a);
    send_int(b);
    send_int(c);
    send_int(d);
    send_int(e);
}

void __export __stdcall GRCOLORMASK(int a, int b)
{
    debug_printf("GRCOLORMASK@8\n");
    send_char(CMD_GrColorMask);
    send_int(a);
    send_int(b);
}

void __export __stdcall GRCONSTANTCOLORVALUE(int a)
{
    debug_printf("GRCONSTANTCOLORVALUE@4\n");
    send_char(CMD_GrConstantColorValue);
    send_int(a);
}

void __export __stdcall GRCONSTANTCOLORVALUE4(int a, int b, int c, int d)
{
    debug_printf("GRCONSTANTCOLORVALUE4@16\n");
    send_char(CMD_GrConstantColorValue4);
    send_int(a);
    send_int(b);
    send_int(c);
    send_int(d);
}

void __export __stdcall GRCULLMODE(int a)
{
    debug_printf("GRCULLMODE@4\n");
    send_char(CMD_GrCullMode);
    send_int(a);
}

void __export __stdcall GRDEPTHBIASLEVEL(int a)
{
    debug_printf("GRDEPTHBIASLEVEL@4\n");
    send_char(CMD_GrDepthBiasLevel);
    send_int(a);
}

void __export __stdcall GRDEPTHBUFFERFUNCTION(int a)
{
    debug_printf("GRDEPTHBUFFERFUNCTION@4\n");
    send_char(CMD_GrDepthBufferFunction);
    send_int(a);
}

void __export __stdcall GRDEPTHBUFFERMODE(int a)
{
    debug_printf("GRDEPTHBUFFERMODE@4\n");
    send_char(CMD_GrDepthBufferMode);
    send_int(a);
}

void __export __stdcall GRDEPTHMASK(int a)
{
    debug_printf("GRDEPTHMASK@4\n");
    send_char(CMD_GrDepthMask);
    send_int(a);
}

void __export __stdcall GRDISABLEALLEFFECTS(void)
{
    debug_printf("GRDISABLEALLEFFECTS@0\n");
    send_char(CMD_GrDisableAllEffects);
}

void __export __stdcall GRDITHERMODE(int a)
{
    debug_printf("GRDITHERMODE@4\n");
}

void __export __stdcall GRFOGMODE(int a)
{
    debug_printf("GRFOGMODE@4\n");
    send_char(CMD_GrFogMode);
    send_int(a);
}

void __export __stdcall GRFOGCOLORVALUE(int a)
{
    debug_printf("GRFOGCOLORVALUE@4\n");
    send_char(CMD_GrFogColorValue);
    send_int(a);
}

void __export __stdcall GRFOGTABLE(GrFog_t *ft)
{
    debug_printf("GRFOGTABLE@4\n");
    send_char(CMD_GrFogTable);
    send_buffer((char *)ft, GR_FOG_TABLE_SIZE * sizeof(GrFog_t));
}

void __export __stdcall GRGLIDESHUTDOWN(void)
{
    debug_printf("GRGLIDESHUTDOWN@0\n");

    send_cd_cmds();

    if(pipe->os != pipe_os_dosbox)
        Mouse_uninstall();

    if(running_nt)
        get_off_31();

    send_char(CMD_GrGlideShutdown);
    send_context_switch();
}

void __export __stdcall GRGLIDESETSTATE(int a)
{
    printf("GRGLIDESETSTATE@4\n");
}

void __export __stdcall GRRENDERBUFFER(int a)
{
    debug_printf("GRRENDERBUFFER@4\n");
    send_char(CMD_GrRenderBuffer);
    send_int(a);
    render_buffer = a;
}

void __export __stdcall GRCHECKFORROOM(int a)
{
    debug_printf("GRCHECKFORROOM@4");
}

void __export __stdcall _GRUPDATEPARAMINDEX(void)
{
    printf("_GRUPDATEPARAMINDEX@0\n");
}

void __export __stdcall _GRREBUILDDATALIST(void)
{
    printf("_GRREBUILDDATALIST@0\n");
}

void __export __stdcall GRLFBCONSTANTALPHA(int a)
{
    debug_printf("GRLFBCONSTANTALPHA@4\n");
    send_char(CMD_GrLFBConstantAlpha);
    send_int(a);
}

void __export __stdcall GRLFBCONSTANTDEPTH(int a)
{
    debug_printf("GRLFBCONSTANTDEPTH@4\n");
    send_char(CMD_GrLFBConstantDepth);
    send_int(a);
}

#define BLUE_SCREEN (0x7ff)
#define BLUEBLUE (BLUE_SCREEN | (BLUE_SCREEN << 16))

static unsigned short lfb_read[480][1024];
static unsigned short lfb_write[480][1024];

static void setup_blue_screen(void)
{
    int y, x;
    unsigned int *line;

    line = (unsigned int *) lfb_write;

    for(y = 0; y < 480; y++)
    {
        for(x = 0; x < 320; x++)
            line[x] = BLUEBLUE;

        line += 512;
    }
}

static void send_blue_screen(GrBuffer_t buffer)
{
    int x, y;
    int maxx = 0,    maxy = 0;
    int minx = 1024, miny = 1024;
    unsigned int *line;

    line = (unsigned int *) lfb_write[0];

    for(y = 0; y < 480; y++)
    {
        for(x = 0; x < 320 && line[x] == BLUEBLUE; x++)
            ;

        if(x < 320)
            break;

        line += 512;
    }

    if(y == 480)
        return;

    miny = y;

    minx = x;

    for(x = 320-1; line[x] == BLUEBLUE; x--)
        ;

    maxx = x;

    line = (unsigned int *) lfb_write[480-1];

    for(y = 480-1; y > miny; y--)
    {
        for(x = 0; line[x] == BLUEBLUE && x < 320; x++)
            ;

        if(x < 320)
            break;

        line -= 512;
    }

    maxy = y;

    if(maxy > miny)
    {
        if(x < minx)
            minx = x;

        for(x = 320-1; line[x] == BLUEBLUE; x--)
            ;

        if(x > maxx)
            maxx = x;

        line = (unsigned int *) lfb_write[miny+1];

        for(y = miny+1; y < maxy; y++)
        {
            for(x = 0; line[x] == BLUEBLUE && x < minx; x++)
                ;

            minx = x;

            for(x = 320-1; line[x] == BLUEBLUE && x > maxx; x--)
                ;

            maxx = x;

            line += 512;
        }
    }

    minx = minx*2;
    maxx = maxx*2+1;

    {
        int xsize = maxx + 1 - minx;
        int ysize = maxy + 1 - miny;

        if(xsize > 256 || ysize > 256)
        {
            send_char(CMD_DisplayLFB);
            send_int(buffer);
            for(y = 0; y < 480; y++)
                send_buffer((char *)lfb_write[y], 640*sizeof(short));
        }
        else
        {
            send_char(CMD_DisplayLFBPart);
            send_int(buffer);
            send_int(minx);
            send_int(miny);
            send_int(xsize);
            send_int(ysize);


            for(y = 0; y < ysize; y++)
                send_buffer((char *)&(lfb_write[miny+y][minx]), xsize * sizeof(short));
        }
    }

    if(buffer == GR_BUFFER_FRONTBUFFER)
        synchronise_with_server();
}

static void flush_lfb_writes(void)
{
    if(lfb_write_pending)
    {
        send_blue_screen(locked_buffer);
        buffer_is_locked = 0;
        lfb_write_pending = 0;
    }
}

FxBool __export __stdcall GRLFBLOCK(GrLock_t type, GrBuffer_t buffer, GrLfbWriteMode_t writeMode, GrOriginLocation_t origin, FxBool pixelPipeline, GrLfbInfo_t *info)
{
    debug_printf("GRLFBLOCK@24 type = %d, mode = %d, buffer = %d\n", type, writeMode, buffer);
    info->strideInBytes = 2 * 1024;
    info->writeMode = GR_LFBWRITEMODE_565;
    info->origin = GR_ORIGIN_UPPER_LEFT;

    if((type & 1) == 1)
    {
        info->lfbPtr = lfb_write;

        if(!lfb_write_pending)
        {
            setup_blue_screen();
            buffer_is_locked = 1;
            locked_buffer = buffer;
        }

        lfb_write_pending = 0;
    }
    else
    {
        char c;
        int x, y;

        info->lfbPtr = lfb_read;

        if(!disable_lfb_read)
        {
            if(buffer == GR_BUFFER_AUXBUFFER)
            {
                if(lfb_read_contents != GR_BUFFER_AUXBUFFER)
                {
                    for(y = 0; y < 480; y++)
                        for(x = 0; x < 640; x++)
                            lfb_read[y][x] = 0xB000;

                    lfb_read_contents = GR_BUFFER_AUXBUFFER;
                }
            }
            else
            {
                if(lfb_read_contents != buffer)
                {
                    send_char(CMD_GrLFBLock);
                    send_int(buffer);
                    send_context_switch();

                    for(y = 0; y < 480; y++)
                        pipe->get((char *)lfb_read[y], 640*2);

                    lfb_read_contents = buffer;
                }
            }
        }
    }

    return FXTRUE;
}

FxBool __export __stdcall GRLFBUNLOCK(GrLock_t type, GrBuffer_t buffer)
{
    debug_printf("GRLFBUNLOCK@8 type = %d, buffer = %d\n", type, buffer);

    if((type & 1) == 1)
    {
       /*
        * If we are delaying LFB writes and some triangles
        * have been drawn this frame, then just mark that
        * there are writes yet to be sent.  If no triangles
        * have yet been drawn then the purpose of the writes
        * is probably to create a background so send them now.
        */
        if(delay_lfb_write && frame_started)
        {
            lfb_write_pending = 1;
        }
        else
        {
            send_blue_screen(buffer);
            buffer_is_locked = 0;
        }
    }

    return FXTRUE;
}

void __export __stdcall GRLFBWRITECOLORFORMAT(int a)
{
    printf("GRLFBWRITECOLORFORMAT@4\n");
}

void __export __stdcall GRLFBWRITECOLORSWIZZLE(int a, int b)
{
    printf("GRLFBWRITECOLORSWIZZLE@8\n");
}

void __export __stdcall GRLFBWRITEREGION(int a, int b, int c, int d,
                      int e, int f, int g, int h)
{
    printf("GRLFBWRITEREGION@32\n");
}

void __export __stdcall GRLFBREADREGION(int a, int b, int c, int d,
                     int e, int f, int g)
{
    printf("GRLFBREADREGION@28\n");
}

FxBool __export __stdcall GRSSTWINOPEN(int a, int b, int c, int d,
                  int e, int f, int g)
{
    debug_printf("GRSSTWINOPEN@28\n");
    text_mode(); /* Put the DOS side into text mode
                    so as to see the server window */
    send_char(CMD_GrSstWinOpen);
    send_int(b);
    send_int(c);
    send_int(d);
    send_int(e);
    send_int(f);
    send_int(g);

    return FXTRUE;
}

void __export __stdcall GRSSTWINCLOSE(void)
{
   /*
    * We request a confirmation and wait for it,
    * to give Glidos the chance to put us (remember
    * we are running in a DOS box) in fullscreen
    * mode.
    */
    debug_printf("GRSSTWINCLOSE@0\n");
    send_cd_cmds();
    send_char(CMD_GrSstWinClose);
    synchronise_with_server();
    if(running_nt)
        timesync_fin();
}

void __export __stdcall GRSSTCONTROL(int a)
{
    printf("GRSSTCONTROL@4\n");
}

void __export __stdcall GRSSTPERFSTATS(int a)
{
    printf("GRSSTPERFSTATS@4\n");
}

void __export __stdcall GRSSTRESETPERFSTATS(void)
{
    printf("GRSSTRESETPERFSTATS@0\n");
}

int __export __stdcall GRSSTSTATUS(void)
{
    debug_printf("GRSSTSTATUS@0\n");

    synchronise_with_server();

    return 0x0FFFF43F;
}

void __export __stdcall GRSSTVIDEOLINE(void)
{
    printf("GRSSTVIDEOLINE@0\n");
}

void __export __stdcall GRSSTVRETRACEON(void)
{
    printf("GRSSTVRETRACEON@0\n");
}

void __export __stdcall GRSSTIDLE(void)
{
    debug_printf("GRSSTIDLE@0\n");
}

void __export __stdcall GRSSTISBUSY(void)
{
    printf("GRSSTISBUSY@0\n");
}

void __export __stdcall GRGAMMACORRECTIONVALUE(int a)
{
    debug_printf("GRGAMMACORRECTIONVALUE@4\n");
    send_char(CMD_GrGammaCorrectionValue);
    send_int(a);
}

void __export __stdcall GRSSTORIGIN(int a)
{
    printf("GRSSTORIGIN@4\n");
}

void __export __stdcall GRSSTCONFIGPIPELINE(int a, int b, int c)
{
    printf("GRSSTCONFIGPIPELINE@12\n");
}

void __export __stdcall GRTEXCLAMPMODE(int a, int b, int c)
{
    debug_printf("GRTEXCLAMPMODE@12\n");
    send_char(CMD_GrTexClampMode);
    send_int(a);
    send_int(b);
    send_int(c);
}

void __export __stdcall GRTEXCOMBINE(int a, int b, int c, int d,
                  int e, int f, int g)
{
    debug_printf("GRTEXCOMBINE@28\n");
    send_char(CMD_GrTexCombine);
    send_int(a);
    send_int(b);
    send_int(c);
    send_int(d);
    send_int(e);
    send_int(f);
    send_int(g);
}

void __export __stdcall _GRTEXDETAILCONTROL(int a, int b)
{
    printf("_GRTEXDETAILCONTROL@8\n");
}

void __export __stdcall GRTEXFILTERMODE(int a, int b, int c)
{
    debug_printf("GRTEXFILTERMODE@12\n");
    send_char(CMD_GrTexFilterMode);
    send_int(a);
    send_int(b);
    send_int(c);
}

void __export __stdcall GRTEXLODBIASVALUE(int a, int b)
{
    debug_printf("GRTEXLODBIASVALUE@8\n");
    send_char(CMD_GrTexLodBiasValue);
    send_int(a);
    send_int(b);
}

void __export __stdcall GRTEXMIPMAPMODE(GrChipID_t tmu, GrMipMapMode_t mode, FxBool lodBlend)
{
    debug_printf("GRTEXMIPMAPMODE@12\n");
    send_char(CMD_GrTexMipMapMode);
    send_int(tmu);
    send_int(mode);
    send_int(lodBlend);
}

void __export __stdcall GRTEXNCCTABLE(int a, int b)
{
    printf("GRTEXNCCTABLE@8\n");
}

void __export __stdcall GRTEXSOURCE(GrChipID_t tmu, FxU32 startAddress, FxU32 evenOdd, GrTexInfo *info)
{
    int size, i;
    debug_printf("GRTEXSOURCE@16\n");

    size = 0;

    for(i = info->largeLod; i <= info->smallLod; i++)
        size += texture_size(i, info->aspectRatio, info->format);

    send_char(CMD_GrTexSource);
    send_int(tmu);
    send_int(startAddress);
    send_int(evenOdd);
    send_buffer((char *) info, sizeof(GrTexInfo));
}

void __export __stdcall GRTEXMULTIBASE(int a, int b)
{
    printf("GRTEXMULTIBASE@8\n");
}

void __export __stdcall GRTEXMULTIBASEADDRESS(int a, int b, int c, int d, int e)
{
    printf("GRTEXMULTIBASEADDRESS@20\n");
}

void __export __stdcall _GRTEXDOWNLOADNCCTABLE(int a, int b, int c, int d, int e)
{
    printf("_GRTEXDOWNLOADNCCTABLE@20\n");
}

void __export __stdcall _GRTEXDOWNLOADPALETTE(int a, int b, int c, int d)
{
    printf("_GRTEXDOWNLOADPALETTE@16\n");
}

void __export __stdcall GRTEXDOWNLOADTABLE(GrChipID_t tmu, GrTexTable_t type, void *data)
{
    debug_printf("GRTEXDOWNLOADTABLE@12\n");
    debug_printf("Pal = %d, NCC = %d", sizeof(GuTexPalette), sizeof(GuNccTable));
    send_char(CMD_GrTexDownloadTable);
    send_int(tmu);
    send_int(type);
    switch(type)
    {
    case GR_TEXTABLE_NCC0:
    case GR_TEXTABLE_NCC1:
        send_buffer(data, sizeof(GuNccTable));
        break;

    case GR_TEXTABLE_PALETTE:
        send_buffer(data, sizeof(GuTexPalette));
        break;
    }
}

void __export __stdcall GRTEXDOWNLOADMIPMAPLEVELPARTIAL(int a, int b, int c, int d,
                                     int e, int f, int g, int h,
                                     int i, int j)
{
    printf("GRTEXDOWNLOADMIPMAPLEVELPARTIAL@40\n");
}

static unsigned char *rle_decode(unsigned short int *pal,
                                 unsigned char *src,
                                 unsigned short int *dest)
{
    unsigned int going = 1;
    unsigned int v, count;

    while(going)
    {
        v = *src++;

        if((v & 0xe0) == 0xe0)
        {
            count = (v & 0x1f);
            v = *src++;

            if(count)
            {
                while(count--)
                    *dest++ = pal[v];
            }
            else
            {
                going = 0;
            }
        }
        else
        {
            *dest++ = pal[v];
        }
    }

    return src;
}

void __export __stdcall CONVERTANDDOWNLOADRLE(GrChipID_t        tmu,
                        FxU32             startAddress,
                        GrLOD_t           thisLod,
                        GrLOD_t           largeLod,
                        GrAspectRatio_t   aspectRatio,
                        GrTextureFormat_t format,
                        FxU32             evenOdd,
                        FxU8              *bm_data,
                        long              bm_h,
                        FxU32             u0,
                        FxU32             v0,
                        FxU32             width,
                        FxU32             height,
                        FxU32             dest_width,
                        FxU32             dest_height,
                        FxU16             *tlut)
{
    GrTexInfo info;
    static unsigned short int texbuf[256*256];
    static unsigned short int linebuf[256];
    int offset, i, y;
    unsigned short int *texptr = texbuf;
    debug_printf("CONVERTANDDOWNLOADRLE@64\n");

    offset = 4 + bm_h;
    for(i = 0; i < v0; i++)
        offset += bm_data[4 + i];

    for(y = 0; y < height; y++)
    {
        rle_decode(tlut, &(bm_data[offset]), linebuf);
        memcpy((char *)texptr, (char *)(linebuf + u0), width * 2);
        offset += bm_data[4 + i++];
        texptr += dest_width;
    }

    info.smallLod    = largeLod;
    info.largeLod    = largeLod;
    info.aspectRatio = aspectRatio;
    info.format      = format;
    info.data        = texbuf;

    {
        int size;

        size = 0;

        for(i = info.largeLod; i <= info.smallLod; i++)
            size += texture_size(i, info.aspectRatio, info.format);

        send_char(CMD_ConvertAndDownloadRLE);
        send_int(tmu);
        send_int(startAddress);
        send_int(evenOdd);
        send_buffer((char *) &info, sizeof(GrTexInfo));
        send_int(size);
        send_buffer(info.data, size);
    }
}

void __export __stdcall GUTEXSOURCE(int a)
{
    debug_printf("GUTEXSOURCE@4\n");
    send_char(CMD_GuTexSource);
    send_int(a-1);
}

void __export __stdcall _GRCOMMANDTRANSPORTMAKEROOM(int a, int b, int c)
{
    printf("_GRCOMMANDTRANSPORTMAKEROOM@12\n");
}

void __export __stdcall PIOINBYTE(int a)
{
    printf("PIOINBYTE@4\n");
}

void __export __stdcall PIOINWORD(int a)
{
    printf("PIOINWORD@4\n");
}

void __export __stdcall PIOINLONG(int a)
{
    printf("PIOINLONG@4\n");
}

void __export __stdcall PIOOUTBYTE(int a, int b)
{
    printf("PIOOUTBYTE@8\n");
}

void __export __stdcall PIOOUTWORD(int a, int b)
{
    printf("PIOOUTWORD@8\n");
}

void __export __stdcall PIOOUTLONG(int a, int b)
{
    printf("PIOOUTLONG@8\n");
}

void __export __stdcall PCIPRINTDEVICELIST(void)
{
    printf("PCIPRINTDEVICELIST@0\n");
}

void __export __stdcall _PCIFETCHREGISTER(int a, int b, int c, int d)
{
    printf("_PCIFETCHREGISTER@16\n");
}

void __export __stdcall _PCIUPDATEREGISTER(int a, int b, int c, int d, int e)
{
    printf("_PCIUPDATEREGISTER@20\n");
}

void __export __stdcall PCIGETERRORSTRING(void)
{
    printf("PCIGETERRORSTRING@0\n");
}

void __export __stdcall PCIGETERRORCODE(void)
{
    printf("PCIGETERRORCODE@0\n");
}

void __export __stdcall PCIOPENEX(int a)
{
    printf("PCIOPENEX@4\n");
}

void __export __stdcall PCIOPEN(void)
{
    printf("PCIOPEN@0\n");
}

void __export __stdcall PCICLOSE(void)
{
    printf("PCICLOSE@0\n");
}

void __export __stdcall PCIDEVICEEXISTS(int a)
{
    printf("PCIDEVICEEXISTS@4\n");
}

void __export __stdcall PCIGETCONFIGDATA(int a, int b, int c, int d, int e)
{
    printf("PCIGETCONFIGDATA@20\n");
}

void __export __stdcall PCIGETCONFIGDATARAW(int a, int b, int c, int d, int e)
{
    printf("PCIGETCONFIGDATARAW@20\n");
}

void __export __stdcall PCISETCONFIGDATA(int a, int b, int c, int d, int e)
{
    printf("PCISETCONFIGDATA@20\n");
}

void __export __stdcall PCISETCONFIGDATARAW(int a, int b, int c, int d, int e)
{
    printf("PCISETCONFIGDATARAW@20\n");
}

void __export __stdcall PCIFINDCARDMULTI(int a, int b, int c, int d)
{
    printf("PCIFINDCARDMULTI@16\n");
}

void __export __stdcall PCIFINDCARDMULTIFUNC(int a, int b, int c, int d, int e)
{
    printf("PCIFINDCARDMULTIFUNC@20\n");
}

void __export __stdcall PCIFINDCARD(int a, int b, int c)
{
    printf("PCIFINDCARD@12\n");
}

void __export __stdcall PCIMAPCARDMULTI(int a, int b, int c, int d, int e, int f)
{
    printf("PCIMAPCARDMULTI@24\n");
}

void __export __stdcall PCIMAPCARDMULTIFUNC(int a, int b, int c, int d, int e, int f)
{
    printf("PCIMAPCARDMULTIFUNC@24\n");
}

void __export __stdcall PCIMAPCARD(int a, int b, int c, int d, int e)
{
    printf("PCIMAPCARD@20\n");
}

void __export __stdcall PCIMAPPHYSICALTOLINEAR(int a, int b, int c)
{
    printf("PCIMAPPHYSICALTOLINEAR@12\n");
}

void __export __stdcall PCIMAPPHYSICALDEVICETOLINEAR(int a, int b, int c, int d)
{
    printf("PCIMAPPHYSICALDEVICETOLINEAR@16\n");
}

void __export __stdcall PCIUNMAPPHYSICAL(int a, int b)
{
    printf("PCIUNMAPPHYSICAL@8\n");
}

void __export __stdcall PCISETPASSTHROUGHBASE(int a, int b)
{
    printf("PCISETPASSTHROUGHBASE@8\n");
}

void __export __stdcall PCIOUTPUTDEBUGSTRING(int a)
{
    printf("PCIOUTPUTDEBUGSTRING@4\n");
}

void __export __stdcall PCILINEARRANGESETPERMISSION(int a, int b, int c)
{
    printf("PCILINEARRANGESETPERMISSION@12\n");
}
