// ImageBMP.h: interface for the CImageBMP class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IMAGEBMP_H__E5DB0E92_8F52_428E_9674_BF42187FAC14__INCLUDED_)
#define AFX_IMAGEBMP_H__E5DB0E92_8F52_428E_9674_BF42187FAC14__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Image.h"

class CImageBMP : public CImage  
{
private:
    FILE             *m_f;
    BITMAPFILEHEADER  m_hdr;
    BITMAPINFOHEADER  m_info;
    int               m_yskip;
    int               m_y;
    unsigned char    *m_buf;
    COLORREF          m_palette[256];
    bool              Load(FILE *f);

public:
	virtual COLORREF GetPixel(int x, int y);
	virtual int      GetHeight();
	virtual int      GetWidth();

	virtual         ~CImageBMP();

    static  CImage  *TryLoad(FILE *f, unsigned char *buf, int size);

private:
	CImageBMP();

};

#endif // !defined(AFX_IMAGEBMP_H__E5DB0E92_8F52_428E_9674_BF42187FAC14__INCLUDED_)
