// TexPartition.h: interface for the CTexPartition class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXPARTITION_H__3FA680F9_A0F4_4789_B9A6_239007E2124B__INCLUDED_)
#define AFX_TEXPARTITION_H__3FA680F9_A0F4_4789_B9A6_239007E2124B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "sdk2_glide.h"
#include "Box.h"


class CTexPartition  
{
public:
	virtual bool UsedRectangle(int n, GrVertex *vlist) = 0;
	virtual void Finalise() = 0;
	CTexPartition();
	virtual ~CTexPartition();
};

#endif // !defined(AFX_TEXPARTITION_H__3FA680F9_A0F4_4789_B9A6_239007E2124B__INCLUDED_)
