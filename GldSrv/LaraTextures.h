#include "Box.h"

#define NUM_BAD_TEXTURES (26)

struct BadTexture
{
    unsigned char hash[16];
    int           count;
    const CBox   *areas;
};


static const CBox bbs0[] =
{
    CBox(192, 248, 224, 256)
};

static const CBox bbs1[] =
{
    CBox(160, 200, 184, 208)
};

static const CBox bbs2[] =
{
    CBox(56, 112, 128, 160)
};

static const CBox bbs3[] =
{
    CBox(160, 216, 200, 232)
};

static const CBox bbs4[] =
{
    CBox(96, 136, 144, 168)
};

static const CBox bbs5[] =
{
    CBox(96, 136, 104, 128)
};

static const CBox bbs6[] =
{
    CBox(128, 168, 232, 256),
    CBox(192, 232, 96, 128)
};

static const CBox bbs7[] =
{
    CBox(192, 248, 0, 32),
    CBox(0, 40, 216, 240)
};

static const CBox bbs8[] =
{
    CBox(136, 192, 56, 88)
};

static const CBox bbs9[] =
{
    CBox(104, 160, 88, 120)
};

static const CBox bbs10[] =
{
    CBox(120, 176, 216, 248)
};

static const CBox bbs11[] =
{
    CBox(120, 160, 8, 32)
};

static const CBox bbs12[] =
{
    CBox(24, 64, 56, 88)
};

static const CBox bbs13[] =
{
    CBox(64, 120, 184, 216)
};

static const CBox bbs14[] =
{
    CBox(16, 56, 72, 96)
};

static const CBox bbs15[] =
{
    CBox(160, 216, 96, 128),
    CBox(200, 240, 232, 256)
};

static const CBox bbs16[] =
{
    CBox(0, 40, 112, 136)
};

static const CBox bbs17[] =
{
    CBox(184, 240, 72, 104)
};

static const CBox bbs18[] =
{
    CBox(0, 56, 224, 256)
};

static const CBox bbs19[] =
{
    CBox(184, 240, 0, 32),
    CBox(144, 184, 216, 240)
};

static const CBox bbs20[] =
{
    CBox(160, 200, 232, 256)
};

static const CBox bbs21[] =
{
    CBox(184, 240, 200, 232)
};

static const CBox bbs22[] =
{
    CBox(192, 248, 208, 240)
};

static const CBox bbs23[] =
{
    CBox(88, 144, 96, 128)
};

static const CBox bbs24[] =
{
    CBox(128, 168, 64, 88)
};

static const CBox bbs25[] =
{
    CBox(64, 88, 232, 248),
    CBox(88, 128, 248, 256)
};


static const BadTexture bad_textures[NUM_BAD_TEXTURES] =
{
    {{0x04, 0x6A, 0x80, 0x22, 0x38, 0x55, 0x2F, 0x75, 0x92, 0x54, 0x5C, 0x5E, 0x51, 0x54, 0xF9, 0x54}, 1, bbs0},
    {{0x0C, 0x4F, 0x02, 0x76, 0x9A, 0xD9, 0x2D, 0x3F, 0x8E, 0xF3, 0x65, 0x32, 0x83, 0xC8, 0xDE, 0xB0}, 1, bbs1},
    {{0x10, 0x3F, 0x3C, 0xAA, 0x13, 0xF0, 0x90, 0x83, 0xB4, 0x1C, 0x10, 0xCF, 0x4A, 0x9E, 0xDF, 0x56}, 1, bbs2},
    {{0x2E, 0x95, 0x8F, 0x63, 0x43, 0x7A, 0x70, 0xFE, 0xF0, 0x69, 0x24, 0x5B, 0x6E, 0x06, 0x4D, 0xF7}, 1, bbs3},
    {{0x32, 0x03, 0xC0, 0x93, 0xF8, 0x0E, 0xB4, 0x91, 0x43, 0x1C, 0xD8, 0x5B, 0xF1, 0x57, 0xA2, 0x22}, 1, bbs4},
    {{0x3D, 0xF4, 0x24, 0xEC, 0x97, 0x67, 0x47, 0x48, 0xBB, 0x5C, 0x65, 0x15, 0x7B, 0x73, 0x04, 0x2F}, 1, bbs5},
    {{0x53, 0x33, 0x06, 0x99, 0x05, 0x96, 0x47, 0xCB, 0x80, 0x82, 0xBE, 0x02, 0x18, 0x5C, 0x0F, 0x46}, 2, bbs6},
    {{0x54, 0x63, 0x7C, 0x4B, 0xF0, 0x30, 0x44, 0x39, 0xB6, 0x3F, 0x51, 0xD3, 0xA5, 0xEE, 0xA3, 0x82}, 2, bbs7},
    {{0x74, 0x21, 0xE2, 0x79, 0xB6, 0x2C, 0x5F, 0x70, 0xDF, 0x2A, 0xCC, 0xDE, 0x4D, 0x20, 0x4D, 0x64}, 1, bbs8},
    {{0x82, 0x8A, 0x9C, 0x3E, 0xBE, 0x11, 0x91, 0x5D, 0xDB, 0xA9, 0x53, 0x66, 0x66, 0x94, 0x8D, 0x79}, 1, bbs9},
    {{0x83, 0x00, 0x65, 0x73, 0x13, 0xCC, 0xCC, 0x44, 0x8C, 0xE1, 0x56, 0x76, 0xF4, 0xD0, 0x19, 0x54}, 2, bbs25},
    {{0x83, 0xBF, 0xA6, 0x5D, 0x61, 0xDE, 0xAB, 0xF4, 0x30, 0x6D, 0x98, 0x33, 0x75, 0x8B, 0xEE, 0x8F}, 1, bbs10},
    {{0x8F, 0x28, 0x0D, 0xCA, 0xA8, 0x01, 0x20, 0xB7, 0x6E, 0x49, 0xB3, 0xBD, 0x10, 0x6C, 0x8A, 0x0B}, 1, bbs11},
    {{0xAE, 0x56, 0x55, 0xF7, 0x67, 0xB5, 0x7D, 0xE3, 0x98, 0x84, 0x5C, 0x2F, 0xDB, 0x75, 0xAF, 0x45}, 1, bbs12},
    {{0xB3, 0x61, 0xFE, 0xFD, 0x00, 0x0A, 0x10, 0x42, 0xAA, 0xC7, 0xF8, 0x85, 0x62, 0x12, 0x6A, 0xCF}, 1, bbs13},
    {{0xC0, 0x05, 0x02, 0xCA, 0xB9, 0x3A, 0x87, 0x2C, 0xC2, 0x94, 0x54, 0xC8, 0xF7, 0xB0, 0xF2, 0x7C}, 1, bbs14},
    {{0xCB, 0xAA, 0x51, 0x9F, 0xAE, 0x4F, 0xDB, 0xDF, 0xD1, 0xF2, 0xA0, 0x18, 0xBF, 0x43, 0xC6, 0xFD}, 2, bbs15},
    {{0xD2, 0xE5, 0x67, 0xFD, 0x51, 0x80, 0xBF, 0xC3, 0xA2, 0x8E, 0x9D, 0x89, 0x0E, 0x21, 0x03, 0x01}, 1, bbs16},
    {{0xD4, 0x71, 0x82, 0xEC, 0x46, 0x78, 0x85, 0xCB, 0x47, 0x04, 0xAB, 0x14, 0x34, 0x68, 0x02, 0x22}, 1, bbs17},
    {{0xD5, 0x67, 0xBD, 0xDB, 0x40, 0x27, 0xA1, 0x93, 0x2F, 0x2B, 0x1D, 0xE2, 0x09, 0x12, 0x90, 0x38}, 1, bbs18},
    {{0xD6, 0xAC, 0xAF, 0x2A, 0xDF, 0xF2, 0xF9, 0x85, 0x68, 0xBA, 0x5F, 0x1B, 0x8F, 0x96, 0xAF, 0xDA}, 2, bbs19},
    {{0xD9, 0x2D, 0x57, 0x05, 0x31, 0xD9, 0x02, 0xE9, 0x44, 0xA0, 0xD4, 0x60, 0x8C, 0xEA, 0xF2, 0xA1}, 1, bbs20},
    {{0xEA, 0x00, 0x36, 0x03, 0x1E, 0x1D, 0x43, 0x9C, 0x3E, 0xE6, 0x60, 0x2F, 0xE9, 0xAD, 0xDC, 0x3C}, 1, bbs21},
    {{0xF4, 0x3F, 0xCE, 0xDA, 0x15, 0xDE, 0x4E, 0x28, 0x53, 0xB0, 0x1E, 0xAE, 0x39, 0xBE, 0x57, 0xFF}, 1, bbs22},
    {{0xF7, 0x59, 0x0E, 0x1E, 0x7A, 0x1B, 0x34, 0x2A, 0x02, 0x26, 0xD0, 0xC4, 0x97, 0x58, 0xB5, 0x89}, 1, bbs23},
    {{0xF7, 0xDB, 0x5E, 0x98, 0x8F, 0xF2, 0xAC, 0x26, 0x1D, 0xF6, 0xFA, 0xE9, 0x1E, 0x66, 0x9C, 0xF4}, 1, bbs24}
};

