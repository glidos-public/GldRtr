#ifndef _XMS_H_
#define _XMS_H_

typedef void __far Xms;

int Xms_present(void);

Xms *Xms_open(void);

int Xms_version(Xms *xms);

unsigned int Xms_maxblock(Xms *xms);

int Xms_allocate(Xms *xms, int size, int __far *block);

void Xms_read(Xms              *xms,
              unsigned int      block,
              long unsigned int block_offset,
              void __far       *buf,
              long unsigned int length);

void Xms_write(Xms              *xms,
               unsigned int      block,
               long unsigned int block_offset,
               void __far       *buf,
               long unsigned int length);

#endif
