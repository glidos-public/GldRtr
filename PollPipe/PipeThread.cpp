// PipeThread.cpp: implementation of the CPipeThread class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <stdio.h>
#include "PipeThread.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPipeThread::CPipeThread()
{
    m_cmd_issued = NULL;
    m_cmd_complete = NULL;
    m_open = false;
}

CPipeThread::~CPipeThread()
{
}

void CPipeThread::Execute()
{
    HANDLE pipe;
    DWORD delivered;
    BOOL  success;

    SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);

    pipe = CreateFile("\\\\.\\PIPE\\GLDPIPE1",
                        GENERIC_READ|GENERIC_WRITE,
                        0,     /* Don't share */
                        NULL,  /* No Security */
                        OPEN_EXISTING,
                        FILE_ATTRIBUTE_NORMAL,
                        NULL);

    m_open = (pipe != INVALID_HANDLE_VALUE);

    if(!m_open)
    {
        MessageBox(NULL, "Server not present", "Glidos", MB_OK);

        m_cmd = CMD_CLOSE;
    }
    
    SetEvent(m_cmd_complete);

    while(m_cmd != CMD_CLOSE)
    {
        WaitForSingleObject(m_cmd_issued, INFINITE);

        ResetEvent(m_cmd_issued);

        switch(m_cmd)
        {
        case CMD_CLOSE:
           /*
            * Nothing to do. We were awoken to allow this
            * thread to complete the loop and shut down.
            */
            break;

        case CMD_READ:
            success = TRUE;
            delivered = 1;

            while(delivered && success && m_count > 0)
            {
                success = ReadFile(pipe, m_buf, m_count, &delivered, NULL);
                if(success)
                {
                    m_count -= delivered;
                    m_buf += delivered;
                }
            }

            if(!success || !delivered)
            {
                MessageBox(NULL, "Read failure", "Glidos", MB_OK);
                Sleep(1000);
                TerminateProcess(GetCurrentProcess(), 0);
            }
            break;

        case CMD_WRITE:
            success = TRUE;
            delivered = 1;

            while(delivered && success && m_count > 0)
            {
                success = WriteFile(pipe, m_buf, m_count, &delivered, NULL);
                if(success)
                {
                    m_count -= delivered;
                    m_buf += delivered;
                }
            }

            if(!success || !delivered)
            {
                MessageBox(NULL, "Write failure", "Glidos", MB_OK);
                Sleep(1000);
                TerminateProcess(GetCurrentProcess(), 0);
            }
            break;
        }

        SetEvent(m_cmd_complete);
    }

    CloseHandle(pipe);

    m_open = false;
}

void CPipeThread::Open()
{
    if(m_open)
        return;

    timeBeginPeriod(1);
    m_cmd_issued = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_cmd_complete = CreateEvent(NULL, TRUE, FALSE, NULL);

    m_cmd = CMD_NONE;

    Start();

    WaitForSingleObject(m_cmd_complete, INFINITE);

    ResetEvent(m_cmd_complete);

    SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_LOWEST);
}

void CPipeThread::Close()
{
    m_cmd = CMD_CLOSE;

    SetEvent(m_cmd_issued);

    Wait();

    CloseHandle(m_cmd_issued);
    CloseHandle(m_cmd_complete);

    m_cmd_issued = NULL;
    m_cmd_complete = NULL;
    timeEndPeriod(1);
}

void CPipeThread::Read(void *buf, int count)
{
    m_buf = (char *)buf;
    m_count = count;
    m_cmd = CMD_READ;

    SetEvent(m_cmd_issued);
}

void CPipeThread::Write(void *buf, int count)
{
    m_buf = (char *)buf;
    m_count = count;
    m_cmd = CMD_WRITE;

    SetEvent(m_cmd_issued);
}

bool CPipeThread::Poll()
{
    if(!m_open)
        return true;

    if(WaitForSingleObject(m_cmd_complete, 1) == WAIT_OBJECT_0)
    {
        ResetEvent(m_cmd_complete);

        return true;
    }
    else
    {
        return false;
    }
}
