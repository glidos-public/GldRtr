// TexSet.h: interface for the CTexSet class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(TEXSET_H)
#define TEXSET_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BmpWriter.h"
#include "TexElementFactory.h"
#include "TexElement.h"
#include "MappingFile.h"

template <class TElement, class TFactory>
class CTexSet  
{
private:
	void Write();
    class CLink
    {
    public:
        TElement    *m_element;
        bool         m_marked;
        CLink       *m_next;

        CLink(TElement *element)
        {
            m_element = element;
            m_marked  = false;
            m_next    = NULL;
        };

        virtual ~CLink()
        {
            delete m_element;
        };
    };

    CLink  *m_list;
    CLink  *m_here;
    CString m_folder;
    TFactory *m_factory;
    TElement *m_any;

public:
	void Add(CBox *b, CBmpWriter *bmp);
	void DeleteMarked(bool delete_file);
	void Mark();
    TElement *Any();
	TElement *Here();
	void Next();
	bool Going();
	void Start();
	void Initialise(CString &folder, TexElementInfoList *list);
	CTexSet(TFactory *factory);
	virtual ~CTexSet();

};

#endif // !defined(TEXSET_H)
#include "texset.cpp"
