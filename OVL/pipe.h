/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * $Id$
 *
 ************************************************************/

#ifndef _PIPE_H_
#define _PIPE_H_

typedef enum
{
    pipe_os_w98,
    pipe_os_nt,
    pipe_os_dosbox
} pipe_os_t;

typedef struct
{
    pipe_os_t os;
    void (*send)(char *buf, int n);
    void (*flush)(void);
	int	 (*readChar)(void);
    void (*get)(char *buf, int n);
} pipe_t;

pipe_t *pipe_open(void);

#endif
