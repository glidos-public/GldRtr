// PropertyPageCheck.cpp : implementation file
//

#include "stdafx.h"

#include "MappingFileCheck.h"
#include "genequivmap.h"
#include "PropertyPageCheck.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

typedef std::map<CBox, NameSet> BoxToNames;

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageCheck property page

IMPLEMENT_DYNCREATE(CPropertyPageCheck, CPropertyPageBase)

CPropertyPageCheck::CPropertyPageCheck() : CPropertyPageBase(CPropertyPageCheck::IDD)
{
	//{{AFX_DATA_INIT(CPropertyPageCheck)
	m_folder = _T("");
	//}}AFX_DATA_INIT
}

CPropertyPageCheck::~CPropertyPageCheck()
{
}

void CPropertyPageCheck::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropertyPageCheck)
	DDX_Text(pDX, IDC_EDIT_FOLDER, m_folder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropertyPageCheck, CPropertyPageBase)
	//{{AFX_MSG_MAP(CPropertyPageCheck)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_FOLDER, OnButtonBrowseFolder)
	ON_BN_CLICKED(IDC_BUTTON_CHECK, OnButtonCheck)
	//}}AFX_MSG_MAP
    ON_UPDATE_COMMAND_UI(IDC_BUTTON_CHECK, OnUpdateCheckButton)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageCheck message handlers

void CPropertyPageCheck::OnButtonBrowseFolder() 
{
    if(GetFolder(m_folder))
        UpdateData(FALSE);
}

void CPropertyPageCheck::OnButtonCheck() 
{
    CMappingFileCheck   map;
    CStringArray       *names;
    CString             ofile;
    FILE               *f;
    int                 i;
    bool                repeat_found = false;

    ofile = GetOutputFileName("repeats.log");
    if(ofile.IsEmpty())
        return;

    f = fopen((LPCSTR)ofile, "w");
    if(f == NULL)
    {
        AfxMessageBox("Could not open file for output", MB_OK);
        return;
    }

    fprintf(f, "Repeat analysis for %s\n", (LPCSTR)m_folder);

    map.Initialise(m_folder);
    names = map.GetNameSet();

    for(i = 0; i < names->GetSize(); i++)
    {
        TexElementInfoList            *list;
        TexElementInfoList::iterator   liter;
        BoxToNames                     b2n;
        BoxToNames::iterator           biter;

        list = map.GetElementInfoList((*names)[i]);
        for(liter = list->begin(); liter != list->end(); liter++)
            b2n[liter->box].push_back((LPCSTR)liter->path);

        for(biter = b2n.begin(); biter != b2n.end(); biter++)
        {
            NameSet &plist = biter->second;

            if(plist.size() > 1)
            {
                const CBox        &box = biter->first;
                NameSet::iterator  pliter;

                repeat_found = true;

                fprintf(f, "\n");
                fprintf(f, "%s\\(%d--%d)(%d--%d)\n", (*names)[i],
                           box.m_xmin, box.m_xmax, box.m_ymin, box.m_ymax);

                for(pliter = plist.begin(); pliter != plist.end(); pliter++)
                    fprintf(f, "    -> %s\n", pliter->c_str());
            }
        }
    }

    delete names;

    if(!repeat_found)
        fprintf(f, "\nTexture pack has no repeats\n");

    fclose(f);

    AfxMessageBox("Log file written");
}

void CPropertyPageCheck::OnUpdateCheckButton(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(!m_folder.IsEmpty());
}
