// GameSettingDouble.cpp: implementation of the CGameSettingDouble class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "GameSettingDouble.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


void CGameSettingDouble::Init(CStoredStruct *st, const char *name, double init)
{
	CGameSetting<CDouble>::Init(st, name);
	
	m_value = init;
};

CGameSettingDouble &CGameSettingDouble::operator=(const double &i)
{
	if(i != m_value)
		m_defined = true;
	
	m_value   = i;
	
	return *this;
};

bool CGameSettingDouble::Parse(CString &val)
{
	float a;
	
	if(sscanf((LPCSTR)val, "%f", &a) == 1)
	{
		m_value = a;
		
		return true;
	}
	
	return false;
};

CString CGameSettingDouble::Print()
{
	CString res;
	
	res.Format("%.2f", m_value);
	
	return res;
};
