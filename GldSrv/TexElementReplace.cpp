// TexElementReplace.cpp: implementation of the CTexElementReplace class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "TexElementReplace.h"
#include "image.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexElementReplace::CTexElementReplace(CTexHandlerReplace *tex_replace)
{
    m_tex_replace = tex_replace;
    m_wrapper = tex_replace->GetGlideState()->GetWrapper();
}

CTexElementReplace::CTexElementReplace(CBox &b, CString &name, CTexHandlerReplace *tex_replace) : CTexElementAct(b, name)
{
    m_tex_replace = tex_replace;
    m_wrapper = tex_replace->GetGlideState()->GetWrapper();
}

CTexElementReplace::~CTexElementReplace()
{

}

bool CTexElementReplace::Load(GrLOD_t old_lod)
{
    unsigned int     *data16 = NULL;
    unsigned int     *line_ptr;
    int               x, y;
    int               t_width;
    int               t_height;
    GrLOD_t           new_lod;
    GrAspectRatio_t   aspect;
    CImage           *image = NULL;
    bool              has_alpha;
    
    image = CImage::Load(m_name);
    
    if(image == NULL)
        goto failure;
    
    m_width  = image->GetWidth();
    m_height = image->GetHeight();

    if(m_width > 256 || m_height > 256)
        goto failure;
    
    m_has_transparency = false;
    has_alpha = image->HasAlpha();
    if(has_alpha)
    {
        /* Find out how alpha is used */
        bool has_black = false;
        bool has_white = false;
        bool has_grey  = false;

        for(y = 0; y < m_height; y++)
        {
            for(x = 0; x < m_width; x++)
            {
                switch(image->GetPixel(x, y) >> 24)
                {
                case 0:
                    has_black = true;
                    break;

                case 255:
                    has_white = true;
                    break;

                default:
                    has_grey = true;
                    break;
                }
            }
        }

        if(has_grey)
            m_has_transparency = true;
        else if(!has_black || !has_white)
            has_alpha = false;
    }
    
    t_width  = PowOfTwo(m_width);
    t_height = PowOfTwo(m_height);
    
    if(t_width / 8 > t_height)
        t_height = t_width / 8;
    
    if(t_height / 8 > t_width)
        t_width = t_height / 8;
    
    CalcLodAspect(t_width, t_height, &new_lod, &aspect);
    
    data16 = new unsigned int[t_width * t_height];
    if(data16 == NULL)
        goto failure;

    line_ptr = data16 + t_width * m_height;

    for(y = 0; y < m_height; y++)
    {
        unsigned short lastval;

        line_ptr -= t_width;

        for(x = 0; x < m_width; x++)
        {
            line_ptr[x] = image->GetPixel(x, y) | (has_alpha ? 0 : 0xff000000);
        }

        lastval = line_ptr[m_width-1];

        for(x = m_width; x < t_width; x++)
            line_ptr[x] = lastval;
    }

    while(line_ptr > data16)
    {
        memcpy(line_ptr - t_width, line_ptr, t_width * sizeof(*line_ptr));
        line_ptr -= t_width;
    }
    
    m_id = m_wrapper->guTexAllocateMemory(0,
        GR_MIPMAPLEVELMASK_BOTH,
        t_width,
        t_height,
        GR_TEXFMT_BGRA_8888,
        GR_MIPMAP_NEAREST,
        new_lod,
        new_lod,
        aspect,
        GR_TEXTURECLAMP_CLAMP,
        GR_TEXTURECLAMP_CLAMP,
        GR_TEXTUREFILTER_BILINEAR,
        GR_TEXTUREFILTER_BILINEAR,
        0.0,
        FXFALSE);
    
    m_wrapper->guTexDownloadMipMap(m_id, (void *)data16, NULL);
    
    m_s_scale = (float)m_width / (float)(m_xmax - m_xmin) * (float)(1<<new_lod) / (float)(1<<old_lod);
    m_t_scale = (float)m_height / (float)(m_ymax - m_ymin) * (float)(1<<new_lod) / (float)(1<<old_lod);
    
    delete [] data16;
    delete    image;
    
    return true;

failure:
    delete [] data16;
    delete    image;
    
    return false;
}

void CTexElementReplace::Override(int n, GrVertex *vlist)
{
    int i;
    
    for(i = 0; i < n; i++)
    {
        /* Reposition s and t within the new texture */
        vlist[i].tmuvtx[0].sow -= (float)m_xmin * vlist[i].oow;
        vlist[i].tmuvtx[0].tow -= (float)m_ymin * vlist[i].oow;
        
        vlist[i].tmuvtx[0].sow *= m_s_scale;
        vlist[i].tmuvtx[0].tow *= m_t_scale;
    }
    
    if(m_has_transparency)
    {
        m_tex_replace->GetStore()->Store(n, vlist, m_id);
    }
    else
    {
        m_wrapper->guTexSource(m_id);
        m_tex_replace->GetGlideState()->MarkTexSourceDirty();
        m_wrapper->grDrawPolygonVertexList(n, vlist);
    }
    
}

void CTexElementReplace::Reset()
{
    m_tex_replace->GetGlideState()->RestoreTexSourceIfDirty();
}

int CTexElementReplace::PowOfTwo(int s)
{
    if(s > 128) return 256;
    if(s > 64)  return 128;
    if(s > 32)  return 64;
    if(s > 16)  return 32;
    if(s > 8)   return 16;
    if(s > 4)   return 8;
    if(s > 2)   return 4;
    if(s > 1)   return 2;
    return 1;
}

void CTexElementReplace::CalcLodAspect(int w, int h, GrLOD_t *lod, GrAspectRatio_t *aspect)
{
    int v = (w > h ? w : h);

    switch(v)
    {
    case 256: *lod = GR_LOD_256; break;
    case 128: *lod = GR_LOD_128; break;
    case  64: *lod = GR_LOD_64; break;
    case  32: *lod = GR_LOD_32; break;
    case  16: *lod = GR_LOD_16; break;
    case   8: *lod = GR_LOD_8; break;
    case   4: *lod = GR_LOD_4; break;
    case   2: *lod = GR_LOD_2; break;
    case   1: *lod = GR_LOD_1; break;
    }

    if(w > h)
    {
        switch(w / h)
        {
        case 8: *aspect = GR_ASPECT_8x1; break;
        case 4: *aspect = GR_ASPECT_4x1; break;
        case 2: *aspect = GR_ASPECT_2x1; break;
        case 1: *aspect = GR_ASPECT_1x1; break;
        }
    }
    else
    {
        switch(h / w)
        {
        case 8: *aspect = GR_ASPECT_1x8; break;
        case 4: *aspect = GR_ASPECT_1x4; break;
        case 2: *aspect = GR_ASPECT_1x2; break;
        case 1: *aspect = GR_ASPECT_1x1; break;
        }
    }
}
