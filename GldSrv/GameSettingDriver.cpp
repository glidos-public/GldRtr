// GameSettingDriver.cpp: implementation of the CGameSettingDriver class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "gldsrv.h"
#include "GameSettingDriver.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

void CGameSettingDriver::Init(CStoredStruct *st, const char *name)
{
	CGameSetting<CGlideDriver>::Init(st, name);
	
	m_value = GLIDE_DRIVER_OGL;
};

CGameSettingDriver &CGameSettingDriver::operator=(const GlideDriver &i)
{
	if(i != m_value)
		m_defined = true;
	
	m_value   = i;
	
	return *this;
};

bool CGameSettingDriver::Parse(CString &val)
{
	if(val[0] == 'O' || val[0] == 'o')
	{
		m_value = GLIDE_DRIVER_OGL;
		
		return true;
	}
	
	if(val[0] == 'D' || val[0] == 'd')
	{
		m_value = GLIDE_DRIVER_D3D;
		
		return true;
	}

    return false;
};

CString CGameSettingDriver::Print()
{
	switch(m_value)
	{
	default:
	case GLIDE_DRIVER_OGL:
		return "OpenGL";
		
	case GLIDE_DRIVER_D3D:
		return "Direct3D";
	}
};

CString CGameSettingDriver::Path()
{
	switch(m_value)
	{
	default:
	case GLIDE_DRIVER_OGL:
		return "OpenGlide\\Glide2x.dll";
		
	case GLIDE_DRIVER_D3D:
		return "psVoodoo\\Glide2x.dll";
	}
}
