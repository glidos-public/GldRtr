/************************************************************
 * Mouse driver for Glidos
 *
 * Copyright (c) Paul Gardiner.
 ************************************************************/

#include <dos.h>

#include "mouse.h"

enum
{
    ButtonPress_None,
    ButtonPress_LDown,
    ButtonPress_LUp,
    ButtonPress_RDown,
    ButtonPress_RUp
};

enum
{
    ButtonState_Left  = 1,
    ButtonState_Right = 2
};

enum
{
    Mouse_Moved           = 0x01,
    Mouse_LButtonPressed  = 0x02,
    Mouse_LButtonReleased = 0x04,
    Mouse_RButtonPressed  = 0x08,
    Mouse_RButtonReleased = 0x10
};

static int installed = 0;

static int button_state = 0;

static int xmin   = 0;
static int ymin   = 0;
static int xmax   = 640;
static int ymax   = 200;
static int xpos   = 640/2;
static int ypos   = 200/2;
static int xrate  = 8;
static int yrate  = 16;

static int xmove = 0;
static int ymove = 0;

static lcount = 0;
static lxpos  = 0;
static lypos  = 0;

static rcount = 0;
static rxpos  = 0;
static rypos  = 0;

static void __far *mouse_callback = 0;

static int mouse_mask   = 0;

extern void call_mouse(void __far *f, int flags, int bstate, int xpos, int ypos);
#pragma aux call_mouse =           \
    "push bp"                      \
    "push di"                      \
    "push si"                      \
    "mov bp, sp"                   \
    "mov si, cx"                    \
    "mov di, dx"                    \
    "callf [bp]"                   \
    "pop si"                       \
    "pop di"                       \
    "pop bp"                       \
    parm [di si] [ax] [bx] [cx] [dx]\
    modify [ax bx cx dx si di];

static mouse_button_simulate(long int button_info)
{
    char *presses = (char *)&button_info;
    int   i;

    for(i = 0; i < 4; i++)
    {
        switch(presses[i])
        {
            case ButtonPress_LDown:
                button_state |= ButtonState_Left;
                lxpos = xpos;
                lypos = ypos;
                lcount += 1;
                if(mouse_callback && (mouse_mask & Mouse_LButtonPressed))
                    call_mouse(mouse_callback, Mouse_LButtonPressed, button_state, xpos, ypos);
                break;

            case ButtonPress_LUp:
                button_state &= ~ButtonState_Left;
                if(mouse_callback && (mouse_mask & Mouse_LButtonReleased))
                    call_mouse(mouse_callback, Mouse_LButtonReleased, button_state, xpos, ypos);
                break;

            case ButtonPress_RDown:
                button_state |= ButtonState_Right;
                rxpos = xpos;
                rypos = ypos;
                rcount += 1;
                if(mouse_callback && (mouse_mask & Mouse_RButtonPressed))
                    call_mouse(mouse_callback, Mouse_RButtonPressed, button_state, xpos, ypos);
                break;

            case ButtonPress_RUp:
                button_state &= ~ButtonState_Right;
                if(mouse_callback && (mouse_mask & Mouse_RButtonReleased))
                    call_mouse(mouse_callback, Mouse_RButtonReleased, button_state, xpos, ypos);
                break;
        }
    }
}

static void (__interrupt __far *old33)();

static void __interrupt __far new33(union INTPACK r)
{
    switch(r.w.ax)
    {
    case 0: /* Reset driver and read status */
        r.w.ax = 0xffff;
        r.w.bx = 2;
        break;

    case 0x3: /* Return position and button status */
        r.w.bx = button_state;
        r.w.cx = xpos;
        r.w.dx = ypos;
        break;

    case 0x4: /* Position mouse cursor */
        xpos = r.w.cx;
        ypos = r.w.dx;
        break;

    case 0x5: /* Return button press data */
        if(r.w.bx == 1)
        {
            r.w.ax = button_state;
            r.w.bx = rcount;
            r.w.cx = rxpos;
            r.w.dx = rypos;
            rcount = 0;
        }
        else
        {
            r.w.ax = button_state;
            r.w.bx = lcount;
            r.w.cx = lxpos;
            r.w.dx = lypos;
            lcount = 0;
        }
        break;

    case 0x7:
        xmin = r.w.cx;
        xmax = r.w.dx;
        break;

    case 0x8:
        ymin = r.w.cx;
        ymax = r.w.dx;
        break;

    case 0xb: /* Read motion counters */
        r.w.cx = xmove/2;
        r.w.dx = ymove/2;

        xmove -= r.w.cx;
        ymove -= r.w.dx;
        break;

    case 0xc: /* Define interrupt subroutine parameters */
        mouse_mask = r.w.cx;
        mouse_callback = (r.w.es:>r.w.dx);
        break;

    case 0xf:
        xrate = r.w.cx;
        yrate = r.w.dx;
        break;

    case 0x15: /* Return driver storage requirements */
        r.w.bx = 0x10;
        break;

    default:
        _chain_intr(old33);
        break;
    }
}

Mouse_init(void)
{
    if(!installed)
    {
        installed = 1;

        old33 = _dos_getvect(0x33);
        _dos_setvect(0x33, new33);
    }
}

Mouse_newState(long int xmickeys, long ymickeys, long buttons)
{
    xmove += xmickeys;
    ymove += ymickeys;
    xpos += xmickeys * 8 / xrate;
    ypos += ymickeys * 8 / yrate;
    if(xpos < xmin) xpos = xmin;
    if(xpos >= xmax) xpos = xmax - 1;
    if(ypos < ymin) ypos = ymin;
    if(ypos >= ymax) ypos = ymax - 1;

    mouse_button_simulate(buttons);
}

