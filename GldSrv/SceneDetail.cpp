// SceneDetail.cpp: implementation of the CSceneDetail class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "SceneDetail.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSceneDetail::CSceneDetail(CSceneLocation *location, int index)
{
    m_location = location;
    m_index    = index;
    if(index == 0)
        index = index;
}

CSceneDetail::~CSceneDetail()
{
    INT_PTR count = m_conditions.GetSize();

    for(INT_PTR i = 0; i < count; i++)
        delete m_conditions[i];
}

void CSceneDetail::AddCondition(const CString &expression)
{
    m_conditions.Add(new CPolyCondition(expression));
}

void CSceneDetail::Trigger(const CPolyCondition::CPoly &poly)
{
    bool                  passes;
    INT_PTR               count, i;
    
    passes = true;
    count  = m_conditions.GetSize();
    
    for(i = 0; i < count; i++)
        if(!m_conditions[i]->Test(poly))
            passes = false;
        else
            passes = passes;
        
    if(passes)
    {
        if(m_index == 0)
            passes = passes;
        m_location->Trigger(m_index);
    }
}
