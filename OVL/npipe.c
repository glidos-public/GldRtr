/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * $Id$
 *
 ************************************************************/

#include "npipe.h"

int handle;

extern void open_file(char *name);

#pragma aux open_file =          \
    "mov eax, 3d02h"             \
    "mov ecx, 0h"                \
    "int 21h"                    \
    "mov handle, eax"            \
    parm [edx]                   \
    modify [eax ecx];


extern void writeN(char *s, int n);

#pragma aux writeN =         \
    "mov eax, 4000h"           \
    "mov ebx, handle"          \
    "int 21h"                  \
    parm [edx] [ecx]           \
    modify [eax ebx];



extern void readN(char *s, int n);

#pragma aux readN =         \
    "mov eax, 3f00h"           \
    "mov ebx, handle"          \
    "int 21h"                  \
    parm [edx] [ecx]           \
    modify [eax ebx];



static void send(char *buf, int n)
{
	writeN(buf, n);
}

static void get(char *buf, int n)
{
	readN(buf, n);
}

static void flush(void)
{
    writeN(0,0);
}

static int readChar(void)
{
	unsigned char c;

	readN(&c, 1);
	return c;
}

static pipe_t pipe = {pipe_os_dosbox, send, flush, readChar, get};


pipe_t *NPipeOpen(void)
{
    open_file("\\\\.\\PIPE\\GLDPIPE1");

    return &pipe;
}
