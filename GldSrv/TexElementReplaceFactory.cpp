// TexElementReplaceFactory.cpp: implementation of the CTexElementReplaceFactory class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "TexElementReplace.h"
#include "TexElementReplaceFactory.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexElementReplaceFactory::CTexElementReplaceFactory(CTexHandlerReplace *tex_replace)
{
    m_tex_replace = tex_replace;
}

CTexElementReplaceFactory::~CTexElementReplaceFactory()
{

}

CTexElementAct *CTexElementReplaceFactory::MakeTexElement()
{
    return new CTexElementReplace(m_tex_replace);
}

CTexElementAct *CTexElementReplaceFactory::MakeTexElement(CBox &b, CString &name)
{
    return new CTexElementReplace(b, name, m_tex_replace);
}
