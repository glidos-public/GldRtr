#include <dos.h>
#include <stdio.h>
#include <string.h>
#include "xms.h"
#include "pipe.h"

#include "vesa.h"

#define GR_TEXFMT_RGB_565 (0xa)
#define GR_TEXFMT_ARGB_1555 (0xb)
#define NUM_MODES (20)
#define OEMStringLen  (6)
#define WINPTRSEG (0xA000)
#define NUMPAGES  (10)

typedef struct
{
    char            VESASignature[4];
    unsigned short  VESAVersion;
    void __far     *OEMStringPtr;
    char            Capabilities[4];
    void __far     *VideoModePtr;
    unsigned short  TotalMemory;
    char            Reserved[236];
} VgaInfoBlock;


typedef struct
{
    unsigned short ModeAttributes;
    unsigned  char WinAAttributes;
    unsigned  char WinBAttributes;
    unsigned short WinGranularity;
    unsigned short WinSize;
    unsigned short WinASegment;
    unsigned short WinBSegment;
    unsigned short WinFuncPtr_offset;
    unsigned short WinFuncPtr_segment;
    unsigned short BytesPerScanLine;
    unsigned short XResolution;
    unsigned short YResolution;
    unsigned  char XCharSize;
    unsigned  char YCharSize;
    unsigned  char NumberOfPlanes;
    unsigned  char BitsPerPixel;
    unsigned  char NumberOfBanks;
    unsigned  char MemoryModel;
    unsigned  char BankSize;
    unsigned  char NumberOfImagePages;
    unsigned  char ReservedOne;
    unsigned  char RedMaskSize;
    unsigned  char RedFieldPosition;
    unsigned  char GreenMaskSize;
    unsigned  char GreenFieldPosition;
    unsigned  char BlueMaskSize;
    unsigned  char BlueFieldPosition;
    unsigned  char RsvdMaskSize;
    unsigned  char RsvdFieldPosition;
    unsigned  char DirectColorModeInfo;
    unsigned  char Reserved[216];
} ModeInfoBlock;


static Xms *xms;
static int  block;
static int  vesa_started   = 0;
static int  mode           = 0x101;
static int  mode_supported = 0;
static int  win_pos;
static int  quiet_mode;


static void set_vga_info(VgaInfoBlock __far *info)
{
    int modes_offset = 0x22;
    int oem_string_offset = modes_offset + (NUM_MODES+1) * sizeof(short);
    short __far *modes = (short __far *) ((char __far *)info + modes_offset);
    int i;

    memset(info, 0, 256);

    info->VESASignature[0] = 'V';
    info->VESASignature[1] = 'E';
    info->VESASignature[2] = 'S';
    info->VESASignature[3] = 'A';

    info->VESAVersion = 0x0102;

    info->OEMStringPtr = (char __far *)info + oem_string_offset;

    info->Capabilities[0] = 0;
    info->Capabilities[1] = 0;
    info->Capabilities[2] = 0;
    info->Capabilities[3] = 0;

    info->VideoModePtr = (char __far *)info + modes_offset;

    info->TotalMemory = NUMPAGES;

    i = 0;

    modes[i++] = 0x100;
    modes[i++] = 0x101;
    modes[i++] = 0x102;
    modes[i++] = 0x103;
    modes[i++] = 0x104;
    modes[i++] = 0x105;
    modes[i++] = 0x106;
    modes[i++] = 0x107;
    modes[i++] = 0x108;
    modes[i++] = 0x109;
    modes[i++] = 0x10a;
    modes[i++] = 0x10b;
    modes[i++] = 0x10c;
    modes[i++] = 0x10d;
    modes[i++] = 0x10e;
    modes[i++] = 0x10f;
    modes[i++] = 0x110;
    modes[i++] = 0x111;
    modes[i++] = 0x112;
    modes[i++] = 0x113;
    modes[i++] = -1;

    strcpy((char __far *)info + oem_string_offset, "GLIDOS");
}


static void set_mode_info(ModeInfoBlock __far *info, int mode)
{
    memset(info, 0, 256);

    info->ModeAttributes = 0x1f;
    if(mode == 0x110)
        info->ModeAttributes &= ~1;

    info->WinAAttributes = 0x7;
    info->WinBAttributes = 0;
    info->WinGranularity = 64;
    info->WinSize        = 64;
    info->WinASegment    = WINPTRSEG;
    info->WinBSegment    = 0;
    info->WinFuncPtr_segment = 0;
    info->WinFuncPtr_offset = 0;
    switch(mode)
    {
        case 0x101:
        info->BytesPerScanLine = 640;
        break;
        case 0x110:
        case 0x111:
        info->BytesPerScanLine = 1280;
        break;

        default:
        info->BytesPerScanLine = 640;
        break;
    }
    info->XResolution    = 640;
    info->YResolution    = 480;
    info->XCharSize      = 6;
    info->YCharSize      = 8;
    info->NumberOfPlanes = 1;
    switch(mode)
    {
        case 0x101:
        info->BitsPerPixel   = 8;
        break;

        case 0x110:
        case 0x111:
        info->BitsPerPixel   = 16;
        break;

        default:
        info->BitsPerPixel   = 8;
        break;
    }
    info->NumberOfBanks  = 1;
    switch(mode)
    {
        case 0x101:
        info->MemoryModel = 4;
        break;

        case 0x110:
        case 0x111:
        info->MemoryModel = 6;
        break;

        default:
        info->MemoryModel = 4;
        break;
    }
    info->MemoryModel    = 4;
    info->BankSize       = 0;
    info->NumberOfImagePages = 1;
    info->ReservedOne    = 1;
    switch(mode)
    {
        case 0x110:
        info->RedMaskSize = 5;
        info->RedFieldPosition = 0xA;
        info->GreenMaskSize = 5;
        info->GreenFieldPosition = 0x5;
        info->BlueMaskSize = 5;
        info->BlueFieldPosition = 0;
        break;

        case 0x111:
        info->RedMaskSize = 5;
        info->RedFieldPosition = 0xB;
        info->GreenMaskSize = 6;
        info->GreenFieldPosition = 0x5;
        info->BlueMaskSize = 5;
        info->BlueFieldPosition = 0;
        break;
    }
}


static void send_winopen(void)
{
    Pipe_sendChar(CMD_SstWinOpen);
    Pipe_sendInt(7 /* GR_RESOLUTION_640x480 */);
    Pipe_sendInt(0 /* GR_REFRESH_60Hz */);
    Pipe_sendInt(0 /* GR_COLORFORMAT_ARGB */);
    Pipe_sendInt(0 /* GR_ORIGIN_UPPER_LEFT */);
    Pipe_sendInt(2);
    Pipe_sendInt(1);
    Pipe_synchronise();
}

static void send_frame(void)
{
    static char line[640*2];
    static char blank[640*2];
    int         fmt;
    int         even_used = 0;
    int         odd_used = 0;
    int         i;

    memset(blank, 0, 640*2);

    for(i = 0; i < 480 && !even_used; i += 2)
    {
        Xms_read(xms, block, (long unsigned int)i * (640*2), line, 640*2);
        if(memcmp(line, blank, 640*2) != 0)
            even_used = 1;
    }

    for(i = 1; i < 480 && !odd_used; i += 2)
    {
        Xms_read(xms, block, (long unsigned int)i * (640*2), line, 640*2);
        if(memcmp(line, blank, 640*2) != 0)
            odd_used = 1;
    }

    switch(mode)
    {
        case 0x110:
        fmt = GR_TEXFMT_ARGB_1555;
        break;

        case 0x111:
        fmt = GR_TEXFMT_RGB_565;
        break;
    }

    if(odd_used && even_used)
    {
        unsigned    i;

        Pipe_sendChar(CMD_DisplayVESA);
        Pipe_sendInt(0);
        Pipe_sendInt(fmt);

        for(i = 0; i < 480; i++)
        {
            Xms_read(xms, block, (long unsigned int)i * (640*2), line, 640*2);
            Pipe_write(line, 640*2);
        }

        Pipe_synchronise();
    }
    else
    {
        Pipe_sendChar(CMD_DisplayVESA);
        Pipe_sendInt(1);
        Pipe_sendInt(fmt);

        for(i = 0; i < 240; i++)
        {
            Xms_read(xms, block, (long unsigned int)(2*i+odd_used) * (640*2), line, 640*2);
            Pipe_write(line, 640*2);
        }

        Pipe_synchronise();
    }
}


static void (__interrupt __far *old10)();

static void __interrupt __far new10(union INTPACK r)
{
    if(!vesa_started)
    {
        Pipe_open();
        Pipe_sendChar(CMD_VESAStart);
        vesa_started = 1;
    }

    switch(r.w.ax & 0xff00)
    {
    case 0:
        break;

    case 0x4f00:
        switch(r.w.ax & 0xff)
        {
            case 0:
            {
                set_vga_info((VgaInfoBlock __far *)(r.w.es:>r.w.di));
            }
            break;

            case 1:
            {
                set_mode_info((ModeInfoBlock __far *)(r.w.es:>r.w.di), r.w.cx);
            }
            break;

            case 2:
            {
                mode = r.w.bx;

                mode_supported = (mode == 0x110 || mode == 0x111);

                win_pos = -1;

                Pipe_printf("VESA mode 0x%x", mode, 0, 0, 0, 0);

                if(mode_supported && !quiet_mode)
                {
                    send_winopen();
                }
            }
            break;

            case 3:
            {
                r.w.bx = mode;
            }
            break;

            case 5:
            if(mode_supported)
            {
                if(0 <= win_pos && win_pos < 10)
                    Xms_write(xms, block, (long unsigned int)win_pos * 0x10000, WINPTRSEG:>0, 0x10000);

                if(r.w.dx < win_pos && !quiet_mode)
                    send_frame();

                win_pos = r.w.dx;

                if(r.w.dx < 10)
                    Xms_read(xms, block, (long unsigned int)win_pos * 0x10000, WINPTRSEG:>0, 0x10000);
            }
            break;

            default:
            if(mode_supported)
            {
                Pipe_printf("VESA cmd %x, %x, %x, %x", r.w.ax, r.w.bx, r.w.cx, r.w.dx, 0);
            }
            break;
        }
        r.w.ax = 0x4f;
        break;

    default:
        _chain_intr(old10);
        break;
    }
}

VESA_init(Xms *xmsin, int quiet_mode_in)
{
    xms        = xmsin;
    quiet_mode = quiet_mode_in;

    if(!Xms_allocate(xms, 640, &block))
    {
        printf("Glidos VESA support failed: XMS allocation\n");
        return;
    }

    printf("Allocated 640K in XMS\n");

    old10 = _dos_getvect(0x10);

    _dos_setvect(0x10, new10);
}
