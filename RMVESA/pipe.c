#include <dos.h>
#include <string.h>
#include "pipe.h"


#define BUF_SIZE (1024)

static Pipe_OnSyncFn       *onSync;
static Pipe_OnMouseEventFn *onMouseEvent;
static enable_fmv;

static void (__interrupt __far *old21)();

static int handle = -1;
static int fmv_handle = -1;

static char prnt_buf[BUF_SIZE];
static int  buf_i;

static int  play_status  = 1;

#pragma aux _call_intr parm [cx bx] modify [ax bx cx dx di si ds es];
static void __declspec(naked) _call_intr(void (__interrupt __far *handler)())
{
    // cx:bx contains old handler. bp points to intpack structure
    __asm
    {
        push gs           // Stash registers that will be trashed.
        push fs
        push bp
        pushf             // Put flags and return address on stack,
        push cs           // so that old handler will return to this handler.
        call resume       // Use trick to pick up label address in dx
        push dx
        mov ax, 1Ch[bp]   // Load flags from intpack
        lahf
        push cx           // Push old handler address on stack for retf below
        push bx
        mov gs, [bp]      // Load regs from intpack
        mov fs, 2h[bp]
        mov es, 4h[bp]
        mov ds, 6h[bp]
        mov di, 8h[bp]
        mov si, 0Ah[bp]
        mov bx, 10h[bp]
        mov dx, 12h[bp]
        mov cx, 14h[bp]
        mov ax, 16h[bp]
        retf              // Jump to old handler
resume:
        call next         // Push address of "next" on the stack
next:
        pop dx            // Put address of "next" in dx and invcrement by 5
        add dx, 5         // to get address of "pop bp" below
        ret               // Return to "call resume" above, with address in dx
        pop bp            // This is where old handler returns to. Pull stored bp off stack (points to intpack)
        mov [bp], gs      // Store regs to intpack
        mov 2h[bp], fs
        mov 4h[bp], es
        mov 6h[bp], ds
        mov 8h[bp], di
        mov 0Ah[bp], si
        mov 10h[bp], bx
        mov 12h[bp], dx
        mov 14h[bp], cx
        mov 16h[bp], ax
        mov ax, 1Ch[bp]   // Store flags to intpack
        sahf
        mov 1Ch[bp], ax
        pop fs            // Restore trashed registers
        pop gs
        retn              // Return to caller
    }
}

extern void open_file(char *name);
#pragma aux open_file =  \
    "mov ax, 3d02h"      \
    "mov cx, 0h"         \
    "int 21h"            \
    "jnc ok"             \
    "mov ax, -1"         \
    "ok: mov handle, ax" \
    parm [ds dx]         \
    modify [ax cx];

extern void close_file(void);
#pragma aux close_file = \
    "mov ax, 3e00h"      \
    "mov bx, handle"     \
    "int 21h"            \
    modify [ax bx];

extern void writeN(char *s, unsigned int n);
#pragma aux writeN =  \
    "mov bx, handle"  \
    "push ds"         \
    "mov ds, cx"      \
    "mov cx, ax"      \
    "mov ax, 4000h"   \
    "int 21h"         \
    "pop ds"          \
    parm [cx dx] [ax] \
    modify [ax bx cx];

extern void readN(char *s, unsigned int n);
#pragma aux readN =   \
    "mov bx, handle"  \
    "push ds"         \
    "mov ds, cx"      \
    "mov cx, ax"      \
    "mov ax, 3f00h"   \
    "int 21h"         \
    "pop ds"          \
    parm [cx dx] [ax] \
    modify [ax bx cx];

static void printc(char c)
{
    prnt_buf[buf_i++] = c;
}

static void printdigit(int i)
{
    printc(i > 9 ? (i - 10 + 'A') : (i + '0'));
}

static void print_int(int i, int base)
{
    char buf[20];
    int d = 0;

    if(i < 0)
    {
        printc('-');
        i = -i; /*FIXME: Doesn't work for MIN_INT */
    }

    if(i == 0)
    {
        printc('0');
    }
    else
    {
        while(i)
        {
            buf[d++] = i%base;
            i /= base;
        }

        while(d)
        {
            printdigit(buf[--d]);
        }
    }
}

static void reportFMVOpen(char *s)
{
    Pipe_sendChar(CMD_FMVOpen);
    writeN(s, strlen(s) + 1);
}

static void reportFMVPlay(void)
{
    char c = 0;

    Pipe_sendChar(CMD_FMVPlay);
    readN(&c, 1);
}

static char *dummy_path = "d:\\fmv\\dummy.rpl";

static void __interrupt __far new21(union INTPACK r)
{
    static volatile int sync    = 0;

    if(r.h.ah == 0x3d
            && strcmp(r.w.ds:>r.w.dx, "\\\\.\\PIPE\\GLDPIPE1") == 0)
    {
        /* Something is trying to open the pipe */
        if(handle != -1)
        {
            /* We already opened it. Give them our handle */
            r.w.ax = handle;
            r.w.flags &= ~INTR_CF;
        }
        else
        {
            /* We have yet to open it. Pass on their call to
             * the old handler and pick up the handle they
             * will receive */
            _call_intr(old21);

            handle = r.w.ax;
        }
    }
    else if(enable_fmv
            && r.h.ah == 0x3d
            && strstr(r.w.ds:>r.w.dx, "\\fmv\\") != NULL
            && handle != -1)
    {
        int ds = r.w.ds;
        int dx = r.w.dx;

        reportFMVOpen(r.w.ds:>r.w.dx);

        r.w.ds = FP_SEG(dummy_path);
        r.w.dx = FP_OFF(dummy_path);
        _call_intr(old21);
        r.w.ds = ds;
        r.w.dx = dx;

        fmv_handle = r.w.ax;
    }
    else if(r.h.ah == 0x3e && r.w.bx == fmv_handle)
    {
        fmv_handle = -1;

        _chain_intr(old21);
    }
    else if(r.h.ah == 0x42 && r.w.bx == fmv_handle)
    {
        if(r.w.cx == 0 &&r.w.dx == 0x238)
            reportFMVPlay();

        _chain_intr(old21);
    }
    else if(r.h.ah == 0x40
              && r.w.bx == handle
              && r.w.cx == 0)
    {
        /* Glide2x.ovl will do a write of size 0 on each
         * synchronisation, which is a good opportunity
         * to send the stored commands. That will
         * cause a recursive call into this function,
         * but not with r.w.cx == 0 */
        onSync();

        /* Record that next two reads are synching */
        sync = 1;
    }
    else if(sync
            && r.h.ah == 0x3f
            && r.w.bx == handle
            && r.w.cx == 12)
    {
        /* This is Glide2x.ovl doing the 1st synchonising read.
         * Pass on its read to the old handler, and look at what's
         * in the buffer to pick up the mouse input */
        long int __far *coords = (long int __far *)(r.w.ds:>r.w.dx);
        _call_intr(old21);

        onMouseEvent(coords[0], coords[1], coords[2]);
    }
    else if(sync
            && r.h.ah == 0x3f
            && r.w.bx == handle
            && r.w.cx == 4)
    {
        /* This is Glide2x.ovl doing the 2nd synchonising read.
         * Pass on their read to the old handler, and look at what's
         * in the buffer to pick up the mp3 play status */
        _call_intr(old21);

        play_status = ((int __far *)(r.w.ds:>r.w.dx))[0];

        sync = 0;
    }
    else
    {
        _chain_intr(old21);
    }
}

void Pipe_init(Pipe_OnSyncFn       *onSyncIn,
               Pipe_OnMouseEventFn *onMouseEventIn,
               int                  fmv_enabled)
{
    onSync       = onSyncIn;
    onMouseEvent = onMouseEventIn;
    enable_fmv   = fmv_enabled;

    old21 = _dos_getvect(0x21);

    _dos_setvect(0x21, new21);
}

void Pipe_open(void)
{
    if(handle == -1)
        open_file("\\\\.\\PIPE\\GLDPIPE1");
}

int Pipe_playStatus(void)
{
    return play_status;
}

void Pipe_setPlayStatus(int s)
{
    play_status = s;
}

void Pipe_write(char *s, unsigned int n)
{
    writeN(s, n);
}

void Pipe_sendChar(char c)
{
    prnt_buf[0] = c;
    writeN(prnt_buf, 1);
}

void Pipe_sendInt(unsigned long int i)
{
    writeN((unsigned char __far *)&i, 4);
}

void Pipe_synchronise(void)
{
    static char buf[16];

    Pipe_sendChar(CMD_ConfirmationRequest);

    readN(buf,16);
}

void Pipe_printf(char *s, int a, int b, int c, int d, int e)
{
    int arg_i;
    int val;

    buf_i = 0;

    prnt_buf[buf_i++] = CMD_Message;

    if(buf_i + 64 >= BUF_SIZE)
        return;

    arg_i = 0;

    while(*s)
    {
        switch(arg_i)
        {
            case 0: val = a; break;
            case 1: val = b; break;
            case 2: val = c; break;
            case 3: val = d; break;
            case 4: val = e; break;
        }

        switch(*s)
        {
        case '%':
            ++s;
            switch(*s)
            {
            case 'x':
                ++s;
                print_int(val, 16);
                arg_i++;
                break;
            case 'd':
                ++s;
                print_int(val, 10);
                arg_i++;
                break;
            }
            break;

        default:
            printc(*s++);
            break;
        }
    }

    prnt_buf[buf_i++] = 0;

    writeN(prnt_buf, buf_i);
}
