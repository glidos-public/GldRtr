#if !defined(AFX_PROPAUDIO_H__639400F1_4D37_4074_83A4_EC7FD3B45724__INCLUDED_)
#define AFX_PROPAUDIO_H__639400F1_4D37_4074_83A4_EC7FD3B45724__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropAudio.h : header file
//

#include "GameRecord.h"

/////////////////////////////////////////////////////////////////////////////
// CPropAudio dialog

class CPropAudio : public CPropertyPage
{
	DECLARE_DYNCREATE(CPropAudio)

private:
    CGameRecord *m_rec;

// Construction
public:
	void Init(CGameRecord *rec);
	CPropAudio();
	~CPropAudio();

// Dialog Data
	//{{AFX_DATA(CPropAudio)
	enum { IDD = IDD_PROP_AUDIO };
	int		m_redbook;
    int     m_fmv_pack;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropAudio)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropAudio)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPAUDIO_H__639400F1_4D37_4074_83A4_EC7FD3B45724__INCLUDED_)
