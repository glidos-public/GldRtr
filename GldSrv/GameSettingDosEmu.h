// GameSettingDosEmu.h: interface for the CGameSettingDosEmu class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAMESETTINGDOSEMU_H__D32E5324_CA51_406B_A03A_8EE8E844CA51__INCLUDED_)
#define AFX_GAMESETTINGDOSEMU_H__D32E5324_CA51_406B_A03A_8EE8E844CA51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GameSetting.h"


class CDosEmu
{
public:
    enum DosEmu
    {
        DOSEMU_WINDOWS,
        DOSEMU_VDOS32,
        DOSEMU_DOSBOX
    };

protected:
    DosEmu m_value;

public:
    operator DosEmu()
    {
        return m_value;
    };
};


class CGameSettingDosEmu : public CGameSetting<CDosEmu>
{
public:
    void                Init(CStoredStruct *st, const char *name);
    CGameSettingDosEmu &operator=(const DosEmu &i);

private:
    bool Parse(CString &val);
    CString Print();
};

#endif // !defined(AFX_GAMESETTINGDOSEMU_H__D32E5324_CA51_406B_A03A_8EE8E844CA51__INCLUDED_)
