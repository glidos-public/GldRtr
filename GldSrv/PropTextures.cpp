// PropTextures.cpp : implementation file
//

#include "stdafx.h"
#include "gldsrv.h"
#include "PropTextures.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropTextures property page

IMPLEMENT_DYNCREATE(CPropTextures, CPropertyPage)

CPropTextures::CPropTextures() : CPropertyPage(CPropTextures::IDD)
{
	//{{AFX_DATA_INIT(CPropTextures)
	m_override = -1;
	//}}AFX_DATA_INIT
}

CPropTextures::~CPropTextures()
{
}

void CPropTextures::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropTextures)
	DDX_Control(pDX, IDC_OLDTEX, m_oldpack);
	DDX_Control(pDX, IDC_LABEL_SELECT, m_label);
	DDX_Control(pDX, IDC_STATIC_SELECT, m_static_select);
	DDX_Control(pDX, IDC_PACKNAME, m_packname);
	DDX_Radio(pDX, IDC_OVERRIDE, m_override);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropTextures, CPropertyPage)
	//{{AFX_MSG_MAP(CPropTextures)
	ON_BN_CLICKED(IDC_OLDTEX, OnOldtex)
	ON_BN_CLICKED(IDC_OVERRIDE, OnOverride)
	ON_BN_CLICKED(IDC_OVERRIDE2, OnOverride2)
	ON_BN_CLICKED(IDC_OVERRIDE3, OnOverride3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropTextures message handlers

void CPropTextures::Init(CGameRecord *rec)
{
    m_rec = rec;
}

BOOL CPropTextures::OnSetActive() 
{
    CFileFind finder;
    BOOL      going;
    int       count;
    int       sel;

    m_override = m_rec->tex_handling;

    m_packname.ResetContent();
    sel   = -1;
    count = 0;

    going = finder.FindFile("Textures\\*.*");
    while(going)
    {
        going = finder.FindNextFile();
        
        if(finder.IsDirectory() && !finder.IsDots())
        {
            CString s = finder.GetFileName();
            m_packname.AddString(s);
            if(m_rec->tex_folder == s)
                sel = count;
            
            count++;
        }
    }

    if(count > 0)
        m_packname.SetCurSel(sel == -1 ? 0 : sel);

    m_oldpack.SetCheck(sel == -1);

    m_static_select.EnableWindow(m_override == CTexHandling::TEX_HANDLING_OVERRIDE);
    m_oldpack.EnableWindow(      m_override == CTexHandling::TEX_HANDLING_OVERRIDE);
    m_label.EnableWindow(        m_override == CTexHandling::TEX_HANDLING_OVERRIDE && !m_oldpack.GetCheck());
    m_packname.EnableWindow(     m_override == CTexHandling::TEX_HANDLING_OVERRIDE && !m_oldpack.GetCheck());

    UpdateData(FALSE);

	return CPropertyPage::OnSetActive();
}

BOOL CPropTextures::OnKillActive() 
{
    CString name("");

    UpdateData();

    m_rec->tex_handling = (CTexHandling::TexHandling) m_override;

    if(!m_oldpack.GetCheck())
        m_packname.GetLBText(m_packname.GetCurSel(), name);

    m_rec->tex_folder = name;

	return CPropertyPage::OnKillActive();
}

void CPropTextures::OnOldtex() 
{
    UpdateData();

    m_label.EnableWindow(   m_override == CTexHandling::TEX_HANDLING_OVERRIDE && !m_oldpack.GetCheck());
    m_packname.EnableWindow(m_override == CTexHandling::TEX_HANDLING_OVERRIDE && !m_oldpack.GetCheck());
}

void CPropTextures::OnOverride() 
{
    UpdateData();

    m_static_select.EnableWindow(m_override == CTexHandling::TEX_HANDLING_OVERRIDE);
    m_oldpack.EnableWindow(      m_override == CTexHandling::TEX_HANDLING_OVERRIDE);
    m_label.EnableWindow(        m_override == CTexHandling::TEX_HANDLING_OVERRIDE && !m_oldpack.GetCheck());
    m_packname.EnableWindow(     m_override == CTexHandling::TEX_HANDLING_OVERRIDE && !m_oldpack.GetCheck());
}

void CPropTextures::OnOverride2() 
{
    OnOverride();
}

void CPropTextures::OnOverride3() 
{
    OnOverride();
}
