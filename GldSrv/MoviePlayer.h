#pragma once

#include "Dshow.h"

class CMoviePlayer
{
private:
    CComPtr<IGraphBuilder>          m_builder;
    CComPtr<IMediaControl>          m_control;
    CComPtr<IMediaEvent>            m_event;
    bool                            m_is_complete;

public:
    CMoviePlayer(void);
    ~CMoviePlayer(void);
    HRESULT Create(CWnd *wnd);
    HRESULT Play(LPCSTR path);
    HRESULT GetEvent(HANDLE *handle);
    bool    IsPlaying();
private:
    HRESULT CreateVMR9(CWnd * wnd);
    HRESULT CreateEVR(CWnd * wnd);
};
