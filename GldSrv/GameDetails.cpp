// GameDetails.cpp: implementation of the CGameDetails class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "GameDetails.h"
#include "PropAudio.h"
#include "PropDisplay.h"
#include "PropExecutable.h"
#include "PropTextures.h"
#include "PropDos.h"
#include "PropExtras.h"
#include "Prop3DMonitor.h"
#include "PropCD.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

static CString regString(HKEY key, const char *keyname[], int nkeys)
{
    char devstr[64];
    char *ptr = devstr;
	CString val;

    int         i;
    LONG        res;

    res = ERROR_SUCCESS;
    for(i = 0; res == ERROR_SUCCESS && i < nkeys-1; i++)
    {
        HKEY old_key = key;

        res = ::RegOpenKeyEx(old_key, keyname[i], 0, (0x100)|KEY_QUERY_VALUE, &key);
        if(res != ERROR_SUCCESS)
            res = ::RegOpenKeyEx(old_key, keyname[i], 0, KEY_QUERY_VALUE, &key);
        if(i > 0)
            RegCloseKey(old_key);
    }

	LPBYTE str = NULL;
	DWORD size = 0;

	if(res == ERROR_SUCCESS)
		res = ::RegQueryValueEx(key, keyname[nkeys-1], 0, NULL, NULL, &size);

	if (res == ERROR_SUCCESS)
	{
		str = new BYTE[size];
		res = ::RegQueryValueEx(key, keyname[nkeys-1], 0, NULL, str, &size);
	}

	if(res == ERROR_SUCCESS)
	{
		val = str;
		delete [] str;
	}

	RegCloseKey(key);

	return val;
}

static CString getSteamPath()
{
    const char *keys[] =
    {
        "Software",
        "Valve",
        "Steam",
        "SteamPath"
    };
    int n = sizeof(keys)/sizeof(*keys);

	CString path = regString(HKEY_CURRENT_USER, keys, n);
	path.Replace('/','\\');
	return path;
}

static CString getGOGPath()
{
    const char *keys[] =
    {
        "SOFTWARE",
        "WOW6432Node",
        "GOG.com",
        "Games",
        "1207663463",
        "path"
    };
    int n = sizeof(keys)/sizeof(*keys);

	return regString(HKEY_LOCAL_MACHINE, keys, n);
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGameDetails::CGameDetails()
{
    m_current = 0;
}

CGameDetails::~CGameDetails()
{
	RemoveRecords();
}

void CGameDetails::RemoveRecords()
{
	INT_PTR n = m_rec.GetSize();
	INT_PTR i;

	for(i = 0; i < n; i++)
		delete m_rec[i];

	m_rec.RemoveAll();

    m_current = 0;
}

bool CGameDetails::ReadSelected(FILE *f, CString &selected)
{
	char buf[256];
	int  pos;

	if(fgets(buf, 256, f) == NULL)
		return false;

	CString line(buf);

	pos = line.Find(':');
	if(pos == -1)
		return false;

	CString key(line.Left(pos));
	CString val(line.Mid(pos+1));
	
	key.TrimLeft();
	key.TrimRight();
	val.TrimLeft();
	val.TrimRight();

	if(key != "Selected")
		return false;

	selected = val;

	return true;
}

void CGameDetails::Load()
{
    FILE   *f;
    INT_PTR n, i;
    CString selected;
    CGameRecord *rec = NULL;

	RemoveRecords();

    f = fopen("Glidos_1_52.ini", "r");
    if(f == NULL || !ReadSelected(f, selected))
    {
        AfxMessageBox(IDS_INI_LOAD_FAIL, MB_OK);
        return;
    }

	while(!feof(f))
	{
		rec = CGameRecord::Read(f);

		if(rec != NULL)
			m_rec.Add(rec);
	}

    fclose(f);

	n = m_rec.GetSize();

    for(i = 0; i < n; i++)
        if(m_rec[i]->name == selected)
            m_current = (int)i;

    UpdateCombo();
}

void CGameDetails::Save()
{
    FILE     *f;
	INT_PTR   n;

	n = m_rec.GetSize();
    f = fopen("Glidos_1_52.ini", "w");
    if(f != NULL && m_current >= 0 && m_current < n)
    {
        INT_PTR i;

        fprintf(f, "Selected: %s\n", (LPCSTR) m_rec[m_current]->name);

        fprintf(f, "\n");

        for(i = 0; i < n; i++)
            m_rec[i]->Write(f);

        fclose(f);
    }
    else
    {
        AfxMessageBox(IDS_INI_SAVE_FAIL, MB_OK);
    }
}

void CGameDetails::Initialise(CComboBox *combo)
{
    m_combo = combo;
}

void CGameDetails::OnGameSelect()
{
    m_current = m_combo->GetCurSel();
    Save();
}

void CGameDetails::Adjust()
{
    CGameRecord       rec;
    CPropertySheet    sheet;
    CPropDisplay      prop_display;
    CPropCD           prop_cd;
    CPropExecutable   prop_executable;
    CPropTextures     prop_textures;
    CPropAudio        prop_audio;
    CPropDos          prop_dos;
    CPropExtras       prop_extras;
    CProp3DMonitor    prop_3d_monitor;

    // Make a tempory copy of the game settings
    rec = *(m_rec[m_current]);

    prop_display.Init(&rec);
    prop_cd.Init(&rec);
    prop_executable.Init(&rec);
    prop_textures.Init(&rec);
    prop_audio.Init(&rec);
    prop_dos.Init(&rec);
    prop_extras.Init(&rec);
    prop_3d_monitor.Init(&rec);

    sheet.SetTitle("Game details");

    sheet.AddPage(&prop_display);
    sheet.AddPage(&prop_cd);
    sheet.AddPage(&prop_executable);
    sheet.AddPage(&prop_textures);
    sheet.AddPage(&prop_audio);
    sheet.AddPage(&prop_dos);
    sheet.AddPage(&prop_extras);
    sheet.AddPage(&prop_3d_monitor);

    if(sheet.DoModal() == IDOK)
    {
        // Store back the changed settings
        *(m_rec[m_current]) = rec;

        UpdateCombo();

        // Save to file
        Save();
    }

    sheet.DestroyWindow();
}

void CGameDetails::UpdateCombo()
{
    INT_PTR n;
    int     i;

    m_combo->ResetContent();

	n = m_rec.GetSize();

    for(i = 0; i < n; i++)
        m_combo->AddString(m_rec[i]->name);

    m_combo->SetCurSel(m_current);
}

CResolution &CGameDetails::GetResolution()
{
    return m_rec[m_current]->resolution;
}

bool CGameDetails::GetFullScreen()
{
    return m_rec[m_current]->full_screen;
}

bool CGameDetails::GetNoModeChange()
{
    return m_rec[m_current]->no_mode_change;
}

void CGameDetails::StartGame(OSType os)
{
    CGameRecord *rec;
    STARTUPINFO sinfo;
    PROCESS_INFORMATION pinfo;
    WIN32_FIND_DATA finfo;
    HANDLE find_handle = NULL;
	CString executable;
    CString exe_folder;
    CString exe_file;
    CString mount_folder;
    CString mount_relative_folder;
    CString glidos_folder;
    int     buf_len;
    int     i;
    CString cdroot;
    CString mount;
    int     subfolders;

    buf_len = ::GetCurrentDirectory(0, NULL);
    ::GetCurrentDirectory(buf_len, glidos_folder.GetBuffer(buf_len));
    glidos_folder.ReleaseBuffer();

    if(m_current < 0 || m_current >= m_rec.GetSize())
    {
        AfxMessageBox(IDS_NO_GAME, MB_OK);
        return;
    }

    rec = m_rec[m_current];

    if(rec->cd_path.GetLength() == 0)
    {
        for(i = 0; i < 26; i++)
        {
            cdroot.Format("%c:\\", 'A' + i);

            if(GetDriveType((LPCSTR)cdroot) == DRIVE_CDROM)
                break;
        }
    }
    else
    {
        cdroot = rec->cd_path;

		if (cdroot.Find("$STEAM") != -1)
			cdroot.Replace("$STEAM", getSteamPath());
		if (cdroot.Find("$GOG") != -1)
			cdroot.Replace("$GOG", getGOGPath());
    }

	executable = rec->executable;
	if (executable.Find("$STEAM") != -1)
		executable.Replace("$STEAM", getSteamPath());
	if (executable.Find("$GOG") != -1)
		executable.Replace("$GOG", getGOGPath());

    bool dos_full_screen    = rec->dos_full_screen;

    if(rec->redbook_emulation == CRedbookHandling::REDBOOK_HANDLING_MP3)
    {
        find_handle = FindFirstFile("Audio\\Tracks\\01maintitle.mp3", &finfo);
        if(find_handle == INVALID_HANDLE_VALUE)
        {
            CString s;

            s.LoadString(IDS_AUDIO_PACK_MISSING);

            AfxMessageBox((LPCSTR) s, MB_OK);

            return;
        }
    }

    FindClose(find_handle);

    if(rec->fmv_pack)
    {
        find_handle = FindFirstFile("FMV\\DUMMY.RPL", &finfo);
        if(find_handle == INVALID_HANDLE_VALUE)
        {
            CString s;

            s.LoadString(IDS_FMV_PACK_MISSING);

            AfxMessageBox((LPCSTR) s, MB_OK);

            return;
        }
    }

    FindClose(find_handle);

    i = executable.ReverseFind('\\');
    if(i == -1 || executable.GetLength() < 2 || executable[1] != ':')
    {
        CString s;

        s.LoadString(IDS_BAD_EXECUTABLE);
        s += executable + "\"";

        AfxMessageBox((LPCSTR) s, MB_OK);

        return;
    }

    exe_folder = executable.Left(i + 1);
    exe_file = executable.Right(executable.GetLength() - i - 1);

    subfolders = rec->subfolders;
    mount_folder = exe_folder.Left(exe_folder.GetLength() - 1);
    while (subfolders-- > 0)
    {
        int i = mount_folder.ReverseFind('\\');
        if (i != -1)
        {
            if (mount_relative_folder.GetLength() > 0)
                mount_relative_folder = CString("\\") + mount_relative_folder;

            mount_relative_folder = mount_folder.Right(mount_folder.GetLength() - i - 1) + mount_relative_folder;

            mount_folder = mount_folder.Left(i);
        }
    }

    find_handle = FindFirstFile(executable, &finfo);
    if(find_handle == INVALID_HANDLE_VALUE)
    {
        CString s, fmt;

        fmt.LoadString(IDS_EXE_MISSING);
        s.Format((LPCSTR)fmt, (LPCSTR)executable, (LPCSTR)rec->name);
        AfxMessageBox((LPCSTR) s, MB_OK);
        return;
    }

    FindClose(find_handle);

    if(rec->sizes.GetSize() > 0)
    {
        int i;
        bool found = false;

        for(i = 0; i < rec->sizes.GetSize(); i++)
            if(rec->sizes[i] == finfo.nFileSizeLow)
                found = true;

        if(!found)
        {
            CString s, fmt;

            fmt.LoadString(IDS_WRONG_EXE);
            s.Format((LPCSTR)fmt, exe_file);
            AfxMessageBox((LPCSTR)s, MB_OK);
            return;
        }
    }

    if(rec->cd_file.GetLength() > 0 && rec->cd_path.GetLength() == 0)
    {
        cdroot = CheckCdPresent(rec->cd_file);

        if(cdroot.IsEmpty())
            return;
    }

    ::DeleteFile(exe_folder+"Glide2x.ovl");
    ::DeleteFile(exe_folder+"PollPipe.dll");
    ::DeleteFile(exe_folder+"Glidos.pif");
    ::DeleteFile(exe_folder+"GlidosF.pif");
    ::DeleteFile(exe_folder+"gldvesa.exe");
    ::DeleteFile(exe_folder+"trkfk.exe");
    ::DeleteFile(exe_folder+"Glidos.bat");

    if(rec->dos_emu == CDosEmu::DOSEMU_WINDOWS)
    {
        FILE *f;
        
        f = fopen((LPCSTR)("Glidos.bat"), "w");
        if(f != NULL)
        {
            fprintf(f, "set PATH=%%PATH%%;%s\n", glidos_folder);
            fprintf(f, "%s\n", (LPCSTR)(exe_folder.Left(2)));
            fprintf(f, "cd %s\n", (LPCSTR)exe_folder);
            fprintf(f, "COMMAND.COM /C Game.bat\n");
            
            fclose(f);
        }
        else
        {
            CString s, fmt;
            
            fmt.LoadString(IDS_CANT_CREATE);
            s.Format((LPCSTR)fmt, "Glidos.bat", (LPCSTR)glidos_folder);
            AfxMessageBox((LPCSTR)s, MB_OK);
            return;
        }
        
        f = fopen((LPCSTR)("Game.bat"), "w");
        if(f != NULL)
        {
            if(os == OS_W2K)
                fprintf(f, "DOSDRV\n");
            
            if(rec->blood_fix)
            {
                fprintf(f, "nolfb.com\n");
                fprintf(f, "set BUILD_640X480=1\n");
                fprintf(f, "set BUILD_CONVTEXTURES=1\n");
                fprintf(f, "set BUILD_GAMMA=1\n");
                fprintf(f, "set BUILD_RESAMPLE=1\n");
                fprintf(f, "pause\n");
            }
            
            if(os == OS_W2K)
            {
                fprintf(f, "%s%s%s\n",
                    rec->redbook_emulation == CRedbookHandling::REDBOOK_HANDLING_MP3 ? "trkfk.exe\n" : "",
                    rec->vesa_support ? "gldvesa.exe " : "",
                    (LPCSTR) exe_file);
            }
            else
            {
                fprintf(f, "%s\n", (LPCSTR) exe_file);
            }
            
            fclose(f);
        }
        else
        {
            CString s, fmt;
            
            fmt.LoadString(IDS_CANT_CREATE);
            s.Format((LPCSTR)fmt, "Game.bat", (LPCSTR)glidos_folder);
            AfxMessageBox((LPCSTR)s, MB_OK);
            return;
        }
    }

    sinfo.cb = sizeof(sinfo);
    sinfo.lpReserved = NULL;
    sinfo.lpDesktop = NULL;
    sinfo.lpTitle = "Glidos";
    sinfo.dwFlags = 0;
    sinfo.cbReserved2 = 0;
    sinfo.lpReserved2 = NULL;
    
    CString cmd;

    switch((CDosEmu::DosEmu)rec->dos_emu)
    {
    case CDosEmu::DOSEMU_VDOS32:
        find_handle = FindFirstFile("VDos32.exe", &finfo);
        if(find_handle == INVALID_HANDLE_VALUE)
        {
            CString s, fmt;
            
            fmt.LoadString(IDS_VDOS32_MISSING);
            s.Format((LPCSTR)fmt, (LPCSTR)executable, (LPCSTR)rec->name);
            AfxMessageBox((LPCSTR) s, MB_OK);
            return;
        }
        
        cmd.Format("VDos32.exe \"%s\" --set \"PATH=%s\"", (LPCSTR)executable, (LPCSTR)glidos_folder);
        if(rec->fmv_pack)
            cmd += " --remote_fmv";

        CreateProcess(NULL, cmd.GetBuffer(0), NULL, NULL,
            FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &sinfo, &pinfo);
        break;

    case CDosEmu::DOSEMU_DOSBOX:
        find_handle = FindFirstFile("DOSBox\\dosbox.exe", &finfo);
        if(find_handle == INVALID_HANDLE_VALUE)
        {
            CString s, fmt;
            
            fmt.LoadString(IDS_DOSBOX_MISSING);
            s.Format((LPCSTR)fmt, (LPCSTR)executable, (LPCSTR)rec->name);
            AfxMessageBox((LPCSTR) s, MB_OK);
            return;
        }
        
        cmd.Format("DOSBox\\dosbox.exe -conf DOSBox\\dosbox-0.73.conf \"%s\" "
                   "-c \"mount d \\\"%s\\\"\" "
                   "-c \"set PATH=d:\\\\\" ",
                   (LPCSTR)executable,
                   (LPCSTR)glidos_folder);

        if (cdroot.GetLength() == 3)
        {
            mount.Format("-c \"mount e %s -t cdrom\" ", (LPCSTR)cdroot);
        }
        else
        {
            mount.Format("-c \"imgmount e \\\"%s\\\" -t iso -fs iso\" ", (LPCSTR)cdroot);
        }

        cmd += mount;

        if(rec->blood_fix)
        {
            cmd +=
                "-c \"set BUILD_640X480=1\" "
                "-c \"set BUILD_CONVTEXTURES=1\" "
                "-c \"set BUILD_GAMMA=1\" "
                "-c \"set BUILD_RESAMPLE=1\" ";
        }

        {
            CString vesa = "rmvesa.exe";

            if(rec->vesa_support)
                vesa += " vesa";

            if(rec->redbook_emulation == CRedbookHandling::REDBOOK_HANDLING_MP3)
                vesa += " cd";

            if(rec->fmv_pack)
                vesa += " fmv";

            cmd += "-c \"" + vesa + "\" ";

            cmd += "-c \"mount c "+ mount_folder +"\" ";
            cmd += "-c \"c:\" ";
            if (mount_relative_folder.GetLength() > 0)
                cmd += "-c \"cd "+ mount_relative_folder +"\" ";
            cmd += "-c \""+ exe_file +"\" ";
        }

        CreateProcess(NULL, cmd.GetBuffer(0), NULL, NULL,
            FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &sinfo, &pinfo);
        break;

    default:
        CreateProcess("Glidos.bat", NULL, NULL, NULL,
            FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &sinfo, &pinfo);
        break;
    }
}

CString CGameDetails::CheckCdPresent(CString &test_file)
{
    CString d;
    CString s;
    WIN32_FIND_DATA finfo;
    char drive;
    HANDLE find_handle;
    bool empty_drive = false;
    bool found_disk = false;
    char empty_drive_letter;

    for(drive = 'D'; drive <= 'Z' && !found_disk; drive++)
    {
        d.Format("%c:\\", drive);
        s.Format("%c:\\*.*", drive);
        find_handle = FindFirstFile((LPCSTR) s, &finfo);

        if(find_handle == INVALID_HANDLE_VALUE)
        {
            if(GetLastError() == ERROR_NOT_READY)
            {
                empty_drive = true;
                empty_drive_letter = drive;
            }
        }
        else
        {
            FindClose(find_handle);
            
            s.Format("%c:\\%s", drive, (LPCSTR)test_file);
            find_handle = FindFirstFile((LPCSTR) s, &finfo);
            
            if(find_handle != INVALID_HANDLE_VALUE)
            {
                FindClose(find_handle);
                found_disk = true;
            }
        }
    }

    if(!found_disk)
    {
        AfxMessageBox(IDS_NO_CD, MB_OK);
        return CString("");
    }

    if(empty_drive)
    {
        CString fmt;

        fmt.LoadString(IDS_EMPTY_DRIVE);
        s.Format((LPCSTR)fmt, empty_drive_letter);
        AfxMessageBox((LPCSTR) s, MB_OK);
        return CString("");
    }

    return d;
}


void CGameDetails::GetGameRecord(CGameRecord &rec)
{
    rec = *(m_rec[m_current]);
}

void CGameDetails::New()
{
    CGameRecord      *rec;
    CPropertySheet    sheet;
    CPropDisplay      prop_display;
    CPropExecutable   prop_executable;

    // Make a new game default game setting record
    rec = new CGameRecord;

    prop_display.Init(rec);
    prop_executable.Init(rec);

    sheet.SetTitle("Game details");
    sheet.SetWizardMode();

    sheet.AddPage(&prop_display);
    sheet.AddPage(&prop_executable);

    if(sheet.DoModal() == ID_WIZFINISH)
    {
        // Add new record
        m_rec.Add(rec);

        UpdateCombo();

        // Save to file
        Save();
    }
    else
    {
        delete rec;
    }

    sheet.DestroyWindow();
}
