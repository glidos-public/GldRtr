// PropAudio.cpp : implementation file
//

#include "stdafx.h"
#include "gldsrv.h"
#include "PropAudio.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropAudio property page

IMPLEMENT_DYNCREATE(CPropAudio, CPropertyPage)

CPropAudio::CPropAudio() : CPropertyPage(CPropAudio::IDD)
{
	//{{AFX_DATA_INIT(CPropAudio)
	m_redbook  = 0;
    m_fmv_pack = 0;
	//}}AFX_DATA_INIT
}

CPropAudio::~CPropAudio()
{
}

void CPropAudio::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropAudio)
	DDX_Radio(pDX, IDC_REDBOOK1, m_redbook);
    DDX_Check(pDX, IDC_FMV, m_fmv_pack);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropAudio, CPropertyPage)
	//{{AFX_MSG_MAP(CPropAudio)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropAudio message handlers

BOOL CPropAudio::OnSetActive() 
{
    m_redbook = m_rec->redbook_emulation;
    m_fmv_pack = m_rec->fmv_pack;

    UpdateData(FALSE);

	return CPropertyPage::OnSetActive();
}

BOOL CPropAudio::OnKillActive() 
{
    UpdateData();

    m_rec->redbook_emulation = (CRedbookHandling::RedbookHandling)m_redbook;
    m_rec->fmv_pack          = (m_fmv_pack != 0);

	return CPropertyPage::OnKillActive();
}

void CPropAudio::Init(CGameRecord *rec)
{
    m_rec = rec;
}
