// TexElementAudio.cpp: implementation of the CTexElementAudio class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "TexElementAudio.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexElementAudio::CTexElementAudio(CTexHandlerAudio *handler)
{
    m_handler = handler;
    m_current = NULL;
}

CTexElementAudio::CTexElementAudio(CBox &b, CString &name, CTexHandlerAudio *handler) : CTexElementAct(b, name)
{
    m_handler = handler;
    m_current = NULL;
}

CTexElementAudio::~CTexElementAudio()
{
    INT_PTR count = m_details.GetSize();

    for(INT_PTR i = 0; i < count; i++)
        delete m_details[i];
}

bool CTexElementAudio::ParseLoc(const CString &line, CString *loc_name, int *index)
{
    int     pos;
    CString s     = line;

    s.TrimLeft();
    pos = s.Find(' ');

    if(pos == -1)
        return false;

    *loc_name = s.Left(pos);

    s = s.Mid(pos);
    
    s.TrimLeft();

    *index = atoi((LPCSTR)s);

    return true;
}

bool CTexElementAudio::Load(GrLOD_t lod)
{
    FILE *f = NULL;
    char  buf[256];
    
    f = fopen((LPCSTR)(m_name), "rb");

    if(f != NULL)
    {
        while(fgets(buf, 256, f) != NULL)
        {
            if(memcmp(buf, "Trigger:", 8) == 0)
            {
                CString location_name;
                int     index;

                if(ParseLoc(buf+8, &location_name, &index))
                {
                    CSceneLocation *location = m_handler->GetLocation(location_name);

                    if(location != NULL)
                    {
                        m_current = new CSceneDetail(location, index);
                        m_details.Add(m_current);
                    }
                    else
                    {
                        m_current = NULL;
                    }
                }
            }
            else if(memcmp(buf, "Check:", 6) == 0)
            {
                if(m_current != NULL)
                    m_current->AddCondition(buf+6);
            }
        }
    }

    if(f != NULL)
        fclose(f);

    return m_current != NULL;
}

void CTexElementAudio::Override(int n, GrVertex *vlist)
{
    CPolyCondition::CPoly poly(n, vlist);
    INT_PTR                   count, i;
    
    count  = m_details.GetSize();
    
    for(i = 0; i < count; i++)
        m_details[i]->Trigger(poly);
}

void CTexElementAudio::Reset()
{
}
