// BmpWriter.cpp: implementation of the CBmpWriter class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BmpWriter.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBmpWriter::CBmpWriter(GrTextureFormat_t fmt, int width, int height)
{
    m_fmt    = fmt;
    m_width  = width;
    m_height = height;
    m_data   = NULL;
}

CBmpWriter::~CBmpWriter()
{
    delete m_data;
}

void CBmpWriter::SetPalette(GuTexPalette *pal)
{
    m_pal = *pal;
}

void CBmpWriter::SetPixels(char *data)
{
    m_data = new char[m_width * m_height];
    memcpy(m_data, data, m_width * m_height);
}

void CBmpWriter::Write(CString &path, CBox *box)
{
    CBox all(0, m_width, 0, m_height);
    if(box == NULL) box = &all;

    switch(m_fmt)
    {
    case GR_TEXFMT_P_8:
        {
            FILE *f = fopen((LPCSTR)path, "wb");
            if(f != NULL)
            {
                BITMAPFILEHEADER hdr;
                BITMAPINFOHEADER info;
                int i;
                int width   = box->m_xmax - box->m_xmin;
                int height  = box->m_ymax - box->m_ymin;
                int padding = (width & 3);

                if(padding > 0)
                    padding = 4 - padding;
                
                ((char *)&hdr.bfType)[0] = 'B';
                ((char *)&hdr.bfType)[1] = 'M';
                
                hdr.bfOffBits = sizeof(BITMAPFILEHEADER)
                    + sizeof(BITMAPINFOHEADER)
                    + 256 * sizeof(RGBQUAD);
                
                hdr.bfSize = hdr.bfOffBits
                    + (width + padding) * height;
                
                hdr.bfReserved1 = hdr.bfReserved2 = 0;
                
                info.biSize = sizeof(BITMAPINFOHEADER);
                info.biWidth = width;
                info.biHeight = height;
                info.biPlanes = 1;
                info.biBitCount = 8;
                info.biCompression = BI_RGB;
                info.biSizeImage = 0;
                info.biXPelsPerMeter = info.biYPelsPerMeter = 3200;
                info.biClrUsed = info.biClrImportant = 0;
                
                fwrite(&hdr, 1, sizeof(BITMAPFILEHEADER), f);
                fwrite(&info, 1, sizeof(BITMAPINFOHEADER), f);
                
                for(i = 0; i < 256; i++)
                {
                    fwrite(&(m_pal.data[i]), 1, 4, f);
                }
                
                char *row = m_data + m_width * box->m_ymax + box->m_xmin;
                for(i = 0; i < height; i++)
                {
                    int pad = 0;

                    row -= m_width;
                    fwrite(row, 1, width, f);

                    if(padding > 0)
                        fwrite(&pad, 1, padding, f);
                }
                
                fclose(f);
            }
        }
        break;
    }
}
