// PropDisplay.cpp : implementation file
//

#include "stdafx.h"
#include <math.h>
#include "gldsrv.h"
#include "PropDisplay.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropDisplay property page

IMPLEMENT_DYNCREATE(CPropDisplay, CPropertyPage)

CPropDisplay::CPropDisplay() : CPropertyPage(CPropDisplay::IDD)
{
	//{{AFX_DATA_INIT(CPropDisplay)
	m_full_screen = FALSE;
	m_gamma_display = _T("");
	m_name = _T("");
	m_smooth = FALSE;
	m_driver = -1;
	m_mipmaps = FALSE;
	//}}AFX_DATA_INIT
}

CPropDisplay::~CPropDisplay()
{
}

void CPropDisplay::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropDisplay)
	DDX_Control(pDX, IDC_16BIT_COLOUR, m_16bit_colour);
	DDX_Control(pDX, IDC_RESOLUTION, m_resolution);
	DDX_Control(pDX, IDC_NO_MODE_CHANGE, m_no_mode_change);
	DDX_Control(pDX, IDC_MANAGED_TEXTURES, m_managed_textures);
	DDX_Control(pDX, IDC_SLIDER, m_slider);
	DDX_Check(pDX, IDC_FULL_SCREEN, m_full_screen);
	DDX_Text(pDX, IDC_GAMMA, m_gamma_display);
	DDX_Text(pDX, IDC_NAME, m_name);
	DDX_Check(pDX, IDC_SMOOTH, m_smooth);
	DDX_Radio(pDX, IDC_DRIVER, m_driver);
	DDX_Check(pDX, IDC_MIPMAPS, m_mipmaps);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropDisplay, CPropertyPage)
	//{{AFX_MSG_MAP(CPropDisplay)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_DRIVER2, OnDriver2)
	ON_BN_CLICKED(IDC_DRIVER, OnDriver1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropDisplay message handlers

void CPropDisplay::Init(CGameRecord *rec)
{
    m_rec = rec;
}

void CPropDisplay::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
    m_gamma = pow(1.01, (double) (2 * m_slider.GetPos()));
    m_gamma_display.Format("%.2f", m_gamma);
    UpdateData(FALSE);
	
	CPropertyPage::OnHScroll(nSBCode, nPos, pScrollBar);
}

BOOL CPropDisplay::OnSetActive() 
{
    char *res[] = {"2880x2160", "1600x1200","1280x1024","1024x768","800x600","640x480",NULL};
    int   i;

    m_name         = m_rec->name;
    m_full_screen  = m_rec->full_screen;

    m_resolution.ResetContent();

    for(i = 0; res[i] != NULL; i++)
        m_resolution.AddString(res[i]);

    m_resolution.SetWindowTextA((CString)m_rec->resolution);

    m_gamma        = m_rec->gamma;
    m_smooth       = m_rec->forced_smoothing;
    m_driver       = m_rec->driver;
    m_mipmaps      = m_rec->mipmaps;
	
    UpdateButtons();

    m_slider.SetRange(-50, 50);
    m_slider.SetTic(0);

    m_slider.SetPos(25);
    m_slider.SetPos((int)(log(m_gamma) / log(1.01) / 2.0));

    m_gamma_display.Format("%.2f", m_gamma);

    UpdateData(FALSE);

    CPropertySheet *sheet = (CPropertySheet *) GetParent();

    sheet->SetWizardButtons(PSWIZB_NEXT);

	return CPropertyPage::OnSetActive();
}

BOOL CPropDisplay::OnKillActive() 
{
    UpdateData();

    if(m_name.IsEmpty())
    {
        AfxMessageBox("Name required", MB_OK);
        return FALSE;
    }

    m_rec->name             = m_name;
    m_rec->full_screen      = (m_full_screen != FALSE);
    CString resStr;
    m_resolution.GetWindowTextA(resStr);
    CResolution res(resStr);
    if (!res.IsOkay())
    {
        CString msg;
        msg.Format("Not a valid resolution: %s", resStr);
        AfxMessageBox(msg, MB_OK);
        return FALSE;
    }
    m_rec->resolution       = res;
    m_rec->gamma            = m_gamma;
    m_rec->forced_smoothing = (m_smooth != FALSE);
    m_rec->driver           = (CGlideDriver::GlideDriver) m_driver;
    m_rec->mipmaps          = (m_mipmaps != FALSE);

    if(m_driver == CGlideDriver::GLIDE_DRIVER_D3D)
    {
        m_rec->colour_16bit     = (m_16bit_colour.GetCheck()     != FALSE);
        m_rec->managed_textures = (m_managed_textures.GetCheck() != FALSE);
        m_rec->no_mode_change   = (m_no_mode_change.GetCheck()   != FALSE);
    }
    else if(!res.IsStandard())
    {
        AfxMessageBox("Only psVoodoo supports non-standard resolutions");
        return FALSE;
    }

	return CPropertyPage::OnKillActive();
}

void CPropDisplay::UpdateButtons()
{
    m_16bit_colour.EnableWindow(m_driver == CGlideDriver::GLIDE_DRIVER_D3D);
    m_managed_textures.EnableWindow(m_driver == CGlideDriver::GLIDE_DRIVER_D3D);
    m_no_mode_change.EnableWindow(m_driver == CGlideDriver::GLIDE_DRIVER_D3D);
    m_16bit_colour.SetCheck(m_driver == CGlideDriver::GLIDE_DRIVER_D3D && m_rec->colour_16bit);
    m_managed_textures.SetCheck(m_driver == CGlideDriver::GLIDE_DRIVER_D3D && m_rec->managed_textures);
    m_no_mode_change.SetCheck(m_driver == CGlideDriver::GLIDE_DRIVER_D3D && m_rec->no_mode_change);
}

void CPropDisplay::OnDriver1() 
{
    UpdateData();
	
    UpdateButtons();
}

void CPropDisplay::OnDriver2() 
{
    UpdateData();

    UpdateButtons();
}
