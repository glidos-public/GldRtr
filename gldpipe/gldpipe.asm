;*********************************************************** RCS
; Copyright (c) Paul Gardiner.
;
; Glide router component.
; 
; $Id$
; $Date$
;
; $Log$
; Revision 1.2  2001/06/20 16:09:28  paul
; Changed from using ebx to edx for second argument to match the call
; semantics of Watcom using -3r
;
; Revision 1.1  2001/06/19 16:08:42  paul
; Initial version
;
;***********************************************************/


PAGE 58,132
;******************************************************************************
TITLE CONTROL - ControlDispatch for Glide pipe
;******************************************************************************
;

    .386p

;******************************************************************************
;                I N C L U D E S
;******************************************************************************

    .xlist
    include vmm.inc
    include debug.inc
    .list


;============================================================================
;        V I R T U A L   D E V I C E   D E C L A R A T I O N
;============================================================================

DECLARE_VIRTUAL_DEVICE    GLDPIPE1, 1, 0, GLDPIPE_Control, UNDEFINED_DEVICE_ID, \
                        UNDEFINED_INIT_ORDER, GLDPIPE_VM, GLDPIPE_PM

VxD_LOCKED_CODE_SEG


extrn _GLDPIPE_VMAPI@12:near
extrn _GLDPIPE_PMAPI@12:near

;
; This left in just to get it into cvs
;
;extrn _HANDLE_INT21API@16:near


BeginProc GLDPIPE_Control
    Control_Dispatch SYS_DYNAMIC_DEVICE_INIT, GLDPIPE_Dynamic_Init, sCall
    Control_Dispatch SYS_DYNAMIC_DEVICE_EXIT, GLDPIPE_Dynamic_Exit, sCall
    Control_Dispatch W32_DEVICEIOCONTROL,     GLDPIPE_W32_DeviceIOControl, sCall, <ecx, ebx, edx, esi>
    clc
    ret
EndProc GLDPIPE_Control

; *************************************************************************
; this routine handles api calls made from V86 mode programs.  For the 
; purpose of this examples it is assumed that the function number is passed
; in eax and any additional parameters are passed in ebx and ecx.
; The C code will do a SWITCH on the eax value to determine which routine to
; call and what parameters to pass.  It is assumed that all functions return
; a value in eax.
BeginProc GLDPIPE_VM
    scall   GLDPIPE_VMAPI, <[ebp].Client_EAX, [ebp].Client_EBX, [ebp].Client_ECX>
    mov    [ebp].Client_EAX,eax    ; put return code
    ret
EndProc   GLDPIPE_VM



; *************************************************************************
; this routine handles api calls made from protected mode programs.  For the 
; purpose of this examples it is assumed that the function number is passed
; in eax and any additional parameters are passed in ebx and ecx.
; The C code will do a SWITCH on the eax value to determine which routine to
; call and what parameters to pass.  It is assumed that all functions return
; a value in eax.
BeginProc GLDPIPE_PM
    scall   GLDPIPE_PMAPI, <[ebp].Client_EAX, [ebp].Client_EDX, [ebp].Client_ECX>
    mov     [ebp].Client_EAX,eax
    ret
EndProc   GLDPIPE_PM

;BeginProc _HANDLE_INT21
;    push    eax
;    push    ecx
;    push    edx
;    Client_Ptr_Flat edx, DS, DX
;    scall   HANDLE_INT21API, <eax, ebx, ebp, edx>
;    pop     edx
;    mov     ecx, 0h
;    cmp     ecx, eax
;    pop     ecx
;    pop     eax
;    ret
;EndProc   _HANDLE_INT21

VxD_LOCKED_CODE_ENDS

;
; Real Mode Init segment: If this VxD is statically loaded a message
; will appear in Real Mode before windows is started.
;
VxD_REAL_INIT_SEG

Load_Success_Msg  DB  'GLDPIPE1: Real Mode Init called',13,10
Load_Msg_Len      EQU $ - Load_Success_Msg

BeginProc GLDPIPE_Real_Init

    mov    ah, 40h
    mov    bx, 1
    mov    cx, Load_Msg_Len
    mov    dx, OFFSET Load_Success_Msg
    int    21h

    xor    bx, bx
    xor    si, si
    xor    edx, edx
    mov    ax, Device_Load_Ok
    ret

EndProc GLDPIPE_Real_Init

VxD_REAL_INIT_ENDS

    END GLDPIPE_Real_Init

