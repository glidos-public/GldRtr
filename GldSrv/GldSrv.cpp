/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide server
 * 
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.6  2006/12/02 17:17:21  paul
 * German translation vy Volker
 *
 * Revision 1.5  2006/04/09 11:12:46  paul
 * Create a version that displays a stack trace if it crashes
 *
 * Revision 1.4  2004/10/12 13:59:53  paul
 * Role the server into the main thread.  Improves pumping of messages and cures the momentary freezes with D2 on ATI cards
 *
 * Revision 1.3  2003/05/26 09:08:21  paul
 * Add French version of app
 *
 * Revision 1.2  2001/06/20 16:17:01  paul
 * Send "abort" command to VXD on shutdown to stop the server thread
 * hanging in a get call.
 *
 * Add buttons for shutdown, and saving the edit box.
 *
 * Us commands in protocol, only GrGlideInit at this stage.
 *
 ************************************************************/

// GldSrv.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "GldSrv.h"
#include "GldSrvDlg.h"
#ifdef STACKWALK
#include "DebugHelp.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef STACKWALK
static void trans_func(unsigned int u, EXCEPTION_POINTERS *pExp)
{
    static CDebugHelp debughelp;

    throw debughelp.Trace(u, pExp);
}
#endif

/////////////////////////////////////////////////////////////////////////////
// CGldSrvApp

BEGIN_MESSAGE_MAP(CGldSrvApp, CWinApp)
	//{{AFX_MSG_MAP(CGldSrvApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGldSrvApp construction

CGldSrvApp::CGldSrvApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGldSrvApp object

CGldSrvApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CGldSrvApp initialization

BOOL CGldSrvApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef STACKWALK
    _set_se_translator(trans_func);
#endif

    m_resDLL = LoadLibrary("GldSrvGER.dll");

    if(m_resDLL == 0)
        m_resDLL = LoadLibrary("GldSrvFRA.dll");

    if(m_resDLL != 0)
        AfxSetResourceHandle(m_resDLL);

    CoInitialize(NULL);

	CGldSrvDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

    CoUninitialize();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CGldSrvApp::ExitInstance() 
{
    if(m_resDLL != 0)
        FreeLibrary(m_resDLL);

	return CWinApp::ExitInstance();
}
