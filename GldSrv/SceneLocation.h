// SceneLocation.h: interface for the CSceneLocation class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCENELOCATION_H__78B39C6F_CE3F_46E9_94F8_6E53C9BCFA03__INCLUDED_)
#define AFX_SCENELOCATION_H__78B39C6F_CE3F_46E9_94F8_6E53C9BCFA03__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PlayAudioFile.h"

class CSceneLocation  
{
private:
    CString      m_file;
    unsigned int m_initial_set;
    unsigned int m_current_set;
    bool         m_triggered;

public:
	CSceneLocation();
	virtual ~CSceneLocation();

    bool Load(const CString &folder, const CString &path);
    void Trigger(int index);
    void TestPeriodEnd(CPlayAudioFile *player);
};

#endif // !defined(AFX_SCENELOCATION_H__78B39C6F_CE3F_46E9_94F8_6E53C9BCFA03__INCLUDED_)
