// Image.cpp: implementation of the CImage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Image.h"
#include "ImageBMP.h"
#include "ImagePNG.h"

extern "C"
{
#include "png.h"
}

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImage::CImage()
{

}

CImage::~CImage()
{

}

CImage *CImage::Load(CString &path)
{
    FILE         *f;
    unsigned char buf[8];
    CImage       *img;

    f = fopen((LPCSTR)path, "rb");

    if(f == NULL)
        return NULL;

    fread(buf, 1, sizeof(buf), f);

    img = CImageBMP::TryLoad(f, buf, sizeof(buf));
    if(img != NULL)
        return img;

    img = CImagePNG::TryLoad(f, buf, sizeof(buf));
    if(img != NULL)
        return img;

    fclose(f);

    return NULL;
}

bool CImage::HasAlpha()
{
    return false;
}
