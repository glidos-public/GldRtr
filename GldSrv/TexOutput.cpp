// TexOutput.cpp: implementation of the CTexHandlerOutput class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TexOutput.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "TexExploder.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexHandlerOutput::CTexHandlerOutput(LPCSTR folder) : m_folder(folder)
{
	::CreateDirectory((LPCSTR)GetFolder(), NULL);
}

CTexHandlerOutput::~CTexHandlerOutput()
{
}

void CTexHandlerOutput::GrTexDownloadTable(GuTexPalette *pal)
{
    m_pal = *pal;
}

CTexPartition *CTexHandlerOutput::NewTexPartition(GrTextureFormat_t fmt, int width, int height, GrLOD_t lod)
{
    return new CTexPartitionOutput(fmt, width, height);
}

void CTexHandlerOutput::SetupTexPartition(CTexPartition *partition, unsigned char hash[], char *data)
{
    ((CTexPartitionOutput *)partition)->Initialise(GetMappingFile(), hash, &m_pal, data);
}

CString CTexHandlerOutput::GetFolder()
{
    return m_folder;
}
