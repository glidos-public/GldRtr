/************************************************************ RCS
 * Copyright (c) Paul Gardiner.
 *
 * Glide server
 * 
 * $Id$
 * $Date$
 *
 * $Log$
 * Revision 1.1  2002/06/24 15:27:55  paul
 * VDD that provides a polling interface to the pipe to
 * allow timer ticks to continue.  The same VDD can
 * also start a thread to simulate an IRQ 0 every millisec,
 * which seems to fix the Descent II control judder.
 *
 * Revision 1.2  2001/06/20 16:17:01  paul
 * Send "abort" command to VXD on shutdown to stop the server thread
 * hanging in a get call.
 *
 * Add buttons for shutdown, and saving the edit box.
 *
 * Us commands in protocol, only GrGlideInit at this stage.
 *
 ************************************************************/

// Thread.cpp: implementation of the CThread class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Thread.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CThread::CThread()
{
	m_thread = NULL;
	m_dying = false;
}

CThread::~CThread()
{
	if(m_thread)
	{
		Kill();
		Wait();
		CloseHandle(m_thread);
	}
}

void CThread::Start()
{
    m_thread = (HANDLE) _beginthreadex(NULL, 0, Hook, this, 0, &m_nThreadID);
}

void CThread::Kill()
{
	m_dying = true;
	if(m_thread) ::PostThreadMessage(m_nThreadID, WM_USER_KILL_THREAD, 0, 0);
}

void CThread::Wait()
{
	MSG msg;

	BOOL going = (m_thread != NULL);
	while(going)
	{
		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			DispatchMessage(&msg);

		if(MsgWaitForMultipleObjects(1, &m_thread, false, INFINITE, QS_ALLINPUT) == 0)
			going = false;
	}
}

UINT CThread::Hook(LPVOID arg)
{
	((CThread *) arg)->Execute();
	return 0;
}

bool CThread::Dying()
{
	return m_dying;
}
