// W98Pipe.cpp: implementation of the CW98Pipe class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "str_code.h"
#include "GldSrv.h"
#include "W98Pipe.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define GLDPIPE_READ (1)
#define GLDPIPE_ABORT (2)
#define GLDPIPE_STATE (3)

CW98Pipe::CW98Pipe()
{
    m_pipe = INVALID_HANDLE_VALUE;
    m_epip = INVALID_HANDLE_VALUE;
    overlapped.hEvent = INVALID_HANDLE_VALUE;
}

CW98Pipe::~CW98Pipe()
{
    if(m_pipe != INVALID_HANDLE_VALUE)
        CloseHandle(m_pipe);

    if(m_epip != INVALID_HANDLE_VALUE)
        CloseHandle(m_epip);

    if(overlapped.hEvent != INVALID_HANDLE_VALUE)
        CloseHandle(overlapped.hEvent);
}

bool CW98Pipe::Create()
{
    static const char pname[] = {STRCODE('\\','\\','.','\\'),STRCODE('G','L','D','P'),
        STRCODE('I','P','E','1'),STRCODE('.','V','X','D'),STRCODE(0,0,0,0)};
    static const char qname[] = {STRCODE('\\','\\','.','\\'),STRCODE('E','P','I','P'),
        STRCODE('D','L','G','1'),STRCODE('.','V','X','D'),STRCODE(0,0,0,0)};
    char pnbuf[20];

    str_decode(pnbuf, pname);
    m_pipe = CreateFile(pnbuf, 0,0,0,
        CREATE_NEW, FILE_FLAG_DELETE_ON_CLOSE|FILE_FLAG_OVERLAPPED, 0);

    str_decode(pnbuf, qname);
    m_epip = CreateFile(pnbuf, 0,0,0,
        CREATE_NEW, FILE_FLAG_DELETE_ON_CLOSE|FILE_FLAG_OVERLAPPED, 0);

    overlapped.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    return m_pipe                  != INVALID_HANDLE_VALUE
		&& m_epip                  != INVALID_HANDLE_VALUE
		&& overlapped.hEvent       != INVALID_HANDLE_VALUE;
}

bool CW98Pipe::Read(char *buf, int requested, unsigned long *returned)
{
    if(m_pipe == INVALID_HANDLE_VALUE)
        return false;

    overlapped.Offset = 0;
    overlapped.OffsetHigh = 0;

    if(DeviceIoControl(m_pipe, GLDPIPE_READ, NULL, 0, (LPVOID)buf, requested, NULL, &overlapped))
	{
		GetOverlappedResult(m_pipe, &overlapped, returned, FALSE);
	}
	else
	{
		if(GetLastError() == ERROR_IO_PENDING)
		{
			if(m_shutdown)
			{
				*returned = 0;
				return false;
			}
			
			PumpWhileWaiting(overlapped.hEvent);
			GetOverlappedResult(m_pipe, &overlapped, returned, FALSE);
		}
		else
		{
		    return false;
		}
		
		ResetEvent(overlapped.hEvent);
	}

	return !m_shutdown;
}

void CW98Pipe::Wake()
{
    unsigned long cbBytesReturned;
    if(m_pipe != INVALID_HANDLE_VALUE)
        DeviceIoControl(m_pipe, GLDPIPE_ABORT, NULL, 0, NULL, 0, &cbBytesReturned, NULL);
    if(m_epip != INVALID_HANDLE_VALUE)
        DeviceIoControl(m_epip, GLDPIPE_ABORT, NULL, 0, NULL, 0, &cbBytesReturned, NULL);
}

CW98Pipe *CW98Pipe::NewPipe()
{
    CW98Pipe *res = new CW98Pipe();

    if(res == NULL)
        return NULL;

    if(res->Create())
    {
        return res;
    }
    else
    {
        delete res;
        return NULL;
    }
}

bool CW98Pipe::Write(char *buf, int n)
{
	DWORD dummy;

    if(m_epip == INVALID_HANDLE_VALUE)
        return false;
	
    return !m_shutdown && TRUE == DeviceIoControl(m_epip, GLDPIPE_READ, NULL, 0, (LPVOID)buf, n, &dummy, NULL);
}
