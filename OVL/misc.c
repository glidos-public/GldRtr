
/* We don't do anything with floating point
 * arithmetic other than pass values back and
 * forth between us and the server, but this
 * symbol needs defining because we aren't
 * building in the usual way */
int _8087;

int ds_copy;

extern void store_ds(void);
#pragma aux store_ds =             \
    "mov dx, ds"                   \
    "mov ds_copy, edx"             \
    modify [edx];

extern void retr_ds(void);
#pragma aux retr_ds =              \
    "push edx"                     \
    "mov edx, cs:ds_copy"          \
    "mov ds, dx"                   \
    "pop edx";


void __pascal __GETDS(void)
{
    retr_ds();
}

void misc_init(void)
{
    store_ds();
}
