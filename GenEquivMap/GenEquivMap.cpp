// GenEquivMap.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "GenEquivMap.h"
#include "PropertyPageCapture.h"
#include "PropertyPageGenerate.h"
#include "PropertyPageJoin.h"
#include "PropertyPageCheck.h"
#include "PropertyPageList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGenEquivMapApp

BEGIN_MESSAGE_MAP(CGenEquivMapApp, CWinApp)
	//{{AFX_MSG_MAP(CGenEquivMapApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGenEquivMapApp construction

CGenEquivMapApp::CGenEquivMapApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGenEquivMapApp object

CGenEquivMapApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CGenEquivMapApp initialization

BOOL CGenEquivMapApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

    CPropertySheet        sheet;
    CPropertyPageCapture  capture;
    CPropertyPageGenerate generate;
    CPropertyPageJoin     join;
    CPropertyPageCheck    check;
    CPropertyPageList     list;

    capture.m_psp.dwFlags &= ~PSP_HASHELP;
    generate.m_psp.dwFlags &= ~PSP_HASHELP;
    join.m_psp.dwFlags &= ~PSP_HASHELP;
    check.m_psp.dwFlags &= ~PSP_HASHELP;
    list.m_psp.dwFlags &= ~PSP_HASHELP;

	m_pMainWnd = &sheet;

    sheet.SetTitle("Equivalence Map Generator v1.07");

    sheet.AddPage(&capture);
    sheet.AddPage(&generate);
    sheet.AddPage(&join);
    sheet.AddPage(&check);
    sheet.AddPage(&list);

    sheet.m_psh.dwFlags &= ~PSH_HASHELP;
    sheet.m_psh.dwFlags |= PSH_NOAPPLYNOW;
	int nResponse = sheet.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
