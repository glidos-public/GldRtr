// TexCache.h: interface for the CTexCache class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXCACHE_H__15E945BB_5CA7_40DF_8D50_09BE0F6906D9__INCLUDED_)
#define AFX_TEXCACHE_H__15E945BB_5CA7_40DF_8D50_09BE0F6906D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "sdk2_glide.h"
#include "GlideDll.h"

#define MAX_DATA_SIZE (16*16*2)
#define NCACHE (256)

class CTexCache  
{
private:
    struct CacheRec
    {
        GrTexInfo     info;
        unsigned char data[MAX_DATA_SIZE];
    };

    struct TexInfo
    {
        FxU32     startAddress;
        FxU32     evenOdd;
        GrTexInfo info;
    };

    CGlideDll *m_wrapper;
    CacheRec  m_cache[NCACHE];
    int       m_ncache;
    TexInfo   m_sourceInfo;
    TexInfo   m_downloadInfo;
    bool      m_source_is_small;
	bool      m_download_buffered;
	bool      m_download_unwritten;
    FxU32     m_lfb_space;
    unsigned char m_data[MAX_DATA_SIZE];

    static void DataSize(GrTexInfo *info, int *x, int *y, int *bpp);

public:
	void PreRender(int n, GrVertex *vlist);
	void TexDownloadMipMap(FxU32 startAddress, FxU32 evenOdd, GrTexInfo *info);
	void TexSource(FxU32 startAddress, FxU32 evenOdd, GrTexInfo *info);
	CTexCache(CGlideDll *wrapper);
	virtual ~CTexCache();

};

#endif // !defined(AFX_TEXCACHE_H__15E945BB_5CA7_40DF_8D50_09BE0F6906D9__INCLUDED_)
