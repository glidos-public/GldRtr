#include "xms.h"

extern int xms_present(void);
#pragma aux xms_present = \
    "mov ax, 4300h"       \
    "int 2fh"             \
    "mov bx, 1"           \
    "cmp al, 80h"         \
    "je ok"               \
    "mov bx, 0"           \
    "ok:"                 \
    value [bx]            \
    modify [ax];

extern void __far *xms_open(void);
#pragma aux xms_open = \
    "mov ax, 4310h"    \
    "push es"          \
    "int 2fh"          \
    "mov cx, es"       \
    "pop es"           \
    value [cx bx]      \
    modify [ax];

extern int xms_version(void __far *xms);
#pragma aux xms_version = \
    "mov ax, 0"           \
    "push bp"             \
    "push cx"             \
    "push bx"             \
    "mov bp, sp"          \
    "callf [bp]"          \
    "pop bx"              \
    "pop cx"              \
    "pop bp"              \
    parm [cx bx]          \
    value [ax]            \
    modify [dx];

extern unsigned int xms_maxblock(void __far *xms);
#pragma aux xms_maxblock = \
    "mov ax, 800h"         \
    "push bp"              \
    "push cx"              \
    "push bx"              \
    "mov bp, sp"           \
    "callf [bp]"           \
    "pop bx"               \
    "pop cx"               \
    "pop bp"               \
    parm [cx bx]           \
    value [ax]             \
    modify [dx];

extern int xms_allocate(void __far *xms, int size, int __far *block);
#pragma aux xms_allocate =        \
    "mov ax, 900h"                \
    "push bp"                     \
    "push cx"                     \
    "push bx"                     \
    "mov bp, sp"                  \
    "callf [bp]"                  \
    "pop bx"                      \
    "pop cx"                      \
    "pop bp"                      \
    "push es"                     \
    "mov es, di"                  \
    "mov es:[si], dx"             \
    "pop es"                      \
    parm [cx bx] [dx] [di si]     \
    value [ax]                    \
    modify [dx];

typedef struct
{
    unsigned long int Length;
    unsigned      int SourceHandle;
    unsigned long int SourceOffset;
    unsigned      int DestHandle;
    unsigned long int DestOffset;
} ExtMemMoveStruct;

extern void xms_move(void __far *xms, ExtMemMoveStruct __far *emms);
#pragma aux xms_move =    \
    "mov ax, 0b00h"       \
    "push ds"             \
    "push bp"             \
    "push cx"             \
    "push bx"             \
    "mov bp, sp"          \
    "mov ds, di"          \
    "callf [bp]"          \
    "pop bx"              \
    "pop cx"              \
    "pop bp"              \
    "pop ds"              \
    parm [cx bx] [di si]  \
    modify [ax];

static ExtMemMoveStruct memMoveStruct;

int Xms_present(void)
{
    return xms_present();
}

Xms *Xms_open(void)
{
    return xms_open();
}

int Xms_version(Xms *xms)
{
    return xms_version(xms);
}

unsigned int Xms_maxblock(Xms *xms)
{
    return xms_maxblock(xms);
}

int Xms_allocate(Xms *xms, int size, int __far *block)
{
    return xms_allocate(xms, size, block);
}

void Xms_read(Xms              *xms,
              unsigned int      block,
              long unsigned int block_offset,
              void __far       *buf,
              long unsigned int length)
{
    memMoveStruct.Length = length;
    memMoveStruct.SourceHandle = block;
    memMoveStruct.SourceOffset = block_offset;
    memMoveStruct.DestHandle   = 0;
    *((void __far **)&memMoveStruct.DestOffset) = buf;

    xms_move(xms, &memMoveStruct);
}

void Xms_write(Xms              *xms,
               unsigned int      block,
               long unsigned int block_offset,
               void __far       *buf,
               long unsigned int length)
{
    memMoveStruct.Length = length;
    memMoveStruct.DestHandle = block;
    memMoveStruct.DestOffset = block_offset;
    memMoveStruct.SourceHandle   = 0;
    *((void __far **)&memMoveStruct.SourceOffset) = buf;

    xms_move(xms, &memMoveStruct);
}
