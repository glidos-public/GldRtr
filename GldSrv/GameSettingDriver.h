// GameSettingDriver.h: interface for the CGameSettingDriver class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAMESETTINGDRIVER_H__905C6CDB_0BD5_423D_A60D_4A460C5F277A__INCLUDED_)
#define AFX_GAMESETTINGDRIVER_H__905C6CDB_0BD5_423D_A60D_4A460C5F277A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GameSetting.h"

class CGlideDriver
{
public:
    enum GlideDriver
    {
        GLIDE_DRIVER_OGL,
        GLIDE_DRIVER_D3D
    };

protected:
    GlideDriver m_value;

public:
    operator GlideDriver()
    {
        return m_value;
    };
};


class CGameSettingDriver : public CGameSetting<CGlideDriver>
{
public:
	CString Path();
    void                 Init(CStoredStruct *st, const char *name);
    CGameSettingDriver &operator=(const GlideDriver &i);

private:
	bool    Parse(CString &val);
	CString Print();
};

#endif // !defined(AFX_GAMESETTINGDRIVER_H__905C6CDB_0BD5_423D_A60D_4A460C5F277A__INCLUDED_)
