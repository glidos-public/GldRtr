#if !defined(AFX_PROPERTYPAGELIST_H__9EBE92AD_9282_434F_9E90_4CEE439B0AF0__INCLUDED_)
#define AFX_PROPERTYPAGELIST_H__9EBE92AD_9282_434F_9E90_4CEE439B0AF0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropertyPageList.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageList dialog

#include "PropertyPageBase.h"

class CPropertyPageList : public CPropertyPageBase
{
	DECLARE_DYNCREATE(CPropertyPageList)

// Construction
public:
	CPropertyPageList();
	~CPropertyPageList();

// Dialog Data
	//{{AFX_DATA(CPropertyPageList)
	enum { IDD = IDD_PROPPAGE_LIST };
	CString	m_folder;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropertyPageList)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyPageList)
	afx_msg void OnButtonBrowseFolder();
	afx_msg void OnButtonList();
	//}}AFX_MSG
    void OnUpdateListButton(CCmdUI *pCmdUI);
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTYPAGELIST_H__9EBE92AD_9282_434F_9E90_4CEE439B0AF0__INCLUDED_)
