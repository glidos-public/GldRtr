// CachedPipe.cpp: implementation of the CCachedPipe class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "CachedPipe.h"
#include "W98Pipe.h"
#include "W2KPipe.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCachedPipe::CCachedPipe()
{
    m_pipe          = NULL;
    m_pipe_cache    = NULL;
    m_pipe_index    = 0;
    m_pipe_used     = 0;
    m_shutting_down = false;
}

CCachedPipe::~CCachedPipe()
{
    ASSERT(m_pipe == NULL);
    ASSERT(m_pipe_cache == NULL);
}

void CCachedPipe::ReadPipe(char *buf, int count)
{
    ASSERT(m_pipe != NULL);
    ASSERT(m_pipe_cache != NULL);

    while(count && !m_shutting_down)
    {
        if(m_pipe_index >= m_pipe_used)
        {
            unsigned long n_returned;

            BOOL res = m_pipe->Read(m_pipe_cache, PIPE_CACHE_SIZE, &n_returned);
            
            if(!res)
			{
				CString s;
				s.Format("Read failed: %d", GetLastError());
                throw s;
			}

            m_pipe_used = n_returned;

            m_pipe_index = 0;
        }

        int available = min(count, m_pipe_used-m_pipe_index);

        if(available)
        {
            memcpy(buf, m_pipe_cache+m_pipe_index, available);
            buf += available;
            count -= available;
            m_pipe_index += available;
        }
    }

    if(m_shutting_down)
        throw CString("");
}

CCachedPipe::OSType CCachedPipe::Create()
{
    m_pipe_index    = 0;
    m_pipe_used     = 0;
    m_shutting_down = false;
    m_pipe_cache    = new char[PIPE_CACHE_SIZE];

    if(m_pipe_cache == NULL)
        return CCachedPipe::OS_NONE;

    m_pipe = CW98Pipe::NewPipe();
	
	if(m_pipe != NULL)
		return CCachedPipe::OS_W98;

    m_pipe = CW2KPipe::NewPipe();

    if(m_pipe != NULL)
        return CCachedPipe::OS_W2K;

    delete [] m_pipe_cache;
    m_pipe_cache = NULL;

    return CCachedPipe::OS_NONE;
}

void CCachedPipe::Wake()
{
    m_shutting_down = true;

    if(m_pipe != NULL)
        m_pipe->Wake();
}

bool CCachedPipe::Write(char *buf, int n)
{
    ASSERT(m_pipe != NULL);

    return m_pipe->Write(buf, n);
}

void CCachedPipe::Destroy()
{
    delete    m_pipe;
    delete [] m_pipe_cache;

    m_pipe          = NULL;
    m_pipe_cache    = NULL;
    m_pipe_index    = 0;
    m_pipe_used     = 0;
    m_shutting_down = false;
}
