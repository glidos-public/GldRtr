// PropExecutable.cpp : implementation file
//

#include "stdafx.h"
#include "gldsrv.h"
#include "PropExecutable.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPropExecutable property page

IMPLEMENT_DYNCREATE(CPropExecutable, CPropertyPage)

CPropExecutable::CPropExecutable() : CPropertyPage(CPropExecutable::IDD)
{
	//{{AFX_DATA_INIT(CPropExecutable)
	m_exe_path = _T("");
	m_sizes = _T("");
    m_subfolders = 0;
	//}}AFX_DATA_INIT
}

CPropExecutable::~CPropExecutable()
{
}

void CPropExecutable::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropExecutable)
	DDX_Text(pDX, IDC_EXE_PATH, m_exe_path);
	DDX_Text(pDX, IDC_SIZES, m_sizes);
    DDX_Text(pDX, IDC_SUBFOLDERS, m_subfolders);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropExecutable, CPropertyPage)
	//{{AFX_MSG_MAP(CPropExecutable)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_EXEC, OnButtonBrowseExec)
	//}}AFX_MSG_MAP
    ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN, &CPropExecutable::OnDeltaposSpin)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropExecutable message handlers

void CPropExecutable::Init(CGameRecord *rec)
{
    m_rec = rec;
}

void CPropExecutable::OnButtonBrowseExec() 
{
    CFileDialog dlg(TRUE, NULL, NULL, OFN_NOCHANGEDIR);

    if(dlg.DoModal() == IDOK)
    {
        m_exe_path = dlg.GetPathName();
        UpdateData(FALSE);
    }
}

BOOL CPropExecutable::OnSetActive() 
{
    m_exe_path = m_rec->executable;
    m_sizes    = m_rec->sizes.Print();
    m_subfolders = m_rec->subfolders;
    
    UpdateData(FALSE);

    CPropertySheet *sheet = (CPropertySheet *) GetParent();

    sheet->SetWizardButtons(PSWIZB_FINISH|PSWIZB_BACK);

	return CPropertyPage::OnSetActive();
}

BOOL CPropExecutable::OnKillActive() 
{
    UpdateData();

    if(m_exe_path.IsEmpty())
    {
        AfxMessageBox("Path for executable required", MB_OK);

        return FALSE;
    }

    if(!m_rec->sizes.Parse(m_sizes))
    {
        AfxMessageBox("Sizes must be a comma separated list of numbers");

        return FALSE;
    }

    m_rec->executable = m_exe_path;
    m_rec->subfolders = m_subfolders;
	
	return CPropertyPage::OnKillActive();
}

BOOL CPropExecutable::OnWizardFinish() 
{
    if(OnKillActive() == FALSE)
        return FALSE;

	return CPropertyPage::OnWizardFinish();
}

void CPropExecutable::OnDeltaposSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
    int subfolders = m_subfolders - pNMUpDown->iDelta;
    if (subfolders >= 0 && subfolders <= 4)
    {
        m_subfolders = subfolders;
        UpdateData(FALSE);
        *pResult = 0;
    }
    else
    {
        *pResult = 1;
    }
}
