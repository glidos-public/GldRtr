#if !defined(AFX_PROPERTYPAGECHECK_H__538E716B_B9C9_4261_812B_FDF1EFEB569D__INCLUDED_)
#define AFX_PROPERTYPAGECHECK_H__538E716B_B9C9_4261_812B_FDF1EFEB569D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropertyPageCheck.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPropertyPageCheck dialog

#include "PropertyPageBase.h"

class CPropertyPageCheck : public CPropertyPageBase
{
	DECLARE_DYNCREATE(CPropertyPageCheck)

// Construction
public:
	CPropertyPageCheck();
	~CPropertyPageCheck();

// Dialog Data
	//{{AFX_DATA(CPropertyPageCheck)
	enum { IDD = IDD_PROPPAGE_CHECK };
	CString	m_folder;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropertyPageCheck)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropertyPageCheck)
	afx_msg void OnButtonBrowseFolder();
	afx_msg void OnButtonCheck();
	//}}AFX_MSG
    void OnUpdateCheckButton(CCmdUI *pCmdUI);
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPERTYPAGECHECK_H__538E716B_B9C9_4261_812B_FDF1EFEB569D__INCLUDED_)
