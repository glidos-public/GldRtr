// TexElementActFactory.h: interface for the CTexElementActFactory class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXELEMENTACTFACTORY_H__594645E0_C55A_44E3_AB09_324C022C4446__INCLUDED_)
#define AFX_TEXELEMENTACTFACTORY_H__594645E0_C55A_44E3_AB09_324C022C4446__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TexElementAct.h"

class CTexElementActFactory  
{
public:
    virtual CTexElementAct *MakeTexElement() = 0;
    virtual CTexElementAct *MakeTexElement(CBox &b, CString &name) = 0;

	CTexElementActFactory();
	virtual ~CTexElementActFactory();

};

#endif // !defined(AFX_TEXELEMENTACTFACTORY_H__594645E0_C55A_44E3_AB09_324C022C4446__INCLUDED_)
