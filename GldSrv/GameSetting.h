// GameSetting.h: interface for the CGameSetting class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAMESETTING_H__F95A4464_002D_4B6E_A73A_0FD57D816943__INCLUDED_)
#define AFX_GAMESETTING_H__F95A4464_002D_4B6E_A73A_0FD57D816943__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "StoredStruct.h"

template <class T>
class CGameSetting : public T, public CStoredStructField
{
private:
	virtual bool    Parse(CString &val) = 0;
	virtual CString Print()             = 0;

protected:
	bool    m_defined;
	CString m_name;

public:
	CGameSetting()
	{
		m_defined = false;
	};

	virtual ~CGameSetting()
	{
	};

	void Init(CStoredStruct *st, const char *name)
	{
		CStoredStructField::Init(st);

		m_name = name;
	};

	bool IsDefined()
	{
		return m_defined;
	};

	bool Read(CString &key, CString &val)
	{
		if(key != m_name)
			return false;

		m_defined = Parse(val);

		return m_defined;
	};

	CString Write()
	{
		return m_defined
			       ? m_name + ": " + Print() + "\n"
				   : "";
	};
};


#endif // !defined(AFX_GAMESETTING_H__F95A4464_002D_4B6E_A73A_0FD57D816943__INCLUDED_)
