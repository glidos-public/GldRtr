// TexElementAudioFactory.cpp: implementation of the CTexElementAudioFactory class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "TexElementAudio.h"
#include "TexElementAudioFactory.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTexElementAudioFactory::CTexElementAudioFactory(CTexHandlerAudio *handler)
{
    m_handler = handler;
}

CTexElementAudioFactory::~CTexElementAudioFactory()
{

}

CTexElementAct *CTexElementAudioFactory::MakeTexElement()
{
    return new CTexElementAudio(m_handler);
}

CTexElementAct *CTexElementAudioFactory::MakeTexElement(CBox &b, CString &name)
{
    return new CTexElementAudio(b, name, m_handler);
}
