// Prop3DMonitor.cpp : implementation file
//

#include "stdafx.h"
#include "gldsrv.h"
#include "Prop3DMonitor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProp3DMonitor property page

IMPLEMENT_DYNCREATE(CProp3DMonitor, CPropertyPage)

CProp3DMonitor::CProp3DMonitor() : CPropertyPage(CProp3DMonitor::IDD)
{
    //{{AFX_DATA_INIT(CProp3DMonitor)
    //}}AFX_DATA_INIT
}

CProp3DMonitor::~CProp3DMonitor()
{
}

void CProp3DMonitor::DoDataExchange(CDataExchange* pDX)
{
    CPropertyPage::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CProp3DMonitor)
    DDX_Control(pDX, IDC_MODE6, m_mode6);
    DDX_Control(pDX, IDC_MODE5, m_mode5);
    DDX_Control(pDX, IDC_MODE4, m_mode4);
    DDX_Control(pDX, IDC_MODE3, m_mode3);
    DDX_Control(pDX, IDC_MODE2, m_mode2);
    DDX_Control(pDX, IDC_MODE1, m_mode1);
    DDX_Control(pDX, IDC_MODE0, m_mode0);
    DDX_Control(pDX, IDC_PRESETS, m_presets);
    DDX_Control(pDX, IDC_OFFSET3, m_offset3);
    DDX_Control(pDX, IDC_OFFSET2, m_offset2);
    DDX_Control(pDX, IDC_OFFSET1, m_offset1);
    DDX_Control(pDX, IDC_LIMIT3, m_limit3);
    DDX_Control(pDX, IDC_LIMIT2, m_limit2);
    DDX_Control(pDX, IDC_LIMIT1, m_limit1);
    DDX_Control(pDX, IDC_FACTOR3, m_factor3);
    DDX_Control(pDX, IDC_FACTOR2, m_factor2);
    DDX_Control(pDX, IDC_FACTOR1, m_factor1);
    DDX_Control(pDX, IDC_EQ3, m_eq3);
    DDX_Control(pDX, IDC_EQ2, m_eq2);
    DDX_Control(pDX, IDC_STEREOSCOPIC, m_stereoscopic);
    //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CProp3DMonitor, CPropertyPage)
    //{{AFX_MSG_MAP(CProp3DMonitor)
    ON_BN_CLICKED(IDC_STEREOSCOPIC, OnStereoscopic)
    ON_BN_CLICKED(IDC_EQ2, OnEq2)
    ON_BN_CLICKED(IDC_EQ3, OnEq3)
    ON_CBN_SELCHANGE(IDC_PRESETS, OnSelchangePresets)
    ON_BN_CLICKED(IDC_MODE0, OnMode0)
    ON_BN_CLICKED(IDC_MODE1, OnMode1)
    ON_BN_CLICKED(IDC_MODE2, OnMode2)
    ON_BN_CLICKED(IDC_MODE3, OnMode3)
    ON_BN_CLICKED(IDC_MODE4, OnMode4)
    ON_BN_CLICKED(IDC_MODE5, OnMode5)
    ON_BN_CLICKED(IDC_MODE6, OnMode6)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProp3DMonitor message handlers

void CProp3DMonitor::OnStereoscopic()
{
    m_count = m_stereoscopic.GetCheck() ? 1 : 0;

    EnableDisable();
}

void CProp3DMonitor::OnEq2()
{
    m_count = m_eq2.GetCheck() ? 2 : 1;

    EnableDisable();
}

void CProp3DMonitor::OnEq3()
{
    m_count = m_eq3.GetCheck() ? 3 : 2;

    EnableDisable();
}

BOOL CProp3DMonitor::OnSetActive()
{
    if(m_rec->driver != CGlideDriver::GLIDE_DRIVER_D3D)
    {
        AfxMessageBox("Supported only in Direct3D mode", MB_OK);
        return FALSE;
    }

    m_presets.Clear();
    m_presets.AddString("Tombraider");
    m_presets.AddString("Redguard");
    m_presets.AddString("Tombraider VR");
    m_presets.AddString("Redguard VR");

    StereoMap sm = m_rec->stereo;

    FillControls(&sm);
    return CPropertyPage::OnSetActive();
}

BOOL CProp3DMonitor::OnKillActive()
{
    StereoMap sm;
    CArray<CTextableFloat, const CTextableFloat &> &vals = m_rec->stereo;

    ReadControls(&sm);

    m_rec->stereo = sm;

    return CPropertyPage::OnKillActive();
}

void CProp3DMonitor::EnableDisable()
{
    m_stereoscopic.SetCheck(m_count > 0);
    m_limit1.EnableWindow(m_count > 0);
    m_factor1.EnableWindow(m_count > 0);
    m_offset1.EnableWindow(m_count > 0);
    m_eq2.EnableWindow(m_count > 0);
    m_mode0.EnableWindow(m_count > 0);
    m_mode1.EnableWindow(m_count > 0);
    m_mode2.EnableWindow(m_count > 0);
    m_mode3.EnableWindow(m_count > 0);
    m_mode4.EnableWindow(m_count > 0);
    m_mode5.EnableWindow(m_count > 0);
    m_mode6.EnableWindow(m_count > 0);

    m_eq2.SetCheck(m_count > 1);
    m_limit2.EnableWindow(m_count > 1);
    m_factor2.EnableWindow(m_count > 1);
    m_offset2.EnableWindow(m_count > 1);
    m_eq3.EnableWindow(m_count > 1);

    m_eq3.SetCheck(m_count > 2);
    m_limit3.EnableWindow(m_count > 2);
    m_factor3.EnableWindow(m_count > 2);
    m_offset3.EnableWindow(m_count > 2);
}

void CProp3DMonitor::Init(CGameRecord *rec)
{
    m_rec = rec;
}

void CProp3DMonitor::OnSelchangePresets() 
{
    CString   s;
    StereoMap sm;
    
    m_presets.GetLBText(m_presets.GetCurSel(), s);

    ReadControls(&sm);

    if(s == "Tombraider")
    {
        sm.nsep = 1;
        sm.sep[0].limit  =    0.05f;
        sm.sep[0].offset =  115.0f;
        sm.sep[0].factor = 8000.0f;
    }
    else if(s == "Redguard")
    {
        sm.nsep = 1;
        sm.sep[0].limit  =    0.05f;
        sm.sep[0].offset =  115.0f;
        sm.sep[0].factor =  800.0f;
    }
    else if(s == "Tombraider VR")
    {
        sm.nsep = 1;
        sm.sep[0].limit  =    0.05f;
        sm.sep[0].offset =    0.0f;
        sm.sep[0].factor = 8000.0f;
    }
    else if(s == "Redguard VR")
    {
        sm.nsep = 1;
        sm.sep[0].limit  =    0.05f;
        sm.sep[0].offset =    0.0f;
        sm.sep[0].factor =  800.0f;
    }

    FillControls(&sm);
    UpdateData(FALSE);
}

void CProp3DMonitor::ReadControls(StereoMap *sm)
{
    CEdit    *edit[] = {&m_limit1, &m_offset1, &m_factor1,
                        &m_limit2, &m_offset2, &m_factor2,
                        &m_limit3, &m_offset3, &m_factor3};
    int       i, j;

    sm->nsep = m_count;

    j = 0;
    for(i = 0; i < m_count; i++)
    {
        CString s;
        edit[j++]->GetWindowText(s);
        sm->sep[i].limit = (float)atof((LPCSTR)s);
        edit[j++]->GetWindowText(s);
        sm->sep[i].offset = (float)atof((LPCSTR)s);
        edit[j++]->GetWindowText(s);
        sm->sep[i].factor = (float)atof((LPCSTR)s);
    }

    if(m_mode0.GetCheck() != 0)
        sm->type = (StereoType)0;
    if(m_mode1.GetCheck() != 0)
        sm->type = (StereoType)1;
    if(m_mode2.GetCheck() != 0)
        sm->type = (StereoType)2;
    if(m_mode3.GetCheck() != 0)
        sm->type = (StereoType)3;
    if(m_mode4.GetCheck() != 0)
        sm->type = (StereoType)4;
    if(m_mode5.GetCheck() != 0)
        sm->type = (StereoType)5;
    if(m_mode6.GetCheck() != 0)
        sm->type = (StereoType)6;
}

void CProp3DMonitor::FillControls(const StereoMap *sm)
{

    CEdit *edit[] = {&m_limit1, &m_offset1, &m_factor1,
                     &m_limit2, &m_offset2, &m_factor2,
                     &m_limit3, &m_offset3, &m_factor3};

    int i,j;

    m_count = sm->nsep;

    j = 0;
    for(i = 0; i < sm->nsep; i++)
    {
        CString s;

        s.Format("%.6g", sm->sep[i].limit);
        edit[j++]->SetWindowText((LPCSTR)s);
        s.Format("%.6g", sm->sep[i].offset);
        edit[j++]->SetWindowText((LPCSTR)s);
        s.Format("%.6g", sm->sep[i].factor);
        edit[j++]->SetWindowText((LPCSTR)s);
    }

    for(; j < sizeof(edit)/sizeof(*edit); j++)
        edit[j]->SetWindowText("");

    m_mode0.SetCheck(sm->type == (StereoType)0);
    m_mode1.SetCheck(sm->type == (StereoType)1);
    m_mode2.SetCheck(sm->type == (StereoType)2);
    m_mode3.SetCheck(sm->type == (StereoType)3);
    m_mode4.SetCheck(sm->type == (StereoType)4);
    m_mode5.SetCheck(sm->type == (StereoType)5);
    m_mode6.SetCheck(sm->type == (StereoType)6);

    EnableDisable();
}

void CProp3DMonitor::OnMode0() 
{
    m_mode1.SetCheck(0);
    m_mode2.SetCheck(0);
    m_mode3.SetCheck(0);
    m_mode4.SetCheck(0);
    m_mode5.SetCheck(0);
    m_mode6.SetCheck(0);
}

void CProp3DMonitor::OnMode1() 
{
    m_mode0.SetCheck(0);
    m_mode2.SetCheck(0);
    m_mode3.SetCheck(0);
    m_mode4.SetCheck(0);
    m_mode5.SetCheck(0);
    m_mode6.SetCheck(0);
}

void CProp3DMonitor::OnMode2() 
{
    m_mode0.SetCheck(0);
    m_mode1.SetCheck(0);
    m_mode3.SetCheck(0);
    m_mode4.SetCheck(0);
    m_mode5.SetCheck(0);
    m_mode6.SetCheck(0);
}

void CProp3DMonitor::OnMode3() 
{
    m_mode0.SetCheck(0);
    m_mode1.SetCheck(0);
    m_mode2.SetCheck(0);
    m_mode4.SetCheck(0);
    m_mode5.SetCheck(0);
    m_mode6.SetCheck(0);
}

void CProp3DMonitor::OnMode4() 
{
    m_mode0.SetCheck(0);
    m_mode1.SetCheck(0);
    m_mode2.SetCheck(0);
    m_mode3.SetCheck(0);
    m_mode5.SetCheck(0);
    m_mode6.SetCheck(0);
}

void CProp3DMonitor::OnMode5() 
{
    m_mode0.SetCheck(0);
    m_mode1.SetCheck(0);
    m_mode2.SetCheck(0);
    m_mode3.SetCheck(0);
    m_mode4.SetCheck(0);
    m_mode6.SetCheck(0);
}

void CProp3DMonitor::OnMode6() 
{
    m_mode0.SetCheck(0);
    m_mode1.SetCheck(0);
    m_mode2.SetCheck(0);
    m_mode3.SetCheck(0);
    m_mode4.SetCheck(0);
    m_mode5.SetCheck(0);
}
