#if !defined(AFX_PROPDOS_H__9B8510F4_5125_42DF_AEDE_1C053193302B__INCLUDED_)
#define AFX_PROPDOS_H__9B8510F4_5125_42DF_AEDE_1C053193302B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropDos.h : header file
//

#include "GameRecord.h"

/////////////////////////////////////////////////////////////////////////////
// CPropDos dialog

class CPropDos : public CPropertyPage
{
	DECLARE_DYNCREATE(CPropDos)

private:
    CGameRecord *m_rec;

// Construction
public:
	void Init(CGameRecord *rec);
	CPropDos();
	~CPropDos();

// Dialog Data
	//{{AFX_DATA(CPropDos)
	enum { IDD = IDD_PROP_DOS };
	CStatic	m_static_judder;
	CButton	m_static_pit;
	CButton	m_static_graph;
	CButton	m_vesa_support;
	CButton	m_dos_graphics;
	CEdit	m_judder;
	CSpinButtonCtrl	m_judder_spin;
	int	m_dos_emu;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CPropDos)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CPropDos)
	afx_msg void OnDosEmu();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPDOS_H__9B8510F4_5125_42DF_AEDE_1C053193302B__INCLUDED_)
