// CDAudio.h: interface for the CCDAudio class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CDAUDIO_H__3807CEAF_D3AB_4C1D_910E_E4E107F32770__INCLUDED_)
#define AFX_CDAUDIO_H__3807CEAF_D3AB_4C1D_910E_E4E107F32770__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "mmsystem.h"

class CCDAudio  
{
public:
	void Disable();
	void Close();
	void Poll();
	void Play(unsigned int start, unsigned duration);
	CCDAudio();
	virtual ~CCDAudio();

private:
	bool m_enabled;
	bool m_open;
	bool playing();
	void start();
	void close();
	void open();
	bool m_playing;
	unsigned int m_duration;
	unsigned int m_start;
	static unsigned int frames2msf(unsigned int f);
	MCIDEVICEID m_id;
};

#endif // !defined(AFX_CDAUDIO_H__3807CEAF_D3AB_4C1D_910E_E4E107F32770__INCLUDED_)
