// TimerThread.h: interface for the CTimerThread class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TIMERTHREAD_H__2AA03AE9_834A_4468_AE4E_CC1D74AD0096__INCLUDED_)
#define AFX_TIMERTHREAD_H__2AA03AE9_834A_4468_AE4E_CC1D74AD0096__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Thread.h"

class CTimerThread : public CThread  
{
public:
	void Poll();
    int m_pic_time;
	void Start(int val);
	CTimerThread();
	virtual ~CTimerThread();

private:
	bool m_high_pri;
	int m_poll_time;
	int m_delay;
	void Execute();
};

#endif // !defined(AFX_TIMERTHREAD_H__2AA03AE9_834A_4468_AE4E_CC1D74AD0096__INCLUDED_)
