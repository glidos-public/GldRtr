// Mouse.cpp: implementation of the CMouse class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "Mouse.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define BUFSIZE (16)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

enum
{
    ButtonPress_None,
    ButtonPress_LDown,
    ButtonPress_LUp,
    ButtonPress_RDown,
    ButtonPress_RUp
};

CMouse::CMouse()
{
    m_dinput   = NULL;
    m_mouse    = NULL;
    m_acquired = false;
}

CMouse::~CMouse()
{
    if(m_mouse)
        m_mouse->Release();

    if(m_dinput)
        m_dinput->Release();
}

void CMouse::Capture(HWND hWnd)
{
    if(m_mouse && hWnd)
    {
        HRESULT res;
        res = m_mouse->SetCooperativeLevel(hWnd, DISCL_FOREGROUND|DISCL_EXCLUSIVE);

        if(res != DI_OK)
        {
            m_mouse->Release();
            m_mouse = NULL;
            AfxMessageBox("Can't access the mouse", MB_OK);
        }
    }

    if(m_mouse)
    {
        HRESULT res;
        DIMOUSESTATE state;

        res = m_mouse->Acquire();
        
        if(res == DI_OK || res == S_FALSE)
        {
            res = m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), &state);

            m_acquired = (res == DI_OK);

            m_x = state.lX;
            m_y = state.lY;
        }
    }
}

void CMouse::Release()
{
    if(m_mouse)
    {
        m_mouse->Unacquire();
    }
}

void CMouse::Poll(int *x, int *y, int *buttons)
{
    *x = 0;
    *y = 0;
    *buttons = 0;

    if(m_mouse)
    {
        if(!m_acquired)
        {
            Capture(NULL);
        }
        else
        {
            {
                HRESULT res;
                DIMOUSESTATE state;
                
                res = m_mouse->Acquire();
                
                if(res != DI_OK && res != S_FALSE)
                {
                    m_acquired = false;
                    return;
                }
                
                res = m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), &state);
                
                if(res != DI_OK)
                {
                    m_acquired = false;
                    return;
                }
                
                *x = state.lX - m_x;
                *y = state.lY - m_y;
                
                m_x = state.lX;
                m_y = state.lY;
            }
            
            {
                HRESULT res;
                DIDEVICEOBJECTDATA items[BUFSIZE];
                DWORD nitems = BUFSIZE;
                DWORD i;
                DWORD j = 0;
                char *presses = (char *)buttons;

                res = m_mouse->GetDeviceData(sizeof(DIDEVICEOBJECTDATA),
                                             items,
                                             &nitems,
                                             0);
                if(res != DI_OK)
                {
                    m_acquired = false;
                    return;
                }

                for(i = (nitems > 4 ? nitems - 4: 0); i < nitems; i++)
                {
                    switch(items[i].dwOfs)
                    {
                    case DIMOFS_BUTTON0:
                        presses[j++] = ((items[i].dwData & 0x80) != 0)
                                                        ? ButtonPress_LDown
                                                        : ButtonPress_LUp;
                        break;

                    case DIMOFS_BUTTON1:
                        presses[j++] = ((items[i].dwData & 0x80) != 0)
                                                        ? ButtonPress_RDown
                                                        : ButtonPress_RUp;
                        break;
                    }
                }

                while(j < 4)
                    presses[j++] = ButtonPress_None;

            }
            if(0 && *buttons != 0)
            {
                CString s;

                s.Format("%x", *buttons);
                AfxMessageBox((LPCSTR)s, MB_OK);
            }
        }
    }
}

void CMouse::Initialise()
{
    HRESULT res;

    res = DirectInputCreateEx(AfxGetInstanceHandle(),
                              DIRECTINPUT_VERSION,
                              IID_IDirectInput7,
                              (void **) &m_dinput,
                              NULL);

    if(res != DI_OK)
    {
        m_dinput = NULL;
        AfxMessageBox("Can't initialise direct input", MB_OK);
    }

    if(m_dinput)
    {
        res = m_dinput->CreateDeviceEx(GUID_SysMouse,
                                 IID_IDirectInputDevice7,
                                 (void **) &m_mouse,
                                 NULL);

        if(res != DI_OK)
        {
            m_mouse = NULL;
            AfxMessageBox("Can't access the mouse", MB_OK);
        }
    }

    if(m_mouse)
    {
        res = m_mouse->SetDataFormat(&c_dfDIMouse);

        if(res != DI_OK)
        {
            m_mouse->Release();
            m_mouse = NULL;
            AfxMessageBox("Can't access the mouse", MB_OK);
        }
    }

    if(m_mouse)
    {
        DIPROPDWORD prop;

        prop.diph.dwSize       = sizeof(DIPROPDWORD);
        prop.diph.dwHeaderSize = sizeof(DIPROPHEADER);
        prop.diph.dwHow        = DIPH_DEVICE;
        prop.diph.dwObj        = 0;
        prop.dwData            = DIPROPAXISMODE_ABS;

        res = m_mouse->SetProperty(DIPROP_AXISMODE, &(prop.diph));

        if(res != DI_OK)
        {
            m_mouse->Release();
            m_mouse = NULL;
            AfxMessageBox("Can't access the mouse", MB_OK);
        }
    }
        
    if(m_mouse)
    {
        DIPROPDWORD prop;

        prop.diph.dwSize       = sizeof(DIPROPDWORD);
        prop.diph.dwHeaderSize = sizeof(DIPROPHEADER);
        prop.diph.dwHow        = DIPH_DEVICE;
        prop.diph.dwObj        = 0;
        prop.dwData            = BUFSIZE;

        res = m_mouse->SetProperty(DIPROP_BUFFERSIZE, &(prop.diph));

        if(res != DI_OK)
        {
            m_mouse->Release();
            m_mouse = NULL;
            AfxMessageBox("Can't access the mouse", MB_OK);
        }
    }
}
