#include "StdAfx.h"
#include "GameSettingResolution.h"


void CGameSettingResolution::Init(CStoredStruct *st, const char *name, CResolution &init)
{
    CGameSetting<CResolution>::Init(st, name);
    *((CResolution *)this) = init;
}

CGameSettingResolution &CGameSettingResolution::operator=(const CResolution &val)
{
    if ((GrScreenResolution_t)val != (GrScreenResolution_t)*this)
        m_defined = true;

    *((CResolution *)this) = val;

    return *this;
}

bool CGameSettingResolution::Parse(CString &val)
{
    char const *legacy[] = {"1600x1200","1280x1024","1024x768","800x600","640x480"};
    int n = sizeof(legacy)/sizeof(*legacy);
    char *buf = val.GetBuffer();
    int index;
    char c;
    int captures = sscanf(buf, "%d%c", &index, &c);
    CString str;
    if (captures == 1 && index < n)
        str = legacy[index];
    else
        str = val;

    CResolution res(str);

    if (res.IsOkay())
    {
        *((CResolution *)this) = res;
        return true;
    }
    else
    {
        return false;
    }
}

CString CGameSettingResolution::Print()
{
    return *((CResolution *)this);
}