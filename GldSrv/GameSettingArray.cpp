// GameSettingArray.cpp: implementation of the CGameSettingArray class.
//
//////////////////////////////////////////////////////////////////////

#ifndef GAMESETTINGARRAY_CPP
#define GAMESETTINGARRAY_CPP

#include "stdafx.h"
#include "GldSrv.h"
#include "GameSettingArray.h"

#ifndef GAMESETTINGARRAY_H
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
#endif

template<class T>
CGameSettingArray<T> &CGameSettingArray<T>::operator=(const CGameSettingArray<T> &a)
{
	INT_PTR n = a.GetSize();
	INT_PTR i;
	
	RemoveAll();
	
	for(i = 0; i < n; i++)
		Add(a[i]);
	
    if(GetSize() > 0)
        m_defined = true;

	return *this;
}

template<class T>
bool CGameSettingArray<T>::Parse(CString &val)
{
	CString local(val);
	char *buf = local.GetBuffer(0);

    RemoveAll();
	
	for(buf = strtok(buf, ", \t\n"); buf; buf = strtok(NULL, ", \t\n"))
	{
		T s;
		
		if(s.Read(buf))
			Add(s);
        else
            return false;
	}
	
	local.ReleaseBuffer();
	
	return true;
}

template<class T>
CString CGameSettingArray<T>::Print()
{
	CString res;
	INT_PTR n = GetSize();
	INT_PTR i;
	
	for(i = 0; i < n; i++)
	{
		if(i != 0)
			res += ", ";
		
		res += GetAt(i).Write();
	}
	
	return res;
}

#endif
