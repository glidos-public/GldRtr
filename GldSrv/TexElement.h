// TexElement.h: interface for the CTexElement class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXELEMENT_H__EB294548_942E_40D7_97A4_88B1BD4157B7__INCLUDED_)
#define AFX_TEXELEMENT_H__EB294548_942E_40D7_97A4_88B1BD4157B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "sdk2_glide.h"
#include "Box.h"

class CTexElement : public CBox  
{
protected:
    CString m_name;

public:
    CString &GetName();
    CTexElement();
    CTexElement(CBox &b, CString &name);
    virtual ~CTexElement();

};

#endif // !defined(AFX_TEXELEMENT_H__EB294548_942E_40D7_97A4_88B1BD4157B7__INCLUDED_)
