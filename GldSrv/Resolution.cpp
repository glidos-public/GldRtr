#include "StdAfx.h"
#include "Resolution.h"

static CResolution::Size sizes[] =
{
    {320, 200},
    {320, 240},
    {400, 256},
    {512, 384},
    {640, 200},
    {640, 350},
    {640, 400},
    {640, 480},
    {800, 600},
    {960, 720},
    {856, 480},
    {512, 256},
    {1024, 768},
    {1280, 1024},
    {1600, 1200},
    {400, 300}
};

CResolution::CResolution()
{
    _width = 1024;
    _height = 768;
    _isOkay = true;
}

CResolution::CResolution(unsigned int width, unsigned int height)
{
    _isOkay = 0 < width && width <= 0xFFFF
           && 0 < height && height <= 0xFFFF;
    _width = width;
    _height = height;
}

CResolution::CResolution(GrScreenResolution_t res)
{
    int n = sizeof(sizes)/sizeof(*sizes);
    if (0 <= res && res < n)
    {
        CResolution::Size size = sizes[res];
        _width = size.width;
        _height = size.height;
        _isOkay = true;
    }
    else
    {
        unsigned int ures = res;
        _width = (ures & 0xffff);
        _height = (ures >> 16);
        _isOkay = 0 <_width || 0 < _height;
    }
}

CResolution::CResolution(CString &string)
{
    char c;
    const char *buf = string.GetBuffer();
    int captures = sscanf(buf, "%dx%d%c", &_width, &_height, &c);
    string.ReleaseBuffer();
    _isOkay = captures = 2 && 0 < _width && _width <= 0xFFFF
                           && 0 < _height && _height <= 0xFFFF;
}

CResolution::operator CResolution::Size() const
{
    CResolution::Size size = {_width, _height};
    return size;
}

CResolution::operator GrScreenResolution_t() const
{
    int n = sizeof(sizes)/sizeof(*sizes);
    for (int i = 0; i < n; i++)
        if (sizes[i].width == _width && sizes[i].height == _height)
            return i;

    return _width | (_height << 16);
}

CResolution::operator CString() const
{
    CString str;
    str.Format("%dx%d", _width, _height);
    return str;
}

bool CResolution::IsOkay()
{
    return _isOkay;
}

bool CResolution::IsStandard()
{
    if (_isOkay)
    {
        int n = sizeof(sizes)/sizeof(*sizes);
        for (int i = 0; i < n; i++)
            if (sizes[i].width == _width && sizes[i].height == _height)
                return true;
    }

    return false;
}
