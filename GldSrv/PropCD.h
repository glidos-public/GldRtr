#pragma once
#include "afxwin.h"

#include "GameRecord.h"

// CPropCD dialog

class CPropCD : public CPropertyPage
{
	DECLARE_DYNAMIC(CPropCD)

private:
    void UpdateButtons();
    CGameRecord *m_rec;

public:
    void Init(CGameRecord *rec);
	CPropCD();
	virtual ~CPropCD();

// Dialog Data
	enum { IDD = IDD_PROP_CD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	CComboBox m_drive;
	CEdit m_iso_path;
	CButton m_browse_iso_path;
	CString m_check_path;
	afx_msg void OnBnClickedButtonBrowseCd();
	afx_msg void OnBnClickedBrowseISO();
    afx_msg void OnBnClickedCd();
    afx_msg void OnBnClickedIso();
    int m_cd_or_iso;
};
