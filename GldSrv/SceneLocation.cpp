// SceneLocation.cpp: implementation of the CSceneLocation class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GldSrv.h"
#include "SceneLocation.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSceneLocation::CSceneLocation()
{
    m_triggered = false;
}

CSceneLocation::~CSceneLocation()
{

}

bool CSceneLocation::Load(const CString &folder, const CString &path)
{
    FILE *f = NULL;
    char  buf[256];

    f = fopen((LPCSTR)(folder + '\\' + path), "rb");
    if(f != NULL)
    {
        while(fgets(buf, 256, f) != NULL)
        {
            if(memcmp(buf, "Play:", 5) == 0)
            {
                CString line(buf+5);
                int     pos;

                line.TrimLeft();
                pos = line.Find(' ');
                if(pos != -1)
                {
                    int n = atoi((LPCSTR)line.Left(pos));

                    line = line.Mid(pos);
                    line.TrimLeft();
                    line.TrimRight();

                    m_file = folder + '\\' + line;
                    m_current_set = m_initial_set = (0xffffffff << n);
                }
            }
        }

        fclose(f);
    }

    return m_file != "";
}

void CSceneLocation::Trigger(int index)
{
    if(index == 0)
        m_current_set = 0;
    else
        m_current_set |= (1 << (index-1));
}

void CSceneLocation::TestPeriodEnd(CPlayAudioFile *player)
{
    if(!m_triggered && m_current_set == 0xffffffff)
    {
        player->Stop();
        player->Start(m_file);

        m_triggered = true;
    }

    m_current_set = m_initial_set;
}
