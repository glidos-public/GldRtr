// GlideDll.h: interface for the CGlideDll class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GLIDEDLL_H__5BA5816F_CF37_4A96_80E2_E30A51AFFDE6__INCLUDED_)
#define AFX_GLIDEDLL_H__5BA5816F_CF37_4A96_80E2_E30A51AFFDE6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "sdk2_3dfx.h"
#include "sdk2_glide.h"
#include "wrapper_config.h"

class CGlideDll  
{
private:
	void GetEntry(FARPROC *pProc, LPCSTR name);
    HMODULE m_lib;

#define GLIDE_ENTRY_POINT(ret, call, name, param, arg, pfix) \
    ret (__stdcall *m_p##name)param;

#include "entry.h"

#undef GLIDE_ENTRY_POINT

public:
	void Load(LPCSTR path);
	void Unload();
	CGlideDll();
	virtual ~CGlideDll();

#define GLIDE_ENTRY_POINT(ret, call, name, param, arg, pfix) \
    ret name param;

#include "entry.h"

#undef GLIDE_ENTRY_POINT

};

#endif // !defined(AFX_GLIDEDLL_H__5BA5816F_CF37_4A96_80E2_E30A51AFFDE6__INCLUDED_)
